#ifndef TIMER_INCLUDED_H
#define TIMER_INCLUDED_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


void Timer_Task(void);
uint32_t Timer_GetMilliseconds(void);
uint32_t Timer_GetSeconds(void);
void Timer_DelayMilliseconds(uint32_t timeDelay_ms);


#ifdef __cplusplus
}
#endif

#endif // TIMER_INCLUDED_H
