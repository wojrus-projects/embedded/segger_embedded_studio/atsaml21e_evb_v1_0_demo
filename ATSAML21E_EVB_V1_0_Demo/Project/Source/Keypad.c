#include <stddef.h>

#include "ATSAML21_LL/LL.h"
#include "Keypad.h"
#include "Board.h"
#include "Timer.h"


#define KEYPAD_COUNTER_MIN      0
#define KEYPAD_COUNTER_MAX      25


struct KeypadState
{
    uint16_t debounceCounter;
    bool keyState;
    bool keyChange;
};


static volatile struct KeypadState KeypadStates[BUTTON_NUM];


void Keypad_Initialize(void)
{
    for (size_t i = 0; i < BUTTON_NUM; i++)
    {
        volatile struct KeypadState * const pState = &KeypadStates[i];

        pState->keyState = Button_Get(i);

        if (pState->keyState == 0)
        {
            pState->debounceCounter = KEYPAD_COUNTER_MIN;
        }
        else
        {
            pState->debounceCounter = KEYPAD_COUNTER_MAX;
        }

        pState->keyChange = false;
    }
}


void Keypad_DebouncerTask(void)
{
    for (size_t i = 0; i < BUTTON_NUM; i++)
    {
        volatile struct KeypadState * const pState = &KeypadStates[i];

        const bool buttonState = Button_Get(i);

        //
        // Button is now pressed.
        //

        if (buttonState == 0)
        {
            if (pState->debounceCounter == KEYPAD_COUNTER_MIN)
            {
                // Continue stable state 'Pressed'.
            }
            else
            {
                pState->debounceCounter--;
                if (pState->debounceCounter == KEYPAD_COUNTER_MIN)
                {
                    // Latch new stable state (0).
                    if (pState->keyState != buttonState)
                    {
                        pState->keyState = buttonState;
                        pState->keyChange = true;
                    }
                    else
                    {
                        // Wait for release.
                    }
                }
            }
        }
        else
        {
            //
            // Button is now released.
            //

            if (pState->debounceCounter == KEYPAD_COUNTER_MAX)
            {
                // Continue stable state 'Released'.
            }
            else
            {
                pState->debounceCounter++;
                if (pState->debounceCounter == KEYPAD_COUNTER_MAX)
                {
                    // Latch new stable state (1).
                    if (pState->keyState != buttonState)
                    {
                        pState->keyState = buttonState;
                        pState->keyChange = true;
                    }
                    else
                    {
                        // Wait for press.
                    }
                }
            }
        }
    }
}


bool Keypad_GetButtonState(enum Button buttonIndex, bool * pOutButtonState)
{
    if (buttonIndex >= BUTTON_NUM)
    {
        return false;
    }

    __disable_irq();

    const bool isChanged = KeypadStates[buttonIndex].keyChange;

    KeypadStates[buttonIndex].keyChange = false;
    *pOutButtonState = KeypadStates[buttonIndex].keyState;

    __enable_irq();

    return isChanged;
}
