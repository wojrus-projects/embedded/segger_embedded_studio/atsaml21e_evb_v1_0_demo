#ifndef LL_TEST_TC_INCLUDED_H
#define LL_TEST_TC_INCLUDED_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif


void LL_Test_TC0_PWM_Simple(void);
void LL_Test_TC0_PWM_DMA(bool oneShot);
void LL_Test_TC0_Capture_PWP(void);
void LL_Test_TC0_Capture_PW(void);
void LL_Test_TC4_Counter_External(void);

void LL_Test_TC0_Capture_PWP_InterruptHandler(void);
void LL_Test_TC0_Capture_PW_InterruptHandler(void);
void LL_Test_TC4_Counter_External_InterruptHandler(void);


#ifdef __cplusplus
}
#endif

#endif // LL_TEST_TC_INCLUDED_H
