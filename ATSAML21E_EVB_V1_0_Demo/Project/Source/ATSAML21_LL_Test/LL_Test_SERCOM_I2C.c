//
// Testy LL_SERCOM_I2C.
//

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "ATSAML21_LL/LL.h"
#include "Board.h"
#include "Timer.h"


enum AddressDirection
{
    AddressDirectionWrite = 0,
    AddressDirectionRead  = 1
};


//
// Function: LL_Test_SERCOM_I2C_Master_Simple
//
// I2C-master odbiera lub wysyła N bajtów z/do slave w trybie bez przerwań NVIC (polling).
// Używany jest SERCOM-3 i piny: SCL=PA23, SDA=PA22.
//
void LL_Test_SERCOM_I2C_Master_Simple(void)
{
    printf("LL_Test_SERCOM_I2C_Master_Simple\n");

    //---------------------------------------------------------------------------------------------
    // SERCOM-3: konfiguracja pinów I2C-master.
    //---------------------------------------------------------------------------------------------

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_SCL, PIN_CLICK_SCL, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_SCL, PIN_CLICK_SCL, LL_PORT_PeripheralFunction_C);

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_SDA, PIN_CLICK_SDA, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_SDA, PIN_CLICK_SDA, LL_PORT_PeripheralFunction_C);

    //---------------------------------------------------------------------------------------------
    // SERCOM-3: obliczenie baud rate (używany jest tylko rejestr BAUD, pozostałe np. BAUDLOW są 0).
    //---------------------------------------------------------------------------------------------

    const uint32_t baudSet = 100 * 1000;
    const uint32_t f_ref = SYSTEM_CLOCK_FREQUENCY_Hz;
    const uint32_t t_rise_ns = 200;
    const uint32_t baudRegister = ((f_ref / baudSet) - 10ul - ((uint64_t)f_ref * (uint64_t)t_rise_ns) / (1000ull * 1000ull * 1000ull)) / 2ul;

    assert((baudRegister >= 0x00) && (baudRegister <= 0xFF));

    const uint32_t baudFromRegister = f_ref / (10ul + 2ul * baudRegister + ((uint64_t)f_ref * (uint64_t)t_rise_ns) / (1000ull * 1000ull * 1000ull));
    const int32_t baudErrorDelta = baudSet - baudFromRegister;
    const float baudErrorPercent = (100.0f * (float)baudErrorDelta) / (float)baudSet;

    printf("BAUD register: %u / 0x%X\n", baudRegister, baudRegister);
    printf("Baud set: %u bps -> baud actual: %u bps\n", baudSet, baudFromRegister);
    printf("Error: %d bps / %f %%\n", baudErrorDelta, baudErrorPercent);

    //---------------------------------------------------------------------------------------------
    // SERCOM-3: konfiguracja I2C-master.
    //---------------------------------------------------------------------------------------------

    LL_GCLK_EnableGenerator(1, 1, LL_GCLK_ClockSource_OSCULP32K, 0, 0, 0, 0, 0);

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_SERCOM3_CORE, 0, 1);
    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_SERCOM_01234_SLOW, 1, 1);

    NVIC_DisableIRQ(SERCOM3_IRQn);
    
    LL_SERCOM_I2C_MASTER_Reset(3);
    LL_SERCOM_I2C_MASTER_Enable(3, 0);
    LL_SERCOM_I2C_MASTER_SetRunInStandby(3, 0);
    LL_SERCOM_I2C_MASTER_SetDebugControl(3, 0);

    LL_SERCOM_I2C_MASTER_EnableFourWireMode(3, 0);
    LL_SERCOM_I2C_MASTER_SetMode(3, LL_SERCOM_I2C_MASTER_Mode_Master);
    LL_SERCOM_I2C_MASTER_SetSpeed(3, LL_SERCOM_I2C_MASTER_Speed_Standard);
    LL_SERCOM_I2C_MASTER_SetClockStretchMode(3, LL_SERCOM_I2C_MASTER_ClockStretchMode_AfterACK);
    LL_SERCOM_I2C_MASTER_SetTimings(3, LL_SERCOM_I2C_MASTER_SDAHoldTime_450NS, 0, 0, LL_SERCOM_I2C_MASTER_InactiveTimeOut_DIS, 0);
    LL_SERCOM_I2C_MASTER_SetBaudRate(3, (uint8_t)baudRegister, 0, 0, 0);
    LL_SERCOM_I2C_MASTER_SetTransactionLength(3, 0, 0);
    LL_SERCOM_I2C_MASTER_EnableSmartMode(3, 1);
    LL_SERCOM_I2C_MASTER_EnableQuickCommand(3, 0);

    LL_SERCOM_I2C_MASTER_InterruptEnableSet(3, LL_SERCOM_I2C_MASTER_InterruptFlag_MB
                                             | LL_SERCOM_I2C_MASTER_InterruptFlag_SB
                                             | LL_SERCOM_I2C_MASTER_InterruptFlag_ERROR);

    //---------------------------------------------------------------------------------------------
    // SERCOM-3: włączenie I2C-master.
    //---------------------------------------------------------------------------------------------

    LL_SERCOM_I2C_MASTER_Enable(3, 1);
    LL_SERCOM_I2C_MASTER_SetBusStateIdle(3);

    //---------------------------------------------------------------------------------------------
    // Przygotowanie testowego transferu danych.
    //---------------------------------------------------------------------------------------------

    // Kierunek transferu I2C.
    const enum AddressDirection testAddressDirection = AddressDirectionRead;

    // I2C slave: EEPROM Microchip 24LC256.
    const uint32_t testSlaveAddress_7bit = 0x50;
    const uint32_t testSlaveAddress_8bit = (testSlaveAddress_7bit << 1) | testAddressDirection;

    // Bufory Tx/Rx.
    enum { DATA_LENGTH = 3 };

    uint8_t writeDataBuffer[DATA_LENGTH] = { 0x01, 0x02, 0x03 };
    uint8_t readDataBuffer[DATA_LENGTH] = {};
    uint32_t writeIndex = 0;
    uint32_t readIndex = 0;

    if (testAddressDirection == AddressDirectionRead)
    {
        // Przy odczycie trzeba z wyprzedzeniem ustawić ACK/NACK dla slave.
        enum LL_SERCOM_I2C_MASTER_AcknowledgeAction acknowledgeAction;
        
        if (DATA_LENGTH > 1)
        {
            acknowledgeAction = LL_SERCOM_I2C_MASTER_AcknowledgeAction_SendACK;
        }
        else
        {
            acknowledgeAction = LL_SERCOM_I2C_MASTER_AcknowledgeAction_SendNACK;
        }

        LL_SERCOM_I2C_MASTER_SetAcknowledgeAction(3, acknowledgeAction);

        printf("I2C-master read %u byte(s) from slave 0x%X\n", DATA_LENGTH, testSlaveAddress_7bit);
    }
    else
    {
        printf("I2C-master write %u byte(s) to slave 0x%X\n", DATA_LENGTH, testSlaveAddress_7bit);
    }

    //---------------------------------------------------------------------------------------------
    // Start transferu I2C-master.
    //---------------------------------------------------------------------------------------------

    LL_SERCOM_I2C_MASTER_SetAddress(3, testSlaveAddress_8bit, 0);

    //---------------------------------------------------------------------------------------------
    // SERCOM-3: obsługa przerwań w trybie polling.
    //---------------------------------------------------------------------------------------------

    for (;;)
    {
        const enum LL_SERCOM_I2C_MASTER_InterruptFlag interruptFlag = LL_SERCOM_I2C_MASTER_GetInterruptFlag(3);

        if (interruptFlag != 0)
        {
            LL_SERCOM_I2C_MASTER_ClearInterruptFlag(3, interruptFlag);

            //
            // I2C master write to slave.
            //
            if (interruptFlag & LL_SERCOM_I2C_MASTER_InterruptFlag_MB)
            {
                const enum LL_SERCOM_I2C_MASTER_Status status = LL_SERCOM_I2C_MASTER_GetStatus(3);

                printf("I2C: MB (status: 0x%X)\n", status);

                if (status & (LL_SERCOM_I2C_MASTER_Status_BUSERR | LL_SERCOM_I2C_MASTER_Status_ARBLOST | LL_SERCOM_I2C_MASTER_Status_RXNACK))
                {
                    if (status & LL_SERCOM_I2C_MASTER_Status_BUSERR)
                    {
                        printf("BUSERR\n");
                    }

                    if (status & LL_SERCOM_I2C_MASTER_Status_ARBLOST)
                    {
                        printf("ARBLOST\n");
                    }

                    if (status & LL_SERCOM_I2C_MASTER_Status_RXNACK)
                    {
                        printf("RXNACK\n");

                        // Next step: send Stop.
                        LL_SERCOM_I2C_MASTER_TriggerCommand(3, LL_SERCOM_I2C_MASTER_Command_Stop);
                    }

                    printf("I2C transfer stop with error\n");
                }
                else
                {
                    if (writeIndex < DATA_LENGTH)
                    {
                        const uint8_t txData = writeDataBuffer[writeIndex];

                        LL_SERCOM_I2C_MASTER_WriteData(3, txData);
                        writeIndex++;
                    }
                    else
                    {
                        // End of write transfer.
                        // Send Stop after last Tx byte.
                        LL_SERCOM_I2C_MASTER_TriggerCommand(3, LL_SERCOM_I2C_MASTER_Command_Stop);

                        printf("I2C write transfer end OK\n");
                    }
                }
            }

            //
            // I2C master read from slave.
            //
            if (interruptFlag & LL_SERCOM_I2C_MASTER_InterruptFlag_SB)
            {
                printf("I2C: SB\n");

                // Trigger stop after last Rx byte.
                if (readIndex == (DATA_LENGTH - 1))
                {
                    LL_SERCOM_I2C_MASTER_TriggerCommand(3, LL_SERCOM_I2C_MASTER_Command_Stop);
                }

                const uint8_t rxData = LL_SERCOM_I2C_MASTER_ReadData(3);

                readDataBuffer[readIndex] = rxData;
                readIndex++;

                if (readIndex == DATA_LENGTH)
                {
                    // End of read transfer.
                    // Stop is pending now.

                    printf("I2C read transfer end OK\nRx data: ");
                    
                    for (uint32_t i = 0; i < DATA_LENGTH; i++)
                    {
                        printf("%02X ", readDataBuffer[i]);
                    }

                    printf("\n");
                }
                else if (readIndex == (DATA_LENGTH - 1))
                {
                    // Prepare last Rx byte (send NACK).
                    LL_SERCOM_I2C_MASTER_SetAcknowledgeAction(3, LL_SERCOM_I2C_MASTER_AcknowledgeAction_SendNACK);
                }
                else
                {
                    // Prepare next Rx byte (send ACK).
                    LL_SERCOM_I2C_MASTER_SetAcknowledgeAction(3, LL_SERCOM_I2C_MASTER_AcknowledgeAction_SendACK);
                }
            }

            //
            // I2C bus error.
            //
            if (interruptFlag & LL_SERCOM_I2C_MASTER_InterruptFlag_ERROR)
            {
                printf("I2C: ERROR\n");

                // TODO: check error context/cases.
            }
        }
    }
}
