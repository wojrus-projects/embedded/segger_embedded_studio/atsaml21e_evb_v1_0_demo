#ifndef LL_TEST_MAIN_INCLUDED_H
#define LL_TEST_MAIN_INCLUDED_H

#include "LL_Test_TC.h"
#include "LL_Test_TCC.h"
#include "LL_Test_ADC.h"
#include "LL_Test_DAC.h"
#include "LL_Test_PORT.h"
#include "LL_Test_SERCOM_USART.h"
#include "LL_Test_SERCOM_SPI.h"
#include "LL_Test_SERCOM_I2C.h"

#ifdef __cplusplus
extern "C" {
#endif


// Run LL tests (global setting).
#define LL_TEST     0

// Select one test function.
#define LL_TEST_TCC0_COUNT_BUTTONS_ENABLE                   0
#define LL_TEST_TCC0_COUNTER_EXTERNAL_ENABLE                0
#define LL_TEST_TCC0_PWM_SIMPLE_ENABLE                      1
#define LL_TEST_TC0_PWM_SIMPLE_ENABLE                       0
#define LL_TEST_TC0_PWM_DMA_ENABLE                          0
#define LL_TEST_TC0_CAPTURE_PWP_ENABLE                      0
#define LL_TEST_TC0_CAPTURE_PW_ENABLE                       0
#define LL_TEST_TC4_COUNTER_EXTERNAL_ENABLE                 0
#define LL_TEST_ADC_SIMPLE_ENABLE                           0
#define LL_TEST_ADC_DMA_ENABLE                              0
#define LL_TEST_DAC_SIMPLE_ENABLE                           0
#define LL_TEST_DAC_DMA_ENABLE                              0
#define LL_TEST_PORT_SIMPLE_ENABLE                          0
#define LL_TEST_PORT_DMA_ENABLE                             0
#define LL_TEST_SERCOM_USART_SIMPLE_ENABLE                  0
#define LL_TEST_SERCOM_USART_DMA_ENABLE                     0
#define LL_TEST_SERCOM_USART_DMA_CIRCULAR_RX_ENABLE         0
#define LL_TEST_SERCOM_USART_DMA_LINEAR_RX_ENABLE           0
#define LL_TEST_SERCOM_USART_DMA_WITH_RX_TIMEOUT_ENABLE     0
#define LL_TEST_SERCOM_SPI_MASTER_SIMPLE_ENABLE             0
#define LL_TEST_SERCOM_SPI_MASTER_DMA_ENABLE                0
#define LL_TEST_SERCOM_I2C_MASTER_SIMPLE_ENABLE             0
#define LL_TEST_DSU_CALCULATE_CRC32_ENABLE                  0


void LL_Test_Main(void);


#ifdef __cplusplus
}
#endif

#endif // LL_TEST_MAIN_INCLUDED_H
