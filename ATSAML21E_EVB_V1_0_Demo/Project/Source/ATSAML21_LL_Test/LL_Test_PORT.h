#ifndef LL_TEST_PORT_INCLUDED_H
#define LL_TEST_PORT_INCLUDED_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif


void LL_Test_PORT_Simple(void);
void LL_Test_PORT_DMA(bool oneShot);


#ifdef __cplusplus
}
#endif

#endif // LL_TEST_PORT_INCLUDED_H
