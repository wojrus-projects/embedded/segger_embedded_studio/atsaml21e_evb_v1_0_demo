//
// Testy modułów LL.
//

#include "LL_Test_Main.h"


void LL_Test_Main(void)
{
#if LL_TEST_TCC0_COUNT_BUTTONS_ENABLE == 1
    LL_Test_TCC0_Count_Buttons();
#endif

#if LL_TEST_TCC0_COUNTER_EXTERNAL_ENABLE == 1
    LL_Test_TCC0_Counter_External();
#endif

#if LL_TEST_TCC0_PWM_SIMPLE_ENABLE == 1
    LL_Test_TCC0_PWM_Simple();
#endif

#if LL_TEST_TC0_PWM_SIMPLE_ENABLE == 1
    LL_Test_TC0_PWM_Simple();
#endif

#if LL_TEST_TC0_PWM_DMA_ENABLE == 1
    LL_Test_TC0_PWM_DMA(1);
#endif

#if LL_TEST_TC0_CAPTURE_PWP_ENABLE == 1
    LL_Test_TC0_Capture_PWP();
#endif

#if LL_TEST_TC0_CAPTURE_PW_ENABLE == 1
    LL_Test_TC0_Capture_PW();
#endif

#if LL_TEST_TC4_COUNTER_EXTERNAL_ENABLE == 1
    LL_Test_TC4_Counter_External();
#endif

#if LL_TEST_ADC_SIMPLE_ENABLE == 1
    LL_Test_ADC_Simple(LL_Test_ADC_Resolution_12bit);
#endif

#if LL_TEST_ADC_DMA_ENABLE == 1
    LL_Test_ADC_DMA(LL_Test_ADC_Resolution_12bit, 1);
#endif

#if LL_TEST_DAC_SIMPLE_ENABLE == 1
    LL_Test_DAC_Simple();
#endif

#if LL_TEST_DAC_DMA_ENABLE == 1
    LL_Test_DAC_DMA(1);
#endif

#if LL_TEST_PORT_SIMPLE_ENABLE == 1
    LL_Test_PORT_Simple();
#endif

#if LL_TEST_PORT_DMA_ENABLE == 1
    LL_Test_PORT_DMA(1);
#endif

#if LL_TEST_SERCOM_USART_SIMPLE_ENABLE == 1
    LL_Test_SERCOM_USART_Simple();
#endif

#if LL_TEST_SERCOM_USART_DMA_ENABLE == 1
    LL_Test_SERCOM_USART_DMA();
#endif

#if LL_TEST_SERCOM_USART_DMA_CIRCULAR_RX_ENABLE == 1
    LL_Test_SERCOM_USART_DMA_Circular_Rx();
#endif

#if LL_TEST_SERCOM_USART_DMA_LINEAR_RX_ENABLE == 1
    LL_Test_SERCOM_USART_DMA_Linear_Rx();
#endif

#if LL_TEST_SERCOM_USART_DMA_WITH_RX_TIMEOUT_ENABLE == 1
    LL_Test_SERCOM_USART_DMA_With_Rx_Timeout();
#endif

#if LL_TEST_SERCOM_SPI_MASTER_SIMPLE_ENABLE == 1
    LL_Test_SERCOM_SPI_Master_Simple(8);
#endif

#if LL_TEST_SERCOM_SPI_MASTER_DMA_ENABLE == 1
    LL_Test_SERCOM_SPI_Master_DMA();
#endif

#if LL_TEST_SERCOM_I2C_MASTER_SIMPLE_ENABLE == 1
    LL_Test_SERCOM_I2C_Master_Simple();
#endif

#if LL_TEST_DSU_CALCULATE_CRC32_ENABLE == 1
    LL_Test_DSU_Calculate_CRC32();
#endif
}
