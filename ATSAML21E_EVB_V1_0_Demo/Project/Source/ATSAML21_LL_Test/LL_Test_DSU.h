#ifndef LL_TEST_DSU_INCLUDED_H
#define LL_TEST_DSU_INCLUDED_H

#ifdef __cplusplus
extern "C" {
#endif


void LL_Test_DSU_Calculate_CRC32(void);


#ifdef __cplusplus
}
#endif

#endif // LL_TEST_DSU_INCLUDED_H
