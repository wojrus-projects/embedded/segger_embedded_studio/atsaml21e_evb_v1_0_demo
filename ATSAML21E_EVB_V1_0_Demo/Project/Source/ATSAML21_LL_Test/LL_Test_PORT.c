//
// Testy LL_PORT.
//

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <assert.h>

#include "ATSAML21_LL/LL.h"
#include "Board.h"
#include "Timer.h"


// Stany wyjść LED-1/LED-2 zakodowane na jednym bajcie (tutaj nr 1) wybranym z portu 32-bitowego. 
#define LED_STATE(led_1_state, led_2_state)     ((((((uint32_t)(led_1_state) != 0) << PIN_LED_1) \
                                                 | (((uint32_t)(led_2_state) != 0) << PIN_LED_2))) >> 8)


//
// Function: LL_Test_PORT_Simple
//
// LED-1 i LED-2 są sterowane (zmieniają stan co 500 ms) przez timer TC0
// pracujący jako generator prostokąta. Sterowanie jest realizowane przez Event System.
//
void LL_Test_PORT_Simple(void)
{
    printf("LL_Test_PORT_Simple\n");

    //---------------------------------------------------------------------------------------------
    // PORT: włączenie reakcji (toggle) wyjść LED-1/2 na zdarzenie TC0_MC0.
    //---------------------------------------------------------------------------------------------

    LED_Set(LED_1, 0);
    LED_Set(LED_2, 1);

    LL_PORT_SetEventAction(PORT_LED_1, 0, 1, LL_PORT_EventAction_TGL, PIN_LED_1);
    LL_PORT_SetEventAction(PORT_LED_2, 1, 1, LL_PORT_EventAction_TGL, PIN_LED_2);

    //---------------------------------------------------------------------------------------------
    // EVSYS channel 0: połączenie zdarzenia TC0_MC0 -> PORT LED-1/2.
    //---------------------------------------------------------------------------------------------

    const uint32_t EVENT_CHANNEL_TC0 = 0;

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_EVSYS_CHANNEL_0, 0, 1);

    LL_EVSYS_InitializeChannel(EVENT_CHANNEL_TC0, LL_EVSYS_EventGenerator_TC0_MC0, LL_EVSYS_Path_SYNCHRONOUS, LL_EVSYS_EdgeDetection_RISING_EDGE, 0, 0);
    LL_EVSYS_AssignUserToChannel(LL_EVSYS_User_PORT_EV0, EVENT_CHANNEL_TC0, 1);
    LL_EVSYS_AssignUserToChannel(LL_EVSYS_User_PORT_EV1, EVENT_CHANNEL_TC0, 1);

    //---------------------------------------------------------------------------------------------
    // TC0: inicjalizacja timera.
    //---------------------------------------------------------------------------------------------

    const uint32_t TIMER_LED = 0;

    NVIC_DisableIRQ(TC0_IRQn);

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_TC0, 0, 1);

    LL_TC_Reset(TIMER_LED);
    LL_TC_Enable(TIMER_LED, 0);
    LL_TC_SetDebugControl(TIMER_LED, 0);
    LL_TC_Initialize(TIMER_LED, LL_TC_TimerMode_COUNT16, LL_TC_CounterPrescaler_DIV1024, LL_TC_Presynchronization_PRESC);
    LL_TC_SetWaveformMode(TIMER_LED, LL_TC_WaweformMode_MatchFrequency);
    LL_TC_SetEventOutput(TIMER_LED, LL_TC_EventOutput_MCEO0);
    LL_TC_SetCounterOneShot(TIMER_LED, 0);
    LL_TC_SetCountingUp(TIMER_LED);
    LL_TC_SetCommand(TIMER_LED, LL_TC_Command_RETRIGGER);

    //---------------------------------------------------------------------------------------------
    // TC0: ustawienie okresu.
    //---------------------------------------------------------------------------------------------

    const uint32_t CCx_Min = 0x0000;
    const uint32_t CCx_Max = 0xFFFF;

    const uint32_t LED_Frequency_Hz = 2;
    const uint32_t Timer0_Clock_Hz = SYSTEM_CLOCK_FREQUENCY_Hz;
    const uint32_t Timer0_Prescaler = 1024;
    const uint32_t Timer0_CC0 = (Timer0_Clock_Hz / Timer0_Prescaler) / LED_Frequency_Hz;

    assert((Timer0_CC0 > CCx_Min) && (Timer0_CC0 <= CCx_Max));

    LL_TC_SetCompareCapture(TIMER_LED, 0, (uint16_t)Timer0_CC0);

    //---------------------------------------------------------------------------------------------
    // Włączenie sterowania LED-1/2 przez TC0.
    //---------------------------------------------------------------------------------------------

    LL_TC_Enable(TIMER_LED, 1);

    for (;;)
    {
    }
}


//
// Function: LL_Test_PORT_DMA
//
// Kontroler DMA jest wyzwalany cyklicznie (f=1 MHz) przez TC0 i pobiera kolejne
// próbki stanów wyjść LED-1/2 (zakodowanych w uint8_t/próbka) z tablicy w RAM, które następnie
// zapisuje do rejestru OUT w PORT (tylko bajt nr 1 wybrany z rejestru 32-bit).
//
// Arguments:
//
// oneShot: Ilość testowych transferów DMA:
//   0 = nieskończone, automatyczne powtarzanie transferu.
//   1 = pojedyńczy transfer i koniec testu.
//
void LL_Test_PORT_DMA(bool oneShot)
{
    printf("LL_Test_PORT_DMA\n");

    //---------------------------------------------------------------------------------------------
    // TC0: inicjalizacja timera TC0 (DMA beat transfer triggering).
    //---------------------------------------------------------------------------------------------

    const uint32_t TIMER_DMA = 0;

    NVIC_DisableIRQ(TC0_IRQn);

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_TC0, 0, 1);

    LL_TC_Reset(TIMER_DMA);
    LL_TC_Enable(TIMER_DMA, 0);
    LL_TC_SetDebugControl(TIMER_DMA, 0);
    LL_TC_Initialize(TIMER_DMA, LL_TC_TimerMode_COUNT16, LL_TC_CounterPrescaler_DIV1, LL_TC_Presynchronization_PRESC);
    LL_TC_SetWaveformMode(TIMER_DMA, LL_TC_WaweformMode_MatchFrequency);
    LL_TC_SetEventAction(TIMER_DMA, LL_TC_EventAction_OFF);
    LL_TC_SetCounterOneShot(TIMER_DMA, 0);
    LL_TC_SetCountingUp(TIMER_DMA);
    LL_TC_SetCommand(TIMER_DMA, LL_TC_Command_RETRIGGER);

    //---------------------------------------------------------------------------------------------
    // TC0: ustawienie okresu wyzwalania DMA.
    //---------------------------------------------------------------------------------------------

    const uint32_t CCx_Min = 0x0000;
    const uint32_t CCx_Max = 0xFFFF;

    const uint32_t DMA_Trigger_Frequency_Hz = 1 * 1000 * 1000;
    const uint32_t Timer0_Clock_Hz = SYSTEM_CLOCK_FREQUENCY_Hz;
    const uint32_t Timer0_Prescaler = 1;
    const uint32_t Timer0_CC0 = (Timer0_Clock_Hz / Timer0_Prescaler) / DMA_Trigger_Frequency_Hz - 1;

    printf("TC0: f_cc0=%u Hz, CC0=%u\n", DMA_Trigger_Frequency_Hz, Timer0_CC0);

    assert((Timer0_CC0 > CCx_Min) && (Timer0_CC0 <= CCx_Max));

    LL_TC_SetCompareCapture(TIMER_DMA, 0, (uint16_t)Timer0_CC0);

    //---------------------------------------------------------------------------------------------
    // Inicjalizacja tablicy stanów pinów PORT.
    //---------------------------------------------------------------------------------------------
  
    // Ilość próbek stanów portu (1 blok DMA).
    enum { PORT_DATA_LENGTH = 18 };

    // Próbki stanu portu są 8-bitowe.
    static uint8_t Port_Data_Array[PORT_DATA_LENGTH] =
    {
        // Stan początkowy.
        LED_STATE(0, 0),

        // Długie włączenie LED-1 + LED-2 (rozbiegówka).
        LED_STATE(1, 1),
        LED_STATE(1, 1),
        LED_STATE(1, 1),
        LED_STATE(0, 0),

        // 2x mignięcie LED-1.
        LED_STATE(1, 0),
        LED_STATE(0, 0),
        LED_STATE(1, 0),
        LED_STATE(0, 0),

        // 2x mignięcie LED-2.
        LED_STATE(0, 1),
        LED_STATE(0, 0),
        LED_STATE(0, 1),
        LED_STATE(0, 0),

        // Pauza / separator.
        LED_STATE(0, 0),

        // 2x mignięcie LED-1 + LED-2.
        LED_STATE(1, 1),
        LED_STATE(0, 0),
        LED_STATE(1, 1),
        LED_STATE(0, 0)
    };

    //---------------------------------------------------------------------------------------------
    // DMAC: inicjalizacja globalna.
    //---------------------------------------------------------------------------------------------

    // Ilość kanałów DMA.
    enum { DMA_CHANNEL_NUM = 1 };

    // Indeksy kanałów DMA.
    enum { DMA_CHANNEL_PORT = 0 };

    static LL_DMAC_DESCRIPTOR_ARRAY(DMA_Descriptors, DMA_CHANNEL_NUM);
    static LL_DMAC_DESCRIPTOR_ARRAY(DMA_Descriptors_WriteBack, DMA_CHANNEL_NUM);

    NVIC_DisableIRQ(DMAC_IRQn);

    LL_DMAC_Reset();
    LL_DMAC_SetDescriptorMemoryBaseAddress(DMA_Descriptors);
    LL_DMAC_SetWriteBackMemoryBaseAddress(DMA_Descriptors_WriteBack);
    LL_DMAC_SetArbitrationMode(LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin);
    LL_DMAC_Enable(1);

    //---------------------------------------------------------------------------------------------
    // DMAC channel 0: inicjalizacja kanału DMA kopiującego dane z bufora w RAM do PORT.
    //---------------------------------------------------------------------------------------------

    // Indeks bajtu (grupa 8 pinów) w obrębie 32-bitowego rejestru PORT.OUT.
    // W tym bajcie znajdują się piny LED-1/2.
    const uint32_t OutputPinGroupByteIndex = 1;

    const uint32_t DMA_CHANNEL_PORT_ADDRESS_SOURCE      = (uint32_t)( &Port_Data_Array[PORT_DATA_LENGTH] );
    const uint32_t DMA_CHANNEL_PORT_ADDRESS_DESTINATION = (uint32_t)( &PORT->Group[0].OUT.reg ) + OutputPinGroupByteIndex;

    LL_DMAC_Descriptor_Initialize(&DMA_Descriptors[DMA_CHANNEL_PORT],
                                  LL_DMAC_TransferBeatSize_BYTE,
                                  1, 0,
                                  LL_DMAC_TransferStepSelection_SRC,
                                  LL_DMAC_TransferStepSize_X1,
                                  LL_DMAC_TransferEventOutput_DISABLE,
                                  LL_DMAC_TransferBlockAction_NOACT);

    LL_DMAC_Descriptor_SetBlockTransferCount(&DMA_Descriptors[DMA_CHANNEL_PORT], PORT_DATA_LENGTH);
    LL_DMAC_Descriptor_SetBlockTransferAddress(&DMA_Descriptors[DMA_CHANNEL_PORT],
                                               (volatile void *)(DMA_CHANNEL_PORT_ADDRESS_SOURCE),
                                               (volatile void *)(DMA_CHANNEL_PORT_ADDRESS_DESTINATION));
    if (oneShot == true)
    {
        LL_DMAC_Descriptor_SetNextDescriptorAddress(&DMA_Descriptors[DMA_CHANNEL_PORT], 0);
    }
    else
    {
        // BANZAI: zapętlenie deskryptora.
        LL_DMAC_Descriptor_SetNextDescriptorAddress(&DMA_Descriptors[DMA_CHANNEL_PORT], &DMA_Descriptors[DMA_CHANNEL_PORT]);
    }

    LL_DMAC_Descriptor_SetValid(&DMA_Descriptors[DMA_CHANNEL_PORT], 1);

    LL_DMAC_Channel_Reset(DMA_CHANNEL_PORT);
    LL_DMAC_Channel_SetTrigger(DMA_CHANNEL_PORT, LL_DMAC_ChannelTriggerSource_TC0_MC0, LL_DMAC_ChannelTriggerAction_BEAT);
    LL_DMAC_Channel_SetEvent(DMA_CHANNEL_PORT, 0, 0, LL_DMAC_ChannelEventAction_NOACT);
    LL_DMAC_Channel_ArbitrationLevel(DMA_CHANNEL_PORT, LL_DMAC_ArbitrationLevel_LVL0);
    LL_DMAC_Channel_Enable(DMA_CHANNEL_PORT, 1, 0);

    LL_DMAC_Channel_InterruptEnableSet(DMA_CHANNEL_PORT, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);
    LL_DMAC_Channel_ClearInterruptFlag(DMA_CHANNEL_PORT, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);

    //---------------------------------------------------------------------------------------------
    // Aktywacja DMA.
    //---------------------------------------------------------------------------------------------

    LED_Set(LED_GREEN, 0);
    LED_Set(LED_RED, 0);

    // Włączenie cyklicznego wyzwalania DMA (jedno wyzwolenie DMA = jedna aktualizacja GPIO).
    LL_TC_Enable(TIMER_DMA, 1);

    //---------------------------------------------------------------------------------------------
    // Oczekiwanie na koniec transferu DMA (w teście one-shot).
    //---------------------------------------------------------------------------------------------

    printf("DMA: start (one-shot: %u)\n", oneShot);

    enum LL_DMAC_ChannelInterruptFlag DMAC_ChannelInterruptFlag;

    do
    {
        DMAC_ChannelInterruptFlag = LL_DMAC_Channel_GetInterruptFlag(DMA_CHANNEL_PORT);
    }
    while (DMAC_ChannelInterruptFlag == 0);

    if ((DMAC_ChannelInterruptFlag & LL_DMAC_ChannelInterruptFlag_TERR) != 0)
    {
        printf("DMA: TERR\n");
    }

    if ((DMAC_ChannelInterruptFlag & LL_DMAC_ChannelInterruptFlag_SUSP) != 0)
    {
        printf("DMA: SUSP\n");
    }

    if ((DMAC_ChannelInterruptFlag & LL_DMAC_ChannelInterruptFlag_TCMPL) != 0)
    {
        printf("DMA: TCMPL\n");
    }

    // Wyłączenie wyzwalania DMA.
    LL_TC_Enable(TIMER_DMA, 0);

    for (;;)
    {
    }
}
