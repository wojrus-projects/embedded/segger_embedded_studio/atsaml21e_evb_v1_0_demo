#ifndef LL_TEST_DAC_INCLUDED_H
#define LL_TEST_DAC_INCLUDED_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif


void LL_Test_DAC_Simple(void);
void LL_Test_DAC_DMA(bool oneShot);


#ifdef __cplusplus
}
#endif

#endif // LL_TEST_DAC_INCLUDED_H
