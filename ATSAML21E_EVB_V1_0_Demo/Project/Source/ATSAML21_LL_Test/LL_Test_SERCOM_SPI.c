//
// Testy LL_SERCOM_SPI.
//

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "ATSAML21_LL/LL.h"
#include "Board.h"
#include "Timer.h"


// Rozmiar buforów DMA Rx/Tx w bajtach.
enum { DMA_BUFFER_SIZE = 8 };

// Globalne bufory DMA (dla debuggera).
uint8_t SPI_DMA_Buffer_Rx[DMA_BUFFER_SIZE];
uint8_t SPI_DMA_Buffer_Tx[DMA_BUFFER_SIZE];

// Indeksy kanałów DMA.
enum { DMA_CHANNEL_SPI_RX = 0, DMA_CHANNEL_SPI_TX = 1 };

// Globalne flagi DMA (dla debuggera).
volatile bool SPI_DMA_Rx_Completed;
volatile bool SPI_DMA_Tx_Completed;


//
// Function: LL_Test_SERCOM_SPI_Master_Simple
//
// SPI-master w SERCOM-0 pracuje w trybie polling i realizuje loop-back 6 bajtów.
//
// Arguments:
//
// dataSizeBits: Rozmiar danych w bitach (8 lub 9).
//
void LL_Test_SERCOM_SPI_Master_Simple(uint32_t dataSizeBits)
{
    printf("LL_Test_SERCOM_SPI_Master_Simple\n");

    assert((dataSizeBits == 8) || (dataSizeBits == 9));

    //---------------------------------------------------------------------------------------------
    // SERCOM-0: konfiguracja pinów SPI-master.
    //---------------------------------------------------------------------------------------------

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_CS, PIN_CLICK_CS, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_CS, PIN_CLICK_CS, LL_PORT_PeripheralFunction_D);

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_SCK, PIN_CLICK_SCK, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_SCK, PIN_CLICK_SCK, LL_PORT_PeripheralFunction_D);

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_MISO, PIN_CLICK_MISO, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_MISO, PIN_CLICK_MISO, LL_PORT_PeripheralFunction_D);

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_MOSI, PIN_CLICK_MOSI, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_MOSI, PIN_CLICK_MOSI, LL_PORT_PeripheralFunction_D);

    //---------------------------------------------------------------------------------------------
    // SERCOM-0: obliczenie baud rate.
    //---------------------------------------------------------------------------------------------

    const uint32_t baudSet = 100 * 1000;
    const uint32_t f_ref = SYSTEM_CLOCK_FREQUENCY_Hz;
    const uint32_t baudRegister = f_ref / (2ul * baudSet) - 1ul;

    assert((baudRegister >= 0x00) && (baudRegister <= 0xFF));

    const uint32_t baudFromRegister = f_ref / (2ul * (baudRegister + 1ul));
    const int32_t baudErrorDelta = baudSet - baudFromRegister;
    const float baudErrorPercent = (100.0f * (float)baudErrorDelta) / (float)baudSet;

    printf("BAUD register: %u / 0x%X\n", baudRegister, baudRegister);
    printf("Baud set: %u bps -> baud actual: %u bps\n", baudSet, baudFromRegister);
    printf("Error: %d bps / %f %%\n", baudErrorDelta, baudErrorPercent);

    //---------------------------------------------------------------------------------------------
    // SERCOM-0: konfiguracja SPI-master.
    //---------------------------------------------------------------------------------------------

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_SERCOM0_CORE, 0, 1);

    NVIC_DisableIRQ(SERCOM0_IRQn);
    
    LL_SERCOM_SPI_Reset(0);
    LL_SERCOM_SPI_Enable(0, 0);
    LL_SERCOM_SPI_SetRunInStandby(0, 0);
    LL_SERCOM_SPI_SetDebugControl(0, 0);

    LL_SERCOM_SPI_SetPins(0, LL_SERCOM_SPI_DataOutPinput_DO0_SCK1_SS2, LL_SERCOM_SPI_DataInPinput_DI3);
    LL_SERCOM_SPI_SetMode(0, LL_SERCOM_SPI_Mode_Master);
    LL_SERCOM_SPI_SetDataFormat(0, LL_SERCOM_SPI_FrameFormat_SPI, LL_SERCOM_SPI_ClockPhase_LeadingEdge, LL_SERCOM_SPI_ClockPolarity_Low, LL_SERCOM_SPI_DataOrder_FirstMSB);
    LL_SERCOM_SPI_SetBaudRate(0, (uint8_t)baudSet);
    LL_SERCOM_SPI_SetAddressMode(0, LL_SERCOM_SPI_AddressMode_MASK);
    LL_SERCOM_SPI_SetAddress(0, 0x00, 0x00);
    LL_SERCOM_SPI_SetCharacterSize(0, (dataSizeBits == 8) ? LL_SERCOM_SPI_CharacterSize_8BIT : LL_SERCOM_SPI_CharacterSize_9BIT);
    LL_SERCOM_SPI_SetSlaveSelect(0, 0, 1);
    LL_SERCOM_SPI_SetImmediateBufferOverflowNotification(0, 0);
    LL_SERCOM_SPI_EnableReceiver(0, 1);

    LL_SERCOM_SPI_InterruptEnableSet(0, LL_SERCOM_SPI_InterruptFlag_DRE
                                      | LL_SERCOM_SPI_InterruptFlag_TXC
                                      | LL_SERCOM_SPI_InterruptFlag_RXC
                                      | LL_SERCOM_SPI_InterruptFlag_SSL
                                      | LL_SERCOM_SPI_InterruptFlag_ERROR);

    //---------------------------------------------------------------------------------------------
    // SERCOM-0: test SPI-master (wysłanie jednej ramki).
    //---------------------------------------------------------------------------------------------

    enum { DATA_LENGTH = 6 };

    uint16_t txDataBuffer[DATA_LENGTH] = { 0x001, 0x002, 0x003, 0x004, 0x005, 0x006 };
    uint16_t rxDataBuffer[DATA_LENGTH] = {};
    uint32_t txIndex = 0;
    uint32_t rxIndex = 0;

    if (dataSizeBits == 9)
    {
        // Dodanie bitu nr 9.
        for (uint32_t i = DATA_LENGTH / 2; i < DATA_LENGTH; i++)
        {
            txDataBuffer[i] |= 0x100;
        }
    }

    printf("SPI-master send %u word(s)\n", DATA_LENGTH);

    LL_SERCOM_SPI_Enable(0, 1);

    for (;;)
    {
        const enum LL_SERCOM_SPI_InterruptFlag interruptFlag = LL_SERCOM_SPI_GetInterruptFlag(0);

        if (interruptFlag != 0)
        {
            LL_SERCOM_SPI_ClearInterruptFlag(0, interruptFlag);

            if ((interruptFlag & LL_SERCOM_SPI_InterruptFlag_DRE)
                && (LL_SERCOM_SPI_GetInterruptEnableState(0) & LL_SERCOM_SPI_InterruptFlag_DRE))
            {
                if (txIndex < DATA_LENGTH)
                {
                    const uint16_t txData = txDataBuffer[txIndex];

                    printf("SPI: DRE [%u]: 0x%03X\n", txIndex, txData);

                    LL_SERCOM_SPI_WriteData(0, txData);
                    txIndex++;

                    if (txIndex == DATA_LENGTH)
                    {
                        LL_SERCOM_SPI_InterruptEnableClear(0, LL_SERCOM_SPI_InterruptFlag_DRE);
                    }
                }
            }

            if (interruptFlag & LL_SERCOM_SPI_InterruptFlag_TXC)
            {
                printf("SPI: TXC\n");
            }

            if (interruptFlag & LL_SERCOM_SPI_InterruptFlag_RXC)
            {
                const uint16_t rxData = LL_SERCOM_SPI_ReadData(0);

                printf("SPI: RXC [%u]: 0x%03X\n", rxIndex, rxData);

                if (rxIndex < DATA_LENGTH)
                {
                    rxDataBuffer[rxIndex] = rxData;
                    rxIndex++;

                    if (rxIndex == DATA_LENGTH)
                    {
                        if (memcmp(rxDataBuffer, txDataBuffer, DATA_LENGTH * sizeof(uint16_t)) == 0)
                        {
                            printf("Rx data is valid\n");
                        }
                        else
                        {
                            printf("Rx data is invalid\n");
                        }
                    }
                }
            }

            if (interruptFlag & LL_SERCOM_SPI_InterruptFlag_SSL)
            {
                printf("SPI: SSL\n");
            }

            if (interruptFlag & LL_SERCOM_SPI_InterruptFlag_ERROR)
            {
                printf("SPI: ERROR\n");
            }
        }
    }
}


//
// Function: LL_Test_SERCOM_SPI_Master_DMA
//
// SPI-master w SERCOM-0 wyzwala DMA (Rx i Tx) i realizuje loop-back 8 bajtów.
//
void LL_Test_SERCOM_SPI_Master_DMA(void)
{
    printf("LL_Test_SERCOM_SPI_Master_DMA\n");

    //---------------------------------------------------------------------------------------------
    // SERCOM-0: konfiguracja pinów SPI-master.
    //---------------------------------------------------------------------------------------------

    // TODO: glitches bezpośrednio po ustawieniu LL_PORT_PeripheralFunction_D.

    LL_PORT_SetOutputDriverStrength(PORT_CLICK_CS,   PIN_CLICK_CS,   LL_PORT_OutputDriverStrength_High);
    LL_PORT_SetOutputDriverStrength(PORT_CLICK_SCK,  PIN_CLICK_SCK,  LL_PORT_OutputDriverStrength_High);
    LL_PORT_SetOutputDriverStrength(PORT_CLICK_MOSI, PIN_CLICK_MOSI, LL_PORT_OutputDriverStrength_High);

    LL_PORT_SetPullMode(PORT_CLICK_CS,   PIN_CLICK_CS,   LL_PORT_PullMode_Up);
    LL_PORT_SetPullMode(PORT_CLICK_SCK,  PIN_CLICK_SCK,  LL_PORT_PullMode_Down);
    LL_PORT_SetPullMode(PORT_CLICK_MOSI, PIN_CLICK_MOSI, LL_PORT_PullMode_Disable);
    LL_PORT_SetPullMode(PORT_CLICK_MISO, PIN_CLICK_MISO, LL_PORT_PullMode_Up);

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_CS, PIN_CLICK_CS, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_CS, PIN_CLICK_CS, LL_PORT_PeripheralFunction_D);

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_SCK, PIN_CLICK_SCK, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_SCK, PIN_CLICK_SCK, LL_PORT_PeripheralFunction_D);

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_MISO, PIN_CLICK_MISO, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_MISO, PIN_CLICK_MISO, LL_PORT_PeripheralFunction_D);

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_MOSI, PIN_CLICK_MOSI, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_MOSI, PIN_CLICK_MOSI, LL_PORT_PeripheralFunction_D);

    //---------------------------------------------------------------------------------------------
    // SERCOM-0: obliczenie baud rate.
    //---------------------------------------------------------------------------------------------

    const uint32_t baudSet = 100 * 1000;
    const uint32_t f_ref = SYSTEM_CLOCK_FREQUENCY_Hz;
    const uint32_t baudRegister = f_ref / (2ul * baudSet) - 1ul;

    assert((baudRegister >= 0x00) && (baudRegister <= 0xFF));

    const uint32_t baudFromRegister = f_ref / (2ul * (baudRegister + 1ul));
    const int32_t baudErrorDelta = baudSet - baudFromRegister;
    const float baudErrorPercent = (100.0f * (float)baudErrorDelta) / (float)baudSet;

    printf("BAUD register: %u / 0x%X\n", baudRegister, baudRegister);
    printf("Baud set: %u bps -> baud actual: %u bps\n", baudSet, baudFromRegister);
    printf("Error: %d bps / %f %%\n", baudErrorDelta, baudErrorPercent);

    //---------------------------------------------------------------------------------------------
    // SERCOM-0: konfiguracja SPI-master.
    //---------------------------------------------------------------------------------------------

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_SERCOM0_CORE, 0, 1);

    NVIC_DisableIRQ(SERCOM0_IRQn);
    
    LL_SERCOM_SPI_Reset(0);
    LL_SERCOM_SPI_Enable(0, 0);
    LL_SERCOM_SPI_SetRunInStandby(0, 0);
    LL_SERCOM_SPI_SetDebugControl(0, 0);

    LL_SERCOM_SPI_SetPins(0, LL_SERCOM_SPI_DataOutPinput_DO0_SCK1_SS2, LL_SERCOM_SPI_DataInPinput_DI3);
    LL_SERCOM_SPI_SetMode(0, LL_SERCOM_SPI_Mode_Master);
    LL_SERCOM_SPI_SetDataFormat(0, LL_SERCOM_SPI_FrameFormat_SPI, LL_SERCOM_SPI_ClockPhase_LeadingEdge, LL_SERCOM_SPI_ClockPolarity_Low, LL_SERCOM_SPI_DataOrder_FirstMSB);
    LL_SERCOM_SPI_SetBaudRate(0, (uint8_t)baudSet);
    LL_SERCOM_SPI_SetAddressMode(0, LL_SERCOM_SPI_AddressMode_MASK);
    LL_SERCOM_SPI_SetAddress(0, 0x00, 0x00);
    LL_SERCOM_SPI_SetCharacterSize(0, LL_SERCOM_SPI_CharacterSize_8BIT);
    LL_SERCOM_SPI_SetSlaveSelect(0, 0, 1);
    LL_SERCOM_SPI_SetImmediateBufferOverflowNotification(0, 0);
    LL_SERCOM_SPI_EnableReceiver(0, 1);

    //---------------------------------------------------------------------------------------------
    // SERCOM-0: włączenie przerwań.
    //---------------------------------------------------------------------------------------------

    LL_SERCOM_SPI_InterruptEnableSet(0, LL_SERCOM_SPI_InterruptFlag_TXC
                                      | LL_SERCOM_SPI_InterruptFlag_SSL
                                      | LL_SERCOM_SPI_InterruptFlag_ERROR);

    NVIC_ClearPendingIRQ(SERCOM0_IRQn);
    NVIC_SetPriority(SERCOM0_IRQn, 0);
    NVIC_EnableIRQ(SERCOM0_IRQn);

    //---------------------------------------------------------------------------------------------
    // DMAC: inicjalizacja globalna.
    //---------------------------------------------------------------------------------------------

    // Ilość kanałów DMA.
    enum { DMA_CHANNEL_NUM = 2 };

    static LL_DMAC_DESCRIPTOR_ARRAY(DMA_Descriptors, DMA_CHANNEL_NUM);
    static LL_DMAC_DESCRIPTOR_ARRAY(DMA_Descriptors_WriteBack, DMA_CHANNEL_NUM);

    NVIC_DisableIRQ(DMAC_IRQn);

    LL_DMAC_Reset();
    LL_DMAC_SetDescriptorMemoryBaseAddress(DMA_Descriptors);
    LL_DMAC_SetWriteBackMemoryBaseAddress(DMA_Descriptors_WriteBack);
    LL_DMAC_SetArbitrationMode(LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin);
    LL_DMAC_Enable(1);

    //---------------------------------------------------------------------------------------------
    // DMAC channel 0: inicjalizacja kanału DMA kopiującego dane z rej. SPI-RX do bufora w RAM.
    //---------------------------------------------------------------------------------------------

    const uint32_t DMA_CHANNEL_SPI_RX_ADDRESS_SOURCE      = (uint32_t)( &SERCOM0->SPI.DATA.reg );
    const uint32_t DMA_CHANNEL_SPI_RX_ADDRESS_DESTINATION = (uint32_t)( &SPI_DMA_Buffer_Rx[DMA_BUFFER_SIZE] );

    LL_DMAC_Descriptor_Initialize(&DMA_Descriptors[DMA_CHANNEL_SPI_RX],
                                  LL_DMAC_TransferBeatSize_BYTE,
                                  0, 1,
                                  LL_DMAC_TransferStepSelection_SRC,
                                  LL_DMAC_TransferStepSize_X1,
                                  LL_DMAC_TransferEventOutput_DISABLE,
                                  LL_DMAC_TransferBlockAction_NOACT);

    LL_DMAC_Descriptor_SetBlockTransferCount(&DMA_Descriptors[DMA_CHANNEL_SPI_RX], DMA_BUFFER_SIZE);
    LL_DMAC_Descriptor_SetBlockTransferAddress(&DMA_Descriptors[DMA_CHANNEL_SPI_RX],
                                               (volatile void *)(DMA_CHANNEL_SPI_RX_ADDRESS_SOURCE),
                                               (volatile void *)(DMA_CHANNEL_SPI_RX_ADDRESS_DESTINATION));
    LL_DMAC_Descriptor_SetNextDescriptorAddress(&DMA_Descriptors[DMA_CHANNEL_SPI_RX], 0);
    LL_DMAC_Descriptor_SetValid(&DMA_Descriptors[DMA_CHANNEL_SPI_RX], 1);

    LL_DMAC_Channel_Reset(DMA_CHANNEL_SPI_RX);
    LL_DMAC_Channel_SetTrigger(DMA_CHANNEL_SPI_RX, LL_DMAC_ChannelTriggerSource_SERCOM0_RX, LL_DMAC_ChannelTriggerAction_BEAT);
    LL_DMAC_Channel_SetEvent(DMA_CHANNEL_SPI_RX, 0, 0, LL_DMAC_ChannelEventAction_NOACT);
    LL_DMAC_Channel_ArbitrationLevel(DMA_CHANNEL_SPI_RX, LL_DMAC_ArbitrationLevel_LVL0);

    LL_DMAC_Channel_InterruptEnableSet(DMA_CHANNEL_SPI_RX, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);
    LL_DMAC_Channel_ClearInterruptFlag(DMA_CHANNEL_SPI_RX, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);

    //---------------------------------------------------------------------------------------------
    // DMAC channel 1: inicjalizacja kanału DMA kopiującego dane z bufora w RAM do rej. SPI-TX.
    //---------------------------------------------------------------------------------------------

    const uint32_t DMA_CHANNEL_SPI_TX_ADDRESS_SOURCE      = (uint32_t)( &SPI_DMA_Buffer_Tx[DMA_BUFFER_SIZE] );
    const uint32_t DMA_CHANNEL_SPI_TX_ADDRESS_DESTINATION = (uint32_t)( &SERCOM0->SPI.DATA.reg );

    LL_DMAC_Descriptor_Initialize(&DMA_Descriptors[DMA_CHANNEL_SPI_TX],
                                  LL_DMAC_TransferBeatSize_BYTE,
                                  1, 0,
                                  LL_DMAC_TransferStepSelection_SRC,
                                  LL_DMAC_TransferStepSize_X1,
                                  LL_DMAC_TransferEventOutput_DISABLE,
                                  LL_DMAC_TransferBlockAction_NOACT);

    LL_DMAC_Descriptor_SetBlockTransferCount(&DMA_Descriptors[DMA_CHANNEL_SPI_TX], DMA_BUFFER_SIZE);
    LL_DMAC_Descriptor_SetBlockTransferAddress(&DMA_Descriptors[DMA_CHANNEL_SPI_TX],
                                               (volatile void *)(DMA_CHANNEL_SPI_TX_ADDRESS_SOURCE),
                                               (volatile void *)(DMA_CHANNEL_SPI_TX_ADDRESS_DESTINATION));
    LL_DMAC_Descriptor_SetNextDescriptorAddress(&DMA_Descriptors[DMA_CHANNEL_SPI_TX], 0);
    LL_DMAC_Descriptor_SetValid(&DMA_Descriptors[DMA_CHANNEL_SPI_TX], 1);

    LL_DMAC_Channel_Reset(DMA_CHANNEL_SPI_TX);
    LL_DMAC_Channel_SetTrigger(DMA_CHANNEL_SPI_TX, LL_DMAC_ChannelTriggerSource_SERCOM0_TX, LL_DMAC_ChannelTriggerAction_BEAT);
    LL_DMAC_Channel_SetEvent(DMA_CHANNEL_SPI_TX, 0, 0, LL_DMAC_ChannelEventAction_NOACT);
    LL_DMAC_Channel_ArbitrationLevel(DMA_CHANNEL_SPI_TX, LL_DMAC_ArbitrationLevel_LVL0);

    LL_DMAC_Channel_InterruptEnableSet(DMA_CHANNEL_SPI_TX, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);
    LL_DMAC_Channel_ClearInterruptFlag(DMA_CHANNEL_SPI_TX, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);

    //---------------------------------------------------------------------------------------------
    // DMAC: włączenie przerwań.
    //---------------------------------------------------------------------------------------------

    NVIC_ClearPendingIRQ(DMAC_IRQn);
    NVIC_SetPriority(DMAC_IRQn, 0);
    NVIC_EnableIRQ(DMAC_IRQn);

    //---------------------------------------------------------------------------------------------
    // Inicjalizacja buforów DMA.
    //---------------------------------------------------------------------------------------------

    memset(SPI_DMA_Buffer_Rx, 0, DMA_BUFFER_SIZE);

    for (uint32_t i = 0; i < DMA_BUFFER_SIZE; i++)
    {
        SPI_DMA_Buffer_Tx[i] = (uint8_t)i;
    }

    SPI_DMA_Rx_Completed = false;
    SPI_DMA_Tx_Completed = false;

    //---------------------------------------------------------------------------------------------
    // Włączenie DMA Rx+Tx oraz SPI-master Rx+Tx.
    //---------------------------------------------------------------------------------------------

    printf("Start SPI-master DMA transfers (Rx/Tx buffers size: %u byte(s))\n", DMA_BUFFER_SIZE);

    LL_DMAC_Channel_Enable(DMA_CHANNEL_SPI_RX, 1, 0);
    LL_DMAC_Channel_Enable(DMA_CHANNEL_SPI_TX, 1, 0);

    LL_SERCOM_SPI_Enable(0, 1);

    for (;;)
    {
    }
}


void LL_Test_SERCOM_SPI_Master_InterruptHandlerSPI(void)
{
    const enum LL_SERCOM_SPI_InterruptFlag interruptFlag = LL_SERCOM_SPI_GetInterruptFlag(0);

    if (interruptFlag != 0)
    {
        LL_SERCOM_SPI_ClearInterruptFlag(0, interruptFlag);

        if ((interruptFlag & LL_SERCOM_SPI_InterruptFlag_DRE)
            && (LL_SERCOM_SPI_GetInterruptEnableState(0) & LL_SERCOM_SPI_InterruptFlag_DRE))
        {
            printf("SPI: DRE\n");
        }

        if (interruptFlag & LL_SERCOM_SPI_InterruptFlag_TXC)
        {
            printf("SPI: TXC\n");

            if (SPI_DMA_Rx_Completed && SPI_DMA_Tx_Completed)
            {
                printf("SPI: completed\n");

                if (memcmp(SPI_DMA_Buffer_Rx, SPI_DMA_Buffer_Tx, DMA_BUFFER_SIZE) == 0)
                {
                    printf("Rx data is valid\n");
                }
                else
                {
                    printf("Rx data is invalid\n");
                }
            }
        }

        if (interruptFlag & LL_SERCOM_SPI_InterruptFlag_RXC)
        {
            printf("SPI: RXC\n");
        }

        if (interruptFlag & LL_SERCOM_SPI_InterruptFlag_SSL)
        {
            printf("SPI: SSL\n");
        }

        if (interruptFlag & LL_SERCOM_SPI_InterruptFlag_ERROR)
        {
            printf("SPI: ERROR\n");
        }
    }
}


void LL_Test_SERCOM_SPI_Master_InterruptHandlerDMAC(void)
{
    const uint32_t pendingChannelBitmap = LL_DMAC_GetInterruptStatus();

    //---------------------------------------------------------------------------------------------
    // DMAC channel 0: SPI Rx.
    //---------------------------------------------------------------------------------------------

    if (pendingChannelBitmap & (1ul << DMA_CHANNEL_SPI_RX))
    {
        const enum LL_DMAC_ChannelInterruptFlag interruptFlag = LL_DMAC_Channel_GetInterruptFlag(DMA_CHANNEL_SPI_RX);

        LL_DMAC_Channel_ClearInterruptFlag(DMA_CHANNEL_SPI_RX, interruptFlag);

        printf("[%u] interrupt flags (Rx): 0x%X\n", DMA_CHANNEL_SPI_RX, interruptFlag);

        if (interruptFlag & LL_DMAC_ChannelInterruptFlag_TERR)
        {
            printf("[%u] TERR\n", DMA_CHANNEL_SPI_RX);
        }

        if (interruptFlag & LL_DMAC_ChannelInterruptFlag_TCMPL)
        {
            printf("[%u] TCMPL\n", DMA_CHANNEL_SPI_RX);

            SPI_DMA_Rx_Completed = true;

            printf("Rx data (hex): ");

            for (uint32_t i = 0; i < DMA_BUFFER_SIZE; i++)
            {
                printf("%02X", SPI_DMA_Buffer_Rx[i]);

                if (i < (DMA_BUFFER_SIZE - 1ul))
                {
                    printf(", ");
                }
            }

            printf("\n");
        }

        if (interruptFlag & LL_DMAC_ChannelInterruptFlag_SUSP)
        {
            printf("[%u] SUSP\n", DMA_CHANNEL_SPI_RX);
        }
    }

    //---------------------------------------------------------------------------------------------
    // DMAC channel 1: SPI Tx.
    //---------------------------------------------------------------------------------------------

    if (pendingChannelBitmap & (1ul << DMA_CHANNEL_SPI_TX))
    {
        const enum LL_DMAC_ChannelInterruptFlag interruptFlag = LL_DMAC_Channel_GetInterruptFlag(DMA_CHANNEL_SPI_TX);

        LL_DMAC_Channel_ClearInterruptFlag(DMA_CHANNEL_SPI_TX, interruptFlag);

        printf("[%u] interrupt flags (Tx): 0x%X\n", DMA_CHANNEL_SPI_TX, interruptFlag);

        if (interruptFlag & LL_DMAC_ChannelInterruptFlag_TERR)
        {
            printf("[%u] TERR\n", DMA_CHANNEL_SPI_TX);
        }

        if (interruptFlag & LL_DMAC_ChannelInterruptFlag_TCMPL)
        {
            printf("[%u] TCMPL\n", DMA_CHANNEL_SPI_TX);

            SPI_DMA_Tx_Completed = true;
        }

        if (interruptFlag & LL_DMAC_ChannelInterruptFlag_SUSP)
        {
            printf("[%u] SUSP\n", DMA_CHANNEL_SPI_TX);
        }
    }
}
