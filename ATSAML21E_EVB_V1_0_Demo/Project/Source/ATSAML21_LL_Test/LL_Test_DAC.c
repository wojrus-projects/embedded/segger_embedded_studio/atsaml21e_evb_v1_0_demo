//
// Testy LL_DAC.
//

#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "ATSAML21_LL/LL.h"
#include "Board.h"
#include "Timer.h"


// Zakres wartości wejściowych DAC 12-bit.
#define DAC_DATA_MIN     0x000
#define DAC_DATA_MAX     0xFFF


//
// Function: LL_Test_DAC_Simple
//
// Konwerter DAC-0 co 5 ms zwiększa napięcie wyjściowe od 0 do Vref.
// Szybkość przyrostu napięcia zmienia się co pełen okres: 
//  - Okres N: przyrost z krokiem 0,1 V.
//  - Okres N + 1: przyrost z krokiem 0,2 V.
// DAC-0 używa wyjście analogowe PA02 (VOUT[0]).
// Napięcie referencyjne DAC Vref = Vana.
//
void LL_Test_DAC_Simple(void)
{
    printf("LL_Test_DAC_Simple\n");

    //---------------------------------------------------------------------------------------------
    // PORT: konfiguracja wyjścia analogowego DAC-0.
    //---------------------------------------------------------------------------------------------

    LL_PORT_EnablePeripheralMultiplexer(PORT_DAC_0, PIN_DAC_0, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_DAC_0, PIN_DAC_0, LL_PORT_PeripheralFunction_B);

    //---------------------------------------------------------------------------------------------
    // DAC-0: inicjalizacja konwertera DAC-0.
    //---------------------------------------------------------------------------------------------

    const uint32_t DAC_CHANNEL = 0;

    NVIC_DisableIRQ(DAC_IRQn);

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_DAC, 0, 1);

    LL_DAC_Reset();
    LL_DAC_Enable(0);
    LL_DAC_SetReferenceVoltage(LL_DAC_ReferenceVoltage_VDDANA);
    LL_DAC_ChannelSetConfiguration(DAC_CHANNEL, 0, LL_DAC_Current_CC12M, 0, LL_DAC_RefreshPeriod_30us);
    LL_DAC_ChannelEnable(DAC_CHANNEL, 1, 0);
    LL_DAC_Enable(1);

    enum LL_DAC_Status dacStatus;

    do
    {
        dacStatus = LL_DAC_GetStatus();
    }
    while ((dacStatus & LL_DAC_Status_READY0) == 0);

    LL_DAC_ChannelWriteData(DAC_CHANNEL, DAC_DATA_MIN);

    //---------------------------------------------------------------------------------------------
    // Start pomiarów.
    //---------------------------------------------------------------------------------------------

    const float DAC_RangeRaw = 4096.0f;
    const float V_Ref = 3.278f;

    uint32_t testCounter = 0;

    for (;;)
    {
        float V_Step;

        LED_Toggle(LED_RED);

        if ((testCounter % 2) == 0)
        {
            V_Step = 0.1f;
        }
        else
        {
            V_Step = 0.2f;
        }

        for (float V_Out = 0.0f; V_Out <= V_Ref; V_Out += V_Step)
        {
            uint32_t rawData = (uint32_t)((DAC_RangeRaw * V_Out) / V_Ref);

            if (rawData > DAC_DATA_MAX)
            {
                rawData = DAC_DATA_MAX;
            }

            LL_DAC_ChannelWriteData(DAC_CHANNEL, (uint16_t)rawData);
            Timer_DelayMilliseconds(5);
        }

        testCounter++;
    }
}


//
// Function: LL_Test_DAC_DMA
//
// Kontroler DMA jest wyzwalany cyklicznie (f=10 kHz) przez TC0 i pobiera kolejne
// próbki z tablicy w RAM, które następnie zapisuje do rejestru danych w DAC-0.
// Tablica zawiera 100 próbek jednego okresu sin() stąd sygnał na wyjściu DAC
// ma częstotliwość 100 Hz.
// DAC-0 używa wyjście analogowe PA02 (VOUT[0]).
// Napięcie referencyjne DAC Vref = Vana.
//
// Arguments:
//
// oneShot: Ilość testowych transferów DMA:
//   0 = nieskończone, automatyczne powtarzanie transferu.
//   1 = pojedyńczy transfer i koniec testu.
//
void LL_Test_DAC_DMA(bool oneShot)
{
    printf("LL_Test_DAC_DMA\n");

    //---------------------------------------------------------------------------------------------
    // PORT: konfiguracja wyjścia analogowego DAC-0.
    //---------------------------------------------------------------------------------------------

    LL_PORT_EnablePeripheralMultiplexer(PORT_DAC_0, PIN_DAC_0, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_DAC_0, PIN_DAC_0, LL_PORT_PeripheralFunction_B);

    //---------------------------------------------------------------------------------------------
    // DAC-0: inicjalizacja konwertera DAC-0.
    //---------------------------------------------------------------------------------------------

    const uint32_t DAC_CHANNEL = 0;

    NVIC_DisableIRQ(DAC_IRQn);

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_DAC, 0, 1);

    LL_DAC_Reset();
    LL_DAC_Enable(0);
    LL_DAC_SetDebugControl(0);
    LL_DAC_SetReferenceVoltage(LL_DAC_ReferenceVoltage_VDDANA);
    LL_DAC_ChannelSetConfiguration(DAC_CHANNEL, 0, LL_DAC_Current_CC12M, 0, LL_DAC_RefreshPeriod_30us);
    LL_DAC_ChannelEnable(DAC_CHANNEL, 1, 0);
    LL_DAC_Enable(1);

    enum LL_DAC_Status dacStatus;

    do
    {
        dacStatus = LL_DAC_GetStatus();
    }
    while ((dacStatus & LL_DAC_Status_READY0) == 0);

    LL_DAC_ChannelWriteData(DAC_CHANNEL, DAC_DATA_MIN);

    //---------------------------------------------------------------------------------------------
    // TC0: inicjalizacja timera TC0 (DMA beat transfer triggering).
    //---------------------------------------------------------------------------------------------

    const uint32_t TIMER_DMA = 0;

    NVIC_DisableIRQ(TC0_IRQn);

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_TC0, 0, 1);

    LL_TC_Reset(TIMER_DMA);
    LL_TC_Enable(TIMER_DMA, 0);
    LL_TC_SetDebugControl(TIMER_DMA, 0);
    LL_TC_Initialize(TIMER_DMA, LL_TC_TimerMode_COUNT16, LL_TC_CounterPrescaler_DIV1, LL_TC_Presynchronization_PRESC);
    LL_TC_SetWaveformMode(TIMER_DMA, LL_TC_WaweformMode_MatchFrequency);
    LL_TC_SetEventAction(TIMER_DMA, LL_TC_EventAction_OFF);
    LL_TC_SetCounterOneShot(TIMER_DMA, 0);
    LL_TC_SetCountingUp(TIMER_DMA);
    LL_TC_SetCommand(TIMER_DMA, LL_TC_Command_RETRIGGER);

    //---------------------------------------------------------------------------------------------
    // TC0: ustawienie okresu wyzwalania DMA.
    //---------------------------------------------------------------------------------------------

    const uint32_t CCx_Min = 0x0000;
    const uint32_t CCx_Max = 0xFFFF;

    const uint32_t DMA_Trigger_Frequency_Hz = 10 * 1000;
    const uint32_t Timer0_Clock_Hz = SYSTEM_CLOCK_FREQUENCY_Hz;
    const uint32_t Timer0_Prescaler = 1;
    const uint32_t Timer0_CC0 = (Timer0_Clock_Hz / Timer0_Prescaler) / DMA_Trigger_Frequency_Hz;

    assert((Timer0_CC0 > CCx_Min) && (Timer0_CC0 <= CCx_Max));

    LL_TC_SetCompareCapture(TIMER_DMA, 0, (uint16_t)Timer0_CC0);

    //---------------------------------------------------------------------------------------------
    // Inicjalizacja tablicy danych DAC.
    //---------------------------------------------------------------------------------------------
  
    // Ilość próbek napięcia (1 blok DMA).
    enum { DAC_DATA_LENGTH = 100 };

    static uint16_t DAC_Data_Array[DAC_DATA_LENGTH];

    // Tablica zawiera 1 okres sin().
    const float Sin_Amplitude = 0.99f * (DAC_DATA_MAX / 2.0f);
    const float Sin_Offset = DAC_DATA_MAX / 2.0f;

    for (uint32_t i = 0; i < DAC_DATA_LENGTH; i++)
    {
        const float angleRadians = (2.0f * (float)(M_PI) * (float)(i)) / (float)(DAC_DATA_LENGTH);

        DAC_Data_Array[i] = (uint16_t)(Sin_Amplitude * sinf(angleRadians) + Sin_Offset);
    }

    // Początkowy stan wyjścia DAC.
    LL_DAC_ChannelWriteData(DAC_CHANNEL, DAC_Data_Array[0]);

    //---------------------------------------------------------------------------------------------
    // DMAC: inicjalizacja globalna.
    //---------------------------------------------------------------------------------------------

    // Ilość kanałów DMA.
    enum { DMA_CHANNEL_NUM = 1 };

    // Indeksy kanałów DMA.
    enum { DMA_CHANNEL_DAC = 0 };

    static LL_DMAC_DESCRIPTOR_ARRAY(DMA_Descriptors, DMA_CHANNEL_NUM);
    static LL_DMAC_DESCRIPTOR_ARRAY(DMA_Descriptors_WriteBack, DMA_CHANNEL_NUM);

    NVIC_DisableIRQ(DMAC_IRQn);

    LL_DMAC_Reset();
    LL_DMAC_SetDescriptorMemoryBaseAddress(DMA_Descriptors);
    LL_DMAC_SetWriteBackMemoryBaseAddress(DMA_Descriptors_WriteBack);
    LL_DMAC_SetArbitrationMode(LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin);
    LL_DMAC_Enable(1);

    //---------------------------------------------------------------------------------------------
    // DMAC channel 0: inicjalizacja kanału DMA kopiującego dane z bufora w RAM do DAC-0.
    //---------------------------------------------------------------------------------------------

    const uint32_t DMA_CHANNEL_DAC_ADDRESS_SOURCE      = (uint32_t)( &DAC_Data_Array[DAC_DATA_LENGTH] );
    const uint32_t DMA_CHANNEL_DAC_ADDRESS_DESTINATION = (uint32_t)( &DAC->DATA[0].reg );

    LL_DMAC_Descriptor_Initialize(&DMA_Descriptors[DMA_CHANNEL_DAC],
                                  LL_DMAC_TransferBeatSize_HWORD,
                                  1, 0,
                                  LL_DMAC_TransferStepSelection_SRC,
                                  LL_DMAC_TransferStepSize_X1,
                                  LL_DMAC_TransferEventOutput_DISABLE,
                                  LL_DMAC_TransferBlockAction_NOACT);

    LL_DMAC_Descriptor_SetBlockTransferCount(&DMA_Descriptors[DMA_CHANNEL_DAC], DAC_DATA_LENGTH);
    LL_DMAC_Descriptor_SetBlockTransferAddress(&DMA_Descriptors[DMA_CHANNEL_DAC],
                                               (volatile void *)(DMA_CHANNEL_DAC_ADDRESS_SOURCE),
                                               (volatile void *)(DMA_CHANNEL_DAC_ADDRESS_DESTINATION));
    if (oneShot == true)
    {
        LL_DMAC_Descriptor_SetNextDescriptorAddress(&DMA_Descriptors[DMA_CHANNEL_DAC], 0);
    }
    else
    {
        // BANZAI: zapętlenie deskryptora.
        LL_DMAC_Descriptor_SetNextDescriptorAddress(&DMA_Descriptors[DMA_CHANNEL_DAC], &DMA_Descriptors[DMA_CHANNEL_DAC]);
    }

    LL_DMAC_Descriptor_SetValid(&DMA_Descriptors[DMA_CHANNEL_DAC], 1);

    LL_DMAC_Channel_Reset(DMA_CHANNEL_DAC);
    LL_DMAC_Channel_SetTrigger(DMA_CHANNEL_DAC, LL_DMAC_ChannelTriggerSource_TC0_MC0, LL_DMAC_ChannelTriggerAction_BEAT);
    LL_DMAC_Channel_SetEvent(DMA_CHANNEL_DAC, 0, 0, LL_DMAC_ChannelEventAction_NOACT);
    LL_DMAC_Channel_ArbitrationLevel(DMA_CHANNEL_DAC, LL_DMAC_ArbitrationLevel_LVL0);
    LL_DMAC_Channel_Enable(DMA_CHANNEL_DAC, 1, 0);

    LL_DMAC_Channel_InterruptEnableSet(DMA_CHANNEL_DAC, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);
    LL_DMAC_Channel_ClearInterruptFlag(DMA_CHANNEL_DAC, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);

    //---------------------------------------------------------------------------------------------
    // Aktywacja DMA.
    //---------------------------------------------------------------------------------------------

    // Czas martwy (rozbiegówka dla oscyloskopu).
    Timer_DelayMilliseconds(10);

    // Włączenie cyklicznego wyzwalania DMA (jedno wyzwolenie DMA = jedna aktualizacja DAC).
    LL_TC_Enable(TIMER_DMA, 1);

    //---------------------------------------------------------------------------------------------
    // Oczekiwanie na koniec transferu DMA (w teście one-shot).
    //---------------------------------------------------------------------------------------------

    LED_Set(LED_RED, 1);

    printf("DMA: start (one-shot: %u)\n", oneShot);

    enum LL_DMAC_ChannelInterruptFlag DMAC_ChannelInterruptFlag;

    do
    {
        DMAC_ChannelInterruptFlag = LL_DMAC_Channel_GetInterruptFlag(DMA_CHANNEL_DAC);
    }
    while (DMAC_ChannelInterruptFlag == 0);

    if ((DMAC_ChannelInterruptFlag & LL_DMAC_ChannelInterruptFlag_TERR) != 0)
    {
        printf("DMA: TERR\n");
    }

    if ((DMAC_ChannelInterruptFlag & LL_DMAC_ChannelInterruptFlag_SUSP) != 0)
    {
        printf("DMA: SUSP\n");
    }

    if ((DMAC_ChannelInterruptFlag & LL_DMAC_ChannelInterruptFlag_TCMPL) != 0)
    {
        printf("DMA: TCMPL\n");
    }

    LED_Set(LED_RED, 0);

    // Wyłączenie wyzwalania DMA.
    LL_TC_Enable(TIMER_DMA, 0);

    // Czas martwy (rozbiegówka dla oscyloskopu).
    Timer_DelayMilliseconds(10);

    // Wyłączenie DAC.
    LL_DAC_Enable(0);

    for (;;)
    {
    }
}
