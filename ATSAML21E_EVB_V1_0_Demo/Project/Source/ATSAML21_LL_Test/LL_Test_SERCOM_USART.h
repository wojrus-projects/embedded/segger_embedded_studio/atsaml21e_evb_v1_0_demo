#ifndef LL_TEST_SERCOM_USART_INCLUDED_H
#define LL_TEST_SERCOM_USART_INCLUDED_H

#ifdef __cplusplus
extern "C" {
#endif


void LL_Test_SERCOM_USART_Simple(void);
void LL_Test_SERCOM_USART_DMA(void);
void LL_Test_SERCOM_USART_DMA_Circular_Rx(void);
void LL_Test_SERCOM_USART_DMA_Linear_Rx(void);

void LL_Test_SERCOM_USART_InterruptHandlerUSART(void);
void LL_Test_SERCOM_USART_InterruptHandlerDMAC_1(void);
void LL_Test_SERCOM_USART_InterruptHandlerDMAC_2(void);
void LL_Test_SERCOM_USART_InterruptHandlerDMAC_3(void);


#ifdef __cplusplus
}
#endif

#endif // LL_TEST_SERCOM_USART_INCLUDED_H
