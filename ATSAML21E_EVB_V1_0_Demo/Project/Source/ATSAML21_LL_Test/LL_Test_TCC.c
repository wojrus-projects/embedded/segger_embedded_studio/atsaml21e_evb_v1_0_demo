//
// Testy LL_TCC.
//

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <assert.h>

#include "ATSAML21_LL/LL.h"
#include "Board.h"
#include "Timer.h"
#include "Keypad.h"


// Zmienna globalna dla debuggera.
uint32_t Button_Counter = 0;


//
// Function: LL_Test_TCC0_Count_Buttons
//
// Timer TCC0 pracuje jako licznik dwóch zdarzeń od dwóch przycisków.
// Button-1 zwiększa licznik o 1, Button-2 zmniejsza licznik o 1.
//
void LL_Test_TCC0_Count_Buttons(void)
{
    printf("LL_Test_TCC0_Count_Buttons\n");

    //---------------------------------------------------------------------------------------------
    // TCC0: inicjalizacja zdarzeń programowych dla dwóch przycisków.
    //---------------------------------------------------------------------------------------------

    const uint32_t EVSYS_CHANNEL_INDEX_BUTTON_1 = 0;
    const uint32_t EVSYS_CHANNEL_INDEX_BUTTON_2 = 1;

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_EVSYS_CHANNEL_0, 0, 1);

    LL_EVSYS_InitializeChannel(EVSYS_CHANNEL_INDEX_BUTTON_1, LL_EVSYS_EventGenerator_NONE, LL_EVSYS_Path_ASYNCHRONOUS, LL_EVSYS_EdgeDetection_NO_EVT_OUTPUT, 0, 0);
    LL_EVSYS_InitializeChannel(EVSYS_CHANNEL_INDEX_BUTTON_2, LL_EVSYS_EventGenerator_NONE, LL_EVSYS_Path_ASYNCHRONOUS, LL_EVSYS_EdgeDetection_NO_EVT_OUTPUT, 0, 0);
    LL_EVSYS_AssignUserToChannel(LL_EVSYS_User_TCC0_EV0, EVSYS_CHANNEL_INDEX_BUTTON_1, 1);
    LL_EVSYS_AssignUserToChannel(LL_EVSYS_User_TCC0_EV1, EVSYS_CHANNEL_INDEX_BUTTON_2, 1);

    //---------------------------------------------------------------------------------------------
    // TCC0: inicjalizacja timera w trybie licznika dwóch zdarzeń.
    //---------------------------------------------------------------------------------------------

    NVIC_DisableIRQ(TCC0_IRQn);

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_TCC0, 0, 1);

    LL_TCC_Reset(0);
    LL_TCC_Enable(0, 0);
    LL_TCC_SetRunInStandby(0, 0);
    LL_TCC_SetDebugControl(0, 0, 0);

    LL_TCC_Initialize(0, LL_TCC_CounterPrescaler_DIV1, LL_TCC_Presynchronization_PRESC, LL_TCC_DitheringResolution_NONE, 0, 0, 0);
    LL_TCC_SetEventAction(0, LL_TCC_EventInput0Action_INC, LL_TCC_EventInput1Action_DEC);
    LL_TCC_SetEventInput(0, 1, 1, 0, 0);
    LL_TCC_SetCountingUp(0);
    LL_TCC_Enable(0, 1);

    //---------------------------------------------------------------------------------------------
    // TCC0: generowanie zdarzeń od przycisków i odczyt licznika.
    //---------------------------------------------------------------------------------------------

    for (;;)
    {
        bool button1State;
        bool button2State;

        const bool button1IsPressed = Keypad_GetButtonState(Button_1, &button1State);
        const bool button2IsPressed = Keypad_GetButtonState(Button_2, &button2State);

        if (button1IsPressed == 1 && button1State == 0)
        {
            LL_TCC_SetCountingUp(0);
            LL_EVSYS_TriggerSoftwareEvent(1ul << EVSYS_CHANNEL_INDEX_BUTTON_1);
        }

        if (button2IsPressed == 1 && button2State == 0)
        {
            LL_TCC_SetCountingDown(0);
            LL_EVSYS_TriggerSoftwareEvent(1ul << EVSYS_CHANNEL_INDEX_BUTTON_2);
        }

        const uint32_t counter = LL_TCC_GetCounter(0);

        if (counter != Button_Counter)
        {
            Button_Counter = counter;

            printf("counter: %u / 0x%X\n", counter, counter);
        }
    }
}


//
// Function: LL_Test_TCC0_Counter_External
//
// Timer TCC0 pracuje w trybie prostego licznika 24-bit.
// zliczającego impulsy na pinie MCU (via EIC+EVSYS).
// Zakres częstotliwości impulsów: 10 Hz - 16,0 MHz (limit wejścia).
// Wejście licznika jest na pinie PIN_CLICK_SCL (EXTINT[7]).
//
void LL_Test_TCC0_Counter_External(void)
{
    printf("LL_Test_TCC0_Counter_External\n");

    //---------------------------------------------------------------------------------------------
    // Inicjalizacja wejścia obsługiwanego przez EIC i EVSYS.
    //---------------------------------------------------------------------------------------------

    //
    // PORT: ustawienie funkcji EXTINT[7] EIC.
    //

    const uint32_t PORT_CAPTURE_INPUT = PORT_CLICK_SCL;
    const uint32_t PIN_CAPTURE_INPUT = PIN_CLICK_SCL;

    LL_PORT_EnablePeripheralMultiplexer(PORT_CAPTURE_INPUT, PIN_CAPTURE_INPUT, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CAPTURE_INPUT, PIN_CAPTURE_INPUT, LL_PORT_PeripheralFunction_A);

    //
    // EVSYS: inicjalizacja kanału asynchronicznego EIC -> TCC0_EV0.
    //

    const uint32_t EVSYS_CHANNEL_INDEX = 0;

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_EVSYS_CHANNEL_0, 0, 1);

    LL_EVSYS_InitializeChannel(EVSYS_CHANNEL_INDEX, LL_EVSYS_EventGenerator_EIC_EXTINT7, LL_EVSYS_Path_ASYNCHRONOUS, LL_EVSYS_EdgeDetection_NO_EVT_OUTPUT, 0, 0);
    LL_EVSYS_AssignUserToChannel(LL_EVSYS_User_TCC0_EV0, EVSYS_CHANNEL_INDEX, 1);

    //
    // EIC: inicjalizacja wejścia.
    //

    const uint32_t EIC_INPUT_INDEX = 7;

    NVIC_DisableIRQ(EIC_IRQn);

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_EIC, 0, 1);

    LL_EIC_Reset();
    LL_EIC_Enable(0);
    LL_EIC_SetClock(LL_EIC_Clock_GCLK_EIC);
    LL_EIC_EnableEventOutput(1ul << EIC_INPUT_INDEX);
    LL_EIC_SetInputConfiguration(EIC_INPUT_INDEX, LL_EIC_InputSense_HIGH, 0);
    LL_EIC_SetAsynchronousEdgeDetectionMode(0);
    LL_EIC_Enable(1);

    //---------------------------------------------------------------------------------------------
    // TCC0: inicjalizacja timera w trybie licznika zdarzeń.
    //---------------------------------------------------------------------------------------------

    NVIC_DisableIRQ(TCC0_IRQn);

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_TCC0, 0, 1);

    LL_TCC_Reset(0);
    LL_TCC_Enable(0, 0);
    LL_TCC_SetRunInStandby(0, 0);
    LL_TCC_SetDebugControl(0, 0, 0);

    LL_TCC_Initialize(0, LL_TCC_CounterPrescaler_DIV1, LL_TCC_Presynchronization_PRESC, LL_TCC_DitheringResolution_NONE, 0, 0, 0);
    LL_TCC_SetEventAction(0, LL_TCC_EventInput0Action_INC, LL_TCC_EventInput1Action_OFF);
    LL_TCC_SetEventInput(0, 1, 0, 0, 0);
    LL_TCC_SetCountingUp(0);
    LL_TCC_SetCounterOneShot(0, 0);

    //---------------------------------------------------------------------------------------------
    // TCC0: włączenie licznika.
    //---------------------------------------------------------------------------------------------

    LL_TCC_Enable(0, 1);

    //---------------------------------------------------------------------------------------------
    // TCC0: odczyt licznika.
    //---------------------------------------------------------------------------------------------

    for (;;)
    {
        Timer_DelayMilliseconds(100);

        LL_TCC_SetCounter(0, 0);

        Timer_DelayMilliseconds(100);

        const uint32_t counter = LL_TCC_GetCounter(0);

        printf("%u / 0x%X\n", counter, counter);
    }
}


//
// Function: LL_Test_TCC0_PWM_Simple
//
// TCC0 pracuje jako generator PWM o częstotliwości bazowej 10 kHz.
// Wypełnienie PWM jest zwiększane o 10% w zakresie 0%-100% w nieskończonej pętli co 200 ms
// z przesunięciem 0%, +25%, +50%, +75% dla kolejnych 4 wyjść PWM0-3.
// 4 wyjścia PWM są na pinach:
//   PA04 = CLICK_MOSI = TCC0/WO[0]
//   PA05 = CLICK_SCK  = TCC0/WO[1]
//   PA18 = CLICK_PWM  = TCC0/WO[2]
//   PA11 = BTN_2      = TCC0/WO[3]
//
void LL_Test_TCC0_PWM_Simple(void)
{
    printf("LL_Test_TCC0_PWM_Simple\n");

    //---------------------------------------------------------------------------------------------
    // Inicjalizacja 4 wyjść PWM.
    //---------------------------------------------------------------------------------------------

    // TCC0/WO[0]
    const uint32_t PORT_PWM_0 = PORT_CLICK_MOSI;
    const uint32_t PIN_PWM_0 = PIN_CLICK_MOSI;

    LL_PORT_EnablePeripheralMultiplexer(PORT_PWM_0, PIN_PWM_0, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_PWM_0, PIN_PWM_0, LL_PORT_PeripheralFunction_E);

    // TCC0/WO[1]
    const uint32_t PORT_PWM_1 = PORT_CLICK_SCK;
    const uint32_t PIN_PWM_1 = PIN_CLICK_SCK;

    LL_PORT_EnablePeripheralMultiplexer(PORT_PWM_1, PIN_PWM_1, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_PWM_1, PIN_PWM_1, LL_PORT_PeripheralFunction_E);

    // TCC0/WO[2]
    const uint32_t PORT_PWM_2 = PORT_CLICK_PWM;
    const uint32_t PIN_PWM_2 = PIN_CLICK_PWM;

    LL_PORT_EnablePeripheralMultiplexer(PORT_PWM_2, PIN_PWM_2, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_PWM_2, PIN_PWM_2, LL_PORT_PeripheralFunction_F);

    // TCC0/WO[3]
    const uint32_t PORT_PWM_3 = PORT_BUTTON_2;
    const uint32_t PIN_PWM_3 = PIN_BUTTON_2;

    LL_PORT_EnablePeripheralMultiplexer(PORT_PWM_3, PIN_PWM_3, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_PWM_3, PIN_PWM_3, LL_PORT_PeripheralFunction_F);

    //---------------------------------------------------------------------------------------------
    // Inicjalizacja TCC0.
    //---------------------------------------------------------------------------------------------

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_TCC0, 0, 1);

    NVIC_DisableIRQ(TCC0_IRQn);

    LL_TCC_Reset(0);
    LL_TCC_Enable(0, 0);
    LL_TCC_SetRunInStandby(0, 0);
    LL_TCC_SetDebugControl(0, 0, 0);

    LL_TCC_Initialize(0, LL_TCC_CounterPrescaler_DIV1, LL_TCC_Presynchronization_PRESC, LL_TCC_DitheringResolution_NONE, 0, 0, 0);
    LL_TCC_SetEventAction(0, LL_TCC_EventInput0Action_OFF, LL_TCC_EventInput1Action_OFF);
    LL_TCC_SetEventInput(0, 0, 0, 0, 0);
    LL_TCC_SetCountingUp(0);
    LL_TCC_SetCounterOneShot(0, 0);

    LL_TCC_Waveform_SetGenerationOperation(0, LL_TCC_WaveformGenerationOperation_NPWM);
    LL_TCC_Waveform_SetRampOperation(0, LL_TCC_RampOperation_RAMP1);
    LL_TCC_Waveform_EnableCircularPeriod(0, 0);
    LL_TCC_Waveform_EnableCircularCompareCapture(0, 0, 0, 0, 0);
    LL_TCC_Waveform_SetChannelPolarity(0, 0, 0, 0, 0);
    LL_TCC_Waveform_SwapDTIOutputPair(0, 0, 0, 0, 0);
    LL_TCC_WaveformExtension_SetOutputMatrix(0, LL_TCC_OutputMatrix_32103210);
    LL_TCC_WaveformExtension_SetDeadTime(0, 0, 0, 0, 0, 0, 0);
    LL_TCC_SetWaveformOutputInversion(0, 0, 0, 0, 0, 0, 0, 0, 0);

    //---------------------------------------------------------------------------------------------
    // TCC0: inicjalizacja trybu PWM.
    //---------------------------------------------------------------------------------------------

    const uint32_t CCx_Min = 0x000000;
    const uint32_t CCx_Max = 0xFFFFFF;
    const uint32_t Timer_Clock_Hz = SYSTEM_CLOCK_FREQUENCY_Hz;
    const uint32_t Timer_Prescaler = 1;
    const uint32_t PWM_Frequency_Hz = 10000;
    const uint32_t PWM_Base_PER = (Timer_Clock_Hz / Timer_Prescaler) / PWM_Frequency_Hz - 1ul;

    assert((PWM_Base_PER > CCx_Min) && (PWM_Base_PER <= CCx_Max));

    LL_TCC_SetPeriod(0, PWM_Base_PER);
    LL_TCC_SetCompareCapture(0, 0, CCx_Min);
    LL_TCC_SetCompareCapture(0, 1, CCx_Min);
    LL_TCC_SetCompareCapture(0, 2, CCx_Min);
    LL_TCC_SetCompareCapture(0, 3, CCx_Min);

    //---------------------------------------------------------------------------------------------
    // Sterowanie wypełnieniem PWM.
    //---------------------------------------------------------------------------------------------

    LL_TCC_Enable(0, 1);

    for (;;)
    {
        for (uint32_t dutyPercent = 0; dutyPercent <= 100; dutyPercent += 10)
        {
            uint32_t dutyPercent0 = dutyPercent + 0;
            uint32_t dutyPercent1 = dutyPercent + 25;
            uint32_t dutyPercent2 = dutyPercent + 50;
            uint32_t dutyPercent3 = dutyPercent + 75;

            if (dutyPercent0 > 100)
            {
                dutyPercent0 -= 100;
            }

            if (dutyPercent1 > 100)
            {
                dutyPercent1 -= 100;
            }

            if (dutyPercent2 > 100)
            {
                dutyPercent2 -= 100;
            }

            if (dutyPercent3 > 100)
            {
                dutyPercent3 -= 100;
            }

            const uint32_t PWM_Duty_CC0 = (PWM_Base_PER * dutyPercent0) / 100;
            const uint32_t PWM_Duty_CC1 = (PWM_Base_PER * dutyPercent1) / 100;
            const uint32_t PWM_Duty_CC2 = (PWM_Base_PER * dutyPercent2) / 100;
            const uint32_t PWM_Duty_CC3 = (PWM_Base_PER * dutyPercent3) / 100;

            LL_TCC_SetCompareCapture(0, 0, PWM_Duty_CC0);
            LL_TCC_SetCompareCapture(0, 1, PWM_Duty_CC1);
            LL_TCC_SetCompareCapture(0, 2, PWM_Duty_CC2);
            LL_TCC_SetCompareCapture(0, 3, PWM_Duty_CC3);

            Timer_DelayMilliseconds(200);
        }
    }
}
