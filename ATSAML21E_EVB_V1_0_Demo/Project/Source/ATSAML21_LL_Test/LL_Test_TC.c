//
// Testy LL_TC.
//

#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "ATSAML21_LL/LL.h"
#include "Board.h"
#include "Timer.h"


// Timery są indeksowane wedle numerów instancji modułów HW.
#define TC4_INDEX   2


struct LL_Test_TC0_Capture_Data
{
    bool isReady;
    uint16_t CC0_PulseWidth;
    uint16_t CC1_Period;
};


static volatile struct LL_Test_TC0_Capture_Data LL_Test_TC0_Capture_DataFromInterrupt;


//
// Function: LL_Test_TC0_PWM_Simple
//
// TC0 pracuje jako generator PWM o częstotliwości bazowej 10 kHz.
// Wypełnienie PWM jest zwiększane o 10% w zakresie 0%-100% w nieskończonej pętli co 100 ms.
// Wyjście PWM jest na pinie PA23 (CLICK_SCL).
//
void LL_Test_TC0_PWM_Simple(void)
{
    printf("LL_Test_TC0_PWM_Simple\n");

    //---------------------------------------------------------------------------------------------
    // Inicjalizacja wyjścia PWM (musi być funkcja TC0/WO[1]).
    //---------------------------------------------------------------------------------------------

    const uint32_t PORT_PWM = PORT_CLICK_SCL;
    const uint32_t PIN_PWM = PIN_CLICK_SCL;

    LL_PORT_EnablePeripheralMultiplexer(PORT_PWM, PIN_PWM, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_PWM, PIN_PWM, LL_PORT_PeripheralFunction_E);

    //---------------------------------------------------------------------------------------------
    // Inicjalizacja TC0.
    //---------------------------------------------------------------------------------------------

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_TC0, 0, 1);

    LL_TC_Reset(0);
    LL_TC_SetDebugControl(0, 0);

    LL_TC_Initialize(0, LL_TC_TimerMode_COUNT16, LL_TC_CounterPrescaler_DIV1, LL_TC_Presynchronization_PRESC);
    LL_TC_SetWaveformMode(0, LL_TC_WaweformMode_MatchPWM);
    LL_TC_SetEventAction(0, LL_TC_EventAction_OFF);
    LL_TC_SetCounterOneShot(0, 0);
    LL_TC_SetCountingUp(0);
    LL_TC_SetCommand(0, LL_TC_Command_RETRIGGER);

    NVIC_DisableIRQ(TC0_IRQn);

    //---------------------------------------------------------------------------------------------
    // TC0: inicjalizacja trybu PWM.
    //---------------------------------------------------------------------------------------------

    const uint32_t CCx_Min = 0x0000;
    const uint32_t CCx_Max = 0xFFFF;
    const uint32_t Timer_Clock_Hz = SYSTEM_CLOCK_FREQUENCY_Hz;
    const uint32_t Timer_Prescaler = 1;
    const uint32_t PWM_Frequency_Hz = 10000;
    const uint32_t PWM_Base_CC0 = (Timer_Clock_Hz / Timer_Prescaler) / PWM_Frequency_Hz;

    assert((PWM_Base_CC0 > CCx_Min) && (PWM_Base_CC0 <= CCx_Max));

    LL_TC_SetCompareCapture(0, 0, (uint16_t)PWM_Base_CC0);
    LL_TC_SetCompareCapture(0, 1, CCx_Min);

    //---------------------------------------------------------------------------------------------
    // Sterowanie wypełnieniem PWM.
    //---------------------------------------------------------------------------------------------

    LL_TC_Enable(0, 1);

    for (;;)
    {
        for (uint32_t dutyPercent = 0; dutyPercent <= 100; dutyPercent += 10)
        {
            const uint16_t PWM_Duty_CC1 = (PWM_Base_CC0 * dutyPercent) / 100;

            LL_TC_SetCompareCapture(0, 1, PWM_Duty_CC1);
            Timer_DelayMilliseconds(100);
        }
    }
}


//
// Function: LL_Test_TC0_PWM_DMA
//
// TC0 pracuje jako generator PWM o częstotliwości bazowej 10 kHz.
// TC1 pracuje jako wyzwalacz pojedyńczych (beat) transferów DMA (częstotliwość 1 kHz).
// DMA co 1 ms pobiera kolejne wartości wypełnienia PWM z tablicy w RAM i zapisuje do rejestru
// CCBUF[1] w TC0 (regulacja wypełnienia PWM).
// Wyjście PWM jest na pinie PA23 (CLICK_SCL).
//
// Arguments:
//
// oneShot: Ilość testowych transferów DMA:
//   0 = nieskończone, automatyczne powtarzanie transferu z tymi samymi danymi.
//   1 = pojedyńczy transfer i koniec testu.
//
void LL_Test_TC0_PWM_DMA(bool oneShot)
{
    printf("LL_Test_TC0_PWM_DMA\n");

    const uint32_t CCx_Min = 0x0000;
    const uint32_t CCx_Max = 0xFFFF;

    //---------------------------------------------------------------------------------------------
    // Inicjalizacja wyjścia PWM (musi być funkcja TC0/WO[1]).
    //---------------------------------------------------------------------------------------------

    const uint32_t PORT_PWM = PORT_CLICK_SCL;
    const uint32_t PIN_PWM = PIN_CLICK_SCL;

    LL_PORT_EnablePeripheralMultiplexer(PORT_PWM, PIN_PWM, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_PWM, PIN_PWM, LL_PORT_PeripheralFunction_E);

    //---------------------------------------------------------------------------------------------
    // Inicjalizacja TC0 (PWM generator).
    //---------------------------------------------------------------------------------------------

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_TC0, 0, 1);

    LL_TC_Reset(0);
    LL_TC_SetDebugControl(0, 0);

    LL_TC_Initialize(0, LL_TC_TimerMode_COUNT16, LL_TC_CounterPrescaler_DIV1, LL_TC_Presynchronization_PRESC);
    LL_TC_SetWaveformMode(0, LL_TC_WaweformMode_MatchPWM);
    LL_TC_SetEventAction(0, LL_TC_EventAction_OFF);
    LL_TC_SetCounterOneShot(0, 0);
    LL_TC_SetCountingUp(0);
    LL_TC_SetCommand(0, LL_TC_Command_RETRIGGER);

    NVIC_DisableIRQ(TC0_IRQn);

    //---------------------------------------------------------------------------------------------
    // TC0: inicjalizacja trybu PWM.
    //---------------------------------------------------------------------------------------------

    const uint32_t Timer0_Clock_Hz = SYSTEM_CLOCK_FREQUENCY_Hz;
    const uint32_t Timer0_Prescaler = 1;
    const uint32_t PWM_Frequency_Hz = 10000;
    const uint32_t PWM_Base_CC0 = (Timer0_Clock_Hz / Timer0_Prescaler) / PWM_Frequency_Hz;
    const uint32_t PWM_Duty_CC0 = CCx_Min;

    assert((PWM_Base_CC0 > CCx_Min) && (PWM_Base_CC0 <= CCx_Max));

    LL_TC_SetCompareCapture(0, 0, (uint16_t)PWM_Base_CC0);
    LL_TC_SetCompareCapture(0, 1, (uint16_t)PWM_Duty_CC0);

    //---------------------------------------------------------------------------------------------
    // Inicjalizacja TC1 (DMA beat transfer triggering).
    //---------------------------------------------------------------------------------------------

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_TC1, 0, 1);

    LL_TC_Reset(1);
    LL_TC_SetDebugControl(1, 0);

    LL_TC_Initialize(1, LL_TC_TimerMode_COUNT16, LL_TC_CounterPrescaler_DIV64, LL_TC_Presynchronization_PRESC);
    LL_TC_SetWaveformMode(1, LL_TC_WaweformMode_MatchFrequency);
    LL_TC_SetEventAction(1, LL_TC_EventAction_OFF);
    LL_TC_SetCounterOneShot(1, 0);
    LL_TC_SetCountingUp(1);
    LL_TC_SetCommand(1, LL_TC_Command_RETRIGGER);

    NVIC_DisableIRQ(TC1_IRQn);

    //---------------------------------------------------------------------------------------------
    // TC1: ustawienie okresu wyzwalania DMA.
    //---------------------------------------------------------------------------------------------

    const uint32_t DMA_Trigger_Frequency_Hz = 1000;
    const uint32_t Timer1_Clock_Hz = SYSTEM_CLOCK_FREQUENCY_Hz;
    const uint32_t Timer1_Prescaler = 64;
    const uint32_t Timer1_CC0 = (Timer1_Clock_Hz / Timer1_Prescaler) / DMA_Trigger_Frequency_Hz;

    assert((Timer1_CC0 > CCx_Min) && (Timer1_CC0 <= CCx_Max));

    LL_TC_SetCompareCapture(1, 0, (uint16_t)Timer1_CC0);

    //---------------------------------------------------------------------------------------------
    // Inicjalizacja tablicy wartości wypełnienia PWM.
    //---------------------------------------------------------------------------------------------
  
    // Ilość wartości wypełnienia PWM.
    enum { DMA_DATA_LENGTH = 128 };

    static uint16_t PWM_Duty_Array[DMA_DATA_LENGTH];

    // Tablica zawiera 1 okres sin().
    const float Sin_Amplitude = 0.99f * (PWM_Base_CC0 / 2.0f);
    const float Sin_Offset = PWM_Base_CC0 / 2.0f;

    for (uint32_t i = 0; i < DMA_DATA_LENGTH; i++)
    {
        const float angleRadians = (2.0f * (float)(M_PI) * (float)(i)) / (float)(DMA_DATA_LENGTH);

        PWM_Duty_Array[i] = (uint16_t)(Sin_Amplitude * sinf(angleRadians) + Sin_Offset);
    }

    // Początkowa wartość wypełnienia PWM. 
    LL_TC_SetCompareCapture(0, 1, PWM_Duty_Array[0]);

    //---------------------------------------------------------------------------------------------
    // Inicjalizacja kanału DMA sterującego wypełnieniem PWM.
    //---------------------------------------------------------------------------------------------

    const uint32_t DMA_CHANNEL = 0;
    const uint32_t DMA_ADDRESS_SOURCE      = (uint32_t)( &PWM_Duty_Array[DMA_DATA_LENGTH] );
    const uint32_t DMA_ADDRESS_DESTINATION = (uint32_t)( &TC0->COUNT16.CCBUF[1].reg );

    static LL_DMAC_DESCRIPTOR_ARRAY(DMA_Descriptors, 1);
    static LL_DMAC_DESCRIPTOR_ARRAY(DMA_Descriptors_WriteBack, 1);

    LL_DMAC_Reset();
    LL_DMAC_SetDescriptorMemoryBaseAddress(&DMA_Descriptors[DMA_CHANNEL]);
    LL_DMAC_SetWriteBackMemoryBaseAddress(&DMA_Descriptors_WriteBack[DMA_CHANNEL]);
    LL_DMAC_SetArbitrationMode(LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin);
    LL_DMAC_Enable(1);

    LL_DMAC_Descriptor_Initialize(&DMA_Descriptors[DMA_CHANNEL],
                                  LL_DMAC_TransferBeatSize_HWORD,
                                  1, 0,
                                  LL_DMAC_TransferStepSelection_SRC,
                                  LL_DMAC_TransferStepSize_X1,
                                  LL_DMAC_TransferEventOutput_DISABLE,
                                  LL_DMAC_TransferBlockAction_NOACT);

    LL_DMAC_Descriptor_SetBlockTransferCount(&DMA_Descriptors[DMA_CHANNEL], DMA_DATA_LENGTH);
    LL_DMAC_Descriptor_SetBlockTransferAddress(&DMA_Descriptors[DMA_CHANNEL],
                                               (volatile void *)(DMA_ADDRESS_SOURCE),
                                               (volatile void *)(DMA_ADDRESS_DESTINATION));
    if (oneShot == true)
    {
        LL_DMAC_Descriptor_SetNextDescriptorAddress(&DMA_Descriptors[DMA_CHANNEL], 0);
    }
    else
    {
        // BANZAI: zapętlenie deskryptora.
        LL_DMAC_Descriptor_SetNextDescriptorAddress(&DMA_Descriptors[DMA_CHANNEL], &DMA_Descriptors[DMA_CHANNEL]);
    }

    LL_DMAC_Descriptor_SetValid(&DMA_Descriptors[DMA_CHANNEL], 1);

    LL_DMAC_Channel_Reset(DMA_CHANNEL);
    LL_DMAC_Channel_SetTrigger(DMA_CHANNEL, LL_DMAC_ChannelTriggerSource_TC1_MC0, LL_DMAC_ChannelTriggerAction_BEAT);
    LL_DMAC_Channel_SetEvent(DMA_CHANNEL, 0, 0, LL_DMAC_ChannelEventAction_NOACT);
    LL_DMAC_Channel_ArbitrationLevel(DMA_CHANNEL, LL_DMAC_ArbitrationLevel_LVL0);
    LL_DMAC_Channel_Enable(DMA_CHANNEL, 1, 0);

    LL_DMAC_Channel_InterruptEnableSet(DMA_CHANNEL, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);
    LL_DMAC_Channel_ClearInterruptFlag(DMA_CHANNEL, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);

    NVIC_DisableIRQ(DMAC_IRQn);

    //---------------------------------------------------------------------------------------------
    // Aktywacja DMA + PWM.
    //---------------------------------------------------------------------------------------------

    // Włączenie generatora PWM.
    LL_TC_Enable(0, 1);

    // Czas martwy (rozbiegówka dla oscyloskopu).
    Timer_DelayMilliseconds(10);

    // Włączenie cyklicznego wyzwalania DMA (jedno wyzwolenie DMA = jedna aktualizacja wypełnienia PWM).
    LL_TC_Enable(1, 1);

    //---------------------------------------------------------------------------------------------
    // Oczekiwanie na koniec transferu DMA (w teście one-shot).
    //---------------------------------------------------------------------------------------------

    LED_Set(LED_RED, 1);

    printf("DMA: start (one-shot: %u)\n", oneShot);

    enum LL_DMAC_ChannelInterruptFlag DMAC_ChannelInterruptFlag;

    do
    {
        DMAC_ChannelInterruptFlag = LL_DMAC_Channel_GetInterruptFlag(DMA_CHANNEL);
    }
    while (DMAC_ChannelInterruptFlag == 0);

    if ((DMAC_ChannelInterruptFlag & LL_DMAC_ChannelInterruptFlag_TERR) != 0)
    {
        printf("DMA: TERR\n");
    }

    if ((DMAC_ChannelInterruptFlag & LL_DMAC_ChannelInterruptFlag_SUSP) != 0)
    {
        printf("DMA: SUSP\n");
    }

    if ((DMAC_ChannelInterruptFlag & LL_DMAC_ChannelInterruptFlag_TCMPL) != 0)
    {
        printf("DMA: TCMPL\n");
    }

    LED_Set(LED_RED, 0);

    // Wyłączenie wyzwalania DMA.
    LL_TC_Enable(1, 0);

    // Czas martwy (rozbiegówka dla oscyloskopu).
    Timer_DelayMilliseconds(10);

    // Wyłączenie generatora PWM.
    LL_TC_Enable(0, 0);

    for (;;)
    {
    }
}


//
// Function: LL_Test_TC0_Capture_PWP
//
// Timer TC0 pracuje w trybie Capture PWP.
// Sygnał wejściowy na pinie PIN_CLICK_SCL (EXTINT[7]) ma mierzoną częstotliwość i wypełnienie.
// Zakres częstotliwości: 1 - 100 kHz.
//
void LL_Test_TC0_Capture_PWP(void)
{
    printf("LL_Test_TC0_Capture_PWP\n");

    //---------------------------------------------------------------------------------------------
    // Inicjalizacja wejścia Capture PWP obsługiwanego przez EIC i EVSYS.
    //---------------------------------------------------------------------------------------------

    //
    // PORT: ustawienie funkcji EXTINT[7] na wejściu Capture PWP.
    //

    const uint32_t PORT_CAPTURE_INPUT = PORT_CLICK_SCL;
    const uint32_t PIN_CAPTURE_INPUT = PIN_CLICK_SCL;

    LL_PORT_EnablePeripheralMultiplexer(PORT_CAPTURE_INPUT, PIN_CAPTURE_INPUT, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CAPTURE_INPUT, PIN_CAPTURE_INPUT, LL_PORT_PeripheralFunction_A);

    //
    // EVSYS: inicjalizacja kanału EVSYS dla wejścia EIC.
    //

    const uint32_t EVSYS_CHANNEL_INDEX = 0;

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_EVSYS_CHANNEL_0, 0, 1);

    // Kanał musi być asynchroniczny (detekcją zboczy zajmuje się TC0).
    LL_EVSYS_InitializeChannel(EVSYS_CHANNEL_INDEX, LL_EVSYS_EventGenerator_EIC_EXTINT7, LL_EVSYS_Path_ASYNCHRONOUS, LL_EVSYS_EdgeDetection_NO_EVT_OUTPUT, 0, 0);
    LL_EVSYS_AssignUserToChannel(LL_EVSYS_User_TC0, EVSYS_CHANNEL_INDEX, 1);

    //
    // EIC: inicjalizacja detekcji na stan (nie na zbocze!) wejścia Capture PWP.
    //

    const uint32_t EIC_INPUT_INDEX = 7;

    NVIC_DisableIRQ(EIC_IRQn);

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_EIC, 0, 1);

    LL_EIC_Reset();
    LL_EIC_Enable(0);
    LL_EIC_SetClock(LL_EIC_Clock_GCLK_EIC);
    LL_EIC_EnableEventOutput(1ul << EIC_INPUT_INDEX);
    LL_EIC_SetInputConfiguration(EIC_INPUT_INDEX, LL_EIC_InputSense_HIGH, 0);
    LL_EIC_SetAsynchronousEdgeDetectionMode(0);
    LL_EIC_Enable(1);

    //---------------------------------------------------------------------------------------------
    // TC0: inicjalizacja timera w trybie Capture PWP.
    //---------------------------------------------------------------------------------------------

    NVIC_DisableIRQ(TC0_IRQn);

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_TC0, 0, 1);

    LL_TC_Reset(0);
    LL_TC_Enable(0, 0);
    LL_TC_SetDebugControl(0, 0);

    LL_TC_Initialize(0, LL_TC_TimerMode_COUNT16, LL_TC_CounterPrescaler_DIV1, LL_TC_Presynchronization_PRESC);
    LL_TC_EnableCaptureChannel(0, 0, 1, LL_TC_CaptureMode_OnEvent);
    LL_TC_EnableCaptureChannel(0, 1, 1, LL_TC_CaptureMode_OnEvent);
    LL_TC_SetEventAction(0, LL_TC_EventAction_PWP);
    LL_TC_SetEventInput(0, 1, 0);
    LL_TC_SetEventOutput(0, LL_TC_EventOutput_Disable);
    LL_TC_SetCounterOneShot(0, 0);
    LL_TC_SetCountingUp(0);
    LL_TC_SetCommand(0, LL_TC_Command_RETRIGGER);

    LL_TC_InterruptEnableSet(0, LL_TC_InterruptFlag_ERR | LL_TC_InterruptFlag_MC1);

    NVIC_ClearPendingIRQ(TC0_IRQn);
    NVIC_SetPriority(TC0_IRQn, 0);
    NVIC_EnableIRQ(TC0_IRQn);

    //---------------------------------------------------------------------------------------------
    // TC0: włączenie obsługi Capture PWP.
    //---------------------------------------------------------------------------------------------

    LL_Test_TC0_Capture_DataFromInterrupt.isReady = false;

    LL_TC_Enable(0, 1);

    //---------------------------------------------------------------------------------------------
    // TC0: obsługa danych odczytanych z Capture PWP.
    //---------------------------------------------------------------------------------------------

    for (;;)
    {
        Timer_DelayMilliseconds(100);

        if (LL_Test_TC0_Capture_DataFromInterrupt.isReady == true)
        {
            if (LL_Test_TC0_Capture_DataFromInterrupt.CC0_PulseWidth >= LL_Test_TC0_Capture_DataFromInterrupt.CC1_Period)
            {
                printf("period: %u, pulse width: %u (invalid range)\n",
                    LL_Test_TC0_Capture_DataFromInterrupt.CC1_Period,
                    LL_Test_TC0_Capture_DataFromInterrupt.CC0_PulseWidth);
            }
            else
            {
                const uint32_t Timer0_Clock_Hz = SYSTEM_CLOCK_FREQUENCY_Hz;
                const uint32_t Timer0_Prescaler = 1;

                const float frequency_Hz = ((float)Timer0_Clock_Hz / (float)Timer0_Prescaler) / (float)LL_Test_TC0_Capture_DataFromInterrupt.CC1_Period;
                const float duty_Percent = (100.0f * (float)LL_Test_TC0_Capture_DataFromInterrupt.CC0_PulseWidth) / (float)LL_Test_TC0_Capture_DataFromInterrupt.CC1_Period;

                printf("period: %u, pulse width: %u, frequency: %f Hz, duty: %f %%\n",
                    LL_Test_TC0_Capture_DataFromInterrupt.CC1_Period,
                    LL_Test_TC0_Capture_DataFromInterrupt.CC0_PulseWidth,
                    frequency_Hz,
                    duty_Percent);
            }

            LL_Test_TC0_Capture_DataFromInterrupt.isReady = false;
        }
        else
        {
            printf("No data from TC0\n");
        }
    }
}


void LL_Test_TC0_Capture_PWP_InterruptHandler(void)
{
    const enum LL_TC_InterruptFlag interruptFlag = LL_TC_GetInterruptFlag(0);

    LL_TC_ClearInterruptFlag(0, interruptFlag);

    if (interruptFlag & LL_TC_InterruptFlag_ERR)
    {
        // Zbyt wolny odczyt CC0/1 (buffer overflow).
        printf("TC0: ERR\n");
    }

    if (interruptFlag & LL_TC_InterruptFlag_MC1)
    {
        // Rejestry CC0/1 są buforowane.
        const uint16_t pulseWidth = LL_TC_GetCompareCapture(0, 0);
        const uint16_t period = LL_TC_GetCompareCapture(0, 1);

        if (LL_Test_TC0_Capture_DataFromInterrupt.isReady == false)
        {
            LL_Test_TC0_Capture_DataFromInterrupt.CC0_PulseWidth = pulseWidth;
            LL_Test_TC0_Capture_DataFromInterrupt.CC1_Period = period;
            LL_Test_TC0_Capture_DataFromInterrupt.isReady = true;
        }
    }
}


//
// Function: LL_Test_TC0_Capture_PW
//
// Timer TC0 pracuje w trybie Capture PW w którym
// jest mierzony czas trwania impulsu dodatniego.
// Zakres czasu impulsu: 100 ns - 1,3 ms.
// Wejście jest na pinie PIN_CLICK_SCL (EXTINT[7]).
//
void LL_Test_TC0_Capture_PW(void)
{
    printf("LL_Test_TC0_Capture_PW\n");

    //---------------------------------------------------------------------------------------------
    // Inicjalizacja wejścia Capture PW obsługiwanego przez EIC i EVSYS.
    //---------------------------------------------------------------------------------------------

    //
    // PORT: ustawienie funkcji EXTINT[7] na wejściu Capture PW.
    //

    const uint32_t PORT_CAPTURE_INPUT = PORT_CLICK_SCL;
    const uint32_t PIN_CAPTURE_INPUT = PIN_CLICK_SCL;

    LL_PORT_EnablePeripheralMultiplexer(PORT_CAPTURE_INPUT, PIN_CAPTURE_INPUT, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CAPTURE_INPUT, PIN_CAPTURE_INPUT, LL_PORT_PeripheralFunction_A);

    //
    // EVSYS: inicjalizacja kanału EVSYS dla wejścia EIC.
    //

    const uint32_t EVSYS_CHANNEL_INDEX = 0;

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_EVSYS_CHANNEL_0, 0, 1);

    // Kanał musi być asynchroniczny (detekcją zboczy zajmuje się TC0).
    LL_EVSYS_InitializeChannel(EVSYS_CHANNEL_INDEX, LL_EVSYS_EventGenerator_EIC_EXTINT7, LL_EVSYS_Path_ASYNCHRONOUS, LL_EVSYS_EdgeDetection_NO_EVT_OUTPUT, 0, 0);
    LL_EVSYS_AssignUserToChannel(LL_EVSYS_User_TC0, EVSYS_CHANNEL_INDEX, 1);

    //
    // EIC: inicjalizacja detekcji na stan (nie na zbocze!) wejścia Capture PW.
    //

    const uint32_t EIC_INPUT_INDEX = 7;

    NVIC_DisableIRQ(EIC_IRQn);

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_EIC, 0, 1);

    LL_EIC_Reset();
    LL_EIC_Enable(0);
    LL_EIC_SetClock(LL_EIC_Clock_GCLK_EIC);
    LL_EIC_EnableEventOutput(1ul << EIC_INPUT_INDEX);
    LL_EIC_SetInputConfiguration(EIC_INPUT_INDEX, LL_EIC_InputSense_HIGH, 0);
    LL_EIC_SetAsynchronousEdgeDetectionMode(0);
    LL_EIC_Enable(1);

    //---------------------------------------------------------------------------------------------
    // TC0: inicjalizacja timera w trybie Capture PW.
    //---------------------------------------------------------------------------------------------

    NVIC_DisableIRQ(TC0_IRQn);

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_TC0, 0, 1);

    LL_TC_Reset(0);
    LL_TC_Enable(0, 0);
    LL_TC_SetDebugControl(0, 0);

    LL_TC_Initialize(0, LL_TC_TimerMode_COUNT16, LL_TC_CounterPrescaler_DIV1, LL_TC_Presynchronization_PRESC);
    LL_TC_EnableCaptureChannel(0, 0, 1, LL_TC_CaptureMode_OnEvent);
    LL_TC_EnableCaptureChannel(0, 1, 0, LL_TC_CaptureMode_OnEvent);
    LL_TC_SetEventAction(0, LL_TC_EventAction_PW);
    LL_TC_SetEventInput(0, 1, 0);
    LL_TC_SetEventOutput(0, LL_TC_EventOutput_Disable);
    LL_TC_SetCounterOneShot(0, 0);
    LL_TC_SetCountingUp(0);
    LL_TC_SetCommand(0, LL_TC_Command_RETRIGGER);

    LL_TC_InterruptEnableSet(0, LL_TC_InterruptFlag_ERR | LL_TC_InterruptFlag_MC0);

    NVIC_ClearPendingIRQ(TC0_IRQn);
    NVIC_SetPriority(TC0_IRQn, 0);
    NVIC_EnableIRQ(TC0_IRQn);

    //---------------------------------------------------------------------------------------------
    // TC0: włączenie obsługi Capture PW.
    //---------------------------------------------------------------------------------------------

    LL_Test_TC0_Capture_DataFromInterrupt.isReady = false;

    LL_TC_Enable(0, 1);

    //---------------------------------------------------------------------------------------------
    // TC0: obsługa danych odczytanych z Capture PW.
    //---------------------------------------------------------------------------------------------

    for (;;)
    {
        Timer_DelayMilliseconds(100);

        if (LL_Test_TC0_Capture_DataFromInterrupt.isReady == true)
        {
            const uint32_t Timer0_Clock_Hz = SYSTEM_CLOCK_FREQUENCY_Hz;
            const uint32_t Timer0_Prescaler = 1;

            const float pulseWidth_Microseconds = (1000000.0f * (float)LL_Test_TC0_Capture_DataFromInterrupt.CC0_PulseWidth) / ((float)Timer0_Clock_Hz / (float)Timer0_Prescaler);

            printf("pulse width: %u (CC0) / %f us\n",
                LL_Test_TC0_Capture_DataFromInterrupt.CC0_PulseWidth, pulseWidth_Microseconds);

            LL_Test_TC0_Capture_DataFromInterrupt.isReady = false;
        }
        else
        {
            printf("No data from TC0\n");
        }
    }
}


void LL_Test_TC0_Capture_PW_InterruptHandler(void)
{
    const enum LL_TC_InterruptFlag interruptFlag = LL_TC_GetInterruptFlag(0);

    LL_TC_ClearInterruptFlag(0, interruptFlag);

    if (interruptFlag & LL_TC_InterruptFlag_ERR)
    {
        // Zbyt wolny odczyt CC0 (buffer overflow).
        printf("TC0: ERR\n");
    }

    if (interruptFlag & LL_TC_InterruptFlag_MC0)
    {
        // Rejestr CC0 jest buforowany.
        const uint16_t pulseWidth = LL_TC_GetCompareCapture(0, 0);

        if (LL_Test_TC0_Capture_DataFromInterrupt.isReady == false)
        {
            LL_Test_TC0_Capture_DataFromInterrupt.CC0_PulseWidth = pulseWidth;
            LL_Test_TC0_Capture_DataFromInterrupt.isReady = true;
        }
    }
}


//
// Function: LL_Test_TC4_Counter_External
//
// Timer TC4 pracuje w trybie prostego licznika 16-bit.
// zliczającego impulsy na pinie MCU (via EIC+EVSYS).
// Zakres częstotliwości impulsów: 10 kHz - 16,0 MHz (limit wejścia).
// Wejście licznika jest na pinie PIN_CLICK_SCL (EXTINT[7]).
// Dodatkowo LED-2 zmienia stan po przepełnieniu licznika.
//
void LL_Test_TC4_Counter_External(void)
{
    printf("LL_Test_TC4_Counter_External\n");

    //---------------------------------------------------------------------------------------------
    // Inicjalizacja wejścia obsługiwanego przez EIC i EVSYS.
    //---------------------------------------------------------------------------------------------

    //
    // PORT: ustawienie funkcji EXTINT[7] EIC.
    //

    const uint32_t PORT_CAPTURE_INPUT = PORT_CLICK_SCL;
    const uint32_t PIN_CAPTURE_INPUT = PIN_CLICK_SCL;

    LL_PORT_EnablePeripheralMultiplexer(PORT_CAPTURE_INPUT, PIN_CAPTURE_INPUT, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CAPTURE_INPUT, PIN_CAPTURE_INPUT, LL_PORT_PeripheralFunction_A);

    //
    // EVSYS: inicjalizacja kanału asynchronicznego EIC -> TC4.
    //

    const uint32_t EVSYS_CHANNEL_INDEX = 0;

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_EVSYS_CHANNEL_0, 0, 1);

    LL_EVSYS_InitializeChannel(EVSYS_CHANNEL_INDEX, LL_EVSYS_EventGenerator_EIC_EXTINT7, LL_EVSYS_Path_ASYNCHRONOUS, LL_EVSYS_EdgeDetection_NO_EVT_OUTPUT, 0, 0);
    LL_EVSYS_AssignUserToChannel(LL_EVSYS_User_TC4, EVSYS_CHANNEL_INDEX, 1);

    //
    // EIC: inicjalizacja wejścia.
    //

    const uint32_t EIC_INPUT_INDEX = 7;

    NVIC_DisableIRQ(EIC_IRQn);

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_EIC, 0, 1);

    LL_EIC_Reset();
    LL_EIC_Enable(0);
    LL_EIC_SetClock(LL_EIC_Clock_GCLK_EIC);
    LL_EIC_EnableEventOutput(1ul << EIC_INPUT_INDEX);
    LL_EIC_SetInputConfiguration(EIC_INPUT_INDEX, LL_EIC_InputSense_HIGH, 0);
    LL_EIC_SetAsynchronousEdgeDetectionMode(0);
    LL_EIC_Enable(1);

    //---------------------------------------------------------------------------------------------
    // TC4: inicjalizacja timera w trybie licznika zdarzeń.
    //---------------------------------------------------------------------------------------------

    NVIC_DisableIRQ(TC4_IRQn);

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_TC4, 0, 1);

    LL_TC_Reset(TC4_INDEX);
    LL_TC_Enable(TC4_INDEX, 0);
    LL_TC_SetDebugControl(TC4_INDEX, 0);

    LL_TC_Initialize(TC4_INDEX, LL_TC_TimerMode_COUNT16, LL_TC_CounterPrescaler_DIV1, LL_TC_Presynchronization_PRESC);
    LL_TC_EnableCaptureChannel(TC4_INDEX, 0, 0, LL_TC_CaptureMode_OnEvent);
    LL_TC_EnableCaptureChannel(TC4_INDEX, 1, 0, LL_TC_CaptureMode_OnEvent);
    LL_TC_SetEventAction(TC4_INDEX, LL_TC_EventAction_COUNT);
    LL_TC_SetEventInput(TC4_INDEX, 1, 0);
    LL_TC_SetEventOutput(TC4_INDEX, LL_TC_EventOutput_Disable);
    LL_TC_SetCounterOneShot(TC4_INDEX, 0);
    LL_TC_SetCountingUp(TC4_INDEX);
    LL_TC_SetCommand(TC4_INDEX, LL_TC_Command_RETRIGGER);

    LL_TC_InterruptEnableSet(TC4_INDEX, LL_TC_InterruptFlag_OVF);

    NVIC_ClearPendingIRQ(TC4_IRQn);
    NVIC_SetPriority(TC4_IRQn, 0);
    NVIC_EnableIRQ(TC4_IRQn);

    //---------------------------------------------------------------------------------------------
    // TC4: włączenie licznika.
    //---------------------------------------------------------------------------------------------

    LL_TC_Enable(TC4_INDEX, 1);

    //---------------------------------------------------------------------------------------------
    // TC4: odczyt licznika.
    //---------------------------------------------------------------------------------------------

    for (;;)
    {
        Timer_DelayMilliseconds(200);

        const uint16_t counter1 = LL_TC_GetCounter(TC4_INDEX);

        Timer_DelayMilliseconds(1);

        const uint16_t counter2 = LL_TC_GetCounter(TC4_INDEX);

        const uint16_t counterDelta = counter2 - counter1;

        printf("%u\n", counterDelta);
    }
}


void LL_Test_TC4_Counter_External_InterruptHandler(void)
{
    const enum LL_TC_InterruptFlag interruptFlag = LL_TC_GetInterruptFlag(TC4_INDEX);

    LL_TC_ClearInterruptFlag(TC4_INDEX, interruptFlag);

    if (interruptFlag & LL_TC_InterruptFlag_OVF)
    {
        LED_Toggle(LED_RED);
    }
}
