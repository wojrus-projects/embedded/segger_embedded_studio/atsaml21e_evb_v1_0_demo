//
// Testy LL_ADC.
//

#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>

#include "ATSAML21_LL/LL.h"
#include "LL_Test_ADC.h"
#include "Board.h"
#include "Timer.h"


#define ADC_DATA_LENGTH_SAMPLES       128


// Globalna tablica wyników pomiarów (dla debuggera).
uint16_t LL_Test_ADC_Data_Array[ADC_DATA_LENGTH_SAMPLES];


//
// Function: LL_Test_ADC_Simple
//
// ADC wykonuje pojedyńcze pomiary w pętli co 100 ms i wyświetlana jest wartość napięcia wejściowego.
// Używane jest wejście PA02 (AIN[0]).
// Napięcie referencyjne Vref = Vcc (zmierzone).
//
// Arguments:
//
// adcResolution: Końcowa rozdzielczość ADC.
//
void LL_Test_ADC_Simple(enum LL_Test_ADC_Resolution adcResolution)
{
    printf("LL_Test_ADC_Simple\n");

    //---------------------------------------------------------------------------------------------
    // Konfiguracja wejścia analogowego ADC.
    //---------------------------------------------------------------------------------------------

    const uint32_t PORT_ADC_INPUT = PORT_CLICK_AN;
    const uint32_t PIN_ADC_INPUT = PIN_CLICK_AN;

    LL_PORT_EnablePeripheralMultiplexer(PORT_ADC_INPUT, PIN_ADC_INPUT, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_ADC_INPUT, PIN_ADC_INPUT, LL_PORT_PeripheralFunction_B);

    //---------------------------------------------------------------------------------------------
    // Konfiguracja modułu ADC.
    //---------------------------------------------------------------------------------------------

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_ADC, 0, 1);

    LL_ADC_Reset();
    LL_ADC_SetClockPrescaler(LL_ADC_ClockPrescaler_DIV2);
    LL_ADC_EnableEventInput(0, 0, 0, 0);
    LL_ADC_EnableEventOutput(0, 0);

    switch (adcResolution)
    {
        case LL_Test_ADC_Resolution_12bit:
            LL_ADC_SetMode(0, 0, 0, 0, LL_ADC_Resolution_12BIT);
            LL_ADC_SetAverage(LL_ADC_Average_1, 0);
            break;

        case LL_Test_ADC_Resolution_16bit:
            LL_ADC_SetMode(0, 0, 0, 0, LL_ADC_Resolution_16BIT);
            LL_ADC_SetAverage(LL_ADC_Average_256, 0);
            break;

        default:
            assert(0);
    }

    LL_ADC_SetInputMux(ADC_LL_NegativeInputMux_GND, ADC_LL_PositiveInputMux_AIN0);
    LL_ADC_SetReference(1, LL_ADC_Reference_INTVCC2);
    LL_ADC_SetGainCorrection(0);
    LL_ADC_SetOffsetCorrection(0);
    LL_ADC_SetSamplingTime(10, 0);
    LL_ADC_SetWindowMonitorMode(LL_ADC_WindowMonitorMode_DISABLE);

    LL_ADC_InterruptEnableSet(LL_ADC_InterruptFlag_RESRDY | LL_ADC_InterruptFlag_OVERRUN | LL_ADC_InterruptFlag_WINMON);

    NVIC_DisableIRQ(ADC_IRQn);

    //---------------------------------------------------------------------------------------------
    // Start pomiarów.
    //---------------------------------------------------------------------------------------------

    LL_ADC_Enable(1, 0, 0);

    const float V_Ref = 3.278f;
    float ADC_Range = 0.0f;

    switch (adcResolution)
    {
        case LL_Test_ADC_Resolution_12bit:
            ADC_Range = 4096.0f;
            break;

        case LL_Test_ADC_Resolution_16bit:
            ADC_Range = 65536.0f;
            break;

        default:
            assert(0);
    }

    const uint32_t TEST_MAX = 100000;

    for (uint32_t i = 0; i < TEST_MAX; i++)
    {
        enum LL_ADC_InterruptFlag interruptFlag;

        LL_ADC_SoftwareTrigger(0, 1);

        do
        {
            interruptFlag = LL_ADC_GetInterruptFlag();
        }
        while (interruptFlag == 0);

        LL_ADC_ClearInterruptFlag(interruptFlag);

        if (interruptFlag & LL_ADC_InterruptFlag_RESRDY)
        {
            const uint16_t ADC_Result = LL_ADC_GetResult();
            
            const float V_In = (V_Ref * (float)ADC_Result) / ADC_Range;

            printf("[%u] ADC: %u / 0x%X, Vin: %f [V]\n", i, ADC_Result, ADC_Result, V_In);
        }
        else
        {
            printf("ADC error\n");
        }

        Timer_DelayMilliseconds(100);
    }

    //---------------------------------------------------------------------------------------------
    // Koniec pomiarów, wyłączenie ADC, zegara GCLK i pinu wejściowego.
    //---------------------------------------------------------------------------------------------

    LL_ADC_Enable(0, 0, 0);
    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_ADC, 0, 0);
    LL_PORT_EnablePeripheralMultiplexer(PORT_ADC_INPUT, PIN_ADC_INPUT, 0);
}


//
// Function: LL_Test_ADC_DMA
//
// Konwersja w ADC jest wyzwalana przez TC0 co 1 ms.
// Po zakończeniu pojedyńczego cyklu pomiarowego ADC uruchamia pojedyńczy transfer (beat) DMA
// jednocześnie na dwóch kanałach DMA:
//  - DMA kanał nr 0 pobiera z ADC wynik pomiaru i kopiuje do bufora w RAM pod kolejnym indeksem.
//  - DMA kanał nr 1 pobiera z ADC wynik pomiaru i kopiuje bezpośrednio do DAC-1.
// Moment wyzwolenia ADC jest sygnalizowany zmianą stanu (toggle) na wyjściu LED-1.
// ADC używa wejście analogowe PA02 (AIN[0]).
// DAC-1 używa wyjście analogowe PA05 (VOUT[1]).
// Napięcie referencyjne ADC Vref = Vcc (zmierzone).
// Napięcie referencyjne DAC Vref = Vana.
//
// Arguments:
//
// adcResolution: Końcowa rozdzielczość ADC.
//
// oneShot: Ilość testowych transferów DMA:
//   0 = nieskończone, automatyczne powtarzanie transferu.
//   1 = pojedyńczy transfer i koniec testu.
//
void LL_Test_ADC_DMA(enum LL_Test_ADC_Resolution adcResolution, bool oneShot)
{
    printf("LL_Test_ADC_DMA\n");

    //---------------------------------------------------------------------------------------------
    // PORT: konfiguracja wejścia analogowego ADC.
    //---------------------------------------------------------------------------------------------

    const uint32_t PORT_ADC_INPUT = PORT_CLICK_AN;
    const uint32_t PIN_ADC_INPUT = PIN_CLICK_AN;

    LL_PORT_EnablePeripheralMultiplexer(PORT_ADC_INPUT, PIN_ADC_INPUT, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_ADC_INPUT, PIN_ADC_INPUT, LL_PORT_PeripheralFunction_B);

    //---------------------------------------------------------------------------------------------
    // PORT: konfiguracja wyjścia LED-1 sterowanego (toggle) przez event TC0_MC0.
    //---------------------------------------------------------------------------------------------

    const uint32_t PORT_EVENT_INDEX = 0;

    LL_PORT_SetEventAction(PORT_LED_1, PORT_EVENT_INDEX, 1, LL_PORT_EventAction_TGL, PIN_LED_1);

    //---------------------------------------------------------------------------------------------
    // ADC: konfiguracja modułu.
    //---------------------------------------------------------------------------------------------

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_ADC, 0, 1);

    LL_ADC_Reset();
    LL_ADC_SetClockPrescaler(LL_ADC_ClockPrescaler_DIV2);
    LL_ADC_EnableEventInput(0, 1, 0, 0);
    LL_ADC_EnableEventOutput(0, 0);

    switch (adcResolution)
    {
        case LL_Test_ADC_Resolution_12bit:
            LL_ADC_SetMode(0, 0, 0, 0, LL_ADC_Resolution_12BIT);
            LL_ADC_SetAverage(LL_ADC_Average_1, 0);
            break;

        case LL_Test_ADC_Resolution_16bit:
            LL_ADC_SetMode(0, 0, 0, 0, LL_ADC_Resolution_16BIT);
            LL_ADC_SetAverage(LL_ADC_Average_256, 4);
            break;

        default:
            assert(0);
    }

    LL_ADC_SetInputMux(ADC_LL_NegativeInputMux_GND, ADC_LL_PositiveInputMux_AIN0);
    LL_ADC_SetReference(1, LL_ADC_Reference_INTVCC2);
    LL_ADC_SetGainCorrection(0);
    LL_ADC_SetOffsetCorrection(0);
    LL_ADC_SetSamplingTime(10, 0);
    LL_ADC_SetWindowMonitorMode(LL_ADC_WindowMonitorMode_DISABLE);

    NVIC_DisableIRQ(ADC_IRQn);

    //---------------------------------------------------------------------------------------------
    // TC0: inicjalizacja timera TC0 (TC0_MC0 event trigger ADC start conversion).
    //---------------------------------------------------------------------------------------------

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_TC0, 0, 1);

    LL_TC_Reset(0);
    LL_TC_SetDebugControl(0, 0);

    LL_TC_Initialize(0, LL_TC_TimerMode_COUNT16, LL_TC_CounterPrescaler_DIV64, LL_TC_Presynchronization_PRESC);
    LL_TC_SetWaveformMode(0, LL_TC_WaweformMode_MatchFrequency);
    LL_TC_SetEventAction(0, LL_TC_EventAction_OFF);
    LL_TC_SetEventOutput(0, LL_TC_EventOutput_MCEO0);
    LL_TC_SetCounterOneShot(0, 0);
    LL_TC_SetCountingUp(0);
    LL_TC_SetCommand(0, LL_TC_Command_RETRIGGER);

    NVIC_DisableIRQ(TC0_IRQn);

    //---------------------------------------------------------------------------------------------
    // TC0: ustawienie okresu wyzwalania ADC.
    //---------------------------------------------------------------------------------------------

    const uint32_t CCx_Min = 0x0000;
    const uint32_t CCx_Max = 0xFFFF;

    const uint32_t ADC_Trigger_Frequency_Hz = 1000;
    const uint32_t Timer0_Clock_Hz = SYSTEM_CLOCK_FREQUENCY_Hz;
    const uint32_t Timer0_Prescaler = 64;
    const uint32_t Timer0_CC0 = (Timer0_Clock_Hz / Timer0_Prescaler) / ADC_Trigger_Frequency_Hz;

    assert((Timer0_CC0 > CCx_Min) && (Timer0_CC0 <= CCx_Max));

    LL_TC_SetCompareCapture(0, 0, (uint16_t)Timer0_CC0);

    //---------------------------------------------------------------------------------------------
    // EVSYS channel 0: połączenie TC0 -> ADC oraz TC0 -> PORT.
    //---------------------------------------------------------------------------------------------

    const uint32_t EVENT_CHANNEL_TC0 = 0;

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_EVSYS_CHANNEL_0, 0, 1);

    LL_EVSYS_InitializeChannel(EVENT_CHANNEL_TC0, LL_EVSYS_EventGenerator_TC0_MC0, LL_EVSYS_Path_SYNCHRONOUS, LL_EVSYS_EdgeDetection_RISING_EDGE, 0, 0);
    LL_EVSYS_AssignUserToChannel(LL_EVSYS_User_ADC_START, EVENT_CHANNEL_TC0, 1);
    LL_EVSYS_AssignUserToChannel(LL_EVSYS_User_PORT_EV0, EVENT_CHANNEL_TC0, 1);

    //---------------------------------------------------------------------------------------------
    // DMAC: inicjalizacja globalna.
    //---------------------------------------------------------------------------------------------

    // Ilość kanałów DMA.
    enum { DMA_CHANNEL_NUM = 2 };

    // Indeksy kanałów DMA.
    enum { DMA_CHANNEL_ADC = 0, DMA_CHANNEL_DAC = 1 };

    static LL_DMAC_DESCRIPTOR_ARRAY(DMA_Descriptors, DMA_CHANNEL_NUM);
    static LL_DMAC_DESCRIPTOR_ARRAY(DMA_Descriptors_WriteBack, DMA_CHANNEL_NUM);

    LL_DMAC_Reset();
    LL_DMAC_SetDescriptorMemoryBaseAddress(DMA_Descriptors);
    LL_DMAC_SetWriteBackMemoryBaseAddress(DMA_Descriptors_WriteBack);
    LL_DMAC_SetArbitrationMode(LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin);
    LL_DMAC_Enable(1);

    NVIC_DisableIRQ(DMAC_IRQn);

    //---------------------------------------------------------------------------------------------
    // DMAC channel 0: inicjalizacja kanału DMA kopiującego dane z ADC do bufora w RAM.
    //---------------------------------------------------------------------------------------------

    const uint32_t DMA_CHANNEL_ADC_ADDRESS_SOURCE      = (uint32_t)( &ADC->RESULT.reg );
    const uint32_t DMA_CHANNEL_ADC_ADDRESS_DESTINATION = (uint32_t)( &LL_Test_ADC_Data_Array[ADC_DATA_LENGTH_SAMPLES] );

    LL_DMAC_Descriptor_Initialize(&DMA_Descriptors[DMA_CHANNEL_ADC],
                                  LL_DMAC_TransferBeatSize_HWORD,
                                  0, 1,
                                  LL_DMAC_TransferStepSelection_DST,
                                  LL_DMAC_TransferStepSize_X1,
                                  LL_DMAC_TransferEventOutput_DISABLE,
                                  LL_DMAC_TransferBlockAction_NOACT);

    LL_DMAC_Descriptor_SetBlockTransferCount(&DMA_Descriptors[DMA_CHANNEL_ADC], ADC_DATA_LENGTH_SAMPLES);
    LL_DMAC_Descriptor_SetBlockTransferAddress(&DMA_Descriptors[DMA_CHANNEL_ADC],
                                               (volatile void *)(DMA_CHANNEL_ADC_ADDRESS_SOURCE),
                                               (volatile void *)(DMA_CHANNEL_ADC_ADDRESS_DESTINATION));
    if (oneShot == true)
    {
        LL_DMAC_Descriptor_SetNextDescriptorAddress(&DMA_Descriptors[DMA_CHANNEL_ADC], 0);
    }
    else
    {
        // BANZAI: zapętlenie deskryptora.
        LL_DMAC_Descriptor_SetNextDescriptorAddress(&DMA_Descriptors[DMA_CHANNEL_ADC], &DMA_Descriptors[DMA_CHANNEL_ADC]);
    }

    LL_DMAC_Descriptor_SetValid(&DMA_Descriptors[DMA_CHANNEL_ADC], 1);

    LL_DMAC_Channel_Reset(DMA_CHANNEL_ADC);
    LL_DMAC_Channel_SetTrigger(DMA_CHANNEL_ADC, LL_DMAC_ChannelTriggerSource_ADC_RESRDY, LL_DMAC_ChannelTriggerAction_BEAT);
    LL_DMAC_Channel_SetEvent(DMA_CHANNEL_ADC, 0, 0, LL_DMAC_ChannelEventAction_NOACT);
    LL_DMAC_Channel_ArbitrationLevel(DMA_CHANNEL_ADC, LL_DMAC_ArbitrationLevel_LVL0);
    LL_DMAC_Channel_Enable(DMA_CHANNEL_ADC, 1, 0);

    LL_DMAC_Channel_InterruptEnableSet(DMA_CHANNEL_ADC, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);
    LL_DMAC_Channel_ClearInterruptFlag(DMA_CHANNEL_ADC, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);

    //---------------------------------------------------------------------------------------------
    // DMAC channel 1: inicjalizacja kanału DMA kopiującego dane z ADC bezpośrednio do DAC-1.
    //---------------------------------------------------------------------------------------------

    const uint32_t DMA_CHANNEL_DAC_ADDRESS_SOURCE      = (uint32_t)( &ADC->RESULT.reg );
    const uint32_t DMA_CHANNEL_DAC_ADDRESS_DESTINATION = (uint32_t)( &DAC->DATA[1].reg );

    LL_DMAC_Descriptor_Initialize(&DMA_Descriptors[DMA_CHANNEL_DAC],
                                  LL_DMAC_TransferBeatSize_HWORD,
                                  0, 0,
                                  LL_DMAC_TransferStepSelection_DST,
                                  LL_DMAC_TransferStepSize_X1,
                                  LL_DMAC_TransferEventOutput_DISABLE,
                                  LL_DMAC_TransferBlockAction_NOACT);

    LL_DMAC_Descriptor_SetBlockTransferCount(&DMA_Descriptors[DMA_CHANNEL_DAC], ADC_DATA_LENGTH_SAMPLES);
    LL_DMAC_Descriptor_SetBlockTransferAddress(&DMA_Descriptors[DMA_CHANNEL_DAC],
                                               (volatile void *)(DMA_CHANNEL_DAC_ADDRESS_SOURCE),
                                               (volatile void *)(DMA_CHANNEL_DAC_ADDRESS_DESTINATION));
    if (oneShot == true)
    {
        LL_DMAC_Descriptor_SetNextDescriptorAddress(&DMA_Descriptors[DMA_CHANNEL_DAC], 0);
    }
    else
    {
        // BANZAI: zapętlenie deskryptora.
        LL_DMAC_Descriptor_SetNextDescriptorAddress(&DMA_Descriptors[DMA_CHANNEL_DAC], &DMA_Descriptors[DMA_CHANNEL_DAC]);
    }

    LL_DMAC_Descriptor_SetValid(&DMA_Descriptors[DMA_CHANNEL_DAC], 1);

    LL_DMAC_Channel_Reset(DMA_CHANNEL_DAC);
    LL_DMAC_Channel_SetTrigger(DMA_CHANNEL_DAC, LL_DMAC_ChannelTriggerSource_ADC_RESRDY, LL_DMAC_ChannelTriggerAction_BEAT);
    LL_DMAC_Channel_SetEvent(DMA_CHANNEL_DAC, 0, 0, LL_DMAC_ChannelEventAction_NOACT);
    LL_DMAC_Channel_ArbitrationLevel(DMA_CHANNEL_DAC, LL_DMAC_ArbitrationLevel_LVL0);
    LL_DMAC_Channel_Enable(DMA_CHANNEL_DAC, 1, 0);

    LL_DMAC_Channel_InterruptEnableSet(DMA_CHANNEL_DAC, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);
    LL_DMAC_Channel_ClearInterruptFlag(DMA_CHANNEL_DAC, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);

    //---------------------------------------------------------------------------------------------
    // PORT: konfiguracja wyjścia analogowego DAC-1.
    //---------------------------------------------------------------------------------------------

    LL_PORT_EnablePeripheralMultiplexer(PORT_DAC_1, PIN_DAC_1, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_DAC_1, PIN_DAC_1, LL_PORT_PeripheralFunction_B);

    //---------------------------------------------------------------------------------------------
    // DAC-1: inicjalizacja konwertera.
    //---------------------------------------------------------------------------------------------

    const uint32_t DAC_CHANNEL = 1;

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_DAC, 0, 1);

    LL_DAC_Reset();
    LL_DAC_Enable(0);
    LL_DAC_SetReferenceVoltage(LL_DAC_ReferenceVoltage_VDDANA);
    LL_DAC_ChannelSetConfiguration(DAC_CHANNEL, 0, LL_DAC_Current_CC12M, 0, LL_DAC_RefreshPeriod_30us);
    LL_DAC_ChannelEnable(DAC_CHANNEL, 1, 0);
    LL_DAC_Enable(1);

    enum LL_DAC_Status dacStatus;

    do
    {
        dacStatus = LL_DAC_GetStatus();
    }
    while ((dacStatus & LL_DAC_Status_READY1) == 0);

    NVIC_DisableIRQ(DAC_IRQn);

    LL_DAC_ChannelWriteData(DAC_CHANNEL, 0);

    //---------------------------------------------------------------------------------------------
    // Start pomiarów.
    //---------------------------------------------------------------------------------------------

    // Czas martwy (rozbiegówka dla oscyloskopu).
    Timer_DelayMilliseconds(10);

    // Włączenie ADC.
    LL_ADC_Enable(1, 0, 0);

    // Włączenie cyklicznego wyzwalania ADC przez TC0.
    LL_TC_Enable(0, 1);

    //---------------------------------------------------------------------------------------------
    // Oczekiwanie na koniec transferu DMA (w teście one-shot).
    //---------------------------------------------------------------------------------------------

    LED_Set(LED_RED, 1);

    printf("DMA: start (one-shot: %u)\n", oneShot);

    enum LL_DMAC_ChannelInterruptFlag DMAC_ChannelInterruptFlag;

    do
    {
        DMAC_ChannelInterruptFlag = LL_DMAC_Channel_GetInterruptFlag(DMA_CHANNEL_ADC);
    }
    while (DMAC_ChannelInterruptFlag == 0);

    if ((DMAC_ChannelInterruptFlag & LL_DMAC_ChannelInterruptFlag_TERR) != 0)
    {
        printf("DMA: TERR\n");
    }

    if ((DMAC_ChannelInterruptFlag & LL_DMAC_ChannelInterruptFlag_SUSP) != 0)
    {
        printf("DMA: SUSP\n");
    }

    if ((DMAC_ChannelInterruptFlag & LL_DMAC_ChannelInterruptFlag_TCMPL) != 0)
    {
        printf("DMA: TCMPL\n");
    }

    LED_Set(LED_RED, 0);

    // Wyłączenie wyzwalania ADC.
    LL_TC_Enable(0, 0);

    // Wyłączenie ADC.
    LL_ADC_Enable(0, 0, 0);

    // Czas martwy (rozbiegówka dla oscyloskopu).
    Timer_DelayMilliseconds(10);

    // Wyłączenie DAC.
    LL_DAC_Enable(0);

    for (;;)
    {
    }
}
