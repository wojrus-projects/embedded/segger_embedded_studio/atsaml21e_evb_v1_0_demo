#ifndef LL_TEST_SERCOM_I2C_INCLUDED_H
#define LL_TEST_SERCOM_I2C_INCLUDED_H

#ifdef __cplusplus
extern "C" {
#endif


void LL_Test_SERCOM_I2C_Master_Simple(void);


#ifdef __cplusplus
}
#endif

#endif // LL_TEST_SERCOM_I2C_INCLUDED_H
