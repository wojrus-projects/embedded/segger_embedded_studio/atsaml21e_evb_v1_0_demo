//
// Testy LL_SERCOM_USART.
//

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "ATSAML21_LL/LL.h"
#include "Board.h"
#include "Timer.h"
#include "Keypad.h"


// Ilość kanałów DMA.
enum { DMA_CHANNEL_NUM = 2 };

// Indeksy kanałów DMA.
enum { DMA_CHANNEL_USART_RXD = 0, DMA_CHANNEL_USART_TXD = 1 };

// Globalne deskryptory DMA (dla debuggera).
LL_DMAC_DESCRIPTOR_ARRAY(DMA_Descriptors, DMA_CHANNEL_NUM);
LL_DMAC_DESCRIPTOR_ARRAY(DMA_Descriptors_WriteBack, DMA_CHANNEL_NUM);

// Rozmiar buforów DMA Rx/Tx w bajtach.
#define DMA_BUFFER_SIZE     8

// Globalne bufory DMA (dla debuggera).
uint8_t USART_Rx_Buffer_0[DMA_BUFFER_SIZE];
uint8_t USART_Rx_Buffer_1[DMA_BUFFER_SIZE];
uint8_t USART_Tx_Buffer[DMA_BUFFER_SIZE];


//
// Function: LL_Test_SERCOM_USART_Simple
//
// USART w SERCOM-1 pracuje w trybie polling i realizuje loop-back
// (dane Rx są od razu odsyłane).
//
void LL_Test_SERCOM_USART_Simple(void)
{
    printf("LL_Test_SERCOM_USART_Simple\n");

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: konfiguracja pinów USART.
    //---------------------------------------------------------------------------------------------

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_TX, PIN_CLICK_TX, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_TX, PIN_CLICK_TX, LL_PORT_PeripheralFunction_C);

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_RX, PIN_CLICK_RX, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_RX, PIN_CLICK_RX, LL_PORT_PeripheralFunction_C);

    LL_PORT_EnablePeripheralMultiplexer(PORT_UART_RTS, PIN_UART_RTS, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_UART_RTS, PIN_UART_RTS, LL_PORT_PeripheralFunction_C);

    LL_PORT_EnablePeripheralMultiplexer(PORT_UART_CTS, PIN_UART_CTS, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_UART_CTS, PIN_UART_CTS, LL_PORT_PeripheralFunction_C);

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: obliczenie baud rate.
    //---------------------------------------------------------------------------------------------

    const uint32_t baudSet = 9600;
    const uint32_t f_ref = SYSTEM_CLOCK_FREQUENCY_Hz;
    const uint32_t samplesPerBit = 8;
    const uint32_t baudRegister = 65536ull - (65536ull * (uint64_t)samplesPerBit * (uint64_t)baudSet) / (uint64_t)f_ref;

    assert((baudRegister > 0x0000) && (baudRegister <= 0xFFFF));

    const uint32_t baudFromRegister = (f_ref / samplesPerBit) - (((uint64_t)f_ref * (uint64_t)baudRegister) / ((uint64_t)samplesPerBit * 65536ull));
    const int32_t baudErrorDelta = baudSet - baudFromRegister;
    const float baudErrorPercent = (100.0f * (float)baudErrorDelta) / (float)baudSet;

    printf("BAUD register: %u / 0x%X\n", baudRegister, baudRegister);
    printf("Baud set: %u bps -> baud actual: %u bps\n", baudSet, baudFromRegister);
    printf("Error: %d bps / %f %%\n", baudErrorDelta, baudErrorPercent);

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: konfiguracja USART.
    //---------------------------------------------------------------------------------------------

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_SERCOM1_CORE, 0, 1);

    NVIC_DisableIRQ(SERCOM1_IRQn);
    
    LL_SERCOM_USART_Reset(1);
    LL_SERCOM_USART_Enable(1, 0);
    LL_SERCOM_USART_SetRunInStandby(1, 0);
    LL_SERCOM_USART_SetDebugControl(1, 0);

    LL_SERCOM_USART_SetPins(1, LL_SERCOM_USART_TransmitDataPinout_TxD0_RTS2_CTS3, LL_SERCOM_USART_ReceiveDataPinout_RxD1);
    LL_SERCOM_USART_SetFrameFormat(1, LL_SERCOM_USART_FrameFormat_USART);
    LL_SERCOM_USART_SetCommunicationMode(1, LL_SERCOM_USART_CommunicationMode_Asynchronous);
    LL_SERCOM_USART_SetClockMode(1, LL_SERCOM_USART_ClockMode_InternalClock);
    LL_SERCOM_USART_SetDataOrder(1, LL_SERCOM_USART_DataOrder_FirstLSB);
    LL_SERCOM_USART_SetBaud(1, (uint16_t)baudRegister);
    LL_SERCOM_USART_SetSampleRate(1, LL_SERCOM_USART_SampleRate_8x_Arithmetic);
    LL_SERCOM_USART_SetSampleAdjustment(1, LL_SERCOM_USART_SampleAdjustment_8x_3_4_5);
    LL_SERCOM_USART_SetCharacterSize(1, LL_SERCOM_USART_CharacterSize_8BIT);
    LL_SERCOM_USART_SetStopBit(1, LL_SERCOM_USART_StopBit_1bit);
    LL_SERCOM_USART_SetParity(1, LL_SERCOM_USART_Parity_Even);
    LL_SERCOM_USART_SetEncoding(1, LL_SERCOM_USART_Encoding_Disabled);
    LL_SERCOM_USART_SetCollisionDetection(1, 0);
    LL_SERCOM_USART_SetStartFrameDetection(1, 0);
    LL_SERCOM_USART_SetImmediateBufferOverflowNotification(1, 0);

    LL_SERCOM_USART_InterruptEnableSet(1, LL_SERCOM_USART_InterruptFlag_DRE
                                        | LL_SERCOM_USART_InterruptFlag_TXC
                                        | LL_SERCOM_USART_InterruptFlag_RXC
                                        | LL_SERCOM_USART_InterruptFlag_RXS
                                        | LL_SERCOM_USART_InterruptFlag_CTSIC
                                        | LL_SERCOM_USART_InterruptFlag_RXBRK
                                        | LL_SERCOM_USART_InterruptFlag_ERROR);

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: włączenie modułu USART oraz odbiornika i nadajnika.
    //---------------------------------------------------------------------------------------------

    LL_SERCOM_USART_EnableReceiver(1, 1);
    LL_SERCOM_USART_EnableTransmitter(1, 1);
    LL_SERCOM_USART_Enable(1, 1);

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: test loop-back.
    //---------------------------------------------------------------------------------------------

    uint16_t rxData;
    bool isRxData = false;
    uint32_t rxDataCounter = 0;

    for (;;)
    {
        const enum LL_SERCOM_USART_InterruptFlag interruptFlag = LL_SERCOM_USART_GetInterruptFlag(1);

        if (interruptFlag != 0)
        {
            LL_SERCOM_USART_ClearInterruptFlag(1, interruptFlag);

            if (interruptFlag & LL_SERCOM_USART_InterruptFlag_DRE)
            {
                if (isRxData == true)
                {
                    LL_SERCOM_USART_WriteData(1, rxData);
                    isRxData = false;

                    printf("[%u] Tx: '%c' / %u / 0x%X\n",
                        rxDataCounter, rxData, rxData, rxData);
                }
            }

            if (interruptFlag & LL_SERCOM_USART_InterruptFlag_TXC)
            {
                printf("USART: TXC\n");
            }

            if (interruptFlag & LL_SERCOM_USART_InterruptFlag_RXC)
            {
                rxData = LL_SERCOM_USART_ReadData(1);
                isRxData = true;
                rxDataCounter++;

                printf("[%u] Rx: '%c' / %u / 0x%X\n",
                    rxDataCounter, rxData, rxData, rxData);
            }

            if (interruptFlag & LL_SERCOM_USART_InterruptFlag_RXS)
            {
                printf("USART: RXS\n");
            }

            if (interruptFlag & LL_SERCOM_USART_InterruptFlag_CTSIC)
            {
                printf("USART: CTSIC\n");
            }

            if (interruptFlag & LL_SERCOM_USART_InterruptFlag_RXBRK)
            {
                printf("USART: RXBRK\n");
            }

            if (interruptFlag & LL_SERCOM_USART_InterruptFlag_ERROR)
            {
                printf("USART: ERROR\n");
            }
        }
    }
}


//
// Function: LL_Test_SERCOM_USART_DMA
//
// USART w SERCOM-1 wyzwala DMA (Rx i Tx) i realizuje loop-back blokami po 8 znaków.
//
void LL_Test_SERCOM_USART_DMA(void)
{
    printf("LL_Test_SERCOM_USART_DMA\n");

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: konfiguracja pinów USART.
    //---------------------------------------------------------------------------------------------

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_TX, PIN_CLICK_TX, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_TX, PIN_CLICK_TX, LL_PORT_PeripheralFunction_C);

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_RX, PIN_CLICK_RX, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_RX, PIN_CLICK_RX, LL_PORT_PeripheralFunction_C);

    LL_PORT_EnablePeripheralMultiplexer(PORT_UART_RTS, PIN_UART_RTS, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_UART_RTS, PIN_UART_RTS, LL_PORT_PeripheralFunction_C);

    LL_PORT_EnablePeripheralMultiplexer(PORT_UART_CTS, PIN_UART_CTS, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_UART_CTS, PIN_UART_CTS, LL_PORT_PeripheralFunction_C);

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: obliczenie baud rate.
    //---------------------------------------------------------------------------------------------

    const uint32_t baudSet = 9600;
    const uint32_t f_ref = SYSTEM_CLOCK_FREQUENCY_Hz;
    const uint32_t samplesPerBit = 8;
    const uint32_t baudRegister = 65536ull - (65536ull * (uint64_t)samplesPerBit * (uint64_t)baudSet) / (uint64_t)f_ref;

    assert((baudRegister > 0x0000) && (baudRegister <= 0xFFFF));

    const uint32_t baudFromRegister = (f_ref / samplesPerBit) - (((uint64_t)f_ref * (uint64_t)baudRegister) / ((uint64_t)samplesPerBit * 65536ull));
    const int32_t baudErrorDelta = baudSet - baudFromRegister;
    const float baudErrorPercent = (100.0f * (float)baudErrorDelta) / (float)baudSet;

    printf("BAUD register: %u / 0x%X\n", baudRegister, baudRegister);
    printf("Baud set: %u bps -> baud actual: %u bps\n", baudSet, baudFromRegister);
    printf("Error: %d bps / %f %%\n", baudErrorDelta, baudErrorPercent);

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: konfiguracja USART.
    //---------------------------------------------------------------------------------------------

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_SERCOM1_CORE, 0, 1);

    NVIC_DisableIRQ(SERCOM1_IRQn);
    
    LL_SERCOM_USART_Reset(1);
    LL_SERCOM_USART_Enable(1, 0);
    LL_SERCOM_USART_SetRunInStandby(1, 0);
    LL_SERCOM_USART_SetDebugControl(1, 0);

    LL_SERCOM_USART_SetPins(1, LL_SERCOM_USART_TransmitDataPinout_TxD0_RTS2_CTS3, LL_SERCOM_USART_ReceiveDataPinout_RxD1);
    LL_SERCOM_USART_SetFrameFormat(1, LL_SERCOM_USART_FrameFormat_USART);
    LL_SERCOM_USART_SetCommunicationMode(1, LL_SERCOM_USART_CommunicationMode_Asynchronous);
    LL_SERCOM_USART_SetClockMode(1, LL_SERCOM_USART_ClockMode_InternalClock);
    LL_SERCOM_USART_SetDataOrder(1, LL_SERCOM_USART_DataOrder_FirstLSB);
    LL_SERCOM_USART_SetBaud(1, (uint16_t)baudRegister);
    LL_SERCOM_USART_SetSampleRate(1, LL_SERCOM_USART_SampleRate_8x_Arithmetic);
    LL_SERCOM_USART_SetSampleAdjustment(1, LL_SERCOM_USART_SampleAdjustment_8x_3_4_5);
    LL_SERCOM_USART_SetCharacterSize(1, LL_SERCOM_USART_CharacterSize_8BIT);
    LL_SERCOM_USART_SetStopBit(1, LL_SERCOM_USART_StopBit_1bit);
    LL_SERCOM_USART_SetParity(1, LL_SERCOM_USART_Parity_Even);
    LL_SERCOM_USART_SetEncoding(1, LL_SERCOM_USART_Encoding_Disabled);
    LL_SERCOM_USART_SetCollisionDetection(1, 0);
    LL_SERCOM_USART_SetStartFrameDetection(1, 0);
    LL_SERCOM_USART_SetImmediateBufferOverflowNotification(1, 0);

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: włączenie przerwań.
    //---------------------------------------------------------------------------------------------

    LL_SERCOM_USART_InterruptEnableSet(1, LL_SERCOM_USART_InterruptFlag_CTSIC
                                        | LL_SERCOM_USART_InterruptFlag_RXBRK
                                        | LL_SERCOM_USART_InterruptFlag_ERROR);

    NVIC_ClearPendingIRQ(SERCOM1_IRQn);
    NVIC_SetPriority(SERCOM1_IRQn, 0);
    NVIC_EnableIRQ(SERCOM1_IRQn);

    //---------------------------------------------------------------------------------------------
    // DMAC: inicjalizacja globalna.
    //---------------------------------------------------------------------------------------------

    NVIC_DisableIRQ(DMAC_IRQn);

    LL_DMAC_Reset();
    LL_DMAC_SetDescriptorMemoryBaseAddress(DMA_Descriptors);
    LL_DMAC_SetWriteBackMemoryBaseAddress(DMA_Descriptors_WriteBack);
    LL_DMAC_SetArbitrationMode(LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin);
    LL_DMAC_Enable(1);

    //---------------------------------------------------------------------------------------------
    // DMAC channel 0: inicjalizacja kanału DMA kopiującego dane z rej. USART-RX do bufora w RAM.
    //---------------------------------------------------------------------------------------------

    const uint32_t DMA_CHANNEL_USART_RXD_ADDRESS_SOURCE      = (uint32_t)( &SERCOM1->USART.DATA.reg );
    const uint32_t DMA_CHANNEL_USART_RXD_ADDRESS_DESTINATION = (uint32_t)( &USART_Rx_Buffer_0[DMA_BUFFER_SIZE] );

    LL_DMAC_Descriptor_Initialize(&DMA_Descriptors[DMA_CHANNEL_USART_RXD],
                                  LL_DMAC_TransferBeatSize_BYTE,
                                  0, 1,
                                  LL_DMAC_TransferStepSelection_SRC,
                                  LL_DMAC_TransferStepSize_X1,
                                  LL_DMAC_TransferEventOutput_DISABLE,
                                  LL_DMAC_TransferBlockAction_NOACT);

    LL_DMAC_Descriptor_SetBlockTransferCount(&DMA_Descriptors[DMA_CHANNEL_USART_RXD], DMA_BUFFER_SIZE);
    LL_DMAC_Descriptor_SetBlockTransferAddress(&DMA_Descriptors[DMA_CHANNEL_USART_RXD],
                                               (volatile void *)(DMA_CHANNEL_USART_RXD_ADDRESS_SOURCE),
                                               (volatile void *)(DMA_CHANNEL_USART_RXD_ADDRESS_DESTINATION));
    LL_DMAC_Descriptor_SetNextDescriptorAddress(&DMA_Descriptors[DMA_CHANNEL_USART_RXD], 0);
    LL_DMAC_Descriptor_SetValid(&DMA_Descriptors[DMA_CHANNEL_USART_RXD], 1);

    LL_DMAC_Channel_Reset(DMA_CHANNEL_USART_RXD);
    LL_DMAC_Channel_SetTrigger(DMA_CHANNEL_USART_RXD, LL_DMAC_ChannelTriggerSource_SERCOM1_RX, LL_DMAC_ChannelTriggerAction_BEAT);
    LL_DMAC_Channel_SetEvent(DMA_CHANNEL_USART_RXD, 0, 0, LL_DMAC_ChannelEventAction_NOACT);
    LL_DMAC_Channel_ArbitrationLevel(DMA_CHANNEL_USART_RXD, LL_DMAC_ArbitrationLevel_LVL0);

    LL_DMAC_Channel_InterruptEnableSet(DMA_CHANNEL_USART_RXD, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);
    LL_DMAC_Channel_ClearInterruptFlag(DMA_CHANNEL_USART_RXD, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);

    //---------------------------------------------------------------------------------------------
    // DMAC channel 1: inicjalizacja kanału DMA kopiującego dane z bufora w RAM do rej. USART-TX.
    //---------------------------------------------------------------------------------------------

    const uint32_t DMA_CHANNEL_USART_TXD_ADDRESS_SOURCE      = (uint32_t)( &USART_Tx_Buffer[DMA_BUFFER_SIZE] );
    const uint32_t DMA_CHANNEL_USART_TXD_ADDRESS_DESTINATION = (uint32_t)( &SERCOM1->USART.DATA.reg );

    LL_DMAC_Descriptor_Initialize(&DMA_Descriptors[DMA_CHANNEL_USART_TXD],
                                  LL_DMAC_TransferBeatSize_BYTE,
                                  1, 0,
                                  LL_DMAC_TransferStepSelection_SRC,
                                  LL_DMAC_TransferStepSize_X1,
                                  LL_DMAC_TransferEventOutput_DISABLE,
                                  LL_DMAC_TransferBlockAction_NOACT);

    LL_DMAC_Descriptor_SetBlockTransferCount(&DMA_Descriptors[DMA_CHANNEL_USART_TXD], DMA_BUFFER_SIZE);
    LL_DMAC_Descriptor_SetBlockTransferAddress(&DMA_Descriptors[DMA_CHANNEL_USART_TXD],
                                               (volatile void *)(DMA_CHANNEL_USART_TXD_ADDRESS_SOURCE),
                                               (volatile void *)(DMA_CHANNEL_USART_TXD_ADDRESS_DESTINATION));
    LL_DMAC_Descriptor_SetNextDescriptorAddress(&DMA_Descriptors[DMA_CHANNEL_USART_TXD], 0);
    LL_DMAC_Descriptor_SetValid(&DMA_Descriptors[DMA_CHANNEL_USART_TXD], 1);

    LL_DMAC_Channel_Reset(DMA_CHANNEL_USART_TXD);
    LL_DMAC_Channel_SetTrigger(DMA_CHANNEL_USART_TXD, LL_DMAC_ChannelTriggerSource_SERCOM1_TX, LL_DMAC_ChannelTriggerAction_BEAT);
    LL_DMAC_Channel_SetEvent(DMA_CHANNEL_USART_TXD, 0, 0, LL_DMAC_ChannelEventAction_NOACT);
    LL_DMAC_Channel_ArbitrationLevel(DMA_CHANNEL_USART_TXD, LL_DMAC_ArbitrationLevel_LVL0);

    LL_DMAC_Channel_InterruptEnableSet(DMA_CHANNEL_USART_TXD, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);
    LL_DMAC_Channel_ClearInterruptFlag(DMA_CHANNEL_USART_TXD, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);

    //---------------------------------------------------------------------------------------------
    // DMAC: włączenie przerwań.
    //---------------------------------------------------------------------------------------------

    NVIC_ClearPendingIRQ(DMAC_IRQn);
    NVIC_SetPriority(DMAC_IRQn, 0);
    NVIC_EnableIRQ(DMAC_IRQn);

    //---------------------------------------------------------------------------------------------
    // Włączenie DMA Rx oraz USART Rx+Tx.
    //---------------------------------------------------------------------------------------------

    printf("Start USART DMA transfers (Rx/Tx buffers size: %u byte(s))\n", DMA_BUFFER_SIZE);

    // Włączenie kanału DMA Rx.
    // Kanał Tx będzie włączony gdy będą przygotowane dane loop-back.
    LL_DMAC_Channel_Enable(DMA_CHANNEL_USART_RXD, 1, 0);

    LL_SERCOM_USART_EnableReceiver(1, 1);
    LL_SERCOM_USART_EnableTransmitter(1, 1);
    LL_SERCOM_USART_Enable(1, 1);

    for (;;)
    {
    }
}


void LL_Test_SERCOM_USART_InterruptHandlerUSART(void)
{
    const enum LL_SERCOM_USART_InterruptFlag interruptFlag = LL_SERCOM_USART_GetInterruptFlag(1);

    LL_SERCOM_USART_ClearInterruptFlag(1, interruptFlag);

    if (interruptFlag & LL_SERCOM_USART_InterruptFlag_DRE)
    {
        printf("USART: DRE\n");
    }

    if (interruptFlag & LL_SERCOM_USART_InterruptFlag_TXC)
    {
        printf("USART: TXC\n");
    }

    if (interruptFlag & LL_SERCOM_USART_InterruptFlag_RXC)
    {
        printf("USART: RXC\n");

        const uint16_t rxData = LL_SERCOM_USART_ReadData(1);

        printf("Rx: '%c' / %u / 0x%X\n",
            rxData, rxData, rxData);
    }

    if (interruptFlag & LL_SERCOM_USART_InterruptFlag_RXS)
    {
        printf("USART: RXS\n");
    }

    if (interruptFlag & LL_SERCOM_USART_InterruptFlag_CTSIC)
    {
        printf("USART: CTSIC\n");
    }

    if (interruptFlag & LL_SERCOM_USART_InterruptFlag_RXBRK)
    {
        printf("USART: RXBRK\n");
    }

    if (interruptFlag & LL_SERCOM_USART_InterruptFlag_ERROR)
    {
        printf("USART: ERROR\n");
    }
}


void LL_Test_SERCOM_USART_InterruptHandlerDMAC_1(void)
{
    const uint32_t pendingChannelBitmap = LL_DMAC_GetInterruptStatus();

    //---------------------------------------------------------------------------------------------
    // DMAC channel 0: USART Rx.
    //---------------------------------------------------------------------------------------------

    if (pendingChannelBitmap & (1ul << DMA_CHANNEL_USART_RXD))
    {
        const enum LL_DMAC_ChannelInterruptFlag interruptFlag = LL_DMAC_Channel_GetInterruptFlag(DMA_CHANNEL_USART_RXD);

        LL_DMAC_Channel_ClearInterruptFlag(DMA_CHANNEL_USART_RXD, interruptFlag);

        printf("DMA channel [%u] interrupt flags (Rx): 0x%X\n", DMA_CHANNEL_USART_RXD, interruptFlag);

        if (interruptFlag & LL_DMAC_ChannelInterruptFlag_TERR)
        {
            printf("[%u] TERR\n", DMA_CHANNEL_USART_RXD);
        }

        if (interruptFlag & LL_DMAC_ChannelInterruptFlag_TCMPL)
        {
            printf("[%u] TCMPL\n", DMA_CHANNEL_USART_RXD);

            printf("Rx data: '");

            for (uint32_t i = 0; i < DMA_BUFFER_SIZE; i++)
            {
                // Loop-back.
                USART_Tx_Buffer[i] = USART_Rx_Buffer_0[i];

                printf("%c", USART_Rx_Buffer_0[i]);
            }

            printf("'\n");

            // Restart Rx.
            LL_DMAC_Channel_Enable(DMA_CHANNEL_USART_RXD, 1, 0);

            // Odesłanie danych Rx w jednym bloku (loop-back).
            LL_DMAC_Channel_Enable(DMA_CHANNEL_USART_TXD, 1, 0);
        }

        if (interruptFlag & LL_DMAC_ChannelInterruptFlag_SUSP)
        {
            printf("[%u] SUSP\n", DMA_CHANNEL_USART_RXD);
        }
    }

    //---------------------------------------------------------------------------------------------
    // DMAC channel 1: USART Tx.
    //---------------------------------------------------------------------------------------------

    if (pendingChannelBitmap & (1ul << DMA_CHANNEL_USART_TXD))
    {
        const enum LL_DMAC_ChannelInterruptFlag interruptFlag = LL_DMAC_Channel_GetInterruptFlag(DMA_CHANNEL_USART_TXD);

        LL_DMAC_Channel_ClearInterruptFlag(DMA_CHANNEL_USART_TXD, interruptFlag);

        printf("DMA channel [%u] interrupt flags (Tx): 0x%X\n", DMA_CHANNEL_USART_TXD, interruptFlag);

        if (interruptFlag & LL_DMAC_ChannelInterruptFlag_TERR)
        {
            printf("[%u] TERR\n", DMA_CHANNEL_USART_TXD);
        }

        if (interruptFlag & LL_DMAC_ChannelInterruptFlag_TCMPL)
        {
            printf("[%u] TCMPL\n", DMA_CHANNEL_USART_TXD);
        }

        if (interruptFlag & LL_DMAC_ChannelInterruptFlag_SUSP)
        {
            printf("[%u] SUSP\n", DMA_CHANNEL_USART_TXD);
        }
    }
}


//
// Function: LL_Test_SERCOM_USART_DMA_Circular_Rx
//
// USART w SERCOM-1 wyzwala DMA (Rx only) i realizuje odbiór ciągły na dwóch zapętlonych buforach Rx.
// Gdy jeden z buforów zapełni się to DMA wykonuje suspend i zgłasza przerwania TCMPL oraz SUSP.
// Naciśnięcie Button-1 w dowolnej chwili wykonuje DMA suspend i zgłasza przerwanie SUSP.
//
void LL_Test_SERCOM_USART_DMA_Circular_Rx(void)
{
    printf("LL_Test_SERCOM_USART_DMA_Circular_Rx\n");

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: konfiguracja pinów USART.
    //---------------------------------------------------------------------------------------------

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_TX, PIN_CLICK_TX, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_TX, PIN_CLICK_TX, LL_PORT_PeripheralFunction_C);

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_RX, PIN_CLICK_RX, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_RX, PIN_CLICK_RX, LL_PORT_PeripheralFunction_C);

    LL_PORT_EnablePeripheralMultiplexer(PORT_UART_RTS, PIN_UART_RTS, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_UART_RTS, PIN_UART_RTS, LL_PORT_PeripheralFunction_C);

    LL_PORT_EnablePeripheralMultiplexer(PORT_UART_CTS, PIN_UART_CTS, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_UART_CTS, PIN_UART_CTS, LL_PORT_PeripheralFunction_C);

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: obliczenie baud rate.
    //---------------------------------------------------------------------------------------------

    const uint32_t baudSet = 9600;
    const uint32_t f_ref = SYSTEM_CLOCK_FREQUENCY_Hz;
    const uint32_t samplesPerBit = 8;
    const uint32_t baudRegister = 65536ull - (65536ull * (uint64_t)samplesPerBit * (uint64_t)baudSet) / (uint64_t)f_ref;

    assert((baudRegister > 0x0000) && (baudRegister <= 0xFFFF));

    const uint32_t baudFromRegister = (f_ref / samplesPerBit) - (((uint64_t)f_ref * (uint64_t)baudRegister) / ((uint64_t)samplesPerBit * 65536ull));
    const int32_t baudErrorDelta = baudSet - baudFromRegister;
    const float baudErrorPercent = (100.0f * (float)baudErrorDelta) / (float)baudSet;

    printf("BAUD register: %u / 0x%X\n", baudRegister, baudRegister);
    printf("Baud set: %u bps -> baud actual: %u bps\n", baudSet, baudFromRegister);
    printf("Error: %d bps / %f %%\n", baudErrorDelta, baudErrorPercent);

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: konfiguracja USART.
    //---------------------------------------------------------------------------------------------

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_SERCOM1_CORE, 0, 1);

    NVIC_DisableIRQ(SERCOM1_IRQn);
    
    LL_SERCOM_USART_Reset(1);
    LL_SERCOM_USART_Enable(1, 0);
    LL_SERCOM_USART_SetRunInStandby(1, 0);
    LL_SERCOM_USART_SetDebugControl(1, 0);

    LL_SERCOM_USART_SetPins(1, LL_SERCOM_USART_TransmitDataPinout_TxD0_RTS2_CTS3, LL_SERCOM_USART_ReceiveDataPinout_RxD1);
    LL_SERCOM_USART_SetFrameFormat(1, LL_SERCOM_USART_FrameFormat_USART);
    LL_SERCOM_USART_SetCommunicationMode(1, LL_SERCOM_USART_CommunicationMode_Asynchronous);
    LL_SERCOM_USART_SetClockMode(1, LL_SERCOM_USART_ClockMode_InternalClock);
    LL_SERCOM_USART_SetDataOrder(1, LL_SERCOM_USART_DataOrder_FirstLSB);
    LL_SERCOM_USART_SetBaud(1, (uint16_t)baudRegister);
    LL_SERCOM_USART_SetSampleRate(1, LL_SERCOM_USART_SampleRate_8x_Arithmetic);
    LL_SERCOM_USART_SetSampleAdjustment(1, LL_SERCOM_USART_SampleAdjustment_8x_3_4_5);
    LL_SERCOM_USART_SetCharacterSize(1, LL_SERCOM_USART_CharacterSize_8BIT);
    LL_SERCOM_USART_SetStopBit(1, LL_SERCOM_USART_StopBit_1bit);
    LL_SERCOM_USART_SetParity(1, LL_SERCOM_USART_Parity_Even);
    LL_SERCOM_USART_SetEncoding(1, LL_SERCOM_USART_Encoding_Disabled);
    LL_SERCOM_USART_SetCollisionDetection(1, 0);
    LL_SERCOM_USART_SetStartFrameDetection(1, 0);
    LL_SERCOM_USART_SetImmediateBufferOverflowNotification(1, 0);

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: włączenie przerwań.
    //---------------------------------------------------------------------------------------------

    LL_SERCOM_USART_InterruptEnableSet(1, LL_SERCOM_USART_InterruptFlag_CTSIC
                                        | LL_SERCOM_USART_InterruptFlag_RXBRK
                                        | LL_SERCOM_USART_InterruptFlag_ERROR);

    NVIC_ClearPendingIRQ(SERCOM1_IRQn);
    NVIC_SetPriority(SERCOM1_IRQn, 0);
    NVIC_EnableIRQ(SERCOM1_IRQn);

    //---------------------------------------------------------------------------------------------
    // DMAC: inicjalizacja globalna.
    //---------------------------------------------------------------------------------------------

    NVIC_DisableIRQ(DMAC_IRQn);

    LL_DMAC_Reset();
    LL_DMAC_SetDescriptorMemoryBaseAddress(DMA_Descriptors);
    LL_DMAC_SetWriteBackMemoryBaseAddress(DMA_Descriptors_WriteBack);
    LL_DMAC_SetArbitrationMode(LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin);
    LL_DMAC_Enable(1);

    //---------------------------------------------------------------------------------------------
    // DMAC channel 0: inicjalizacja kanału DMA kopiującego dane z rej. USART-RX do bufora w RAM.
    //---------------------------------------------------------------------------------------------

    const uint32_t DMA_CHANNEL_USART_RXD_ADDRESS_SOURCE        = (uint32_t)( &SERCOM1->USART.DATA.reg );
    const uint32_t DMA_CHANNEL_USART_RXD_ADDRESS_DESTINATION_0 = (uint32_t)( &USART_Rx_Buffer_0[DMA_BUFFER_SIZE] );
    const uint32_t DMA_CHANNEL_USART_RXD_ADDRESS_DESTINATION_1 = (uint32_t)( &USART_Rx_Buffer_1[DMA_BUFFER_SIZE] );

    //
    // Inicjalizacja deskryptora DMA (bufor nr 0, jest połączony z buforem nr 1).
    //

    LL_DMAC_Descriptor_Initialize(&DMA_Descriptors[0],
                                  LL_DMAC_TransferBeatSize_BYTE,
                                  0, 1,
                                  LL_DMAC_TransferStepSelection_SRC,
                                  LL_DMAC_TransferStepSize_X1,
                                  LL_DMAC_TransferEventOutput_DISABLE,
                                  LL_DMAC_TransferBlockAction_BOTH);

    LL_DMAC_Descriptor_SetBlockTransferCount(&DMA_Descriptors[0], DMA_BUFFER_SIZE);
    LL_DMAC_Descriptor_SetBlockTransferAddress(&DMA_Descriptors[0],
                                               (volatile void *)(DMA_CHANNEL_USART_RXD_ADDRESS_SOURCE),
                                               (volatile void *)(DMA_CHANNEL_USART_RXD_ADDRESS_DESTINATION_0));
    LL_DMAC_Descriptor_SetNextDescriptorAddress(&DMA_Descriptors[0], &DMA_Descriptors[1]);
    LL_DMAC_Descriptor_SetValid(&DMA_Descriptors[0], 1);

    //
    // Inicjalizacja deskryptora DMA (bufor nr 1, jest połączony z buforem nr 0).
    //

    LL_DMAC_Descriptor_Initialize(&DMA_Descriptors[1],
                                  LL_DMAC_TransferBeatSize_BYTE,
                                  0, 1,
                                  LL_DMAC_TransferStepSelection_SRC,
                                  LL_DMAC_TransferStepSize_X1,
                                  LL_DMAC_TransferEventOutput_DISABLE,
                                  LL_DMAC_TransferBlockAction_BOTH);

    LL_DMAC_Descriptor_SetBlockTransferCount(&DMA_Descriptors[1], DMA_BUFFER_SIZE);
    LL_DMAC_Descriptor_SetBlockTransferAddress(&DMA_Descriptors[1],
                                               (volatile void *)(DMA_CHANNEL_USART_RXD_ADDRESS_SOURCE),
                                               (volatile void *)(DMA_CHANNEL_USART_RXD_ADDRESS_DESTINATION_1));
    LL_DMAC_Descriptor_SetNextDescriptorAddress(&DMA_Descriptors[1], &DMA_Descriptors[0]);
    LL_DMAC_Descriptor_SetValid(&DMA_Descriptors[1], 1);

    //
    // Inicjalizacja kanału DMA.
    //

    LL_DMAC_Channel_Reset(DMA_CHANNEL_USART_RXD);
    LL_DMAC_Channel_SetTrigger(DMA_CHANNEL_USART_RXD, LL_DMAC_ChannelTriggerSource_SERCOM1_RX, LL_DMAC_ChannelTriggerAction_BEAT);
    LL_DMAC_Channel_SetEvent(DMA_CHANNEL_USART_RXD, 0, 1, LL_DMAC_ChannelEventAction_SUSPEND);
    LL_DMAC_Channel_ArbitrationLevel(DMA_CHANNEL_USART_RXD, LL_DMAC_ArbitrationLevel_LVL0);

    LL_DMAC_Channel_InterruptEnableSet(DMA_CHANNEL_USART_RXD, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);
    LL_DMAC_Channel_ClearInterruptFlag(DMA_CHANNEL_USART_RXD, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);

    //---------------------------------------------------------------------------------------------
    // DMAC: włączenie przerwań.
    //---------------------------------------------------------------------------------------------

    NVIC_ClearPendingIRQ(DMAC_IRQn);
    NVIC_SetPriority(DMAC_IRQn, 0);
    NVIC_EnableIRQ(DMAC_IRQn);

    //---------------------------------------------------------------------------------------------
    // EVSYS: inicjalizacja zdarzenia programowego do ręcznego sterowania kanałem DMA.
    //---------------------------------------------------------------------------------------------

    const uint32_t EVSYS_CHANNEL_INDEX_BUTTON_1 = 0;

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_EVSYS_CHANNEL_0, 0, 1);

    LL_EVSYS_InitializeChannel(EVSYS_CHANNEL_INDEX_BUTTON_1, LL_EVSYS_EventGenerator_NONE, LL_EVSYS_Path_ASYNCHRONOUS, LL_EVSYS_EdgeDetection_NO_EVT_OUTPUT, 0, 0);
    LL_EVSYS_AssignUserToChannel(LL_EVSYS_User_DMAC_CH0, EVSYS_CHANNEL_INDEX_BUTTON_1, 1);

    //---------------------------------------------------------------------------------------------
    // Włączenie DMA Rx oraz USART Rx.
    //---------------------------------------------------------------------------------------------

    printf("Start USART DMA transfers (Rx buffers size: %u byte(s))\n", DMA_BUFFER_SIZE);

    // Włączenie kanału DMA Rx.
    LL_DMAC_Channel_Enable(DMA_CHANNEL_USART_RXD, 1, 0);

    LL_SERCOM_USART_EnableReceiver(1, 1);
    LL_SERCOM_USART_Enable(1, 1);

    //---------------------------------------------------------------------------------------------
    // Ręczne generowanie DMA suspend w dowolnej chwili.
    //---------------------------------------------------------------------------------------------

    for (;;)
    {
        bool button1State;

        const bool button1IsPressed = Keypad_GetButtonState(Button_1, &button1State);

        if (button1IsPressed == 1 && button1State == 0)
        {
            LL_EVSYS_TriggerSoftwareEvent(1ul << EVSYS_CHANNEL_INDEX_BUTTON_1);
            printf("Button-1 trigger suspend DMA channel\n");
        }
    }
}


void LL_Test_SERCOM_USART_InterruptHandlerDMAC_2(void)
{
    const uint32_t pendingChannelBitmap = LL_DMAC_GetInterruptStatus();

    //---------------------------------------------------------------------------------------------
    // DMAC channel 0: USART Rx.
    //---------------------------------------------------------------------------------------------

    if (pendingChannelBitmap & (1ul << DMA_CHANNEL_USART_RXD))
    {
        const enum LL_DMAC_ChannelInterruptFlag interruptFlag = LL_DMAC_Channel_GetInterruptFlag(DMA_CHANNEL_USART_RXD);

        LL_DMAC_Channel_ClearInterruptFlag(DMA_CHANNEL_USART_RXD, interruptFlag);

        printf("DMA channel [%u] interrupt flags (Rx): 0x%X\n", DMA_CHANNEL_USART_RXD, interruptFlag);

        //
        // Flaga TERR.
        //

        if (interruptFlag & LL_DMAC_ChannelInterruptFlag_TERR)
        {
            printf("[%u] TERR\n", DMA_CHANNEL_USART_RXD);
        }

        //
        // Flaga TCMPL.
        //

        if (interruptFlag & LL_DMAC_ChannelInterruptFlag_TCMPL)
        {
            printf("[%u] TCMPL\n", DMA_CHANNEL_USART_RXD);

            printf("VALID: 0x%X\n", DMA_Descriptors_WriteBack[0].BTCTRL.bit.VALID);
            printf("BTCNT: 0x%X\n", DMA_Descriptors_WriteBack[0].BTCNT.reg);
            printf("DSTADDR: 0x%X\n", DMA_Descriptors_WriteBack[0].DSTADDR.reg);
        }

        //
        // Flaga SUSP.
        //

        if (interruptFlag & LL_DMAC_ChannelInterruptFlag_SUSP)
        {
            printf("[%u] SUSP\n", DMA_CHANNEL_USART_RXD);

            printf("VALID: 0x%X\n", DMA_Descriptors_WriteBack[0].BTCTRL.bit.VALID);
            printf("BTCNT: 0x%X\n", DMA_Descriptors_WriteBack[0].BTCNT.reg);
            printf("DSTADDR: 0x%X\n", DMA_Descriptors_WriteBack[0].DSTADDR.reg);

            if (DMA_Descriptors_WriteBack[0].BTCTRL.bit.VALID == 0)
            {
                printf("Suspend source: transfer completion\n");
            }
            else
            {
                printf("Suspend source: EVSYS action\n");
            }

            const uint8_t * currentBufferAddress = (uint8_t *)(DMA_Descriptors_WriteBack[0].DSTADDR.reg - DMA_BUFFER_SIZE);
            const uint32_t currentDataLength = DMA_BUFFER_SIZE - DMA_Descriptors_WriteBack[0].BTCNT.reg;

            uint32_t bufferIndex = 0;

            if (currentBufferAddress == &USART_Rx_Buffer_0[0])
            {
                bufferIndex = 0;
            }
            else if (currentBufferAddress == &USART_Rx_Buffer_1[0])
            {
                bufferIndex = 1;
            }
            else
            {
                printf("Unknown buffer\n");
                return;
            }

            printf("Buffer index: %u, data length: %u byte(s)\n", bufferIndex, currentDataLength);

            printf("Rx data: '");

            for (uint32_t i = 0; i < currentDataLength; i++)
            {
                printf("%c", currentBufferAddress[i]);
            }

            printf("'\n");

            // Kontynuacja Rx.
            LL_DMAC_Channel_SetCommand(DMA_CHANNEL_USART_RXD, LL_DMAC_ChannelCommand_RESUME);
        }
    }
}


//
// Function: LL_Test_SERCOM_USART_DMA_Linear_Rx
//
// USART w SERCOM-1 wyzwala DMA (Rx only) i realizuje odbiór do jednego bufora Rx.
// Gdy bufor zapełni się to DMA wykonuje suspend i zgłasza przerwania TCMPL oraz SUSP.
// Naciśnięcie Button-1 w dowolnej chwili wykonuje DMA suspend i zgłasza przerwanie SUSP.
//
void LL_Test_SERCOM_USART_DMA_Linear_Rx(void)
{
    printf("LL_Test_SERCOM_USART_DMA_Linear_Rx\n");

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: konfiguracja pinów USART.
    //---------------------------------------------------------------------------------------------

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_TX, PIN_CLICK_TX, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_TX, PIN_CLICK_TX, LL_PORT_PeripheralFunction_C);

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_RX, PIN_CLICK_RX, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_RX, PIN_CLICK_RX, LL_PORT_PeripheralFunction_C);

    LL_PORT_EnablePeripheralMultiplexer(PORT_UART_RTS, PIN_UART_RTS, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_UART_RTS, PIN_UART_RTS, LL_PORT_PeripheralFunction_C);

    LL_PORT_EnablePeripheralMultiplexer(PORT_UART_CTS, PIN_UART_CTS, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_UART_CTS, PIN_UART_CTS, LL_PORT_PeripheralFunction_C);

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: obliczenie baud rate.
    //---------------------------------------------------------------------------------------------

    const uint32_t baudSet = 9600;
    const uint32_t f_ref = SYSTEM_CLOCK_FREQUENCY_Hz;
    const uint32_t samplesPerBit = 8;
    const uint32_t baudRegister = 65536ull - (65536ull * (uint64_t)samplesPerBit * (uint64_t)baudSet) / (uint64_t)f_ref;

    assert((baudRegister > 0x0000) && (baudRegister <= 0xFFFF));

    const uint32_t baudFromRegister = (f_ref / samplesPerBit) - (((uint64_t)f_ref * (uint64_t)baudRegister) / ((uint64_t)samplesPerBit * 65536ull));
    const int32_t baudErrorDelta = baudSet - baudFromRegister;
    const float baudErrorPercent = (100.0f * (float)baudErrorDelta) / (float)baudSet;

    printf("BAUD register: %u / 0x%X\n", baudRegister, baudRegister);
    printf("Baud set: %u bps -> baud actual: %u bps\n", baudSet, baudFromRegister);
    printf("Error: %d bps / %f %%\n", baudErrorDelta, baudErrorPercent);

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: konfiguracja USART.
    //---------------------------------------------------------------------------------------------

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_SERCOM1_CORE, 0, 1);

    NVIC_DisableIRQ(SERCOM1_IRQn);
    
    LL_SERCOM_USART_Reset(1);
    LL_SERCOM_USART_Enable(1, 0);
    LL_SERCOM_USART_SetRunInStandby(1, 0);
    LL_SERCOM_USART_SetDebugControl(1, 0);

    LL_SERCOM_USART_SetPins(1, LL_SERCOM_USART_TransmitDataPinout_TxD0_RTS2_CTS3, LL_SERCOM_USART_ReceiveDataPinout_RxD1);
    LL_SERCOM_USART_SetFrameFormat(1, LL_SERCOM_USART_FrameFormat_USART);
    LL_SERCOM_USART_SetCommunicationMode(1, LL_SERCOM_USART_CommunicationMode_Asynchronous);
    LL_SERCOM_USART_SetClockMode(1, LL_SERCOM_USART_ClockMode_InternalClock);
    LL_SERCOM_USART_SetDataOrder(1, LL_SERCOM_USART_DataOrder_FirstLSB);
    LL_SERCOM_USART_SetBaud(1, (uint16_t)baudRegister);
    LL_SERCOM_USART_SetSampleRate(1, LL_SERCOM_USART_SampleRate_8x_Arithmetic);
    LL_SERCOM_USART_SetSampleAdjustment(1, LL_SERCOM_USART_SampleAdjustment_8x_3_4_5);
    LL_SERCOM_USART_SetCharacterSize(1, LL_SERCOM_USART_CharacterSize_8BIT);
    LL_SERCOM_USART_SetStopBit(1, LL_SERCOM_USART_StopBit_1bit);
    LL_SERCOM_USART_SetParity(1, LL_SERCOM_USART_Parity_Even);
    LL_SERCOM_USART_SetEncoding(1, LL_SERCOM_USART_Encoding_Disabled);
    LL_SERCOM_USART_SetCollisionDetection(1, 0);
    LL_SERCOM_USART_SetStartFrameDetection(1, 0);
    LL_SERCOM_USART_SetImmediateBufferOverflowNotification(1, 0);

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: włączenie przerwań.
    //---------------------------------------------------------------------------------------------

    LL_SERCOM_USART_InterruptEnableSet(1, LL_SERCOM_USART_InterruptFlag_CTSIC
                                        | LL_SERCOM_USART_InterruptFlag_RXBRK
                                        | LL_SERCOM_USART_InterruptFlag_ERROR);

    NVIC_ClearPendingIRQ(SERCOM1_IRQn);
    NVIC_SetPriority(SERCOM1_IRQn, 0);
    NVIC_EnableIRQ(SERCOM1_IRQn);

    //---------------------------------------------------------------------------------------------
    // DMAC: inicjalizacja globalna.
    //---------------------------------------------------------------------------------------------

    NVIC_DisableIRQ(DMAC_IRQn);

    LL_DMAC_Reset();
    LL_DMAC_SetDescriptorMemoryBaseAddress(DMA_Descriptors);
    LL_DMAC_SetWriteBackMemoryBaseAddress(DMA_Descriptors_WriteBack);
    LL_DMAC_SetArbitrationMode(LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin);
    LL_DMAC_Enable(1);

    //---------------------------------------------------------------------------------------------
    // DMAC channel 0: inicjalizacja kanału DMA kopiującego dane z rej. USART-RX do bufora w RAM.
    //---------------------------------------------------------------------------------------------

    const uint32_t DMA_CHANNEL_USART_RXD_ADDRESS_SOURCE      = (uint32_t)( &SERCOM1->USART.DATA.reg );
    const uint32_t DMA_CHANNEL_USART_RXD_ADDRESS_DESTINATION = (uint32_t)( &USART_Rx_Buffer_0[DMA_BUFFER_SIZE] );

    //
    // Inicjalizacja deskryptora DMA.
    //

    LL_DMAC_Descriptor_Initialize(&DMA_Descriptors[0],
                                  LL_DMAC_TransferBeatSize_BYTE,
                                  0, 1,
                                  LL_DMAC_TransferStepSelection_SRC,
                                  LL_DMAC_TransferStepSize_X1,
                                  LL_DMAC_TransferEventOutput_DISABLE,
                                  LL_DMAC_TransferBlockAction_BOTH);

    LL_DMAC_Descriptor_SetBlockTransferCount(&DMA_Descriptors[0], DMA_BUFFER_SIZE);
    LL_DMAC_Descriptor_SetBlockTransferAddress(&DMA_Descriptors[0],
                                               (volatile void *)(DMA_CHANNEL_USART_RXD_ADDRESS_SOURCE),
                                               (volatile void *)(DMA_CHANNEL_USART_RXD_ADDRESS_DESTINATION));
    LL_DMAC_Descriptor_SetNextDescriptorAddress(&DMA_Descriptors[0], &DMA_Descriptors[0]);
    LL_DMAC_Descriptor_SetValid(&DMA_Descriptors[0], 1);

    //
    // Inicjalizacja kanału DMA.
    //

    LL_DMAC_Channel_Reset(DMA_CHANNEL_USART_RXD);
    LL_DMAC_Channel_SetTrigger(DMA_CHANNEL_USART_RXD, LL_DMAC_ChannelTriggerSource_SERCOM1_RX, LL_DMAC_ChannelTriggerAction_BEAT);
    LL_DMAC_Channel_SetEvent(DMA_CHANNEL_USART_RXD, 0, 1, LL_DMAC_ChannelEventAction_SUSPEND);
    LL_DMAC_Channel_ArbitrationLevel(DMA_CHANNEL_USART_RXD, LL_DMAC_ArbitrationLevel_LVL0);

    LL_DMAC_Channel_InterruptEnableSet(DMA_CHANNEL_USART_RXD, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);
    LL_DMAC_Channel_ClearInterruptFlag(DMA_CHANNEL_USART_RXD, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);

    //---------------------------------------------------------------------------------------------
    // DMAC: włączenie przerwań.
    //---------------------------------------------------------------------------------------------

    NVIC_ClearPendingIRQ(DMAC_IRQn);
    NVIC_SetPriority(DMAC_IRQn, 0);
    NVIC_EnableIRQ(DMAC_IRQn);

    //---------------------------------------------------------------------------------------------
    // EVSYS: inicjalizacja zdarzenia programowego do ręcznego sterowania kanałem DMA.
    //---------------------------------------------------------------------------------------------

    const uint32_t EVSYS_CHANNEL_INDEX_BUTTON_1 = 0;

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_EVSYS_CHANNEL_0, 0, 1);

    LL_EVSYS_InitializeChannel(EVSYS_CHANNEL_INDEX_BUTTON_1, LL_EVSYS_EventGenerator_NONE, LL_EVSYS_Path_ASYNCHRONOUS, LL_EVSYS_EdgeDetection_NO_EVT_OUTPUT, 0, 0);
    LL_EVSYS_AssignUserToChannel(LL_EVSYS_User_DMAC_CH0, EVSYS_CHANNEL_INDEX_BUTTON_1, 1);

    //---------------------------------------------------------------------------------------------
    // Włączenie DMA Rx oraz USART Rx.
    //---------------------------------------------------------------------------------------------

    printf("Start USART DMA transfers (Rx buffer size: %u byte(s))\n", DMA_BUFFER_SIZE);

    // Włączenie kanału DMA Rx.
    LL_DMAC_Channel_Enable(DMA_CHANNEL_USART_RXD, 1, 0);

    LL_SERCOM_USART_EnableReceiver(1, 1);
    LL_SERCOM_USART_Enable(1, 1);

    //---------------------------------------------------------------------------------------------
    // Ręczne generowanie DMA suspend w dowolnej chwili.
    //---------------------------------------------------------------------------------------------

    for (;;)
    {
        bool button1State;

        const bool button1IsPressed = Keypad_GetButtonState(Button_1, &button1State);

        if (button1IsPressed == 1 && button1State == 0)
        {
            LL_EVSYS_TriggerSoftwareEvent(1ul << EVSYS_CHANNEL_INDEX_BUTTON_1);
            printf("Button-1 trigger suspend DMA channel\n");
        }
    }
}


void LL_Test_SERCOM_USART_InterruptHandlerDMAC_3(void)
{
    const uint32_t pendingChannelBitmap = LL_DMAC_GetInterruptStatus();

    //---------------------------------------------------------------------------------------------
    // DMAC channel 0: USART Rx.
    //---------------------------------------------------------------------------------------------

    if (pendingChannelBitmap & (1ul << DMA_CHANNEL_USART_RXD))
    {
        const enum LL_DMAC_ChannelInterruptFlag interruptFlag = LL_DMAC_Channel_GetInterruptFlag(DMA_CHANNEL_USART_RXD);

        LL_DMAC_Channel_ClearInterruptFlag(DMA_CHANNEL_USART_RXD, interruptFlag);

        printf("DMA channel [%u] interrupt flags (Rx): 0x%X\n", DMA_CHANNEL_USART_RXD, interruptFlag);

        //
        // Flaga TERR.
        //

        if (interruptFlag & LL_DMAC_ChannelInterruptFlag_TERR)
        {
            printf("[%u] TERR\n", DMA_CHANNEL_USART_RXD);
        }

        //
        // Flaga TCMPL.
        //

        if (interruptFlag & LL_DMAC_ChannelInterruptFlag_TCMPL)
        {
            printf("[%u] TCMPL\n", DMA_CHANNEL_USART_RXD);

            printf("VALID: 0x%X\n", DMA_Descriptors_WriteBack[0].BTCTRL.bit.VALID);
            printf("BTCNT: 0x%X\n", DMA_Descriptors_WriteBack[0].BTCNT.reg);
            printf("DSTADDR: 0x%X\n", DMA_Descriptors_WriteBack[0].DSTADDR.reg);
        }

        //
        // Flaga SUSP.
        //

        if (interruptFlag & LL_DMAC_ChannelInterruptFlag_SUSP)
        {
            static uint32_t suspendCounter = 0;

            suspendCounter++;

            printf("[%u] SUSP\n", DMA_CHANNEL_USART_RXD);

            printf("VALID: 0x%X\n", DMA_Descriptors_WriteBack[0].BTCTRL.bit.VALID);
            printf("BTCNT: 0x%X\n", DMA_Descriptors_WriteBack[0].BTCNT.reg);
            printf("DSTADDR: 0x%X\n", DMA_Descriptors_WriteBack[0].DSTADDR.reg);

            const bool transferIsCompleted = (DMA_Descriptors_WriteBack[0].BTCNT.reg == 0);

            if (transferIsCompleted == true)
            {
                printf("Transfer is completed\n");
            }
            else
            {
                printf("Transfer is suspended\n");
            }

            const uint8_t * currentBufferAddress = (uint8_t *)(DMA_Descriptors_WriteBack[0].DSTADDR.reg - DMA_BUFFER_SIZE);
            const uint32_t currentDataLength = DMA_BUFFER_SIZE - DMA_Descriptors_WriteBack[0].BTCNT.reg;

            printf("[%u] Rx data (address: 0x%X, length: %u): '",
                suspendCounter, currentBufferAddress, currentDataLength);

            for (uint32_t i = 0; i < currentDataLength; i++)
            {
                printf("%c", currentBufferAddress[i]);
            }

            printf("'\n");

            LL_DMAC_Channel_SetCommand(DMA_CHANNEL_USART_RXD, LL_DMAC_ChannelCommand_RESUME);
        }
    }
}


//
// Function: LL_Test_SERCOM_USART_DMA_With_Rx_Timeout
//
// USART w SERCOM-1 wyzwala DMA (Rx only) i realizuje odbiór do jednego bufora Rx.
// Gdy bufor zapełni się to DMA wykonuje suspend i zgłasza przerwania TCMPL oraz SUSP.
//
// Dodatkowo jest zaimplementowana funkcja 'UART RX timeout' na timerze TC0.
// Timer jest restartowany przez DMA po każdym odebranym znaku z UART.
// Gdy pomiędzy znakami z UART jest przerwana większa od 1000 ms to timer generuje event
// który wykonuje DMA suspend i zgłoszenie przerwania SUSP.
//
void LL_Test_SERCOM_USART_DMA_With_Rx_Timeout(void)
{
    printf("LL_Test_SERCOM_USART_DMA_With_Rx_Timeout\n");

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: konfiguracja pinów USART.
    //---------------------------------------------------------------------------------------------

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_TX, PIN_CLICK_TX, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_TX, PIN_CLICK_TX, LL_PORT_PeripheralFunction_C);

    LL_PORT_EnablePeripheralMultiplexer(PORT_CLICK_RX, PIN_CLICK_RX, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_CLICK_RX, PIN_CLICK_RX, LL_PORT_PeripheralFunction_C);

    LL_PORT_EnablePeripheralMultiplexer(PORT_UART_RTS, PIN_UART_RTS, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_UART_RTS, PIN_UART_RTS, LL_PORT_PeripheralFunction_C);

    LL_PORT_EnablePeripheralMultiplexer(PORT_UART_CTS, PIN_UART_CTS, 1);
    LL_PORT_SetPeripheralMultiplexing(PORT_UART_CTS, PIN_UART_CTS, LL_PORT_PeripheralFunction_C);

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: obliczenie baud rate.
    //---------------------------------------------------------------------------------------------

    const uint32_t baudSet = 9600;
    const uint32_t f_ref = SYSTEM_CLOCK_FREQUENCY_Hz;
    const uint32_t samplesPerBit = 8;
    const uint32_t baudRegister = 65536ull - (65536ull * (uint64_t)samplesPerBit * (uint64_t)baudSet) / (uint64_t)f_ref;

    assert((baudRegister > 0x0000) && (baudRegister <= 0xFFFF));

    const uint32_t baudFromRegister = (f_ref / samplesPerBit) - (((uint64_t)f_ref * (uint64_t)baudRegister) / ((uint64_t)samplesPerBit * 65536ull));
    const int32_t baudErrorDelta = baudSet - baudFromRegister;
    const float baudErrorPercent = (100.0f * (float)baudErrorDelta) / (float)baudSet;

    printf("BAUD register: %u / 0x%X\n", baudRegister, baudRegister);
    printf("Baud set: %u bps -> baud actual: %u bps\n", baudSet, baudFromRegister);
    printf("Error: %d bps / %f %%\n", baudErrorDelta, baudErrorPercent);

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: konfiguracja USART.
    //---------------------------------------------------------------------------------------------

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_SERCOM1_CORE, 0, 1);

    NVIC_DisableIRQ(SERCOM1_IRQn);
    
    LL_SERCOM_USART_Reset(1);
    LL_SERCOM_USART_Enable(1, 0);
    LL_SERCOM_USART_SetRunInStandby(1, 0);
    LL_SERCOM_USART_SetDebugControl(1, 0);

    LL_SERCOM_USART_SetPins(1, LL_SERCOM_USART_TransmitDataPinout_TxD0_RTS2_CTS3, LL_SERCOM_USART_ReceiveDataPinout_RxD1);
    LL_SERCOM_USART_SetFrameFormat(1, LL_SERCOM_USART_FrameFormat_USART);
    LL_SERCOM_USART_SetCommunicationMode(1, LL_SERCOM_USART_CommunicationMode_Asynchronous);
    LL_SERCOM_USART_SetClockMode(1, LL_SERCOM_USART_ClockMode_InternalClock);
    LL_SERCOM_USART_SetDataOrder(1, LL_SERCOM_USART_DataOrder_FirstLSB);
    LL_SERCOM_USART_SetBaud(1, (uint16_t)baudRegister);
    LL_SERCOM_USART_SetSampleRate(1, LL_SERCOM_USART_SampleRate_8x_Arithmetic);
    LL_SERCOM_USART_SetSampleAdjustment(1, LL_SERCOM_USART_SampleAdjustment_8x_3_4_5);
    LL_SERCOM_USART_SetCharacterSize(1, LL_SERCOM_USART_CharacterSize_8BIT);
    LL_SERCOM_USART_SetStopBit(1, LL_SERCOM_USART_StopBit_1bit);
    LL_SERCOM_USART_SetParity(1, LL_SERCOM_USART_Parity_Even);
    LL_SERCOM_USART_SetEncoding(1, LL_SERCOM_USART_Encoding_Disabled);
    LL_SERCOM_USART_SetCollisionDetection(1, 0);
    LL_SERCOM_USART_SetStartFrameDetection(1, 0);
    LL_SERCOM_USART_SetImmediateBufferOverflowNotification(1, 0);

    //---------------------------------------------------------------------------------------------
    // SERCOM-1: włączenie przerwań.
    //---------------------------------------------------------------------------------------------

    LL_SERCOM_USART_InterruptEnableSet(1, LL_SERCOM_USART_InterruptFlag_CTSIC
                                        | LL_SERCOM_USART_InterruptFlag_RXBRK
                                        | LL_SERCOM_USART_InterruptFlag_ERROR);

    NVIC_ClearPendingIRQ(SERCOM1_IRQn);
    NVIC_SetPriority(SERCOM1_IRQn, 0);
    NVIC_EnableIRQ(SERCOM1_IRQn);

    //---------------------------------------------------------------------------------------------
    // DMAC: inicjalizacja globalna.
    //---------------------------------------------------------------------------------------------

    NVIC_DisableIRQ(DMAC_IRQn);

    LL_DMAC_Reset();
    LL_DMAC_SetDescriptorMemoryBaseAddress(DMA_Descriptors);
    LL_DMAC_SetWriteBackMemoryBaseAddress(DMA_Descriptors_WriteBack);
    LL_DMAC_SetArbitrationMode(LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin,
                               LL_DMAC_ArbitrationMode_RoundRobin);
    LL_DMAC_Enable(1);

    //---------------------------------------------------------------------------------------------
    // DMAC channel 0: inicjalizacja kanału DMA kopiującego dane z rej. USART-RX do bufora w RAM.
    //---------------------------------------------------------------------------------------------

    const uint32_t DMA_CHANNEL_USART_RXD_ADDRESS_SOURCE      = (uint32_t)( &SERCOM1->USART.DATA.reg );
    const uint32_t DMA_CHANNEL_USART_RXD_ADDRESS_DESTINATION = (uint32_t)( &USART_Rx_Buffer_0[DMA_BUFFER_SIZE] );

    //
    // Inicjalizacja deskryptora DMA.
    //

    LL_DMAC_Descriptor_Initialize(&DMA_Descriptors[0],
                                  LL_DMAC_TransferBeatSize_BYTE,
                                  0, 1,
                                  LL_DMAC_TransferStepSelection_SRC,
                                  LL_DMAC_TransferStepSize_X1,
                                  LL_DMAC_TransferEventOutput_BEAT,
                                  LL_DMAC_TransferBlockAction_BOTH);

    LL_DMAC_Descriptor_SetBlockTransferCount(&DMA_Descriptors[0], DMA_BUFFER_SIZE);
    LL_DMAC_Descriptor_SetBlockTransferAddress(&DMA_Descriptors[0],
                                               (volatile void *)(DMA_CHANNEL_USART_RXD_ADDRESS_SOURCE),
                                               (volatile void *)(DMA_CHANNEL_USART_RXD_ADDRESS_DESTINATION));
    LL_DMAC_Descriptor_SetNextDescriptorAddress(&DMA_Descriptors[0], &DMA_Descriptors[0]);
    LL_DMAC_Descriptor_SetValid(&DMA_Descriptors[0], 1);

    //
    // Inicjalizacja kanału DMA.
    //

    LL_DMAC_Channel_Reset(DMA_CHANNEL_USART_RXD);
    LL_DMAC_Channel_SetTrigger(DMA_CHANNEL_USART_RXD, LL_DMAC_ChannelTriggerSource_SERCOM1_RX, LL_DMAC_ChannelTriggerAction_BEAT);
    LL_DMAC_Channel_SetEvent(DMA_CHANNEL_USART_RXD, 1, 1, LL_DMAC_ChannelEventAction_SUSPEND);
    LL_DMAC_Channel_ArbitrationLevel(DMA_CHANNEL_USART_RXD, LL_DMAC_ArbitrationLevel_LVL0);

    LL_DMAC_Channel_InterruptEnableSet(DMA_CHANNEL_USART_RXD, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);
    LL_DMAC_Channel_ClearInterruptFlag(DMA_CHANNEL_USART_RXD, LL_DMAC_ChannelInterruptFlag_TERR | LL_DMAC_ChannelInterruptFlag_TCMPL | LL_DMAC_ChannelInterruptFlag_SUSP);

    //---------------------------------------------------------------------------------------------
    // DMAC: włączenie przerwań.
    //---------------------------------------------------------------------------------------------

    NVIC_ClearPendingIRQ(DMAC_IRQn);
    NVIC_SetPriority(DMAC_IRQn, 0);
    NVIC_EnableIRQ(DMAC_IRQn);

    //---------------------------------------------------------------------------------------------
    // EVSYS: inicjalizacja 2 kanałów łączących DMAC-CH0 <-> TC0.
    //---------------------------------------------------------------------------------------------

    enum EVSYS_Channel_Index { EVSYS_Channel_Index_DMAC = 0, EVSYS_Channel_Index_TC0 };

    //
    // Channel 0: TC0 (MC0) [match] -> DMAC (CH0) [suspend].
    //

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_EVSYS_CHANNEL_0, 0, 1);
    LL_EVSYS_InitializeChannel(EVSYS_Channel_Index_DMAC, LL_EVSYS_EventGenerator_TC0_MC0, LL_EVSYS_Path_ASYNCHRONOUS, LL_EVSYS_EdgeDetection_NO_EVT_OUTPUT, 0, 0);
    LL_EVSYS_AssignUserToChannel(LL_EVSYS_User_DMAC_CH0, EVSYS_Channel_Index_DMAC, 1);

    //
    // Channel 1: DMAC (CH0) [beat] -> TC0 [retrigger].
    //

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_EVSYS_CHANNEL_1, 0, 1);
    LL_EVSYS_InitializeChannel(EVSYS_Channel_Index_TC0, LL_EVSYS_EventGenerator_DMAC_CH0, LL_EVSYS_Path_ASYNCHRONOUS, LL_EVSYS_EdgeDetection_NO_EVT_OUTPUT, 0, 0);
    LL_EVSYS_AssignUserToChannel(LL_EVSYS_User_TC0, EVSYS_Channel_Index_TC0, 1);

    //---------------------------------------------------------------------------------------------
    // TC0: inicjalizacja timera do obsługi funkcji 'UART Rx timeout'.
    //---------------------------------------------------------------------------------------------

    NVIC_DisableIRQ(TC0_IRQn);

    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_TC0, 0, 1);

    LL_TC_Reset(0);
    LL_TC_SetDebugControl(0, 0);
    LL_TC_Initialize(0, LL_TC_TimerMode_COUNT16, LL_TC_CounterPrescaler_DIV1024, LL_TC_Presynchronization_PRESC);
    LL_TC_SetCounterOneShot(0, 1);
    LL_TC_SetCountingUp(0);

    //
    // Zdarzenia timera:
    // 1. TC0 odbiera z DMAC-CH0 [beat] i wykonuje Retrigger.
    // 2. TC0 wysyła do DMAC-CH0 [suspend] gdy jest Match-0.
    //

    LL_TC_SetEventInput(0, 1, 0);
    LL_TC_SetEventAction(0, LL_TC_EventAction_RETRIGGER);
    LL_TC_SetEventOutput(0, LL_TC_EventOutput_MCEO0);

    //
    // Ustawienie czasu Rx_Timeout.
    //

    const uint32_t CCx_Min = 0x0000;
    const uint32_t CCx_Max = 0xFFFF;
    const uint32_t Timer_Clock_Hz = SYSTEM_CLOCK_FREQUENCY_Hz;
    const uint32_t Timer_Prescaler = 1024;
    const uint32_t Rx_Timeout_ms = 1000;
    const uint32_t Rx_Timeout_CC0 = ((uint64_t)(Timer_Clock_Hz / Timer_Prescaler) * (uint64_t)Rx_Timeout_ms) / 1000ull;

    assert((Rx_Timeout_CC0 > CCx_Min) && (Rx_Timeout_CC0 <= CCx_Max));

    LL_TC_SetCompareCapture(0, 0, (uint16_t)Rx_Timeout_CC0);
    LL_TC_SetCounter(0, 0);

    printf("TC0: Rx timeout: %u ms, CC0: %u\n", Rx_Timeout_ms, Rx_Timeout_CC0);

    //---------------------------------------------------------------------------------------------
    // Włączenie TC0, DMA Rx oraz USART Rx.
    //---------------------------------------------------------------------------------------------

    printf("Start USART DMA transfers (Rx buffer size: %u byte(s))\n", DMA_BUFFER_SIZE);

    LL_TC_Enable(0, 1);

    LL_DMAC_Channel_Enable(DMA_CHANNEL_USART_RXD, 1, 0);

    LL_SERCOM_USART_EnableReceiver(1, 1);
    LL_SERCOM_USART_Enable(1, 1);

    for (;;)
    {
    }
}
