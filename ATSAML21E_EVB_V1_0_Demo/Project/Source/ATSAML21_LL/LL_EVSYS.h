//
// Peripheral Driver Low Layer (EVSYS module).
//

#ifndef LL_EVSYS_INCLUDED
#define LL_EVSYS_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


// Number of channels
#define LL_EVSYS_CHANNELS      EVSYS_CHANNELS


enum LL_EVSYS_EventGenerator
{
    LL_EVSYS_EventGenerator_NONE         = 0x00,
    LL_EVSYS_EventGenerator_RTC_CMP0     = 0x01,
    LL_EVSYS_EventGenerator_RTC_CMP1     = 0x02,
    LL_EVSYS_EventGenerator_RTC_OVF      = 0x03,
    LL_EVSYS_EventGenerator_RTC_PER0     = 0x04,
    LL_EVSYS_EventGenerator_RTC_PER1     = 0x05,
    LL_EVSYS_EventGenerator_RTC_PER2     = 0x06,
    LL_EVSYS_EventGenerator_RTC_PER3     = 0x07,
    LL_EVSYS_EventGenerator_RTC_PER4     = 0x08,
    LL_EVSYS_EventGenerator_RTC_PER5     = 0x09,
    LL_EVSYS_EventGenerator_RTC_PER6     = 0x0A,
    LL_EVSYS_EventGenerator_RTC_PER7     = 0x0B,
    LL_EVSYS_EventGenerator_EIC_EXTINT0  = 0x0C,
    LL_EVSYS_EventGenerator_EIC_EXTINT1  = 0x0D,
    LL_EVSYS_EventGenerator_EIC_EXTINT2  = 0x0E,
    LL_EVSYS_EventGenerator_EIC_EXTINT3  = 0x0F,
    LL_EVSYS_EventGenerator_EIC_EXTINT4  = 0x10,
    LL_EVSYS_EventGenerator_EIC_EXTINT5  = 0x11,
    LL_EVSYS_EventGenerator_EIC_EXTINT6  = 0x12,
    LL_EVSYS_EventGenerator_EIC_EXTINT7  = 0x13,
    LL_EVSYS_EventGenerator_EIC_EXTINT8  = 0x14,
    LL_EVSYS_EventGenerator_EIC_EXTINT9  = 0x15,
    LL_EVSYS_EventGenerator_EIC_EXTINT10 = 0x16,
    LL_EVSYS_EventGenerator_EIC_EXTINT11 = 0x17,
    LL_EVSYS_EventGenerator_EIC_EXTINT12 = 0x18,
    LL_EVSYS_EventGenerator_EIC_EXTINT13 = 0x19,
    LL_EVSYS_EventGenerator_EIC_EXTINT14 = 0x2A,
    LL_EVSYS_EventGenerator_EIC_EXTINT15 = 0x2B,
    LL_EVSYS_EventGenerator_DMAC_CH0     = 0x1C,
    LL_EVSYS_EventGenerator_DMAC_CH1     = 0x1D,
    LL_EVSYS_EventGenerator_DMAC_CH2     = 0x1E,
    LL_EVSYS_EventGenerator_DMAC_CH3     = 0x1D,
    LL_EVSYS_EventGenerator_DMAC_CH4     = 0x20,
    LL_EVSYS_EventGenerator_DMAC_CH5     = 0x21,
    LL_EVSYS_EventGenerator_DMAC_CH6     = 0x22,
    LL_EVSYS_EventGenerator_DMAC_CH7     = 0x23,
    LL_EVSYS_EventGenerator_TCC0_OVF     = 0x24,
    LL_EVSYS_EventGenerator_TCC0_TRG     = 0x25,
    LL_EVSYS_EventGenerator_TCC0_CNT     = 0x26,
    LL_EVSYS_EventGenerator_TCC0_MCX0    = 0x27,
    LL_EVSYS_EventGenerator_TCC0_MCX1    = 0x28,
    LL_EVSYS_EventGenerator_TCC0_MCX2    = 0x29,
    LL_EVSYS_EventGenerator_TCC0_MCX3    = 0x2A,
    LL_EVSYS_EventGenerator_TCC1_OVF     = 0x2B,
    LL_EVSYS_EventGenerator_TCC1_TRG     = 0x2C,
    LL_EVSYS_EventGenerator_TCC1_CNT     = 0x2D,
    LL_EVSYS_EventGenerator_TCC1_MCX0    = 0x2E,
    LL_EVSYS_EventGenerator_TCC1_MCX1    = 0x2F,
    LL_EVSYS_EventGenerator_TCC2_OVF     = 0x30,
    LL_EVSYS_EventGenerator_TCC2_TRG     = 0x31,
    LL_EVSYS_EventGenerator_TCC2_CNT     = 0x32,
    LL_EVSYS_EventGenerator_TCC2_MCX0    = 0x33,
    LL_EVSYS_EventGenerator_TCC2_MCX1    = 0x34,
    LL_EVSYS_EventGenerator_TC0_OVF      = 0x35,
    LL_EVSYS_EventGenerator_TC0_MC0      = 0x36,
    LL_EVSYS_EventGenerator_TC0_MC1      = 0x37,
    LL_EVSYS_EventGenerator_TC1_OVF      = 0x38,
    LL_EVSYS_EventGenerator_TC1_MC0      = 0x39,
    LL_EVSYS_EventGenerator_TC1_MC1      = 0x3A,
    LL_EVSYS_EventGenerator_TC2_OVF      = 0x3B,
    LL_EVSYS_EventGenerator_TC2_MC0      = 0x3C,
    LL_EVSYS_EventGenerator_TC2_MC1      = 0x3D,
    LL_EVSYS_EventGenerator_TC3_OVF      = 0x3E,
    LL_EVSYS_EventGenerator_TC3_MC0      = 0x3F,
    LL_EVSYS_EventGenerator_TC3_MC1      = 0x40,
    LL_EVSYS_EventGenerator_TC4_OVF      = 0x41,
    LL_EVSYS_EventGenerator_TC4_MC0      = 0x42,
    LL_EVSYS_EventGenerator_TC4_MC1      = 0x43,
    LL_EVSYS_EventGenerator_ADC_RESRDY   = 0x44,
    LL_EVSYS_EventGenerator_ADC_WINMON   = 0x45,
    LL_EVSYS_EventGenerator_AC_COMP0     = 0x46,
    LL_EVSYS_EventGenerator_AC_COMP1     = 0x47,
    LL_EVSYS_EventGenerator_AC_WIN0      = 0x48,
    LL_EVSYS_EventGenerator_DAC_EMPTY0   = 0x49,
    LL_EVSYS_EventGenerator_DAC_EMPTY1   = 0x4A,
    LL_EVSYS_EventGenerator_PTC_EOC      = 0x4B,
    LL_EVSYS_EventGenerator_PTC_WCOMP    = 0x4C,
    LL_EVSYS_EventGenerator_TRNG_READY   = 0x4D,
    LL_EVSYS_EventGenerator_CCL_LUTOUT0  = 0x4E,
    LL_EVSYS_EventGenerator_CCL_LUTOUT1  = 0x4F,
    LL_EVSYS_EventGenerator_CCL_LUTOUT2  = 0x50,
    LL_EVSYS_EventGenerator_CCL_LUTOUT3  = 0x51,
    LL_EVSYS_EventGenerator_PAC_ACCERR   = 0x52
};


enum LL_EVSYS_Path
{
    LL_EVSYS_Path_SYNCHRONOUS    = 0x0,
    LL_EVSYS_Path_RESYNCHRONIZED = 0x1,
    LL_EVSYS_Path_ASYNCHRONOUS   = 0x2
};


enum LL_EVSYS_EdgeDetection
{
    LL_EVSYS_EdgeDetection_NO_EVT_OUTPUT = 0x0,
    LL_EVSYS_EdgeDetection_RISING_EDGE   = 0x1,
    LL_EVSYS_EdgeDetection_FALLING_EDGE  = 0x2,
    LL_EVSYS_EdgeDetection_BOTH_EDGES    = 0x3
};


enum LL_EVSYS_User
{
    LL_EVSYS_User_PORT_EV0    = 0,
    LL_EVSYS_User_PORT_EV1    = 1,
    LL_EVSYS_User_PORT_EV2    = 2,
    LL_EVSYS_User_PORT_EV3    = 3,
    LL_EVSYS_User_DMAC_CH0    = 4,
    LL_EVSYS_User_DMAC_CH1    = 5,
    LL_EVSYS_User_DMAC_CH2    = 6,
    LL_EVSYS_User_DMAC_CH3    = 7,
    LL_EVSYS_User_DMAC_CH4    = 8,
    LL_EVSYS_User_DMAC_CH5    = 9,
    LL_EVSYS_User_DMAC_CH6    = 10,
    LL_EVSYS_User_DMAC_CH7    = 11,
    LL_EVSYS_User_TCC0_EV0    = 12,
    LL_EVSYS_User_TCC0_EV1    = 13,
    LL_EVSYS_User_TCC0_MC0    = 14,
    LL_EVSYS_User_TCC0_MC1    = 15,
    LL_EVSYS_User_TCC0_MC2    = 16,
    LL_EVSYS_User_TCC0_MC3    = 17,
    LL_EVSYS_User_TCC1_EV0    = 18,
    LL_EVSYS_User_TCC1_EV1    = 19,
    LL_EVSYS_User_TCC1_MC0    = 20,
    LL_EVSYS_User_TCC1_MC1    = 21,
    LL_EVSYS_User_TCC2_EV0    = 22,
    LL_EVSYS_User_TCC2_EV1    = 23,
    LL_EVSYS_User_TCC2_MC0    = 24,
    LL_EVSYS_User_TCC2_MC1    = 25,
    LL_EVSYS_User_TC0         = 26,
    LL_EVSYS_User_TC1         = 27,
    LL_EVSYS_User_TC2         = 28,
    LL_EVSYS_User_TC3         = 29,
    LL_EVSYS_User_TC4         = 30,
    LL_EVSYS_User_ADC_START   = 31,
    LL_EVSYS_User_ADC_SYNC    = 32,
    LL_EVSYS_User_AC_COMP0    = 33,
    LL_EVSYS_User_AC_COMP1    = 34,
    LL_EVSYS_User_DAC_START0  = 35,
    LL_EVSYS_User_DAC_START1  = 36,
    LL_EVSYS_User_PTC_STCONV  = 37,
    LL_EVSYS_User_CCL_LUTIN_0 = 38,
    LL_EVSYS_User_CCL_LUTIN_1 = 39,
    LL_EVSYS_User_CCL_LUTIN_2 = 40,
    LL_EVSYS_User_CCL_LUTIN_3 = 41,
    LL_EVSYS_User_MTB_START   = 43,
    LL_EVSYS_User_MTB_STOP    = 44
};


void LL_EVSYS_Reset(void);
void LL_EVSYS_InitializeChannel(uint32_t channelIndex, enum LL_EVSYS_EventGenerator eventGenerator, enum LL_EVSYS_Path path, enum LL_EVSYS_EdgeDetection edgeDetection, uint32_t runInStandby, uint32_t onDemand);
void LL_EVSYS_AssignUserToChannel(enum LL_EVSYS_User userIndex, uint32_t channelIndex, uint32_t assign);
void LL_EVSYS_TriggerSoftwareEvent(uint32_t eventChannelBitmap);

uint32_t LL_EVSYS_GetStatusUserReady(void);
uint32_t LL_EVSYS_GetStatusChannelBusy(void);

void LL_EVSYS_InterruptEnableClear(uint32_t overrunChannelBitmap, uint32_t eventDetectedChannelBitmap);
void LL_EVSYS_InterruptEnableSet(uint32_t overrunChannelBitmap, uint32_t eventDetectedChannelBitmap);
void LL_EVSYS_GetInterruptFlag(uint32_t * pOverrunChannelBitmap, uint32_t * pEventDetectedChannelBitmap);
void LL_EVSYS_ClearInterruptFlag(uint32_t overrunChannelBitmap, uint32_t eventDetectedChannelBitmap);


#ifdef __cplusplus
}
#endif

#endif // LL_EVSYS_INCLUDED
