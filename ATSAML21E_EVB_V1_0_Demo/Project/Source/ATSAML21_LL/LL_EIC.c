#include <sam.h>
#include "LL_EIC.h"


void LL_EIC_Reset(void)
{
    EIC->CTRLA.bit.SWRST = 1;

    while (EIC->SYNCBUSY.bit.SWRST == 1)
    {
    }
}


void LL_EIC_Enable(uint32_t enable)
{
    EIC->CTRLA.bit.ENABLE = (enable != 0);

    while (EIC->SYNCBUSY.bit.ENABLE == 1)
    {
    }
}


void LL_EIC_SetClock(enum LL_EIC_Clock clock)
{
    EIC->CTRLA.bit.CKSEL = clock & 0x1;
}


void LL_EIC_EnableEventOutput(uint32_t eventOutputBitmap)
{
    EIC->EVCTRL.reg = eventOutputBitmap & 0xFFFF;
}


void LL_EIC_SetAsynchronousEdgeDetectionMode(uint32_t inputBitmap)
{
    EIC->ASYNCH.reg = inputBitmap & 0xFFFF;
}


void LL_EIC_SetInputConfiguration(uint32_t inputIndex, enum LL_EIC_InputSense senseMode, uint32_t enableFilter)
{
    const uint32_t configBitShift = (inputIndex % 8) * 4;
    const uint32_t configOneInput = ((senseMode & 0x7) << 0) | ((enableFilter != 0) << 3);

    EIC_CONFIG_Type config = EIC->CONFIG[inputIndex / 8];

    config.reg &= ~(0xFul << configBitShift);
    config.reg |= configOneInput << configBitShift;

    EIC->CONFIG[inputIndex / 8] = config;
}


void LL_EIC_InitializeNMI(enum LL_EIC_NMI_Sense senseConfiguration, uint32_t filterEnable, uint32_t asynchronousEdgeDetectionMode)
{
    EIC_NMICTRL_Type nmictrl;

    nmictrl.reg = 0;
    nmictrl.bit.NMISENSE = senseConfiguration & 0x7;
    nmictrl.bit.NMIFILTEN = (filterEnable != 0);
    nmictrl.bit.NMIASYNCH = (asynchronousEdgeDetectionMode != 0);

    EIC->NMICTRL = nmictrl;
}


uint32_t LL_EIC_GetNMIInterruptFlag(void)
{
    return EIC->NMIFLAG.bit.NMI;
}


void LL_EIC_ClearNMIInterruptFlag(void)
{
    EIC->NMIFLAG.reg = EIC_NMIFLAG_NMI;
}


void LL_EIC_InterruptEnableClear(enum LL_EIC_InterruptFlag interruptFlag)
{
    EIC->INTENCLR.reg = interruptFlag;
}


void LL_EIC_InterruptEnableSet(enum LL_EIC_InterruptFlag interruptFlag)
{
    EIC->INTENSET.reg = interruptFlag;
}


enum LL_EIC_InterruptFlag LL_EIC_GetInterruptFlag(void)
{
    return EIC->INTFLAG.reg;
}


void LL_EIC_ClearInterruptFlag(enum LL_EIC_InterruptFlag interruptFlag)
{
    EIC->INTFLAG.reg = interruptFlag;
}
