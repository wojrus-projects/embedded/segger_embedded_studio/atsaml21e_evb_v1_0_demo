#include <sam.h>
#include "LL_OSCCTRL.h"


enum LL_OSCCTRL_Status LL_OSCCTRL_GetStatus(void)
{
    return OSCCTRL->STATUS.reg;
}


void LL_OSCCTRL_OSC16M_Enable(uint32_t enable, enum LL_OSCCTRL_OSC16M_Frequency frequency, uint32_t runInStandby, uint32_t onDemand)
{
    OSCCTRL_OSC16MCTRL_Type osc16mctrl;

    osc16mctrl.reg = 0;
    osc16mctrl.bit.ENABLE = (enable != 0);
    osc16mctrl.bit.FSEL = frequency & 0x3;
    osc16mctrl.bit.RUNSTDBY = (runInStandby != 0);
    osc16mctrl.bit.ONDEMAND = (onDemand != 0);

    OSCCTRL->OSC16MCTRL = osc16mctrl;

    if (enable != 0)
    {
        while (OSCCTRL->STATUS.bit.OSC16MRDY == 0)
        {
        }
    }
}


void LL_OSCCTRL_XOSC_Enable(uint32_t enable,
                           uint32_t crystalOscillatorEnable,
                           uint32_t runInStandby,
                           uint32_t onDemand,
                           enum LL_OSCCTRL_XOSC_Gain gain,
                           uint32_t automaticAmplitudeGainEnable,
                           enum LL_OSCCTRL_XOSC_StartUpTime startUpTime)
{
    OSCCTRL_XOSCCTRL_Type xoscctrl;

    xoscctrl.reg = 0;
    xoscctrl.bit.ENABLE = (enable != 0);
    xoscctrl.bit.XTALEN = (crystalOscillatorEnable != 0);
    xoscctrl.bit.RUNSTDBY = (runInStandby != 0);
    xoscctrl.bit.ONDEMAND = (onDemand != 0);
    xoscctrl.bit.GAIN = gain & 0x7;
    xoscctrl.bit.AMPGC = (automaticAmplitudeGainEnable != 0);
    xoscctrl.bit.STARTUP = startUpTime & 0xF;

    OSCCTRL->XOSCCTRL = xoscctrl;

    if (enable != 0)
    {
        while (OSCCTRL->STATUS.bit.XOSCRDY == 0)
        {
        }
    }
}


void LL_OSCCTRL_DFLL48M_Enable(uint32_t enable, uint32_t runInStandby, uint32_t onDemand, enum LL_OSCCTRL_DFLL48M_Mode mode)
{
    OSCCTRL_DFLLCTRL_Type dfll;

    dfll.reg = 0;
    dfll.bit.ENABLE = (enable != 0);
    dfll.bit.MODE = mode & 0x1;
    dfll.bit.STABLE = 0;
    dfll.bit.LLAW = 0;
    dfll.bit.USBCRM = 0;
    dfll.bit.RUNSTDBY = (runInStandby != 0);
    dfll.bit.ONDEMAND = (onDemand != 0);
    dfll.bit.CCDIS = 0;
    dfll.bit.QLDIS = 0;
    dfll.bit.BPLCKC = 0;
    dfll.bit.WAITLOCK = 1;

    OSCCTRL->DFLLCTRL = dfll;

    while (OSCCTRL->STATUS.bit.DFLLRDY == 0)
    {
    }
}


void LL_OSCCTRL_DFLL48M_SetValue(uint32_t coarse, uint32_t fine)
{
    OSCCTRL_DFLLVAL_Type dfllval;

    dfllval.reg = 0;
    dfllval.bit.COARSE = coarse & 0x3F;
    dfllval.bit.FINE = fine & 0x3FF;

    OSCCTRL->DFLLVAL = dfllval;

    while (OSCCTRL->STATUS.bit.DFLLRDY == 0)
    {
    }
}


void LL_OSCCTRL_DPLL_Enable(uint32_t enable, uint32_t runInStandby, uint32_t onDemand)
{
    OSCCTRL_DPLLCTRLA_Type dpllctrla;

    dpllctrla.reg = 0;
    dpllctrla.bit.ENABLE = (enable != 0);
    dpllctrla.bit.RUNSTDBY = (runInStandby != 0);
    dpllctrla.bit.ONDEMAND = (onDemand != 0);

    OSCCTRL->DPLLCTRLA = dpllctrla;

    while (OSCCTRL->DPLLSYNCBUSY.bit.ENABLE == 1)
    {
    }

    if (enable != 0)
    {
        enum LL_OSCCTRL_DPLL_Status dpllStatus;

        do
        {
            dpllStatus = LL_OSCCTRL_DPLL_GetStatus();
        }
        while (((dpllStatus & LL_OSCCTRL_DPLL_Status_LOCK) == 0)
            || ((dpllStatus & LL_OSCCTRL_DPLL_Status_CLKRDY) == 0));
    }
}


void LL_OSCCTRL_DPLL_Initialize(enum LL_OSCCTRL_DPLL_Filter filter,
                                uint32_t lowPowerEnable,
                                uint32_t wakeUpFastEnable,
                                enum LL_OSCCTRL_DPLL_LockTime lockTime,
                                uint32_t lockBypassEnable)
{
    OSCCTRL_DPLLCTRLB_Type dpllctrlb = OSCCTRL->DPLLCTRLB;

    dpllctrlb.bit.FILTER = filter & 0x3;
    dpllctrlb.bit.LPEN = (lowPowerEnable != 0);
    dpllctrlb.bit.WUF = (wakeUpFastEnable != 0);
    dpllctrlb.bit.LTIME = lockTime & 0x7;
    dpllctrlb.bit.LBYPASS = (lockBypassEnable != 0);

    OSCCTRL->DPLLCTRLB = dpllctrlb;
}


void LL_OSCCTRL_DPLL_SetClock(enum LL_OSCCTRL_DPLL_ReferenceClock referenceClock, uint32_t clockDivider)
{
    OSCCTRL_DPLLCTRLB_Type dpllctrlb = OSCCTRL->DPLLCTRLB;

    dpllctrlb.bit.REFCLK = referenceClock & 0x3;
    dpllctrlb.bit.DIV = clockDivider & 0x7FF;

    OSCCTRL->DPLLCTRLB = dpllctrlb;
}


void LL_OSCCTRL_DPLL_SetRatio(uint32_t loopDividerRatio, uint32_t loopDividerRatioFractionalPart)
{
    OSCCTRL_DPLLRATIO_Type dpllratio;

    dpllratio.reg = 0;
    dpllratio.bit.LDR = loopDividerRatio & 0xFFF;
    dpllratio.bit.LDRFRAC = loopDividerRatioFractionalPart & 0xF;

    OSCCTRL->DPLLRATIO = dpllratio;

    while (OSCCTRL->DPLLSYNCBUSY.bit.DPLLRATIO == 1)
    {
    }
}


void LL_OSCCTRL_DPLL_SetPrescaler(enum LL_OSCCTRL_DPLL_Prescaler prescaler)
{
    OSCCTRL_DPLLPRESC_Type dpllpresc;

    dpllpresc.reg = 0;
    dpllpresc.bit.PRESC = prescaler & 0x3;

    OSCCTRL->DPLLPRESC = dpllpresc;

    while (OSCCTRL->DPLLSYNCBUSY.bit.DPLLPRESC == 1)
    {
    }
}


enum LL_OSCCTRL_DPLL_Status LL_OSCCTRL_DPLL_GetStatus(void)
{
    return OSCCTRL->DPLLSTATUS.reg;
}


void LL_OSCCTRL_InterruptEnableClear(enum LL_OSCCTRL_InterruptFlag interruptFlag)
{
    OSCCTRL->INTENCLR.reg = interruptFlag;
}


void LL_OSCCTRL_InterruptEnableSet(enum LL_OSCCTRL_InterruptFlag interruptFlag)
{
    OSCCTRL->INTENSET.reg = interruptFlag;
}


enum LL_OSCCTRL_InterruptFlag LL_OSCCTRL_GetInterruptFlag(void)
{
    return OSCCTRL->INTFLAG.reg;
}


void LL_OSCCTRL_ClearInterruptFlag(enum LL_OSCCTRL_InterruptFlag interruptFlag)
{
    OSCCTRL->INTFLAG.reg = interruptFlag;
}
