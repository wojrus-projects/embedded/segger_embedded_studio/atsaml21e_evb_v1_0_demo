#include <sam.h>
#include "LL_TCC.h"


static Tcc* const TCC_Instances[TCC_INST_NUM] = TCC_INSTS;


void LL_TCC_Reset(uint32_t timerIndex)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->CTRLA.bit.SWRST = 1;

    while (pTcc->SYNCBUSY.bit.SWRST == 1)
    {
    }
}


void LL_TCC_Enable(uint32_t timerIndex, uint32_t state)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->CTRLA.bit.ENABLE = (state != 0);

    while (pTcc->SYNCBUSY.bit.ENABLE == 1)
    {
    }
}


void LL_TCC_SetRunInStandby(uint32_t timerIndex, uint32_t runInStandby)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->CTRLA.bit.RUNSTDBY = (runInStandby != 0);
}


void LL_TCC_SetDebugControl(uint32_t timerIndex, uint32_t runInDebugHaltedState, uint32_t faultDetectionOnDebugBreakDetection)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->DBGCTRL.bit.DBGRUN = ((runInDebugHaltedState != 0) << TCC_DBGCTRL_DBGRUN_Pos)
                             | ((faultDetectionOnDebugBreakDetection != 0) << TCC_DBGCTRL_FDDBD_Pos);
}


void LL_TCC_Initialize(uint32_t timerIndex,
                       enum LL_TCC_CounterPrescaler prescaler,
                       enum LL_TCC_Presynchronization presynchronization,
                       enum LL_TCC_DitheringResolution dithering,
                       uint32_t autoLockEnable,
                       uint32_t masterSynchronizationEnable,
                       uint32_t dmaOneShotTriggerEnable)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    TCC_CTRLA_Type ctrla = pTcc->CTRLA;

    ctrla.bit.PRESCALER = prescaler & 0x7;
    ctrla.bit.PRESCSYNC = presynchronization & 0x3;
    ctrla.bit.RESOLUTION = dithering & 0x3;
    ctrla.bit.ALOCK = (autoLockEnable != 0);
    ctrla.bit.MSYNC = (masterSynchronizationEnable != 0);
    ctrla.bit.DMAOS = (dmaOneShotTriggerEnable != 0);

    pTcc->CTRLA = ctrla;
}


void LL_TCC_EnableCaptureChannel(uint32_t timerIndex, uint32_t captureChannelIndex, uint32_t enable)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    TCC_CTRLA_Type ctrla = pTcc->CTRLA;

    switch (captureChannelIndex)
    {
        case 0:
            ctrla.bit.CPTEN0 = (enable != 0);
            break;

        case 1:
            ctrla.bit.CPTEN1 = (enable != 0);
            break;

        case 2:
            ctrla.bit.CPTEN2 = (enable != 0);
            break;

        case 3:
            ctrla.bit.CPTEN3 = (enable != 0);
            break;

        default:
            return;
    }

    pTcc->CTRLA = ctrla;  
}


void LL_TCC_SetEventAction(uint32_t timerIndex, enum LL_TCC_EventInput0Action input0Action, enum LL_TCC_EventInput1Action input1Action)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    TCC_EVCTRL_Type evctrl = pTcc->EVCTRL;

    evctrl.bit.EVACT0 = input0Action & 0x7;
    evctrl.bit.EVACT1 = input1Action & 0x7;

    pTcc->EVCTRL = evctrl;
}


void LL_TCC_SetEventInput(uint32_t timerIndex, uint32_t eventInput0Enable, uint32_t eventInput1Enable, uint32_t eventInput0Invert, uint32_t eventInput1Invert)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    TCC_EVCTRL_Type evctrl = pTcc->EVCTRL;

    evctrl.bit.TCEI0 = (eventInput0Enable != 0);
    evctrl.bit.TCEI1 = (eventInput1Enable != 0);
    evctrl.bit.TCINV0 = (eventInput0Invert != 0);
    evctrl.bit.TCINV1 = (eventInput1Invert != 0);

    pTcc->EVCTRL = evctrl;
}


void LL_TCC_EnableEventInputMatchCapture(uint32_t timerIndex, uint32_t matchCaptureChannelIndex, uint32_t enable)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    TCC_EVCTRL_Type evctrl = pTcc->EVCTRL;

    switch (matchCaptureChannelIndex)
    {
        case 0:
            evctrl.bit.MCEI0 = (enable != 0);
            break;

        case 1:
            evctrl.bit.MCEI1 = (enable != 0);
            break;

        case 2:
            evctrl.bit.MCEI2 = (enable != 0);
            break;

        case 3:
            evctrl.bit.MCEI3 = (enable != 0);
            break;

        default:
            return;
    }

    pTcc->EVCTRL = evctrl;
}


void LL_TCC_EnableEventOutputMatchCapture(uint32_t timerIndex, uint32_t matchCaptureChannelIndex, uint32_t enable)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    TCC_EVCTRL_Type evctrl = pTcc->EVCTRL;

    switch (matchCaptureChannelIndex)
    {
        case 0:
            evctrl.bit.MCEO0 = (enable != 0);
            break;

        case 1:
            evctrl.bit.MCEO1 = (enable != 0);
            break;

        case 2:
            evctrl.bit.MCEO2 = (enable != 0);
            break;

        case 3:
            evctrl.bit.MCEO3 = (enable != 0);
            break;

        default:
            return;
    }

    pTcc->EVCTRL = evctrl;
}


void LL_TCC_EnableEventOutput(uint32_t timerIndex, uint32_t overflowUnderflowEnable, uint32_t retriggerEnable, uint32_t timerCounterEnable)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    TCC_EVCTRL_Type evctrl = pTcc->EVCTRL;

    evctrl.bit.OVFEO = (overflowUnderflowEnable != 0);
    evctrl.bit.TRGEO = (retriggerEnable != 0);
    evctrl.bit.CNTEO = (timerCounterEnable != 0);

    pTcc->EVCTRL = evctrl;
}


void LL_TCC_SetTimerCounterInterruptEventMode(uint32_t timerIndex, enum LL_TCC_InterruptEventMode mode)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    TCC_EVCTRL_Type evctrl = pTcc->EVCTRL;

    evctrl.bit.CNTSEL = mode & 0x3;

    pTcc->EVCTRL = evctrl;
}


void LL_TCC_Waveform_SetGenerationOperation(uint32_t timerIndex, enum LL_TCC_WaveformGenerationOperation operation)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->WAVE.bit.WAVEGEN = operation & 0x7;
}


void LL_TCC_Waveform_SetRampOperation(uint32_t timerIndex, enum LL_TCC_RampOperation operation)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->WAVE.bit.RAMP = operation & 0x3;
}


void LL_TCC_Waveform_EnableCircularPeriod(uint32_t timerIndex, uint32_t state)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->WAVE.bit.CIPEREN = (state != 0);

    while (pTcc->SYNCBUSY.bit.WAVE == 1)
    {
    }
}


void LL_TCC_Waveform_EnableCircularCompareCapture(uint32_t timerIndex, uint32_t cc0State, uint32_t cc1State, uint32_t cc2State, uint32_t cc3State)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    TCC_WAVE_Type wave = pTcc->WAVE;

    wave.bit.CICCEN0 = (cc0State != 0);
    wave.bit.CICCEN1 = (cc1State != 0);
    wave.bit.CICCEN2 = (cc2State != 0);
    wave.bit.CICCEN3 = (cc3State != 0);

    pTcc->WAVE = wave;

    while (pTcc->SYNCBUSY.bit.WAVE == 1)
    {
    }
}


void LL_TCC_Waveform_SetChannelPolarity(uint32_t timerIndex, uint32_t ch0State, uint32_t ch1State, uint32_t ch2State, uint32_t ch3State)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    TCC_WAVE_Type wave = pTcc->WAVE;

    wave.bit.POL0 = (ch0State != 0);
    wave.bit.POL1 = (ch1State != 0);
    wave.bit.POL2 = (ch2State != 0);
    wave.bit.POL3 = (ch3State != 0);

    pTcc->WAVE = wave;

    while (pTcc->SYNCBUSY.bit.WAVE == 1)
    {
    }
}


void LL_TCC_Waveform_SwapDTIOutputPair(uint32_t timerIndex, uint32_t out0State, uint32_t out1State, uint32_t out2State, uint32_t out3State)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    TCC_WAVE_Type wave = pTcc->WAVE;

    wave.bit.SWAP0 = (out0State != 0);
    wave.bit.SWAP1 = (out1State != 0);
    wave.bit.SWAP2 = (out2State != 0);
    wave.bit.SWAP3 = (out3State != 0);

    pTcc->WAVE = wave;

    while (pTcc->SYNCBUSY.bit.WAVE == 1)
    {
    }
}


void LL_TCC_WaveformExtension_SetOutputMatrix(uint32_t timerIndex, enum LL_TCC_OutputMatrix value)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->WEXCTRL.bit.OTMX = value & 0x3;
}


void LL_TCC_WaveformExtension_SetDeadTime(uint32_t timerIndex,
                                          uint32_t dt0Enable,
                                          uint32_t dt1Enable,
                                          uint32_t dt2Enable,
                                          uint32_t dt3Enable,
                                          uint32_t lowSideClockCycles,
                                          uint32_t highSideClockCycles)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    TCC_WEXCTRL_Type wextctrl = pTcc->WEXCTRL;

    wextctrl.bit.DTIEN0 = (dt0Enable != 0);
    wextctrl.bit.DTIEN1 = (dt1Enable != 0);
    wextctrl.bit.DTIEN2 = (dt2Enable != 0);
    wextctrl.bit.DTIEN3 = (dt3Enable != 0);
    wextctrl.bit.DTLS = lowSideClockCycles & 0xFF;
    wextctrl.bit.DTHS = lowSideClockCycles & 0xFF;

    pTcc->WEXCTRL = wextctrl;
}


void LL_TCC_SetWaveformOutputInversion(uint32_t timerIndex,
                                       uint32_t wo0,
                                       uint32_t wo1,
                                       uint32_t wo2,
                                       uint32_t wo3,
                                       uint32_t wo4,
                                       uint32_t wo5,
                                       uint32_t wo6,
                                       uint32_t wo7)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    TCC_DRVCTRL_Type drvctrl = pTcc->DRVCTRL;

    drvctrl.bit.INVEN0 = (wo0 != 0);
    drvctrl.bit.INVEN1 = (wo1 != 0);
    drvctrl.bit.INVEN2 = (wo2 != 0);
    drvctrl.bit.INVEN3 = (wo3 != 0);
    drvctrl.bit.INVEN4 = (wo4 != 0);
    drvctrl.bit.INVEN5 = (wo5 != 0);
    drvctrl.bit.INVEN6 = (wo6 != 0);
    drvctrl.bit.INVEN7 = (wo7 != 0);

    pTcc->DRVCTRL = drvctrl;
}


void LL_TCC_SetLockUpdate(uint32_t timerIndex, uint32_t state)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->CTRLBCLR.reg = (state != 0) << TCC_CTRLBCLR_LUPD_Pos;

    while (pTcc->SYNCBUSY.bit.CTRLB == 1)
    {
    } 
}


void LL_TCC_SetCountingUp(uint32_t timerIndex)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->CTRLBCLR.reg = TCC_CTRLBCLR_DIR;

    while (pTcc->SYNCBUSY.bit.CTRLB == 1)
    {
    }
}


void LL_TCC_SetCountingDown(uint32_t timerIndex)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->CTRLBSET.reg = TCC_CTRLBSET_DIR;

    while (pTcc->SYNCBUSY.bit.CTRLB == 1)
    {
    }
}


void LL_TCC_SetCounterOneShot(uint32_t timerIndex, uint32_t state)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->CTRLBSET.reg = (state != 0) << TCC_CTRLBCLR_ONESHOT_Pos;

    while (pTcc->SYNCBUSY.bit.CTRLB == 1)
    {
    }
}


void LL_TCC_SetCommand(uint32_t timerIndex, enum LL_TCC_Command command)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->CTRLBSET.reg = TCC_CTRLBCLR_CMD(command);

    while (pTcc->SYNCBUSY.bit.CTRLB == 1)
    {
    }
}


void LL_TCC_SetRampIndexCommand(uint32_t timerIndex, enum LL_TCC_RampIndexCommand command)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->CTRLBSET.reg = TCC_CTRLBCLR_IDXCMD(command);

    while (pTcc->SYNCBUSY.bit.CTRLB == 1)
    {
    } 
}


void LL_TCC_SetCounter(uint32_t timerIndex, uint32_t value)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->COUNT.reg = value;

    while (pTcc->SYNCBUSY.bit.COUNT == 1)
    {
    }
}


uint32_t LL_TCC_GetCounter(uint32_t timerIndex)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->CTRLBSET.reg = TCC_CTRLBSET_CMD_READSYNC;

    while (pTcc->SYNCBUSY.bit.CTRLB == 1)
    {
    }

    while (pTcc->CTRLBSET.bit.CMD != TCC_CTRLBSET_CMD_NONE_Val)
    {
    }

    return pTcc->COUNT.reg;
}


void LL_TCC_SetPeriod(uint32_t timerIndex, uint32_t value)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->PER.reg = value;

    while (pTcc->SYNCBUSY.bit.PER == 1)
    {
    }
}


uint32_t LL_TCC_GetPeriod(uint32_t timerIndex)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    return pTcc->PER.reg;
}


void LL_TCC_SetPeriodBuffer(uint32_t timerIndex, uint32_t value)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->PERBUF.reg = value;

    while (pTcc->SYNCBUSY.bit.PER == 1)
    {
    }
}


uint32_t LL_TCC_GetPeriodBuffer(uint32_t timerIndex)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    return pTcc->PERBUF.reg;
}


void LL_TCC_SetCompareCapture(uint32_t timerIndex, uint32_t channelIndex, uint32_t value)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    TCC_CC_Type cc;

    cc.reg = 0;
    cc.bit.CC = value & 0xFFFFFF;

    pTcc->CC[channelIndex] = cc;

    switch (channelIndex)
    {
        case 0:
            while (pTcc->SYNCBUSY.bit.CC0 == 1)
            {
            }
            return;

        case 1:
            while (pTcc->SYNCBUSY.bit.CC1 == 1)
            {
            }
            return;

        case 2:
            while (pTcc->SYNCBUSY.bit.CC2 == 1)
            {
            }
            return;

        case 3:
            while (pTcc->SYNCBUSY.bit.CC3 == 1)
            {
            }
            return;

        default:
            return;
    }
}


uint32_t LL_TCC_GetCompareCapture(uint32_t timerIndex, uint32_t channelIndex)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    return pTcc->CC[channelIndex].bit.CC;
}


void LL_TCC_SetCompareCaptureBuffer(uint32_t timerIndex, uint32_t channelIndex, uint32_t value)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    TCC_CCBUF_Type ccbuf;

    ccbuf.reg = 0;
    ccbuf.bit.CCBUF = value & 0xFFFFFF;

    pTcc->CCBUF[channelIndex] = ccbuf;

    switch (channelIndex)
    {
        case 0:
            while (pTcc->SYNCBUSY.bit.CC0 == 1)
            {
            }
            return;

        case 1:
            while (pTcc->SYNCBUSY.bit.CC1 == 1)
            {
            }
            return;

        case 2:
            while (pTcc->SYNCBUSY.bit.CC2 == 1)
            {
            }
            return;

        case 3:
            while (pTcc->SYNCBUSY.bit.CC3 == 1)
            {
            }
            return;

        default:
            return;
    }
}


uint32_t LL_TCC_GetCompareCaptureBuffer(uint32_t timerIndex, uint32_t channelIndex)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    return pTcc->CCBUF[channelIndex].bit.CCBUF;
}


enum LL_TCC_Status LL_TCC_GetStatus(uint32_t timerIndex)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    return pTcc->STATUS.reg;
}


void LL_TCC_SetStatus(uint32_t timerIndex, enum LL_TCC_Status value)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->STATUS.reg = value;

    while (pTcc->SYNCBUSY.bit.STATUS == 1)
    {
    }
}


void LL_TCC_InterruptEnableClear(uint32_t timerIndex, enum LL_TCC_InterruptFlag interruptFlag)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->INTENCLR.reg = interruptFlag;
}


void LL_TCC_InterruptEnableSet(uint32_t timerIndex, enum LL_TCC_InterruptFlag interruptFlag)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->INTENSET.reg = interruptFlag;
}


enum LL_TCC_InterruptFlag LL_TCC_GetInterruptFlag(uint32_t timerIndex)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    return pTcc->INTFLAG.reg;
}


void LL_TCC_ClearInterruptFlag(uint32_t timerIndex, enum LL_TCC_InterruptFlag interruptFlag)
{
    Tcc* const pTcc = TCC_Instances[timerIndex];

    pTcc->INTFLAG.reg = interruptFlag;
}
