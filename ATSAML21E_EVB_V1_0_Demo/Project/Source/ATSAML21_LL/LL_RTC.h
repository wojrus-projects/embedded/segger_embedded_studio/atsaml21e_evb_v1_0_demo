//
// Peripheral Driver Low Layer (RTC module).
//

#ifndef LL_RTC_INCLUDED
#define LL_RTC_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif


void LL_RTC_Reset(void);


#ifdef __cplusplus
}
#endif

#endif // LL_RTC_INCLUDED
