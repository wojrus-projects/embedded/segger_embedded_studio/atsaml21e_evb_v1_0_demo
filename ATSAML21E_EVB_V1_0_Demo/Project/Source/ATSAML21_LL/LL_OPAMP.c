#include <sam.h>
#include "LL_OPAMP.h"


void LL_OPAMP_Reset(void)
{
    OPAMP->CTRLA.bit.SWRST = 1;

    while (OPAMP->CTRLA.bit.SWRST == 1)
    {
    }
}


void LL_OPAMP_Enable(uint32_t enable, uint32_t lowPowerInputMuxEnable)
{
    OPAMP_CTRLA_Type ctrla;

    ctrla.reg = 0;
    ctrla.bit.ENABLE = (enable != 0);
    ctrla.bit.LPMUX = (lowPowerInputMuxEnable != 0);

    OPAMP->CTRLA = ctrla;
}


enum LL_OPAMP_Status LL_OPAMP_GetStatus(void)
{
    return OPAMP->STATUS.reg;
}


void LL_OPAMP_SetInputMux(uint32_t opampIndex, enum LL_OPAMP_PositiveInputMux positiveInputMux, enum LL_OPAMP_NegativeInputMux negativeInputMux)
{
    OPAMP_OPAMPCTRL_Type opampctrl = OPAMP->OPAMPCTRL[opampIndex];

    opampctrl.bit.MUXPOS = positiveInputMux & 0x7;
    opampctrl.bit.MUXNEG = negativeInputMux & 0x7;

    OPAMP->OPAMPCTRL[opampIndex] = opampctrl;
}


void LL_OPAMP_SetResistors(uint32_t opampIndex,
                           uint32_t resistorLadderToOutputEnable,
                           uint32_t resistorLadderToVCCEnable,
                           uint32_t resistor1Enable,
                           enum LL_OPAMP_Resistor1Mux resistor1Mux,
                           enum LL_OPAMP_Potentiometer potentiometer)
{
    OPAMP_OPAMPCTRL_Type opampctrl = OPAMP->OPAMPCTRL[opampIndex];

    opampctrl.bit.RES2OUT = (resistorLadderToOutputEnable != 0);
    opampctrl.bit.RES2VCC = (resistorLadderToVCCEnable != 0);
    opampctrl.bit.RES1EN = (resistor1Enable != 0);
    opampctrl.bit.RES1MUX = resistor1Mux & 0x3;
    opampctrl.bit.POTMUX = potentiometer & 0x7;

    OPAMP->OPAMPCTRL[opampIndex] = opampctrl;
}


void LL_OPAMP_OpampEnable(uint32_t opampIndex, uint32_t enable, uint32_t analogOutputEnable, enum LL_OPAMP_Bias bias, uint32_t runInStandby, uint32_t onDemand)
{
    OPAMP_OPAMPCTRL_Type opampctrl = OPAMP->OPAMPCTRL[opampIndex];

    opampctrl.bit.ENABLE = (enable != 0);
    opampctrl.bit.ANAOUT = (analogOutputEnable != 0);
    opampctrl.bit.BIAS = bias & 0x3;
    opampctrl.bit.RUNSTDBY = (runInStandby != 0);
    opampctrl.bit.ONDEMAND = (onDemand != 0);

    OPAMP->OPAMPCTRL[opampIndex] = opampctrl;
}
