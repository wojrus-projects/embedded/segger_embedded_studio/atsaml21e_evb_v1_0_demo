//
// Peripheral Driver Low Layer (PM module).
//

#ifndef LL_PM_INCLUDED
#define LL_PM_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


enum LL_PM_PerformanceLevel
{
    LL_PM_PerformanceLevel_PL0 = 0x0,
    LL_PM_PerformanceLevel_PL2 = 0x2
};


enum LL_PM_SleepMode
{
    LL_PM_SleepMode_IDLE    = 0x2,
    LL_PM_SleepMode_STANDBY = 0x4,
    LL_PM_SleepMode_BACKUP  = 0x5,
    LL_PM_SleepMode_OFF     = 0x6
};


void LL_PM_SetPerformanceLevel(uint32_t disableAutomaticLevelSelection, enum LL_PM_PerformanceLevel level);
void LL_PM_SetSleepMode(enum LL_PM_SleepMode mode);


#ifdef __cplusplus
}
#endif

#endif // LL_PM_INCLUDED
