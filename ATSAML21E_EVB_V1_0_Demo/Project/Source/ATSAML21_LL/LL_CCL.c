#include <sam.h>
#include "LL_CCL.h"


void LL_CCL_Reset(void)
{
    CCL->CTRL.bit.SWRST = 1;

    while (CCL->CTRL.bit.SWRST == 1)
    {
    }
}


void LL_CCL_Enable(uint32_t enable, uint32_t runInStandby)
{
    CCL_CTRL_Type ctrl;

    ctrl.reg = 0;
    ctrl.bit.ENABLE = (enable != 0);
    ctrl.bit.RUNSTDBY = (runInStandby != 0);

    CCL->CTRL = ctrl;
}


void LL_CCL_SelectSequential(uint32_t seqIndex, enum LL_CCL_Sequential sequentialMode)
{
    CCL->SEQCTRL[seqIndex].bit.SEQSEL = sequentialMode & 0xF;
}


void LL_CCL_LUT_Enable(uint32_t lutIndex, uint32_t enable)
{
    CCL_LUTCTRL_Type lutctrl = CCL->LUTCTRL[lutIndex];

    lutctrl.bit.ENABLE = (enable != 0);

    CCL->LUTCTRL[lutIndex] = lutctrl;
}


void LL_CCL_LUT_EnableEvent(uint32_t lutIndex, uint32_t invertedEventInputEnable, uint32_t eventInputEnable, uint32_t eventOutputEnable)
{
    CCL_LUTCTRL_Type lutctrl = CCL->LUTCTRL[lutIndex];

    lutctrl.bit.INVEI = (invertedEventInputEnable != 0);
    lutctrl.bit.LUTEI = (eventInputEnable != 0);
    lutctrl.bit.LUTEO = (eventOutputEnable != 0);

    CCL->LUTCTRL[lutIndex] = lutctrl;
}


void LL_CCL_LUT_SetTruthTable(uint32_t lutIndex, uint8_t truthTable)
{
    CCL_LUTCTRL_Type lutctrl = CCL->LUTCTRL[lutIndex];

    lutctrl.bit.TRUTH = truthTable;

    CCL->LUTCTRL[lutIndex] = lutctrl;
}


void LL_CCL_LUT_SelectInput(uint32_t lutIndex, enum LL_CCL_LUT_Input input0, enum LL_CCL_LUT_Input input1, enum LL_CCL_LUT_Input input2)
{
    CCL_LUTCTRL_Type lutctrl = CCL->LUTCTRL[lutIndex];

    lutctrl.bit.INSEL0 = input0 & 0xF;
    lutctrl.bit.INSEL1 = input1 & 0xF;
    lutctrl.bit.INSEL2 = input2 & 0xF;

    CCL->LUTCTRL[lutIndex] = lutctrl;
}


void LL_CCL_LUT_EnableEdgeDetection(uint32_t lutIndex, uint32_t enable)
{
    CCL_LUTCTRL_Type lutctrl = CCL->LUTCTRL[lutIndex];

    lutctrl.bit.EDGESEL = (enable != 0);

    CCL->LUTCTRL[lutIndex] = lutctrl;
}


void LL_CCL_LUT_SetFilter(uint32_t lutIndex, enum LL_CCL_LUT_Filter filter)
{
    CCL_LUTCTRL_Type lutctrl = CCL->LUTCTRL[lutIndex];

    lutctrl.bit.FILTSEL = filter & 0x3;

    CCL->LUTCTRL[lutIndex] = lutctrl;
}
