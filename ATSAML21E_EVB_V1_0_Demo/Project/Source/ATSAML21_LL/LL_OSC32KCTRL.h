//
// Peripheral Driver Low Layer (OSC32KCTRL module).
//

#ifndef LL_OSC32KCTRL_INCLUDED
#define LL_OSC32KCTRL_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


enum LL_OSC32KCTRL_XOSC32K_OscillatorStartUpTime
{
    LL_OSC32KCTRL_XOSC32K_OscillatorStartUpTime_60ms  = 0x0,
    LL_OSC32KCTRL_XOSC32K_OscillatorStartUpTime_130ms = 0x1,
    LL_OSC32KCTRL_XOSC32K_OscillatorStartUpTime_500ms = 0x2,
    LL_OSC32KCTRL_XOSC32K_OscillatorStartUpTime_1s    = 0x3,
    LL_OSC32KCTRL_XOSC32K_OscillatorStartUpTime_2s    = 0x4,
    LL_OSC32KCTRL_XOSC32K_OscillatorStartUpTime_4s    = 0x5,
    LL_OSC32KCTRL_XOSC32K_OscillatorStartUpTime_8s    = 0x6
};


enum LL_OSC32KCTRL_OSC32K_OscillatorStartUpTime
{
    LL_OSC32KCTRL_OSC32K_OscillatorStartUpTime_92us  = 0x0,
    LL_OSC32KCTRL_OSC32K_OscillatorStartUpTime_122us = 0x1,
    LL_OSC32KCTRL_OSC32K_OscillatorStartUpTime_183us = 0x2,
    LL_OSC32KCTRL_OSC32K_OscillatorStartUpTime_305us = 0x3,
    LL_OSC32KCTRL_OSC32K_OscillatorStartUpTime_549us = 0x4,
    LL_OSC32KCTRL_OSC32K_OscillatorStartUpTime_1ms   = 0x5,
    LL_OSC32KCTRL_OSC32K_OscillatorStartUpTime_2ms   = 0x6,
    LL_OSC32KCTRL_OSC32K_OscillatorStartUpTime_4ms   = 0x7
};


enum LL_OSC32KCTRL_RTC_Clock
{
    LL_OSC32KCTRL_RTC_Clock_ULP1K   = 0x0,
    LL_OSC32KCTRL_RTC_Clock_ULP32K  = 0x1,
    LL_OSC32KCTRL_RTC_Clock_OSC1K   = 0x2,
    LL_OSC32KCTRL_RTC_Clock_OSC32K  = 0x3,
    LL_OSC32KCTRL_RTC_Clock_XOSC1K  = 0x4,
    LL_OSC32KCTRL_RTC_Clock_XOSC32K = 0x5
};


enum LL_OSC32KCTRL_Status
{
    LL_OSC32KCTRL_Status_XOSC32KRDY = 1 << 0,
    LL_OSC32KCTRL_Status_OSC32KRDY  = 1 << 1
};


enum LL_OSC32KCTRL_InterruptFlag
{
    LL_OSC32KCTRL_InterruptFlag_XOSC32KRDY = 1 << 0,
    LL_OSC32KCTRL_InterruptFlag_OSC32KRDY  = 1 << 1
};


void LL_OSC32KCTRL_XOSC32K_Initialize(uint32_t crystalOscillatorEnable, uint32_t output32kHzEnable, uint32_t output1kHzEnable, enum LL_OSC32KCTRL_XOSC32K_OscillatorStartUpTime oscillatorStartUpTime, uint32_t writeLock);
void LL_OSC32KCTRL_XOSC32K_Enable(uint32_t enable, uint32_t runInStandby, uint32_t onDemand);
void LL_OSC32KCTRL_OSC32K_Initialize(uint32_t output32kHzEnable, uint32_t output1kHzEnable, enum LL_OSC32KCTRL_OSC32K_OscillatorStartUpTime oscillatorStartUpTime, uint32_t oscillatorCalibration, uint32_t writeLock);
void LL_OSC32KCTRL_OSC32K_Enable(uint32_t enable, uint32_t runInStandby, uint32_t onDemand);
void LL_OSC32KCTRL_OSCULP32K_Initialize(uint32_t oscillatorCalibration, uint32_t writeLock);

void LL_OSC32KCTRL_Select_RTC_Clock(enum LL_OSC32KCTRL_RTC_Clock clockSource);
enum LL_OSC32KCTRL_Status LL_OSC32KCTRL_GetStatus(void);

void LL_OSC32KCTRL_InterruptEnableClear(enum LL_OSC32KCTRL_InterruptFlag interruptFlag);
void LL_OSC32KCTRL_InterruptEnableSet(enum LL_OSC32KCTRL_InterruptFlag interruptFlag);
enum LL_OSC32KCTRL_InterruptFlag LL_OSC32KCTRL_GetInterruptFlag(void);
void LL_OSC32KCTRL_ClearInterruptFlag(enum LL_OSC32KCTRL_InterruptFlag interruptFlag);


#ifdef __cplusplus
}
#endif

#endif // LL_OSC32KCTRL_INCLUDED
