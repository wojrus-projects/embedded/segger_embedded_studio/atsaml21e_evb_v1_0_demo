#include <sam.h>
#include "LL_SERCOM_I2C_MASTER.h"


static Sercom* const SERCOM_Instances[SERCOM_INST_NUM] = SERCOM_INSTS;


void LL_SERCOM_I2C_MASTER_Reset(uint32_t sercomIndex)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CM.CTRLA.bit.SWRST = 1;

    while (pSercom->I2CM.SYNCBUSY.bit.SWRST == 1)
    {
    }
}


void LL_SERCOM_I2C_MASTER_Enable(uint32_t sercomIndex, uint32_t enable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CM.CTRLA.bit.ENABLE = (enable != 0);

    while (pSercom->I2CM.SYNCBUSY.bit.ENABLE == 1)
    {
    }
}


void LL_SERCOM_I2C_MASTER_SetRunInStandby(uint32_t sercomIndex, uint32_t runInStandby)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CM.CTRLA.bit.RUNSTDBY = (runInStandby != 0);
}


void LL_SERCOM_I2C_MASTER_SetDebugControl(uint32_t sercomIndex, uint32_t runInDebugHaltedState)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CM.DBGCTRL.reg = (runInDebugHaltedState != 0) << SERCOM_I2CM_DBGCTRL_DBGSTOP_Pos;
}


void LL_SERCOM_I2C_MASTER_EnableFourWireMode(uint32_t sercomIndex, uint32_t enable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CM.CTRLA.bit.PINOUT = (enable != 0);
}


void LL_SERCOM_I2C_MASTER_SetMode(uint32_t sercomIndex, enum LL_SERCOM_I2C_MASTER_Mode mode)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CM.CTRLA.bit.MODE = mode & 0x7;
}


void LL_SERCOM_I2C_MASTER_SetSpeed(uint32_t sercomIndex, enum LL_SERCOM_I2C_MASTER_Speed speed)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CM.CTRLA.bit.SPEED = speed & 0x3;
}


void LL_SERCOM_I2C_MASTER_SetClockStretchMode(uint32_t sercomIndex, enum LL_SERCOM_I2C_MASTER_ClockStretchMode mode)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CM.CTRLA.bit.SCLSM = mode & 0x1;
}


void LL_SERCOM_I2C_MASTER_SetTimings(uint32_t sercomIndex,
                                     enum LL_SERCOM_I2C_MASTER_SDAHoldTime sdaHoldTime,
                                     uint32_t masterSCLLowExtendTimeOutEnable,
                                     uint32_t slaveSCLLowExtendTimeOutEnable,
                                     enum LL_SERCOM_I2C_MASTER_InactiveTimeOut inactiveTimeOut,
                                     uint32_t sclLowTimeOutEnable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    SERCOM_I2CM_CTRLA_Type ctrla = pSercom->I2CM.CTRLA;

    ctrla.bit.SDAHOLD = sdaHoldTime & 0x3;
    ctrla.bit.MEXTTOEN = (masterSCLLowExtendTimeOutEnable != 0);
    ctrla.bit.SEXTTOEN = (slaveSCLLowExtendTimeOutEnable != 0);
    ctrla.bit.INACTOUT = inactiveTimeOut & 0x3;
    ctrla.bit.LOWTOUTEN = (sclLowTimeOutEnable != 0);

    pSercom->I2CM.CTRLA = ctrla;
}


void LL_SERCOM_I2C_MASTER_SetBaudRate(uint32_t sercomIndex, uint8_t baudRate, uint8_t baudRateLow, uint8_t highSpeedBaudRate, uint8_t highSpeedBaudRateLow)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    SERCOM_I2CM_BAUD_Type baudReg;

    baudReg.bit.BAUD = baudRate;
    baudReg.bit.BAUDLOW = baudRateLow;
    baudReg.bit.HSBAUD = highSpeedBaudRate;
    baudReg.bit.HSBAUDLOW = highSpeedBaudRateLow;

    pSercom->I2CM.BAUD = baudReg;
}


void LL_SERCOM_I2C_MASTER_SetAcknowledgeAction(uint32_t sercomIndex, enum LL_SERCOM_I2C_MASTER_AcknowledgeAction action)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CM.CTRLB.bit.ACKACT = action & 0x1;
}


void LL_SERCOM_I2C_MASTER_SetAddress(uint32_t sercomIndex, uint32_t address, uint32_t tenBitAddressingEnable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    SERCOM_I2CM_ADDR_Type addrReg = pSercom->I2CM.ADDR;

    addrReg.bit.ADDR = address & 0x7FF;
    addrReg.bit.TENBITEN = (tenBitAddressingEnable != 0);

    pSercom->I2CM.ADDR = addrReg;

    while (pSercom->I2CM.SYNCBUSY.bit.SYSOP == 1)
    {
    }
}


void LL_SERCOM_I2C_MASTER_SetTransactionLength(uint32_t sercomIndex, uint32_t transactionLength, uint32_t transferLengthEnable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    SERCOM_I2CM_ADDR_Type addrReg = pSercom->I2CM.ADDR;

    addrReg.bit.LEN = transactionLength & 0xFF;
    addrReg.bit.LENEN = (transferLengthEnable != 0);

    pSercom->I2CM.ADDR = addrReg;

    while (pSercom->I2CM.SYNCBUSY.bit.SYSOP == 1)
    {
    }
}


void LL_SERCOM_I2C_MASTER_EnableSmartMode(uint32_t sercomIndex, uint32_t enable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CM.CTRLB.bit.SMEN = (enable != 0);
}


void LL_SERCOM_I2C_MASTER_EnableQuickCommand(uint32_t sercomIndex, uint32_t enable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CM.CTRLB.bit.QCEN = (enable != 0);
}


void LL_SERCOM_I2C_MASTER_TriggerCommand(uint32_t sercomIndex, enum LL_SERCOM_I2C_MASTER_Command command)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CM.CTRLB.bit.CMD = command & 0x3;

    while (pSercom->I2CM.SYNCBUSY.bit.SYSOP == 1)
    {
    }
}


void LL_SERCOM_I2C_MASTER_SetBusStateIdle(uint32_t sercomIndex)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    if (pSercom->I2CM.STATUS.bit.BUSSTATE == 0x0)
    {
        pSercom->I2CM.STATUS.bit.BUSSTATE = 0x1;

        while (pSercom->I2CM.SYNCBUSY.bit.SYSOP == 1)
        {
        }
    }
}


enum LL_SERCOM_I2C_MASTER_Status LL_SERCOM_I2C_MASTER_GetStatus(uint32_t sercomIndex)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    return pSercom->I2CM.STATUS.reg;
}


void LL_SERCOM_I2C_MASTER_ClearStatus(uint32_t sercomIndex, enum LL_SERCOM_I2C_MASTER_Status status)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CM.STATUS.reg = status;

    while (pSercom->I2CM.SYNCBUSY.bit.SYSOP == 1)
    {
    }
}


uint8_t LL_SERCOM_I2C_MASTER_ReadData(uint32_t sercomIndex)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    return (uint8_t)pSercom->I2CM.DATA.reg;
}


void LL_SERCOM_I2C_MASTER_WriteData(uint32_t sercomIndex, uint8_t dataTx)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CM.DATA.reg = (uint16_t)dataTx;

    while (pSercom->I2CM.SYNCBUSY.bit.SYSOP == 1)
    {
    }
}


void LL_SERCOM_I2C_MASTER_InterruptEnableClear(uint32_t sercomIndex, enum LL_SERCOM_I2C_MASTER_InterruptFlag interruptFlag)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CM.INTENCLR.reg = interruptFlag;
}


void LL_SERCOM_I2C_MASTER_InterruptEnableSet(uint32_t sercomIndex, enum LL_SERCOM_I2C_MASTER_InterruptFlag interruptFlag)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CM.INTENSET.reg = interruptFlag;
}


enum LL_SERCOM_I2C_MASTER_InterruptFlag LL_SERCOM_I2C_MASTER_GetInterruptFlag(uint32_t sercomIndex)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    return pSercom->I2CM.INTFLAG.reg;
}


void LL_SERCOM_I2C_MASTER_ClearInterruptFlag(uint32_t sercomIndex, enum LL_SERCOM_I2C_MASTER_InterruptFlag interruptFlag)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->I2CM.INTFLAG.reg = interruptFlag;
}
