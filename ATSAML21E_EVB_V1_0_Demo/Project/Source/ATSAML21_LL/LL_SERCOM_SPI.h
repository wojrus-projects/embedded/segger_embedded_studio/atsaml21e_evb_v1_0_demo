//
// Peripheral Driver Low Layer (SERCOM_SPI module).
//

#ifndef LL_SERCOM_SPI_INCLUDED
#define LL_SERCOM_SPI_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


// Number of SERCOM-SPI modules
#define LL_SERCOM_SPI_NUM     SERCOM_INST_NUM


enum LL_SERCOM_SPI_DataOutPinput
{
    LL_SERCOM_SPI_DataOutPinput_DO0_SCK1_SS2 = 0x0,
    LL_SERCOM_SPI_DataOutPinput_DO2_SCK3_SS1 = 0x1,
    LL_SERCOM_SPI_DataOutPinput_DO3_SCK1_SS2 = 0x2,
    LL_SERCOM_SPI_DataOutPinput_DO0_SCK3_SS1 = 0x3
};


enum LL_SERCOM_SPI_DataInPinput
{
    LL_SERCOM_SPI_DataInPinput_DI0 = 0x0,
    LL_SERCOM_SPI_DataInPinput_DI1 = 0x1,
    LL_SERCOM_SPI_DataInPinput_DI2 = 0x2,
    LL_SERCOM_SPI_DataInPinput_DI3 = 0x3
};


enum LL_SERCOM_SPI_Mode
{
    LL_SERCOM_SPI_Mode_Slave  = 0x2,
    LL_SERCOM_SPI_Mode_Master = 0x3
};


enum LL_SERCOM_SPI_FrameFormat
{
    LL_SERCOM_SPI_FrameFormat_SPI      = 0x0,
    LL_SERCOM_SPI_FrameFormat_SPI_ADDR = 0x2
};


enum LL_SERCOM_SPI_ClockPhase
{
    LL_SERCOM_SPI_ClockPhase_LeadingEdge  = 0x0,
    LL_SERCOM_SPI_ClockPhase_TrailingEdge = 0x1
};


enum LL_SERCOM_SPI_ClockPolarity
{
    LL_SERCOM_SPI_ClockPolarity_Low  = 0x0,
    LL_SERCOM_SPI_ClockPolarity_High = 0x1
};


enum LL_SERCOM_SPI_DataOrder
{
    LL_SERCOM_SPI_DataOrder_FirstMSB = 0x0,
    LL_SERCOM_SPI_DataOrder_FirstLSB = 0x1
};


enum LL_SERCOM_SPI_CharacterSize
{
    LL_SERCOM_SPI_CharacterSize_8BIT = 0x0,
    LL_SERCOM_SPI_CharacterSize_9BIT = 0x1
};


enum LL_SERCOM_SPI_AddressMode
{
    LL_SERCOM_SPI_AddressMode_MASK    = 0x0,
    LL_SERCOM_SPI_AddressMode_2_ADDRS = 0x1,
    LL_SERCOM_SPI_AddressMode_RANGE   = 0x2
};


enum LL_SERCOM_SPI_Status
{
    LL_SERCOM_SPI_Status_BUFOVF = 1 << 2
};


enum LL_SERCOM_SPI_InterruptFlag
{
    LL_SERCOM_SPI_InterruptFlag_DRE   = 1 << 0,
    LL_SERCOM_SPI_InterruptFlag_TXC   = 1 << 1,
    LL_SERCOM_SPI_InterruptFlag_RXC   = 1 << 2,
    LL_SERCOM_SPI_InterruptFlag_SSL   = 1 << 3,
    LL_SERCOM_SPI_InterruptFlag_ERROR = 1 << 7
};


void LL_SERCOM_SPI_Reset(uint32_t sercomIndex);
void LL_SERCOM_SPI_Enable(uint32_t sercomIndex, uint32_t enable);
void LL_SERCOM_SPI_SetRunInStandby(uint32_t sercomIndex, uint32_t runInStandby);
void LL_SERCOM_SPI_SetDebugControl(uint32_t sercomIndex, uint32_t runInDebugHaltedState);

void LL_SERCOM_SPI_SetPins(uint32_t sercomIndex, enum LL_SERCOM_SPI_DataOutPinput dataOutPinout, enum LL_SERCOM_SPI_DataInPinput dataInPinout);
void LL_SERCOM_SPI_SetMode(uint32_t sercomIndex, enum LL_SERCOM_SPI_Mode mode);
void LL_SERCOM_SPI_SetDataFormat(uint32_t sercomIndex, enum LL_SERCOM_SPI_FrameFormat frameFormat, enum LL_SERCOM_SPI_ClockPhase clockPhase, enum LL_SERCOM_SPI_ClockPolarity clockPolarity, enum LL_SERCOM_SPI_DataOrder dataOrder);
void LL_SERCOM_SPI_SetBaudRate(uint32_t sercomIndex, uint8_t baudRate);
void LL_SERCOM_SPI_SetAddressMode(uint32_t sercomIndex, enum LL_SERCOM_SPI_AddressMode addressMode);
void LL_SERCOM_SPI_SetAddress(uint32_t sercomIndex, uint8_t address, uint8_t addressMask);
void LL_SERCOM_SPI_SetCharacterSize(uint32_t sercomIndex, enum LL_SERCOM_SPI_CharacterSize size);
void LL_SERCOM_SPI_EnableSlaveDataPreload(uint32_t sercomIndex, uint32_t enable);
void LL_SERCOM_SPI_SetSlaveSelect(uint32_t sercomIndex, uint32_t slaveSelectLowDetectEnable, uint32_t masterSlaveSelectEnable);
void LL_SERCOM_SPI_SetImmediateBufferOverflowNotification(uint32_t sercomIndex, uint32_t state);
void LL_SERCOM_SPI_EnableReceiver(uint32_t sercomIndex, uint32_t enable);

enum LL_SERCOM_SPI_Status LL_SERCOM_SPI_GetStatus(uint32_t sercomIndex);

uint16_t LL_SERCOM_SPI_ReadData(uint32_t sercomIndex);
void LL_SERCOM_SPI_WriteData(uint32_t sercomIndex, uint16_t dataTx);

void LL_SERCOM_SPI_InterruptEnableClear(uint32_t sercomIndex, enum LL_SERCOM_SPI_InterruptFlag interruptFlag);
void LL_SERCOM_SPI_InterruptEnableSet(uint32_t sercomIndex, enum LL_SERCOM_SPI_InterruptFlag interruptFlag);
enum LL_SERCOM_SPI_InterruptFlag LL_SERCOM_SPI_GetInterruptEnableState(uint32_t sercomIndex);
enum LL_SERCOM_SPI_InterruptFlag LL_SERCOM_SPI_GetInterruptFlag(uint32_t sercomIndex);
void LL_SERCOM_SPI_ClearInterruptFlag(uint32_t sercomIndex, enum LL_SERCOM_SPI_InterruptFlag interruptFlag);


#ifdef __cplusplus
}
#endif

#endif // LL_SERCOM_SPI_INCLUDED
