//
// Peripheral Driver Low Layer (SERCOM_USART module).
//

#ifndef LL_SERCOM_USART_INCLUDED
#define LL_SERCOM_USART_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


// Number of SERCOM-USART modules
#define LL_SERCOM_USART_NUM     SERCOM_INST_NUM


enum LL_SERCOM_USART_TransmitDataPinout
{
    LL_SERCOM_USART_TransmitDataPinout_TxD0_XCK1      = 0x0,
    LL_SERCOM_USART_TransmitDataPinout_TxD2_XCK3      = 0x1,
    LL_SERCOM_USART_TransmitDataPinout_TxD0_RTS2_CTS3 = 0x2,
    LL_SERCOM_USART_TransmitDataPinout_TxD0_XCK1_RTS2 = 0x3
};


enum LL_SERCOM_USART_ReceiveDataPinout
{
    LL_SERCOM_USART_ReceiveDataPinout_RxD0 = 0x0,
    LL_SERCOM_USART_ReceiveDataPinout_RxD1 = 0x1,
    LL_SERCOM_USART_ReceiveDataPinout_RxD2 = 0x2,
    LL_SERCOM_USART_ReceiveDataPinout_RxD3 = 0x3
};


enum LL_SERCOM_USART_FrameFormat
{
    LL_SERCOM_USART_FrameFormat_USART                    = 0x0,
    LL_SERCOM_USART_FrameFormat_USART_WithParity         = 0x1,
    LL_SERCOM_USART_FrameFormat_BreakAutoBaud            = 0x4,
    LL_SERCOM_USART_FrameFormat_BreakAutoBaud_WithParity = 0x5
};


enum LL_SERCOM_USART_CommunicationMode
{
    LL_SERCOM_USART_CommunicationMode_Asynchronous = 0x0,
    LL_SERCOM_USART_CommunicationMode_Synchronous  = 0x1
};


enum LL_SERCOM_USART_ClockMode
{
    LL_SERCOM_USART_ClockMode_ExternalClock = 0x0,
    LL_SERCOM_USART_ClockMode_InternalClock = 0x1
};


enum LL_SERCOM_USART_ClockPolarity
{
    LL_SERCOM_USART_ClockPolarity_RisingXCKEdge  = 0x0,
    LL_SERCOM_USART_ClockPolarity_FallingXCKEdge = 0x1
};


enum LL_SERCOM_USART_DataOrder
{
    LL_SERCOM_USART_DataOrder_FirstMSB = 0x0,
    LL_SERCOM_USART_DataOrder_FirstLSB = 0x1
};


enum LL_SERCOM_USART_CharacterSize
{
    LL_SERCOM_USART_CharacterSize_8BIT = 0x0,
    LL_SERCOM_USART_CharacterSize_9BIT = 0x1,
    LL_SERCOM_USART_CharacterSize_5BIT = 0x5,
    LL_SERCOM_USART_CharacterSize_6BIT = 0x6,
    LL_SERCOM_USART_CharacterSize_7BIT = 0x7
};


enum LL_SERCOM_USART_StopBit
{
    LL_SERCOM_USART_StopBit_1bit = 0x0,
    LL_SERCOM_USART_StopBit_2bit = 0x1
};


enum LL_SERCOM_USART_Parity
{
    LL_SERCOM_USART_Parity_Even = 0x0,
    LL_SERCOM_USART_Parity_Odd  = 0x1
};


enum LL_SERCOM_USART_Encoding
{
    LL_SERCOM_USART_Encoding_Disabled = 0x0,
    LL_SERCOM_USART_Encoding_IrDA     = 0x1
};


enum LL_SERCOM_USART_SampleRate
{
    LL_SERCOM_USART_SampleRate_16x_Arithmetic = 0x0,
    LL_SERCOM_USART_SampleRate_16x_Fractional = 0x1,
    LL_SERCOM_USART_SampleRate_8x_Arithmetic  = 0x2,
    LL_SERCOM_USART_SampleRate_8x_Fractional  = 0x3,
    LL_SERCOM_USART_SampleRate_3x_Arithmetic  = 0x4
};


enum LL_SERCOM_USART_SampleAdjustment
{
    LL_SERCOM_USART_SampleAdjustment_8x_3_4_5     = 0x0,
    LL_SERCOM_USART_SampleAdjustment_8x_4_5_6     = 0x1,
    LL_SERCOM_USART_SampleAdjustment_8x_5_6_7     = 0x2,
    LL_SERCOM_USART_SampleAdjustment_8x_6_7_8     = 0x3,
    LL_SERCOM_USART_SampleAdjustment_16x_7_8_9    = 0x0,
    LL_SERCOM_USART_SampleAdjustment_16x_9_10_11  = 0x1,
    LL_SERCOM_USART_SampleAdjustment_16x_11_12_13 = 0x2,
    LL_SERCOM_USART_SampleAdjustment_16x_13_14_15 = 0x3
};


enum LL_SERCOM_USART_InterruptFlag
{
    LL_SERCOM_USART_InterruptFlag_DRE   = 1 << 0,
    LL_SERCOM_USART_InterruptFlag_TXC   = 1 << 1,
    LL_SERCOM_USART_InterruptFlag_RXC   = 1 << 2,
    LL_SERCOM_USART_InterruptFlag_RXS   = 1 << 3,
    LL_SERCOM_USART_InterruptFlag_CTSIC = 1 << 4,
    LL_SERCOM_USART_InterruptFlag_RXBRK = 1 << 5,
    LL_SERCOM_USART_InterruptFlag_ERROR = 1 << 7
};


enum LL_SERCOM_USART_Status
{
    LL_SERCOM_USART_Status_PERR   = 1 << 0,
    LL_SERCOM_USART_Status_FERR   = 1 << 1,
    LL_SERCOM_USART_Status_BUFOVF = 1 << 2,
    LL_SERCOM_USART_Status_CTS    = 1 << 3,
    LL_SERCOM_USART_Status_ISF    = 1 << 4,
    LL_SERCOM_USART_Status_COLL   = 1 << 5
};


void LL_SERCOM_USART_Reset(uint32_t sercomIndex);
void LL_SERCOM_USART_Enable(uint32_t sercomIndex, uint32_t enable);
void LL_SERCOM_USART_SetRunInStandby(uint32_t sercomIndex, uint32_t runInStandby);
void LL_SERCOM_USART_SetDebugControl(uint32_t sercomIndex, uint32_t runInDebugHaltedState);

void LL_SERCOM_USART_SetPins(uint32_t sercomIndex, enum LL_SERCOM_USART_TransmitDataPinout transmitDataPinout, enum LL_SERCOM_USART_ReceiveDataPinout receiveDataPinout);
void LL_SERCOM_USART_SetFrameFormat(uint32_t sercomIndex, enum LL_SERCOM_USART_FrameFormat frameFormat);
void LL_SERCOM_USART_SetCommunicationMode(uint32_t sercomIndex, enum LL_SERCOM_USART_CommunicationMode communicationMode);
void LL_SERCOM_USART_SetClockMode(uint32_t sercomIndex, enum LL_SERCOM_USART_ClockMode clockMode);
void LL_SERCOM_USART_SetClockPolarity(uint32_t sercomIndex, enum LL_SERCOM_USART_ClockPolarity clockPolarity);
void LL_SERCOM_USART_SetDataOrder(uint32_t sercomIndex, enum LL_SERCOM_USART_DataOrder dataOrder);
void LL_SERCOM_USART_SetBaud(uint32_t sercomIndex, uint16_t baud);
void LL_SERCOM_USART_SetBaudFractional(uint32_t sercomIndex, uint16_t baud, uint16_t fraction);
void LL_SERCOM_USART_SetSampleRate(uint32_t sercomIndex, enum LL_SERCOM_USART_SampleRate sampleRate);
void LL_SERCOM_USART_SetSampleAdjustment(uint32_t sercomIndex, enum LL_SERCOM_USART_SampleAdjustment sampleAdjustment);
void LL_SERCOM_USART_SetCharacterSize(uint32_t sercomIndex, enum LL_SERCOM_USART_CharacterSize size);
void LL_SERCOM_USART_SetStopBit(uint32_t sercomIndex, enum LL_SERCOM_USART_StopBit stopBit);
void LL_SERCOM_USART_SetParity(uint32_t sercomIndex, enum LL_SERCOM_USART_Parity parity);
void LL_SERCOM_USART_SetEncoding(uint32_t sercomIndex, enum LL_SERCOM_USART_Encoding encoding);
void LL_SERCOM_USART_SetReceivePulseLength(uint32_t sercomIndex, uint32_t pulseLength);
void LL_SERCOM_USART_SetCollisionDetection(uint32_t sercomIndex, uint32_t state);
void LL_SERCOM_USART_SetStartFrameDetection(uint32_t sercomIndex, uint32_t state);
void LL_SERCOM_USART_SetImmediateBufferOverflowNotification(uint32_t sercomIndex, uint32_t state);
void LL_SERCOM_USART_EnableReceiver(uint32_t sercomIndex, uint32_t enable);
void LL_SERCOM_USART_EnableTransmitter(uint32_t sercomIndex, uint32_t enable);

enum LL_SERCOM_USART_Status LL_SERCOM_USART_GetStatus(uint32_t sercomIndex);

uint16_t LL_SERCOM_USART_ReadData(uint32_t sercomIndex);
void LL_SERCOM_USART_WriteData(uint32_t sercomIndex, uint16_t dataTx);

void LL_SERCOM_USART_InterruptEnableClear(uint32_t sercomIndex, enum LL_SERCOM_USART_InterruptFlag interruptFlag);
void LL_SERCOM_USART_InterruptEnableSet(uint32_t sercomIndex, enum LL_SERCOM_USART_InterruptFlag interruptFlag);
enum LL_SERCOM_USART_InterruptFlag LL_SERCOM_USART_GetInterruptFlag(uint32_t sercomIndex);
void LL_SERCOM_USART_ClearInterruptFlag(uint32_t sercomIndex, enum LL_SERCOM_USART_InterruptFlag interruptFlag);


#ifdef __cplusplus
}
#endif

#endif // LL_SERCOM_USART_INCLUDED
