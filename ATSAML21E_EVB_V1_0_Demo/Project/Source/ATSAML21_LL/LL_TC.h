//
// Peripheral Driver Low Layer (TC module).
//

#ifndef LL_TC_INCLUDED
#define LL_TC_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


// Number of TC timers
#define LL_TC_NUM       TC_INST_NUM


enum LL_TC_TimerMode
{
    LL_TC_TimerMode_COUNT16 = 0x0,
    LL_TC_TimerMode_COUNT8  = 0x1,
    LL_TC_TimerMode_COUNT32 = 0x2
};


enum LL_TC_CounterPrescaler
{
    LL_TC_CounterPrescaler_DIV1    = 0x0,
    LL_TC_CounterPrescaler_DIV2    = 0x1,
    LL_TC_CounterPrescaler_DIV4    = 0x2,
    LL_TC_CounterPrescaler_DIV8    = 0x3,
    LL_TC_CounterPrescaler_DIV16   = 0x4,
    LL_TC_CounterPrescaler_DIV64   = 0x5,
    LL_TC_CounterPrescaler_DIV256  = 0x6,
    LL_TC_CounterPrescaler_DIV1024 = 0x7
};


enum LL_TC_Presynchronization
{
    LL_TC_Presynchronization_GCLK   = 0x0,
    LL_TC_Presynchronization_PRESC  = 0x1,
    LL_TC_Presynchronization_RESYNC = 0x2
};


enum LL_TC_CaptureMode
{
    LL_TC_CaptureMode_OnEvent = 0,
    LL_TC_CaptureMode_OnPin   = 1
};


enum LL_TC_WaweformMode
{
    LL_TC_WaweformMode_NormalFrequency = 0x0,
    LL_TC_WaweformMode_MatchFrequency  = 0x1,
    LL_TC_WaweformMode_NormalPWM       = 0x2,
    LL_TC_WaweformMode_MatchPWM        = 0x3
};


enum LL_TC_EventAction
{
    LL_TC_EventAction_OFF       = 0x0,
    LL_TC_EventAction_RETRIGGER = 0x1,
    LL_TC_EventAction_COUNT     = 0x2,
    LL_TC_EventAction_START     = 0x3,
    LL_TC_EventAction_STAMP     = 0x4,
    LL_TC_EventAction_PPW       = 0x5,
    LL_TC_EventAction_PWP       = 0x6,
    LL_TC_EventAction_PW        = 0x7
};


enum LL_TC_EventOutput
{
    LL_TC_EventOutput_Disable = 0,
    LL_TC_EventOutput_OVFEO   = 1 << 8,
    LL_TC_EventOutput_MCEO0   = 1 << 12,
    LL_TC_EventOutput_MCEO1   = 1 << 13
};


enum LL_TC_Command
{
    LL_TC_Command_NONE      = 0x0,
    LL_TC_Command_RETRIGGER = 0x1,
    LL_TC_Command_STOP      = 0x2,
    LL_TC_Command_UPDATE    = 0x3,
    LL_TC_Command_READSYNC  = 0x4
};


enum LL_TC_Status
{
    LL_TC_Status_STOP    = 1 << 0,
    LL_TC_Status_SLAVE   = 1 << 1,
    LL_TC_Status_PERBUFV = 1 << 3,
    LL_TC_Status_CCBUFV0 = 1 << 4,
    LL_TC_Status_CCBUFV1 = 1 << 5
};


enum LL_TC_InterruptFlag
{
    LL_TC_InterruptFlag_OVF = 1 << 0,
    LL_TC_InterruptFlag_ERR = 1 << 1,
    LL_TC_InterruptFlag_MC0 = 1 << 4,
    LL_TC_InterruptFlag_MC1 = 1 << 5
};


void LL_TC_Reset(uint32_t timerIndex);
void LL_TC_Enable(uint32_t timerIndex, uint32_t state);
void LL_TC_SetRunInStandby(uint32_t timerIndex, uint32_t runInStandby);
void LL_TC_SetDebugControl(uint32_t timerIndex, uint32_t runInDebugHaltedState);

void LL_TC_Initialize(uint32_t timerIndex, enum LL_TC_TimerMode mode, enum LL_TC_CounterPrescaler prescaler, enum LL_TC_Presynchronization presynchronization);
void LL_TC_EnableCaptureChannel(uint32_t timerIndex, uint32_t captureChannelIndex, uint32_t enable, enum LL_TC_CaptureMode mode);
void LL_TC_SetWaveformMode(uint32_t timerIndex, enum LL_TC_WaweformMode mode);
void LL_TC_SetOutputWaveformInvert(uint32_t timerIndex, uint32_t invertOutput0, uint32_t invertOutput1);
void LL_TC_SetEventAction(uint32_t timerIndex, enum LL_TC_EventAction action);
void LL_TC_SetEventOutput(uint32_t timerIndex, enum LL_TC_EventOutput output);
void LL_TC_SetEventInput(uint32_t timerIndex, uint32_t tcEventEnable, uint32_t inputEventSourceIsInverted);

void LL_TC_SetLockUpdate(uint32_t timerIndex, uint32_t state);
void LL_TC_SetCountingUp(uint32_t timerIndex);
void LL_TC_SetCountingDown(uint32_t timerIndex);
void LL_TC_SetCounterOneShot(uint32_t timerIndex, uint32_t state);
void LL_TC_SetCommand(uint32_t timerIndex, enum LL_TC_Command command);

void LL_TC_SetCounter(uint32_t timerIndex, uint16_t value);
uint16_t LL_TC_GetCounter(uint32_t timerIndex);

void LL_TC_SetCompareCapture(uint32_t timerIndex, uint32_t channelIndex, uint16_t value);
uint16_t LL_TC_GetCompareCapture(uint32_t timerIndex, uint32_t channelIndex);
void LL_TC_SetCompareCaptureBuffer(uint32_t timerIndex, uint32_t channelIndex, uint16_t value);
uint16_t LL_TC_GetCompareCaptureBuffer(uint32_t timerIndex, uint32_t channelIndex);

enum LL_TC_Status LL_TC_GetStatus(uint32_t timerIndex);
void LL_TC_SetStatus(uint32_t timerIndex, enum LL_TC_Status value);

void LL_TC_InterruptEnableClear(uint32_t timerIndex, enum LL_TC_InterruptFlag interruptFlag);
void LL_TC_InterruptEnableSet(uint32_t timerIndex, enum LL_TC_InterruptFlag interruptFlag);
enum LL_TC_InterruptFlag LL_TC_GetInterruptFlag(uint32_t timerIndex);
void LL_TC_ClearInterruptFlag(uint32_t timerIndex, enum LL_TC_InterruptFlag interruptFlag);


#ifdef __cplusplus
}
#endif

#endif // LL_TC_INCLUDED
