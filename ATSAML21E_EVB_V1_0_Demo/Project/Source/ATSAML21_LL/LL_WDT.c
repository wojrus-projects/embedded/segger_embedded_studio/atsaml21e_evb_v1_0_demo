#include <sam.h>
#include "LL_WDT.h"


void LL_WDT_SetConfiguration(enum LL_WDT_TimeOutPeriod timeOutPeriod, enum LL_WDT_WindowTimeOutPeriod windowModeTimeOutPeriod)
{
    WDT_CONFIG_Type config;

    config.bit.PER = timeOutPeriod & 0xF;
    config.bit.WINDOW = windowModeTimeOutPeriod & 0xF;

    WDT->CONFIG = config;
}


void LL_WDT_SetEarlyWarningInterruptTimeOffset(enum LL_WDT_EarlyWarningTimeOffset timeOffset)
{
    WDT_EWCTRL_Type ewctrl;

    ewctrl.reg = 0;
    ewctrl.bit.EWOFFSET = timeOffset & 0xF;

    WDT->EWCTRL = ewctrl;
}


void LL_WDT_Enable(uint32_t enable, uint32_t windowModeEnable, uint32_t alwaysOn)
{
    WDT_CTRLA_Type ctrla;

    ctrla.reg = 0;
    ctrla.bit.ENABLE = (enable != 0);
    ctrla.bit.WEN = (windowModeEnable != 0);
    ctrla.bit.ALWAYSON = (alwaysOn != 0);

    WDT->CTRLA = ctrla;

    while ((WDT->SYNCBUSY.bit.ENABLE == 1) || (WDT->SYNCBUSY.bit.WEN == 1) || (WDT->SYNCBUSY.bit.ALWAYSON == 1))
    {
    }
}


uint32_t LL_WDT_IsEnabled(void)
{
    return (WDT->CTRLA.bit.ENABLE == 1);
}


void LL_WDT_Clear(void)
{
    WDT->CLEAR.reg = 0xA5;

    while (WDT->SYNCBUSY.bit.CLEAR == 1)
    {
    }
}


void LL_WDT_InterruptEnableClear(void)
{
    WDT->INTENCLR.reg = WDT_INTENCLR_EW;
}


void LL_WDT_InterruptEnableSet(void)
{
    WDT->INTENSET.reg = WDT_INTENSET_EW;
}


uint32_t LL_WDT_GetInterruptFlag(void)
{
    return (WDT->INTFLAG.bit.EW == 1);
}


void LL_WDT_ClearInterruptFlag(void)
{
    WDT->INTFLAG.reg = WDT_INTFLAG_EW;
}
