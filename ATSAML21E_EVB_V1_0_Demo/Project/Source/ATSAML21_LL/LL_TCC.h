//
// Peripheral Driver Low Layer (TCC module).
//

#ifndef LL_TCC_INCLUDED
#define LL_TCC_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


// Number of TCC timers
#define LL_TCC_NUM       TCC_INST_NUM


enum LL_TCC_CounterPrescaler
{
    LL_TCC_CounterPrescaler_DIV1    = 0x0,
    LL_TCC_CounterPrescaler_DIV2    = 0x1,
    LL_TCC_CounterPrescaler_DIV4    = 0x2,
    LL_TCC_CounterPrescaler_DIV8    = 0x3,
    LL_TCC_CounterPrescaler_DIV16   = 0x4,
    LL_TCC_CounterPrescaler_DIV64   = 0x5,
    LL_TCC_CounterPrescaler_DIV256  = 0x6,
    LL_TCC_CounterPrescaler_DIV1024 = 0x7
};


enum LL_TCC_Presynchronization
{
    LL_TCC_Presynchronization_GCLK   = 0x0,
    LL_TCC_Presynchronization_PRESC  = 0x1,
    LL_TCC_Presynchronization_RESYNC = 0x2
};


enum LL_TCC_DitheringResolution
{
    LL_TCC_DitheringResolution_NONE  = 0x0,
    LL_TCC_DitheringResolution_DITH4 = 0x1,
    LL_TCC_DitheringResolution_DITH5 = 0x2,
    LL_TCC_DitheringResolution_DITH6 = 0x3
};


enum LL_TCC_EventInput0Action
{
    LL_TCC_EventInput0Action_OFF       = 0x0,
    LL_TCC_EventInput0Action_RETRIGGER = 0x1,
    LL_TCC_EventInput0Action_COUNTEV   = 0x2,
    LL_TCC_EventInput0Action_START     = 0x3,
    LL_TCC_EventInput0Action_INC       = 0x4,
    LL_TCC_EventInput0Action_COUNT     = 0x5,
    LL_TCC_EventInput0Action_STAMP     = 0x6,
    LL_TCC_EventInput0Action_FAULT     = 0x7
};


enum LL_TCC_EventInput1Action
{
    LL_TCC_EventInput1Action_OFF       = 0x0,
    LL_TCC_EventInput1Action_RETRIGGER = 0x1,
    LL_TCC_EventInput1Action_DIR       = 0x2,
    LL_TCC_EventInput1Action_STOP      = 0x3,
    LL_TCC_EventInput1Action_DEC       = 0x4,
    LL_TCC_EventInput1Action_PPW       = 0x5,
    LL_TCC_EventInput1Action_PWP       = 0x6,
    LL_TCC_EventInput1Action_FAULT     = 0x7
};


enum LL_TCC_InterruptEventMode
{
    LL_TCC_InterruptEventMode_BEGIN    = 0x0,
    LL_TCC_InterruptEventMode_END      = 0x1,
    LL_TCC_InterruptEventMode_BETWEEN  = 0x2,
    LL_TCC_InterruptEventMode_BOUNDARY = 0x3
};


enum LL_TCC_Command
{
    LL_TCC_Command_NONE      = 0x0,
    LL_TCC_Command_RETRIGGER = 0x1,
    LL_TCC_Command_STOP      = 0x2,
    LL_TCC_Command_UPDATE    = 0x3,
    LL_TCC_Command_READSYNC  = 0x4
};


enum LL_TCC_RampIndexCommand
{
    LL_TCC_RampIndexCommand_DISABLE = 0x0,
    LL_TCC_RampIndexCommand_SET     = 0x1,
    LL_TCC_RampIndexCommand_CLEAR   = 0x2,
    LL_TCC_RampIndexCommand_HOLD    = 0x3
};


enum LL_TCC_WaveformGenerationOperation
{
    LL_TCC_WaveformGenerationOperation_NFRQ       = 0x0,
    LL_TCC_WaveformGenerationOperation_MFRQ       = 0x1,
    LL_TCC_WaveformGenerationOperation_NPWM       = 0x2,
    LL_TCC_WaveformGenerationOperation_DSCRITICAL = 0x4,
    LL_TCC_WaveformGenerationOperation_DSBOTTOM   = 0x5,
    LL_TCC_WaveformGenerationOperation_DSBOTH     = 0x6,
    LL_TCC_WaveformGenerationOperation_DSTOP      = 0x7
};


enum LL_TCC_RampOperation
{
    LL_TCC_RampOperation_RAMP1  = 0x0,
    LL_TCC_RampOperation_RAMP2A = 0x1,
    LL_TCC_RampOperation_RAMP2  = 0x2,
    LL_TCC_RampOperation_RAMP2C = 0x3
};


enum LL_TCC_OutputMatrix
{
    LL_TCC_OutputMatrix_32103210 = 0x0,
    LL_TCC_OutputMatrix_10101010 = 0x1,
    LL_TCC_OutputMatrix_00000000 = 0x2,
    LL_TCC_OutputMatrix_11111110 = 0x3
};


enum LL_TCC_Status
{
    LL_TCC_Status_STOP     = 1 << 0,
    LL_TCC_Status_IDX      = 1 << 1,
    LL_TCC_Status_UFS      = 1 << 2,
    LL_TCC_Status_DFS      = 1 << 3,
    LL_TCC_Status_PATTBUFV = 1 << 5,
    LL_TCC_Status_PERBUFV  = 1 << 7,
    LL_TCC_Status_FAULTAIN = 1 << 8,
    LL_TCC_Status_FAULTBIN = 1 << 9,
    LL_TCC_Status_FAULT0IN = 1 << 10,
    LL_TCC_Status_FAULT1IN = 1 << 11,
    LL_TCC_Status_FAULTA   = 1 << 12,
    LL_TCC_Status_FAULTB   = 1 << 13,
    LL_TCC_Status_FAULT0   = 1 << 14,
    LL_TCC_Status_FAULT1   = 1 << 15,
    LL_TCC_Status_CCBUFV0  = 1 << 16,
    LL_TCC_Status_CCBUFV1  = 1 << 17,
    LL_TCC_Status_CCBUFV2  = 1 << 18,
    LL_TCC_Status_CCBUFV3  = 1 << 19,
    LL_TCC_Status_CMP0     = 1 << 24,
    LL_TCC_Status_CMP1     = 1 << 25,
    LL_TCC_Status_CMP2     = 1 << 26,
    LL_TCC_Status_CMP3     = 1 << 27
};


enum LL_TCC_InterruptFlag
{
    LL_TCC_InterruptFlag_OVF    = 1 << 0,
    LL_TCC_InterruptFlag_TRG    = 1 << 1,
    LL_TCC_InterruptFlag_CNT    = 1 << 2,
    LL_TCC_InterruptFlag_ERR    = 1 << 3,
    LL_TCC_InterruptFlag_UFS    = 1 << 10,
    LL_TCC_InterruptFlag_DFS    = 1 << 11,
    LL_TCC_InterruptFlag_FAULTA = 1 << 12,
    LL_TCC_InterruptFlag_FAULTB = 1 << 13,
    LL_TCC_InterruptFlag_FAULT0 = 1 << 14,
    LL_TCC_InterruptFlag_FAULT1 = 1 << 15,
    LL_TCC_InterruptFlag_MC0    = 1 << 16,
    LL_TCC_InterruptFlag_MC1    = 1 << 17,
    LL_TCC_InterruptFlag_MC2    = 1 << 18,
    LL_TCC_InterruptFlag_MC3    = 1 << 19
};


void LL_TCC_Reset(uint32_t timerIndex);
void LL_TCC_Enable(uint32_t timerIndex, uint32_t state);
void LL_TCC_SetRunInStandby(uint32_t timerIndex, uint32_t runInStandby);
void LL_TCC_SetDebugControl(uint32_t timerIndex, uint32_t runInDebugHaltedState, uint32_t faultDetectionOnDebugBreakDetection);

void LL_TCC_Initialize(uint32_t timerIndex, enum LL_TCC_CounterPrescaler prescaler, enum LL_TCC_Presynchronization presynchronization, enum LL_TCC_DitheringResolution dithering, uint32_t autoLockEnable, uint32_t masterSynchronizationEnable, uint32_t dmaOneShotTriggerEnable);
void LL_TCC_EnableCaptureChannel(uint32_t timerIndex, uint32_t captureChannelIndex, uint32_t enable);
void LL_TCC_SetEventAction(uint32_t timerIndex, enum LL_TCC_EventInput0Action input0Action, enum LL_TCC_EventInput1Action input1Action);
void LL_TCC_SetEventInput(uint32_t timerIndex, uint32_t eventInput0Enable, uint32_t eventInput1Enable, uint32_t eventInput0Invert, uint32_t eventInput1Invert);
void LL_TCC_EnableEventInputMatchCapture(uint32_t timerIndex, uint32_t matchCaptureChannelIndex, uint32_t enable);
void LL_TCC_EnableEventOutputMatchCapture(uint32_t timerIndex, uint32_t matchCaptureChannelIndex, uint32_t enable);
void LL_TCC_EnableEventOutput(uint32_t timerIndex, uint32_t overflowUnderflowEnable, uint32_t retriggerEnable, uint32_t timerCounterEnable);
void LL_TCC_SetTimerCounterInterruptEventMode(uint32_t timerIndex, enum LL_TCC_InterruptEventMode mode);

void LL_TCC_Waveform_SetGenerationOperation(uint32_t timerIndex, enum LL_TCC_WaveformGenerationOperation operation);
void LL_TCC_Waveform_SetRampOperation(uint32_t timerIndex, enum LL_TCC_RampOperation operation);
void LL_TCC_Waveform_EnableCircularPeriod(uint32_t timerIndex, uint32_t state);
void LL_TCC_Waveform_EnableCircularCompareCapture(uint32_t timerIndex, uint32_t cc0State, uint32_t cc1State, uint32_t cc2State, uint32_t cc3State);
void LL_TCC_Waveform_SetChannelPolarity(uint32_t timerIndex, uint32_t ch0State, uint32_t ch1State, uint32_t ch2State, uint32_t ch3State);
void LL_TCC_Waveform_SwapDTIOutputPair(uint32_t timerIndex, uint32_t out0State, uint32_t out1State, uint32_t out2State, uint32_t out3State);

void LL_TCC_WaveformExtension_SetOutputMatrix(uint32_t timerIndex, enum LL_TCC_OutputMatrix value);
void LL_TCC_WaveformExtension_SetDeadTime(uint32_t timerIndex, uint32_t dt0Enable, uint32_t dt1Enable, uint32_t dt2Enable, uint32_t dt3Enable, uint32_t lowSideClockCycles, uint32_t highSideClockCycles);

void LL_TCC_SetWaveformOutputInversion(uint32_t timerIndex, uint32_t wo0, uint32_t wo1, uint32_t wo2, uint32_t wo3, uint32_t wo4, uint32_t wo5, uint32_t wo6, uint32_t wo7);

void LL_TCC_SetLockUpdate(uint32_t timerIndex, uint32_t state);
void LL_TCC_SetCountingUp(uint32_t timerIndex);
void LL_TCC_SetCountingDown(uint32_t timerIndex);
void LL_TCC_SetCounterOneShot(uint32_t timerIndex, uint32_t state);
void LL_TCC_SetCommand(uint32_t timerIndex, enum LL_TCC_Command command);
void LL_TCC_SetRampIndexCommand(uint32_t timerIndex, enum LL_TCC_RampIndexCommand command);

void LL_TCC_SetCounter(uint32_t timerIndex, uint32_t value);
uint32_t LL_TCC_GetCounter(uint32_t timerIndex);

void LL_TCC_SetPeriod(uint32_t timerIndex, uint32_t value);
uint32_t LL_TCC_GetPeriod(uint32_t timerIndex);
void LL_TCC_SetPeriodBuffer(uint32_t timerIndex, uint32_t value);
uint32_t LL_TCC_GetPeriodBuffer(uint32_t timerIndex);

void LL_TCC_SetCompareCapture(uint32_t timerIndex, uint32_t channelIndex, uint32_t value);
uint32_t LL_TCC_GetCompareCapture(uint32_t timerIndex, uint32_t channelIndex);
void LL_TCC_SetCompareCaptureBuffer(uint32_t timerIndex, uint32_t channelIndex, uint32_t value);
uint32_t LL_TCC_GetCompareCaptureBuffer(uint32_t timerIndex, uint32_t channelIndex);

enum LL_TCC_Status LL_TCC_GetStatus(uint32_t timerIndex);
void LL_TCC_SetStatus(uint32_t timerIndex, enum LL_TCC_Status value);

void LL_TCC_InterruptEnableClear(uint32_t timerIndex, enum LL_TCC_InterruptFlag interruptFlag);
void LL_TCC_InterruptEnableSet(uint32_t timerIndex, enum LL_TCC_InterruptFlag interruptFlag);
enum LL_TCC_InterruptFlag LL_TCC_GetInterruptFlag(uint32_t timerIndex);
void LL_TCC_ClearInterruptFlag(uint32_t timerIndex, enum LL_TCC_InterruptFlag interruptFlag);


#ifdef __cplusplus
}
#endif

#endif // LL_TCC_INCLUDED
