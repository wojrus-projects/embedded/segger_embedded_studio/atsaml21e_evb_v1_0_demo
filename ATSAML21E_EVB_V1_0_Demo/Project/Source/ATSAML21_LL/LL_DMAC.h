//
// Peripheral Driver Low Layer (DMAC module).
//

#ifndef LL_DMAC_INCLUDED
#define LL_DMAC_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


// Number of channels
#define LL_DMAC_CHANNEL_NUM                         DMAC_CH_NUM

// Array of descriptors
#define LL_DMAC_DESCRIPTOR_ARRAY(name, items)       __attribute__ ((section(".lpram"), aligned(16))) DmacDescriptor name[items]


enum LL_DMAC_ArbitrationMode
{
    LL_DMAC_ArbitrationMode_Static     = 0,
    LL_DMAC_ArbitrationMode_RoundRobin = 1
};


enum LL_DMAC_ChannelCommand
{
    LL_DMAC_ChannelCommand_NOACT   = 0x0,
    LL_DMAC_ChannelCommand_SUSPEND = 0x1,
    LL_DMAC_ChannelCommand_RESUME  = 0x2
};


enum LL_DMAC_ChannelTriggerSource
{
    LL_DMAC_ChannelTriggerSource_DISABLE    = 0x00,
    LL_DMAC_ChannelTriggerSource_SERCOM0_RX = 0x01,
    LL_DMAC_ChannelTriggerSource_SERCOM0_TX = 0x02,
    LL_DMAC_ChannelTriggerSource_SERCOM1_RX = 0x03,
    LL_DMAC_ChannelTriggerSource_SERCOM1_TX = 0x04,
    LL_DMAC_ChannelTriggerSource_SERCOM2_RX = 0x05,
    LL_DMAC_ChannelTriggerSource_SERCOM2_TX = 0x06,
    LL_DMAC_ChannelTriggerSource_SERCOM3_RX = 0x07,
    LL_DMAC_ChannelTriggerSource_SERCOM3_TX = 0x08,
    LL_DMAC_ChannelTriggerSource_SERCOM4_RX = 0x09,
    LL_DMAC_ChannelTriggerSource_SERCOM4_TX = 0x0A,
    LL_DMAC_ChannelTriggerSource_TCC0_OVF   = 0x0B,
    LL_DMAC_ChannelTriggerSource_TCC0_MC0   = 0x0C,
    LL_DMAC_ChannelTriggerSource_TCC0_MC1   = 0x0D,
    LL_DMAC_ChannelTriggerSource_TCC0_MC2   = 0x0E,
    LL_DMAC_ChannelTriggerSource_TCC0_MC3   = 0x0F,
    LL_DMAC_ChannelTriggerSource_TCC1_OVF   = 0x10,
    LL_DMAC_ChannelTriggerSource_TCC1_MC0   = 0x11,
    LL_DMAC_ChannelTriggerSource_TCC1_MC1   = 0x12,
    LL_DMAC_ChannelTriggerSource_TCC2_OVF   = 0x13,
    LL_DMAC_ChannelTriggerSource_TCC2_MC0   = 0x14,
    LL_DMAC_ChannelTriggerSource_TCC2_MC1   = 0x15,
    LL_DMAC_ChannelTriggerSource_TC0_OVF    = 0x16,
    LL_DMAC_ChannelTriggerSource_TC0_MC0    = 0x17,
    LL_DMAC_ChannelTriggerSource_TC0_MC1    = 0x18,
    LL_DMAC_ChannelTriggerSource_TC1_OVF    = 0x19,
    LL_DMAC_ChannelTriggerSource_TC1_MC0    = 0x1A,
    LL_DMAC_ChannelTriggerSource_TC1_MC1    = 0x1B,
    LL_DMAC_ChannelTriggerSource_TC2_OVF    = 0x1C,
    LL_DMAC_ChannelTriggerSource_TC2_MC0    = 0x1D,
    LL_DMAC_ChannelTriggerSource_TC2_MC1    = 0x1E,
    LL_DMAC_ChannelTriggerSource_TC3_OVF    = 0x1F,
    LL_DMAC_ChannelTriggerSource_TC3_MC0    = 0x20,
    LL_DMAC_ChannelTriggerSource_TC3_MC1    = 0x21,
    LL_DMAC_ChannelTriggerSource_TC4_OVF    = 0x22,
    LL_DMAC_ChannelTriggerSource_TC4_MC0    = 0x23,
    LL_DMAC_ChannelTriggerSource_TC4_MC1    = 0x24,
    LL_DMAC_ChannelTriggerSource_ADC_RESRDY = 0x25,
    LL_DMAC_ChannelTriggerSource_DAC0_EMPTY = 0x26,
    LL_DMAC_ChannelTriggerSource_DAC1_EMPTY = 0x27,
    LL_DMAC_ChannelTriggerSource_AES_WR     = 0x2C,
    LL_DMAC_ChannelTriggerSource_AES_RD     = 0x2D
};


enum LL_DMAC_ChannelTriggerAction
{
    LL_DMAC_ChannelTriggerAction_BLOCK       = 0x0,
    LL_DMAC_ChannelTriggerAction_BEAT        = 0x2,
    LL_DMAC_ChannelTriggerAction_TRANSACTION = 0x3
};


enum LL_DMAC_ChannelEventAction
{
    LL_DMAC_ChannelEventAction_NOACT   = 0x0,
    LL_DMAC_ChannelEventAction_TRIG    = 0x1,
    LL_DMAC_ChannelEventAction_CTRIG   = 0x2,
    LL_DMAC_ChannelEventAction_CBLOCK  = 0x3,
    LL_DMAC_ChannelEventAction_SUSPEND = 0x4,
    LL_DMAC_ChannelEventAction_RESUME  = 0x5,
    LL_DMAC_ChannelEventAction_SSKIP   = 0x6
};


enum LL_DMAC_ArbitrationLevel
{
    LL_DMAC_ArbitrationLevel_LVL0 = 0x0,
    LL_DMAC_ArbitrationLevel_LVL1 = 0x1,
    LL_DMAC_ArbitrationLevel_LVL2 = 0x2,
    LL_DMAC_ArbitrationLevel_LVL3 = 0x3
};


enum LL_DMAC_ChannelStatus
{
    LL_DMAC_ChannelStatus_PEND = 1 << 0,
    LL_DMAC_ChannelStatus_BUSY = 1 << 1,
    LL_DMAC_ChannelStatus_FERR = 1 << 2
};


enum LL_DMAC_ChannelInterruptFlag
{
    LL_DMAC_ChannelInterruptFlag_TERR  = 1 << 0,
    LL_DMAC_ChannelInterruptFlag_TCMPL = 1 << 1,
    LL_DMAC_ChannelInterruptFlag_SUSP  = 1 << 2
};


enum LL_DMAC_TransferBeatSize
{
    LL_DMAC_TransferBeatSize_BYTE  = 0x0,
    LL_DMAC_TransferBeatSize_HWORD = 0x1,
    LL_DMAC_TransferBeatSize_WORD  = 0x2
};


enum LL_DMAC_TransferStepSelection
{
    LL_DMAC_TransferStepSelection_DST = 0x0,
    LL_DMAC_TransferStepSelection_SRC = 0x1
};


enum LL_DMAC_TransferStepSize
{
    LL_DMAC_TransferStepSize_X1   = 0x0,
    LL_DMAC_TransferStepSize_X2   = 0x1,
    LL_DMAC_TransferStepSize_X4   = 0x2,
    LL_DMAC_TransferStepSize_X8   = 0x3,
    LL_DMAC_TransferStepSize_X16  = 0x4,
    LL_DMAC_TransferStepSize_X32  = 0x5,
    LL_DMAC_TransferStepSize_X64  = 0x6,
    LL_DMAC_TransferStepSize_X128 = 0x7
};


enum LL_DMAC_TransferBlockAction
{
    LL_DMAC_TransferBlockAction_NOACT   = 0x0,
    LL_DMAC_TransferBlockAction_INT     = 0x1,
    LL_DMAC_TransferBlockAction_SUSPEND = 0x2,
    LL_DMAC_TransferBlockAction_BOTH    = 0x3
};


enum LL_DMAC_TransferEventOutput
{
    LL_DMAC_TransferEventOutput_DISABLE = 0x0,
    LL_DMAC_TransferEventOutput_BLOCK   = 0x1,
    LL_DMAC_TransferEventOutput_BEAT    = 0x3
};


void LL_DMAC_Reset(void);
void LL_DMAC_Enable(uint32_t enable);
void LL_DMAC_SetDebugControl(uint32_t runInDebugHaltedState);
void LL_DMAC_SetDescriptorMemoryBaseAddress(DmacDescriptor *pBaseAddress);
void LL_DMAC_SetWriteBackMemoryBaseAddress(DmacDescriptor *pBaseAddress);
void LL_DMAC_SetArbitrationMode(enum LL_DMAC_ArbitrationMode level0, enum LL_DMAC_ArbitrationMode level1, enum LL_DMAC_ArbitrationMode level2, enum LL_DMAC_ArbitrationMode level3);

void LL_DMAC_SoftwareTrigger(uint32_t channelBitmap);
uint32_t LL_DMAC_GetBusyChannels(void);
uint32_t LL_DMAC_GetPendingChannels(void);
uint32_t LL_DMAC_GetInterruptStatus(void);

void LL_DMAC_Channel_Reset(uint32_t channelIndex);
void LL_DMAC_Channel_Enable(uint32_t channelIndex, uint32_t enable, uint32_t runInStandby);
void LL_DMAC_Channel_SetCommand(uint32_t channelIndex, enum LL_DMAC_ChannelCommand command);
void LL_DMAC_Channel_SetTrigger(uint32_t channelIndex, enum LL_DMAC_ChannelTriggerSource triggerSource, enum LL_DMAC_ChannelTriggerAction triggerAction);
void LL_DMAC_Channel_SetEvent(uint32_t channelIndex, uint32_t eventOutputEnable, uint32_t eventInputEnable, enum LL_DMAC_ChannelEventAction eventInputAction);
void LL_DMAC_Channel_ArbitrationLevel(uint32_t channelIndex, enum LL_DMAC_ArbitrationLevel arbitrationLevel);
enum LL_DMAC_ChannelStatus LL_DMAC_Channel_GetStatus(uint32_t channelIndex);

void LL_DMAC_Channel_InterruptEnableClear(uint32_t channelIndex, enum LL_DMAC_ChannelInterruptFlag interruptFlag);
void LL_DMAC_Channel_InterruptEnableSet(uint32_t channelIndex, enum LL_DMAC_ChannelInterruptFlag interruptFlag);
enum LL_DMAC_ChannelInterruptFlag LL_DMAC_Channel_GetInterruptFlag(uint32_t channelIndex);
void LL_DMAC_Channel_ClearInterruptFlag(uint32_t channelIndex, enum LL_DMAC_ChannelInterruptFlag interruptFlag);

void LL_DMAC_Descriptor_Initialize(DmacDescriptor *pDescriptor, enum LL_DMAC_TransferBeatSize beatSize, uint32_t sourceAddressIncrementEnable, uint32_t destinationAddressIncrementEnable, enum LL_DMAC_TransferStepSelection stepSelection, enum LL_DMAC_TransferStepSize addressIncrementStepSize, enum LL_DMAC_TransferEventOutput eventOutputSelection, enum LL_DMAC_TransferBlockAction blockAction);
void LL_DMAC_Descriptor_SetBlockTransferCount(DmacDescriptor *pDescriptor, uint32_t blockTransferCount);
void LL_DMAC_Descriptor_SetBlockTransferAddress(DmacDescriptor *pDescriptor, volatile void *pAddressSource, volatile void *pAddressDestination);
void LL_DMAC_Descriptor_SetNextDescriptorAddress(DmacDescriptor *pDescriptor, DmacDescriptor *pNextDescriptorAddress);
void LL_DMAC_Descriptor_SetValid(DmacDescriptor *pDescriptor, uint32_t valid);


#ifdef __cplusplus
}
#endif

#endif // LL_DMAC_INCLUDED
