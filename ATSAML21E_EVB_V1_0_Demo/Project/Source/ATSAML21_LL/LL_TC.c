#include <sam.h>
#include "LL_TC.h"


static Tc* const TC_Instances[TC_INST_NUM] = TC_INSTS;


void LL_TC_Reset(uint32_t timerIndex)
{
    Tc* const pTc = TC_Instances[timerIndex];

    pTc->COUNT16.CTRLA.bit.SWRST = 1;

    while (pTc->COUNT16.SYNCBUSY.bit.SWRST == 1)
    {
    }
}


void LL_TC_Enable(uint32_t timerIndex, uint32_t state)
{
    Tc* const pTc = TC_Instances[timerIndex];

    pTc->COUNT16.CTRLA.bit.ENABLE = (state != 0);

    while (pTc->COUNT16.SYNCBUSY.bit.ENABLE == 1)
    {
    }
}


void LL_TC_SetRunInStandby(uint32_t timerIndex, uint32_t runInStandby)
{
    Tc* const pTc = TC_Instances[timerIndex];

    pTc->COUNT16.CTRLA.bit.RUNSTDBY = (runInStandby != 0);
}


void LL_TC_SetDebugControl(uint32_t timerIndex, uint32_t runInDebugHaltedState)
{
    Tc* const pTc = TC_Instances[timerIndex];

    pTc->COUNT16.DBGCTRL.bit.DBGRUN = (runInDebugHaltedState != 0);
}


void LL_TC_Initialize(uint32_t timerIndex, enum LL_TC_TimerMode mode, enum LL_TC_CounterPrescaler prescaler, enum LL_TC_Presynchronization presynchronization)
{
    Tc* const pTc = TC_Instances[timerIndex];

    TC_CTRLA_Type ctrla = pTc->COUNT16.CTRLA;

    ctrla.bit.MODE = mode & 0x3;
    ctrla.bit.PRESCALER = prescaler & 0x7;
    ctrla.bit.PRESCSYNC = presynchronization & 0x3;

    pTc->COUNT16.CTRLA = ctrla;

    while (pTc->COUNT16.SYNCBUSY.bit.ENABLE == 1)
    {
    }
}


void LL_TC_EnableCaptureChannel(uint32_t timerIndex, uint32_t captureChannelIndex, uint32_t enable, enum LL_TC_CaptureMode mode)
{
    Tc* const pTc = TC_Instances[timerIndex];

    TC_CTRLA_Type ctrla = pTc->COUNT16.CTRLA;

    switch (captureChannelIndex)
    {
        case 0:
            ctrla.bit.COPEN0 = mode & 0x1;
            ctrla.bit.CAPTEN0 = (enable != 0);
            break;

        case 1:
            ctrla.bit.COPEN1 = mode & 0x1;
            ctrla.bit.CAPTEN1 = (enable != 0);
            break;

        default:
            return;
    }

    pTc->COUNT16.CTRLA = ctrla;

    while (pTc->COUNT16.SYNCBUSY.bit.ENABLE == 1)
    {
    }
}


void LL_TC_SetWaveformMode(uint32_t timerIndex, enum LL_TC_WaweformMode mode)
{
    Tc* const pTc = TC_Instances[timerIndex];

    pTc->COUNT16.WAVE.reg = mode & 0x3;
}


void LL_TC_SetOutputWaveformInvert(uint32_t timerIndex, uint32_t invertOutput0, uint32_t invertOutput1)
{
    Tc* const pTc = TC_Instances[timerIndex];

    pTc->COUNT16.DRVCTRL.reg = (invertOutput0 ? TC_DRVCTRL_INVEN0 : 0) | (invertOutput1 ? TC_DRVCTRL_INVEN1 : 0);
}


void LL_TC_SetEventAction(uint32_t timerIndex, enum LL_TC_EventAction action)
{
    Tc* const pTc = TC_Instances[timerIndex];

    TC_EVCTRL_Type evctrl = pTc->COUNT16.EVCTRL;

    evctrl.bit.EVACT = action & 0x7;

    pTc->COUNT16.EVCTRL = evctrl;
}


void LL_TC_SetEventOutput(uint32_t timerIndex, enum LL_TC_EventOutput output)
{
    Tc* const pTc = TC_Instances[timerIndex];

    TC_EVCTRL_Type evctrl = pTc->COUNT16.EVCTRL;

    evctrl.bit.OVFEO = ((output & LL_TC_EventOutput_OVFEO) != 0);
    evctrl.bit.MCEO0 = ((output & LL_TC_EventOutput_MCEO0) != 0);
    evctrl.bit.MCEO1 = ((output & LL_TC_EventOutput_MCEO1) != 0);

    pTc->COUNT16.EVCTRL = evctrl;
}


void LL_TC_SetEventInput(uint32_t timerIndex, uint32_t tcEventEnable, uint32_t inputEventSourceIsInverted)
{
    Tc* const pTc = TC_Instances[timerIndex];

    TC_EVCTRL_Type evctrl = pTc->COUNT16.EVCTRL;

    evctrl.bit.TCEI = (tcEventEnable != 0);
    evctrl.bit.TCINV = (inputEventSourceIsInverted != 0);

    pTc->COUNT16.EVCTRL = evctrl;
}


void LL_TC_SetLockUpdate(uint32_t timerIndex, uint32_t state)
{
    Tc* const pTc = TC_Instances[timerIndex];

    pTc->COUNT16.CTRLBCLR.reg = (state != 0) << TC_CTRLBCLR_LUPD_Pos;

    while (pTc->COUNT16.SYNCBUSY.bit.CTRLB == 1)
    {
    } 
}


void LL_TC_SetCountingUp(uint32_t timerIndex)
{
    Tc* const pTc = TC_Instances[timerIndex];

    pTc->COUNT16.CTRLBCLR.reg = TC_CTRLBCLR_DIR;

    while (pTc->COUNT16.SYNCBUSY.bit.CTRLB == 1)
    {
    }
}


void LL_TC_SetCountingDown(uint32_t timerIndex)
{
    Tc* const pTc = TC_Instances[timerIndex];

    pTc->COUNT16.CTRLBSET.reg = TC_CTRLBSET_DIR;

    while (pTc->COUNT16.SYNCBUSY.bit.CTRLB == 1)
    {
    }
}


void LL_TC_SetCounterOneShot(uint32_t timerIndex, uint32_t state)
{
    Tc* const pTc = TC_Instances[timerIndex];

    if (state == 0)
    {
        pTc->COUNT16.CTRLBCLR.reg = TC_CTRLBCLR_ONESHOT;
    }
    else
    {
        pTc->COUNT16.CTRLBSET.reg = TC_CTRLBSET_ONESHOT;
    }

    while (pTc->COUNT16.SYNCBUSY.bit.CTRLB == 1)
    {
    }
}


void LL_TC_SetCommand(uint32_t timerIndex, enum LL_TC_Command command)
{
    Tc* const pTc = TC_Instances[timerIndex];

    pTc->COUNT16.CTRLBSET.reg = TC_CTRLBCLR_CMD(command);

    while (pTc->COUNT16.SYNCBUSY.bit.CTRLB == 1)
    {
    }
}


void LL_TC_SetCounter(uint32_t timerIndex, uint16_t value)
{
    Tc* const pTc = TC_Instances[timerIndex];

    pTc->COUNT16.COUNT.reg = value;

    while (pTc->COUNT16.SYNCBUSY.bit.COUNT == 1)
    {
    }
}


uint16_t LL_TC_GetCounter(uint32_t timerIndex)
{
    Tc* const pTc = TC_Instances[timerIndex];

    pTc->COUNT16.CTRLBSET.reg = TC_CTRLBSET_CMD_READSYNC;

    while (pTc->COUNT16.SYNCBUSY.bit.CTRLB == 1)
    {
    }

    while (pTc->COUNT16.CTRLBSET.bit.CMD != TC_CTRLBSET_CMD_NONE_Val)
    {
    }

    return pTc->COUNT16.COUNT.reg;
}


void LL_TC_SetCompareCapture(uint32_t timerIndex, uint32_t channelIndex, uint16_t value)
{
    Tc* const pTc = TC_Instances[timerIndex];

    pTc->COUNT16.CC[channelIndex].reg = value;

    while ((pTc->COUNT16.SYNCBUSY.bit.CC0 == 1) || (pTc->COUNT16.SYNCBUSY.bit.CC1 == 1))
    {
    }
}


uint16_t LL_TC_GetCompareCapture(uint32_t timerIndex, uint32_t channelIndex)
{
    Tc* const pTc = TC_Instances[timerIndex];

    return pTc->COUNT16.CC[channelIndex].reg;
}


void LL_TC_SetCompareCaptureBuffer(uint32_t timerIndex, uint32_t channelIndex, uint16_t value)
{
    Tc* const pTc = TC_Instances[timerIndex];

    pTc->COUNT16.CCBUF[channelIndex].reg = value;

    while ((pTc->COUNT16.SYNCBUSY.bit.CC0 == 1) || (pTc->COUNT16.SYNCBUSY.bit.CC1 == 1))
    {
    }
}


uint16_t LL_TC_GetCompareCaptureBuffer(uint32_t timerIndex, uint32_t channelIndex)
{
    Tc* const pTc = TC_Instances[timerIndex];

    return pTc->COUNT16.CCBUF[channelIndex].reg;
}


enum LL_TC_Status LL_TC_GetStatus(uint32_t timerIndex)
{
    Tc* const pTc = TC_Instances[timerIndex];

    return pTc->COUNT16.STATUS.reg;
}


void LL_TC_SetStatus(uint32_t timerIndex, enum LL_TC_Status value)
{
    Tc* const pTc = TC_Instances[timerIndex];

    pTc->COUNT16.STATUS.reg = value;
}


void LL_TC_InterruptEnableClear(uint32_t timerIndex, enum LL_TC_InterruptFlag interruptFlag)
{
    Tc* const pTc = TC_Instances[timerIndex];

    pTc->COUNT16.INTENCLR.reg = interruptFlag;
}


void LL_TC_InterruptEnableSet(uint32_t timerIndex, enum LL_TC_InterruptFlag interruptFlag)
{
    Tc* const pTc = TC_Instances[timerIndex];

    pTc->COUNT16.INTENSET.reg = interruptFlag;
}


enum LL_TC_InterruptFlag LL_TC_GetInterruptFlag(uint32_t timerIndex)
{
    Tc* const pTc = TC_Instances[timerIndex];

    return pTc->COUNT16.INTFLAG.reg;
}


void LL_TC_ClearInterruptFlag(uint32_t timerIndex, enum LL_TC_InterruptFlag interruptFlag)
{
    Tc* const pTc = TC_Instances[timerIndex];

    pTc->COUNT16.INTFLAG.reg = interruptFlag;
}
