#include <sam.h>
#include "LL_MCLK.h"


void LL_MCLK_InitializeClocks(void)
{
    MCLK->AHBMASK.reg = MCLK_AHBMASK_RESETVALUE;
    MCLK->APBAMASK.reg = MCLK_APBAMASK_RESETVALUE;
    MCLK->APBBMASK.reg = MCLK_APBBMASK_RESETVALUE;
    MCLK->APBCMASK.reg = MCLK_APBCMASK_RESETVALUE;
    MCLK->APBDMASK.reg = MCLK_APBDMASK_RESETVALUE;
    MCLK->APBEMASK.reg = MCLK_APBEMASK_RESETVALUE;
}


void LL_MCLK_SetClockDivision(enum LL_MCLK_CPU_Clock_Division cpuClock,
                              enum LL_MCLK_Low_Power_Clock_Division lowPowerClock,
                              enum LL_MCLK_Backup_Clock_Division backupClock)
{
    MCLK->CPUDIV.reg = cpuClock & 0xFF;
    MCLK->LPDIV.reg = lowPowerClock & 0xFF;
    MCLK->BUPDIV.reg = backupClock & 0xFF;
}
