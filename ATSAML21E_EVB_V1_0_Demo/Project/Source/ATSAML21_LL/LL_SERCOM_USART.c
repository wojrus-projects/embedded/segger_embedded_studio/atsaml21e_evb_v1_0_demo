#include <sam.h>
#include "LL_SERCOM_USART.h"


static Sercom* const SERCOM_Instances[SERCOM_INST_NUM] = SERCOM_INSTS;


void LL_SERCOM_USART_Reset(uint32_t sercomIndex)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.CTRLA.bit.SWRST = 1;

    while (pSercom->USART.SYNCBUSY.bit.SWRST == 1)
    {
    }
}


void LL_SERCOM_USART_Enable(uint32_t sercomIndex, uint32_t enable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.CTRLA.bit.ENABLE = (enable != 0);

    while (pSercom->USART.SYNCBUSY.bit.ENABLE == 1)
    {
    }
}


void LL_SERCOM_USART_SetRunInStandby(uint32_t sercomIndex, uint32_t runInStandby)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.CTRLA.bit.RUNSTDBY = (runInStandby != 0);
}


void LL_SERCOM_USART_SetDebugControl(uint32_t sercomIndex, uint32_t runInDebugHaltedState)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.DBGCTRL.reg = (runInDebugHaltedState != 0) << SERCOM_USART_DBGCTRL_DBGSTOP_Pos;
}


void LL_SERCOM_USART_SetPins(uint32_t sercomIndex, enum LL_SERCOM_USART_TransmitDataPinout transmitDataPinout, enum LL_SERCOM_USART_ReceiveDataPinout receiveDataPinout)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    SERCOM_USART_CTRLA_Type ctrla = pSercom->USART.CTRLA;

    ctrla.bit.TXPO = transmitDataPinout & 0x3;
    ctrla.bit.RXPO = receiveDataPinout & 0x3;

    pSercom->USART.CTRLA = ctrla;
}


void LL_SERCOM_USART_SetFrameFormat(uint32_t sercomIndex, enum LL_SERCOM_USART_FrameFormat frameFormat)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.CTRLA.bit.FORM = frameFormat & 0xF;
}


void LL_SERCOM_USART_SetCommunicationMode(uint32_t sercomIndex, enum LL_SERCOM_USART_CommunicationMode communicationMode)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.CTRLA.bit.CMODE = communicationMode & 0x1;
}


void LL_SERCOM_USART_SetClockMode(uint32_t sercomIndex, enum LL_SERCOM_USART_ClockMode clockMode)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.CTRLA.bit.MODE = clockMode & 0x7;
}


void LL_SERCOM_USART_SetClockPolarity(uint32_t sercomIndex, enum LL_SERCOM_USART_ClockPolarity clockPolarity)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.CTRLA.bit.CPOL = clockPolarity & 0x1;
}


void LL_SERCOM_USART_SetDataOrder(uint32_t sercomIndex, enum LL_SERCOM_USART_DataOrder dataOrder)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.CTRLA.bit.DORD = dataOrder & 0x1;
}


void LL_SERCOM_USART_SetBaud(uint32_t sercomIndex, uint16_t baud)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.BAUD.reg = baud;
}


void LL_SERCOM_USART_SetBaudFractional(uint32_t sercomIndex, uint16_t baud, uint16_t fraction)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    SERCOM_USART_BAUD_Type baudReg;

    baudReg.FRAC.BAUD = baud & 0x1FFF;
    baudReg.FRAC.FP = fraction & 0x7;

    pSercom->USART.BAUD = baudReg;
}


void LL_SERCOM_USART_SetSampleRate(uint32_t sercomIndex, enum LL_SERCOM_USART_SampleRate sampleRate)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.CTRLA.bit.SAMPR = sampleRate & 0x7;
}


void LL_SERCOM_USART_SetSampleAdjustment(uint32_t sercomIndex, enum LL_SERCOM_USART_SampleAdjustment sampleAdjustment)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.CTRLA.bit.SAMPA = sampleAdjustment & 0x3;
}


void LL_SERCOM_USART_SetCharacterSize(uint32_t sercomIndex, enum LL_SERCOM_USART_CharacterSize size)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.CTRLB.bit.CHSIZE = size & 0x7;
}


void LL_SERCOM_USART_SetStopBit(uint32_t sercomIndex, enum LL_SERCOM_USART_StopBit stopBit)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.CTRLB.bit.SBMODE = stopBit & 0x1;
}


void LL_SERCOM_USART_SetParity(uint32_t sercomIndex, enum LL_SERCOM_USART_Parity parity)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.CTRLB.bit.PMODE = parity & 0x1;
}


void LL_SERCOM_USART_SetEncoding(uint32_t sercomIndex, enum LL_SERCOM_USART_Encoding encoding)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.CTRLB.bit.ENC = encoding & 0x1;
}


void LL_SERCOM_USART_SetReceivePulseLength(uint32_t sercomIndex, uint32_t pulseLength)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.RXPL.reg = pulseLength;
}


void LL_SERCOM_USART_SetCollisionDetection(uint32_t sercomIndex, uint32_t state)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.CTRLB.bit.COLDEN = (state != 0);
}


void LL_SERCOM_USART_SetStartFrameDetection(uint32_t sercomIndex, uint32_t state)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.CTRLB.bit.SFDE = (state != 0);
}


void LL_SERCOM_USART_SetImmediateBufferOverflowNotification(uint32_t sercomIndex, uint32_t state)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.CTRLA.bit.IBON = (state != 0);
}


void LL_SERCOM_USART_EnableReceiver(uint32_t sercomIndex, uint32_t enable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.CTRLB.bit.RXEN = (enable != 0);

    while (pSercom->USART.SYNCBUSY.bit.CTRLB == 1)
    {
    }
}


void LL_SERCOM_USART_EnableTransmitter(uint32_t sercomIndex, uint32_t enable)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.CTRLB.bit.TXEN = (enable != 0);

    while (pSercom->USART.SYNCBUSY.bit.CTRLB == 1)
    {
    }
}


enum LL_SERCOM_USART_Status LL_SERCOM_USART_GetStatus(uint32_t sercomIndex)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    return pSercom->USART.STATUS.reg;
}


uint16_t LL_SERCOM_USART_ReadData(uint32_t sercomIndex)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    return pSercom->USART.DATA.reg;
}


void LL_SERCOM_USART_WriteData(uint32_t sercomIndex, uint16_t dataTx)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.DATA.reg = dataTx;
}


void LL_SERCOM_USART_InterruptEnableClear(uint32_t sercomIndex, enum LL_SERCOM_USART_InterruptFlag interruptFlag)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.INTENCLR.reg = interruptFlag;
}


void LL_SERCOM_USART_InterruptEnableSet(uint32_t sercomIndex, enum LL_SERCOM_USART_InterruptFlag interruptFlag)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.INTENSET.reg = interruptFlag;
}


enum LL_SERCOM_USART_InterruptFlag LL_SERCOM_USART_GetInterruptFlag(uint32_t sercomIndex)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    return pSercom->USART.INTFLAG.reg;
}


void LL_SERCOM_USART_ClearInterruptFlag(uint32_t sercomIndex, enum LL_SERCOM_USART_InterruptFlag interruptFlag)
{
    Sercom* const pSercom = SERCOM_Instances[sercomIndex];

    pSercom->USART.INTFLAG.reg = interruptFlag;
}
