#include <sam.h>
#include "LL_ADC.h"


void LL_ADC_Reset(void)
{
    ADC->CTRLA.bit.SWRST = 1;

    while (ADC->SYNCBUSY.bit.SWRST == 1)
    {
    }
}


void LL_ADC_Enable(uint32_t enable, uint32_t runInStandby, uint32_t onDemand)
{
    ADC_CTRLA_Type ctrla;

    ctrla.reg = 0;
    ctrla.bit.ENABLE = (enable != 0);
    ctrla.bit.RUNSTDBY = (runInStandby != 0);
    ctrla.bit.ONDEMAND = (onDemand != 0);

    ADC->CTRLA = ctrla;

    while (ADC->SYNCBUSY.bit.ENABLE == 1)
    {
    }
}


void LL_ADC_SetDebugControl(uint32_t runInDebugHaltedState)
{
    ADC->DBGCTRL.reg = (runInDebugHaltedState != 0) << ADC_DBGCTRL_DBGRUN_Pos;
}


void LL_ADC_SetClockPrescaler(enum LL_ADC_ClockPrescaler prescaler)
{
    ADC->CTRLB.reg = ADC_CTRLB_PRESCALER(prescaler);
}


void LL_ADC_EnableEventInput(uint32_t flushEventEnable, uint32_t startConversionEventEnable, uint32_t flushEventInvert, uint32_t startConversionEventInvert)
{
    ADC_EVCTRL_Type evctrl = ADC->EVCTRL;

    evctrl.bit.FLUSHEI = (flushEventEnable != 0);
    evctrl.bit.STARTEI = (startConversionEventEnable != 0);
    evctrl.bit.FLUSHINV = (flushEventInvert != 0);
    evctrl.bit.STARTINV = (startConversionEventInvert != 0);

    ADC->EVCTRL = evctrl;
}


void LL_ADC_EnableEventOutput(uint32_t resultReadyEventEnable, uint32_t windowMonitorEventEnable)
{
    ADC_EVCTRL_Type evctrl = ADC->EVCTRL;

    evctrl.bit.RESRDYEO = (resultReadyEventEnable != 0);
    evctrl.bit.WINMONEO = (windowMonitorEventEnable != 0);

    ADC->EVCTRL = evctrl;
}


void LL_ADC_SetMode(uint32_t differentialMode, uint32_t leftAdjustedResult, uint32_t freeRunningMode, uint32_t digitalCorrectionLogicEnabled, enum LL_ADC_Resolution resolution)
{
    ADC_CTRLC_Type ctrlc = ADC->CTRLC;

    ctrlc.bit.DIFFMODE = (differentialMode != 0);
    ctrlc.bit.LEFTADJ = (leftAdjustedResult != 0);
    ctrlc.bit.FREERUN = (freeRunningMode != 0);
    ctrlc.bit.CORREN = (digitalCorrectionLogicEnabled != 0);
    ctrlc.bit.RESSEL = resolution & 0x3;

    ADC->CTRLC = ctrlc;

    while (ADC->SYNCBUSY.bit.CTRLC == 1)
    {
    }
}


void LL_ADC_SetInputMux(enum ADC_LL_NegativeInputMux negativeInputMux, enum ADC_LL_PositiveInputMux positiveInputMux)
{
    ADC_INPUTCTRL_Type inputctrl;

    inputctrl.reg = 0;
    inputctrl.bit.MUXNEG = negativeInputMux & 0x1F;
    inputctrl.bit.MUXPOS = positiveInputMux & 0x1F;

    ADC->INPUTCTRL = inputctrl;

    while (ADC->SYNCBUSY.bit.INPUTCTRL == 1)
    {
    }
}


void LL_ADC_SetReference(uint32_t referenceBufferOffsetCompensationEnable, enum LL_ADC_Reference reference)
{
    ADC_REFCTRL_Type refctrl;

    refctrl.reg = 0;
    refctrl.bit.REFCOMP = (referenceBufferOffsetCompensationEnable != 0);
    refctrl.bit.REFSEL = reference & 0xF;

    ADC->REFCTRL = refctrl;
}


void LL_ADC_SetGainCorrection(uint32_t gainCorrectionValue)
{
    ADC->GAINCORR.reg = gainCorrectionValue & 0xFFF;

    while (ADC->SYNCBUSY.bit.GAINCORR == 1)
    {
    }
}


void LL_ADC_SetOffsetCorrection(uint32_t offsetCorrectionValue)
{
    ADC->OFFSETCORR.reg = offsetCorrectionValue & 0xFFF;

    while (ADC->SYNCBUSY.bit.OFFSETCORR == 1)
    {
    }
}


void LL_ADC_SetAverage(enum LL_ADC_Average numberOfSamples, uint32_t adjustingDivisionCoefficient)
{
    ADC_AVGCTRL_Type avgctrl;

    avgctrl.bit.SAMPLENUM = numberOfSamples & 0xF;
    avgctrl.bit.ADJRES = adjustingDivisionCoefficient & 0x7;

    ADC->AVGCTRL = avgctrl;

    while (ADC->SYNCBUSY.bit.AVGCTRL == 1)
    {
    }
}


void LL_ADC_SetSamplingTime(uint32_t samplingTimeLength, uint32_t comparatorOffsetCompensationEnable)
{
    ADC_SAMPCTRL_Type sampctrl;

    sampctrl.bit.SAMPLEN = samplingTimeLength & 0x3F;
    sampctrl.bit.OFFCOMP = (comparatorOffsetCompensationEnable != 0);

    ADC->SAMPCTRL = sampctrl;

    while (ADC->SYNCBUSY.bit.SAMPCTRL == 1)
    {
    }
}


void LL_ADC_SetWindowMonitorLowerThreshold(uint16_t windowLowerThreshold)
{
    ADC->WINLT.reg = windowLowerThreshold;

    while (ADC->SYNCBUSY.bit.WINLT == 1)
    {
    }
}


void LL_ADC_SetWindowMonitorUpperThreshold(uint16_t windowUpperThreshold)
{
    ADC->WINUT.reg = windowUpperThreshold;

    while (ADC->SYNCBUSY.bit.WINUT == 1)
    {
    }
}


void LL_ADC_SetWindowMonitorMode(enum LL_ADC_WindowMonitorMode mode)
{
    ADC_CTRLC_Type ctrlc = ADC->CTRLC;

    ctrlc.bit.WINMODE = mode & 0x7;

    ADC->CTRLC = ctrlc;

    while (ADC->SYNCBUSY.bit.CTRLC == 1)
    {
    }
}


void LL_ADC_InterruptEnableClear(enum LL_ADC_InterruptFlag interruptFlag)
{
    ADC->INTENCLR.reg = interruptFlag;
}


void LL_ADC_InterruptEnableSet(enum LL_ADC_InterruptFlag interruptFlag)
{
    ADC->INTENSET.reg = interruptFlag;
}


enum LL_ADC_InterruptFlag LL_ADC_GetInterruptFlag(void)
{
    return ADC->INTFLAG.reg;
}


void LL_ADC_ClearInterruptFlag(enum LL_ADC_InterruptFlag interruptFlag)
{
    ADC->INTFLAG.reg = interruptFlag;
}


void LL_ADC_SoftwareTrigger(uint32_t conversionFlush, uint32_t startConversion)
{
    ADC->SWTRIG.reg = ((conversionFlush != 0) << ADC_SWTRIG_FLUSH_Pos) | ((startConversion != 0) << ADC_SWTRIG_START_Pos);

    while (ADC->SYNCBUSY.bit.SWTRIG == 1)
    {
    }
}


uint16_t LL_ADC_GetResult(void)
{
    return ADC->RESULT.reg;
}
