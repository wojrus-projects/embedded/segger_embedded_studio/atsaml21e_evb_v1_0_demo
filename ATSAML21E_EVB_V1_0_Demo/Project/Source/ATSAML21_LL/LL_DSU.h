//
// Peripheral Driver Low Layer (DSU module).
//

#ifndef LL_DSU_INCLUDED
#define LL_DSU_INCLUDED

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif


enum LL_DSU_StatusA
{
    LL_DSU_StatusA_DONE    = 1 << 0,
    LL_DSU_StatusA_CRSTEXT = 1 << 1,
    LL_DSU_StatusA_BERR    = 1 << 2,
    LL_DSU_StatusA_FAIL    = 1 << 3,
    LL_DSU_StatusA_PERR    = 1 << 4
};


enum LL_DSU_StatusB
{
    LL_DSU_StatusB_PROT    = 1 << 0,
    LL_DSU_StatusB_DBGPRES = 1 << 1,
    LL_DSU_StatusB_DCCD0   = 1 << 2,
    LL_DSU_StatusB_DCCD1   = 1 << 3,
    LL_DSU_StatusB_HPE     = 1 << 4
};


struct LL_DSU_DeviceIdentification
{
    uint8_t processor;
    uint8_t productFamily;
    uint8_t productSeries;
    uint8_t dieNumber;
    uint8_t revisionNumber;
    uint8_t deviceSelection;
};


void LL_DSU_Reset(void);
void LL_DSU_StartCRC32Check(void);
void LL_DSU_StartMemoryTest(void);
void LL_DSU_ChipErase(void);
enum LL_DSU_StatusA LL_DSU_GetStatusA(void);
void LL_DSU_ClearStatusA(enum LL_DSU_StatusA status);
enum LL_DSU_StatusB LL_DSU_GetStatusB(void);
struct LL_DSU_DeviceIdentification LL_DSU_GetDeviceIdentification(void);
bool LL_DSU_CalculateCRC32(const uint32_t * pInData, uint32_t dataLengthWords, uint32_t initialValue, uint32_t * pOutResult);


#ifdef __cplusplus
}
#endif

#endif // LL_DSU_INCLUDED
