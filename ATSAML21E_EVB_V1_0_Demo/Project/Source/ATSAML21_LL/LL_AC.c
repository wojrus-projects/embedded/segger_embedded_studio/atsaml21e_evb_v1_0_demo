#include <sam.h>
#include "LL_AC.h"


void LL_AC_Reset(void)
{
    AC->CTRLA.bit.SWRST = 1;

    while (AC->SYNCBUSY.bit.SWRST == 1)
    {
    }
}


void LL_AC_Enable(uint32_t enable)
{
    AC->CTRLA.bit.ENABLE = (enable != 0);

    while (AC->SYNCBUSY.bit.ENABLE == 1)
    {
    }
}


void LL_AC_SetDebugControl(uint32_t runInDebugHaltedState)
{
    AC->DBGCTRL.reg = (runInDebugHaltedState != 0) << AC_DBGCTRL_DBGRUN_Pos;
}


void LL_AC_StartSingleComparison(uint32_t startComparator0, uint32_t startComparator1)
{
    AC->CTRLB.reg = ((startComparator0 != 0) << AC_CTRLB_START0_Pos) | ((startComparator1 != 0) << AC_CTRLB_START1_Pos);
}


void LL_AC_EnableWindowMode(uint32_t enable, enum AC_LL_WindowMode windowMode)
{
    AC_WINCTRL_Type winctrl;

    winctrl.reg = 0;
    winctrl.bit.WEN0 = (enable != 0);
    winctrl.bit.WINTSEL0 = windowMode & 0x3;

    AC->WINCTRL = winctrl;

    while (AC->SYNCBUSY.bit.WINCTRL == 1)
    {
    }
}


void LL_AC_EnableEventInput(uint32_t acIndex, uint32_t enable, uint32_t invertInput)
{
    AC_EVCTRL_Type evctrl = AC->EVCTRL;

    switch (acIndex)
    {
        case 0:
            evctrl.bit.COMPEI0 = (enable != 0);
            evctrl.bit.INVEI0 = (invertInput != 0);
            break;

        case 1:
            evctrl.bit.COMPEI1 = (enable != 0);
            evctrl.bit.INVEI1 = (invertInput != 0);
            break;

        default:
            return;
    };

    AC->EVCTRL = evctrl;
}


void LL_AC_EnableEventOutput(uint32_t comparator0OutputEventEnable, uint32_t comparator1OutputEventEnable, uint32_t windowEventEnable)
{
    AC_EVCTRL_Type evctrl = AC->EVCTRL;

    evctrl.bit.COMPEO0 = (comparator0OutputEventEnable != 0);
    evctrl.bit.COMPEO1 = (comparator1OutputEventEnable != 0);
    evctrl.bit.WINEO0 = (windowEventEnable != 0);

    AC->EVCTRL = evctrl;
}


void LL_AC_SetScaler(uint32_t acIndex, uint32_t value)
{
    AC->SCALER[acIndex].bit.VALUE = value & 0x3F;
}


void LL_AC_EnableChannel(uint32_t acIndex, uint32_t enable, uint32_t runInStandby)
{
    AC_COMPCTRL_Type compctrl = AC->COMPCTRL[acIndex];

    compctrl.bit.ENABLE = (enable != 0);
    compctrl.bit.RUNSTDBY = (runInStandby != 0);

    AC->COMPCTRL[acIndex] = compctrl;

    while ((AC->SYNCBUSY.bit.COMPCTRL0 == 1) || (AC->SYNCBUSY.bit.COMPCTRL1 == 1))
    {
    }
}


void LL_AC_EnableSingleShotMode(uint32_t acIndex, uint32_t enable)
{
    AC->COMPCTRL[acIndex].bit.SINGLE = (enable != 0);
}


void LL_AC_SetInterruptOrEventMode(uint32_t acIndex, enum LL_AC_InterruptMode mode)
{
    AC->COMPCTRL[acIndex].bit.INTSEL = mode & 0x3;
}


void LL_AC_SetNegativeInputMux(uint32_t acIndex, enum AC_LL_NegativeInputMux negativeInputMux)
{
    AC->COMPCTRL[acIndex].bit.MUXNEG = negativeInputMux & 0x7;
}


void LL_AC_SetPositiveInputMux(uint32_t acIndex, enum AC_LL_PositiveInputMux positiveInputMux)
{
    AC->COMPCTRL[acIndex].bit.MUXPOS = positiveInputMux & 0x7;
}


void LL_AC_SwapInputs(uint32_t acIndex, uint32_t swapInputs)
{
    AC->COMPCTRL[acIndex].bit.SWAP = (swapInputs != 0);
}


void LL_AC_SetSpeed(uint32_t acIndex, enum AC_LL_Speed speed)
{
    AC->COMPCTRL[acIndex].bit.SPEED = speed & 0x3;
}


void LL_AC_EnableHysteresis(uint32_t acIndex, uint32_t enable, enum AC_LL_Hysteresis hysteresisLevel)
{
    AC->COMPCTRL[acIndex].bit.HYSTEN = (enable != 0);
    AC->COMPCTRL[acIndex].bit.HYST = hysteresisLevel & 0x3;
}


void LL_AC_SetFilter(uint32_t acIndex, enum AC_LL_Filter filterLength)
{
    AC->COMPCTRL[acIndex].bit.FLEN = filterLength & 0x7;
}


void LL_AC_SetOutput(uint32_t acIndex, enum AC_LL_Output output)
{
    AC->COMPCTRL[acIndex].bit.OUT = output & 0x3;
}


void LL_AC_InterruptEnableClear(enum LL_AC_InterruptFlag interruptFlag)
{
    AC->INTENCLR.reg = interruptFlag;
}


void LL_AC_InterruptEnableSet(enum LL_AC_InterruptFlag interruptFlag)
{
    AC->INTENSET.reg = interruptFlag;
}


enum LL_AC_InterruptFlag LL_AC_GetInterruptFlag(void)
{
    return AC->INTFLAG.reg;
}


void LL_AC_ClearInterruptFlag(enum LL_AC_InterruptFlag interruptFlag)
{
    AC->INTFLAG.reg = interruptFlag;
}


uint32_t LL_AC_IsOutputReady(uint32_t acIndex)
{
    uint32_t isReady = 0;

    switch (acIndex)
    {
        case 0:
            isReady = AC->STATUSB.bit.READY0;
            break;

        case 1:
            isReady = AC->STATUSB.bit.READY1;
            break;
    };

    return isReady;
}


uint32_t LL_AC_GetOutputState(uint32_t acIndex)
{
    uint32_t state = 0;

    switch (acIndex)
    {
        case 0:
            state = AC->STATUSA.bit.STATE0;
            break;

        case 1:
            state = AC->STATUSA.bit.STATE1;
            break;
    };

    return state;
}


enum LL_AC_WindowState LL_AC_GetWindowState(void)
{
    return AC->STATUSA.bit.WSTATE0;
}
