//
// Peripheral Driver Low Layer (ADC module).
//

#ifndef LL_ADC_INCLUDED
#define LL_ADC_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


enum LL_ADC_Reference
{
    LL_ADC_Reference_INTREF  = 0x0,
    LL_ADC_Reference_INTVCC0 = 0x1,
    LL_ADC_Reference_INTVCC1 = 0x2,
    LL_ADC_Reference_VREFA   = 0x3,
    LL_ADC_Reference_VREFB   = 0x4,
    LL_ADC_Reference_INTVCC2 = 0x5
};


enum LL_ADC_InterruptFlag
{
    LL_ADC_InterruptFlag_RESRDY  = 1 << 0,
    LL_ADC_InterruptFlag_OVERRUN = 1 << 1,
    LL_ADC_InterruptFlag_WINMON  = 1 << 2
};


enum LL_ADC_ClockPrescaler
{
    LL_ADC_ClockPrescaler_DIV2   = 0x0,
    LL_ADC_ClockPrescaler_DIV4   = 0x1,
    LL_ADC_ClockPrescaler_DIV8   = 0x2,
    LL_ADC_ClockPrescaler_DIV16  = 0x3,
    LL_ADC_ClockPrescaler_DIV32  = 0x4,
    LL_ADC_ClockPrescaler_DIV64  = 0x5,
    LL_ADC_ClockPrescaler_DIV128 = 0x6,
    LL_ADC_ClockPrescaler_DIV256 = 0x7
};


enum ADC_LL_NegativeInputMux
{
    ADC_LL_NegativeInputMux_AIN0 = 0x00,
    ADC_LL_NegativeInputMux_AIN1 = 0x01,
    ADC_LL_NegativeInputMux_AIN2 = 0x02,
    ADC_LL_NegativeInputMux_AIN3 = 0x03,
    ADC_LL_NegativeInputMux_AIN4 = 0x04,
    ADC_LL_NegativeInputMux_AIN5 = 0x05,
    ADC_LL_NegativeInputMux_AIN6 = 0x06,
    ADC_LL_NegativeInputMux_AIN7 = 0x07,
    ADC_LL_NegativeInputMux_GND  = 0x18
};


enum ADC_LL_PositiveInputMux
{
    ADC_LL_PositiveInputMux_AIN0          = 0x00,
    ADC_LL_PositiveInputMux_AIN1          = 0x01,
    ADC_LL_PositiveInputMux_AIN2          = 0x02,
    ADC_LL_PositiveInputMux_AIN3          = 0x03,
    ADC_LL_PositiveInputMux_AIN4          = 0x04,
    ADC_LL_PositiveInputMux_AIN5          = 0x05,
    ADC_LL_PositiveInputMux_AIN6          = 0x06,
    ADC_LL_PositiveInputMux_AIN7          = 0x07,
    ADC_LL_PositiveInputMux_AIN8          = 0x08,
    ADC_LL_PositiveInputMux_AIN9          = 0x09,
    ADC_LL_PositiveInputMux_AIN10         = 0x0A,
    ADC_LL_PositiveInputMux_AIN11         = 0x0B,
    ADC_LL_PositiveInputMux_AIN12         = 0x0C,
    ADC_LL_PositiveInputMux_AIN13         = 0x0D,
    ADC_LL_PositiveInputMux_AIN14         = 0x0E,
    ADC_LL_PositiveInputMux_AIN15         = 0x0F,
    ADC_LL_PositiveInputMux_AIN16         = 0x10,
    ADC_LL_PositiveInputMux_AIN17         = 0x11,
    ADC_LL_PositiveInputMux_AIN18         = 0x12,
    ADC_LL_PositiveInputMux_AIN19         = 0x13,
    ADC_LL_PositiveInputMux_TEMP          = 0x18,
    ADC_LL_PositiveInputMux_BANDGAP       = 0x19,
    ADC_LL_PositiveInputMux_SCALEDCOREVCC = 0x1A,
    ADC_LL_PositiveInputMux_SCALEDIOVCC   = 0x1B,
    ADC_LL_PositiveInputMux_DAC           = 0x1C,
    ADC_LL_PositiveInputMux_SCALEDVBAT    = 0x1D,
    ADC_LL_PositiveInputMux_OPAMP01       = 0x1E,
    ADC_LL_PositiveInputMux_OPAMP2        = 0x1F
};


enum LL_ADC_Resolution
{
    LL_ADC_Resolution_12BIT = 0x0,
    LL_ADC_Resolution_16BIT = 0x1,
    LL_ADC_Resolution_10BIT = 0x2,
    LL_ADC_Resolution_8BIT  = 0x3
};


enum LL_ADC_WindowMonitorMode
{
    LL_ADC_WindowMonitorMode_DISABLE = 0x0,
    LL_ADC_WindowMonitorMode_MODE1   = 0x1,
    LL_ADC_WindowMonitorMode_MODE2   = 0x2,
    LL_ADC_WindowMonitorMode_MODE3   = 0x3,
    LL_ADC_WindowMonitorMode_MODE4   = 0x4
};


enum LL_ADC_Average
{
    LL_ADC_Average_1    = 0x0,
    LL_ADC_Average_2    = 0x1,
    LL_ADC_Average_4    = 0x2,
    LL_ADC_Average_8    = 0x3,
    LL_ADC_Average_16   = 0x4,
    LL_ADC_Average_32   = 0x5,
    LL_ADC_Average_64   = 0x6,
    LL_ADC_Average_128  = 0x7,
    LL_ADC_Average_256  = 0x8,
    LL_ADC_Average_512  = 0x9,
    LL_ADC_Average_1024 = 0xA
};


void LL_ADC_Reset(void);
void LL_ADC_Enable(uint32_t enable, uint32_t runInStandby, uint32_t onDemand);
void LL_ADC_SetDebugControl(uint32_t runInDebugHaltedState);
void LL_ADC_SetClockPrescaler(enum LL_ADC_ClockPrescaler prescaler);
void LL_ADC_EnableEventInput(uint32_t flushEventEnable, uint32_t startConversionEventEnable, uint32_t flushEventInvert, uint32_t startConversionEventInvert);
void LL_ADC_EnableEventOutput(uint32_t resultReadyEventEnable, uint32_t windowMonitorEventEnable);

void LL_ADC_SetMode(uint32_t differentialMode, uint32_t leftAdjustedResult, uint32_t freeRunningMode, uint32_t digitalCorrectionLogicEnabled, enum LL_ADC_Resolution resolution);
void LL_ADC_SetInputMux(enum ADC_LL_NegativeInputMux negativeInputMux, enum ADC_LL_PositiveInputMux positiveInputMux);
void LL_ADC_SetReference(uint32_t referenceBufferOffsetCompensationEnable, enum LL_ADC_Reference reference);
void LL_ADC_SetGainCorrection(uint32_t gainCorrectionValue);
void LL_ADC_SetOffsetCorrection(uint32_t offsetCorrectionValue);
void LL_ADC_SetAverage(enum LL_ADC_Average numberOfSamples, uint32_t adjustingDivisionCoefficient);
void LL_ADC_SetSamplingTime(uint32_t samplingTimeLength, uint32_t comparatorOffsetCompensationEnable);
void LL_ADC_SetWindowMonitorLowerThreshold(uint16_t windowLowerThreshold);
void LL_ADC_SetWindowMonitorUpperThreshold(uint16_t windowUpperThreshold);
void LL_ADC_SetWindowMonitorMode(enum LL_ADC_WindowMonitorMode mode);

void LL_ADC_InterruptEnableClear(enum LL_ADC_InterruptFlag interruptFlag);
void LL_ADC_InterruptEnableSet(enum LL_ADC_InterruptFlag interruptFlag);
enum LL_ADC_InterruptFlag LL_ADC_GetInterruptFlag(void);
void LL_ADC_ClearInterruptFlag(enum LL_ADC_InterruptFlag interruptFlag);

void LL_ADC_SoftwareTrigger(uint32_t conversionFlush, uint32_t startConversion);
uint16_t LL_ADC_GetResult(void);


#ifdef __cplusplus
}
#endif

#endif // LL_ADC_INCLUDED
