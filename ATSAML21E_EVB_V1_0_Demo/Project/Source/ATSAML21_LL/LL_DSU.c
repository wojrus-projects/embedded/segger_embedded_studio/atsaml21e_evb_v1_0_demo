#include <stddef.h>

#include <sam.h>
#include "LL_DSU.h"


void LL_DSU_Reset(void)
{
    DSU->CTRL.reg = DSU_CTRL_SWRST;

    while (DSU->CTRL.bit.SWRST == 1)
    {
    }
}


void LL_DSU_StartCRC32Check(void)
{
    DSU->CTRL.reg = DSU_CTRL_CRC;
}


void LL_DSU_StartMemoryTest(void)
{
    DSU->CTRL.reg = DSU_CTRL_MBIST;
}


void LL_DSU_ChipErase(void)
{
    DSU->CTRL.reg = DSU_CTRL_CE;
}


enum LL_DSU_StatusA LL_DSU_GetStatusA(void)
{
    return DSU->STATUSA.reg;
}


void LL_DSU_ClearStatusA(enum LL_DSU_StatusA status)
{
    DSU->STATUSA.reg = status;
}


enum LL_DSU_StatusB LL_DSU_GetStatusB(void)
{
    return DSU->STATUSB.reg;
}


struct LL_DSU_DeviceIdentification LL_DSU_GetDeviceIdentification(void)
{
    const DSU_DID_Type did = DSU->DID;

    struct LL_DSU_DeviceIdentification deviceIdentification;

    deviceIdentification.processor = did.bit.PROCESSOR;
    deviceIdentification.productFamily = did.bit.FAMILY;
    deviceIdentification.productSeries = did.bit.SERIES;
    deviceIdentification.dieNumber = did.bit.DIE;
    deviceIdentification.revisionNumber = did.bit.REVISION;
    deviceIdentification.deviceSelection = did.bit.DEVSEL;

    return deviceIdentification;
}


bool LL_DSU_CalculateCRC32(const uint32_t * pInData, uint32_t dataLengthWords, uint32_t initialValue, uint32_t * pOutResult)
{
    if ((pInData == NULL) || (pOutResult == NULL) || (dataLengthWords == 0))
    {
        return false;
    }

    if ((uintptr_t)pInData % 4 != 0)
    {
        return false;
    }

    DSU->ADDR.bit.ADDR = (uint32_t)pInData / sizeof(uint32_t);
    DSU->ADDR.bit.AMOD = 0;
    DSU->LENGTH.bit.LENGTH = dataLengthWords;
    DSU->DATA.reg = initialValue;

    DSU->STATUSA.reg = DSU_STATUSA_DONE | DSU_STATUSA_BERR;
    DSU->CTRL.reg = DSU_CTRL_CRC;

    while (DSU->STATUSA.bit.DONE == 0)
    {
    }

    if (DSU->STATUSA.bit.BERR == 1)
    {
        return false;
    }

    *pOutResult = DSU->DATA.reg;

    return true;
}
