//
// Peripheral Driver Low Layer (OSCCTRL module).
//

#ifndef LL_OSCCTRL_INCLUDED
#define LL_OSCCTRL_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


#define LL_OSCCTRL_DFLL48M_COARSE_STEPS     64
#define LL_OSCCTRL_DFLL48M_FINE_STEPS       1024


enum LL_OSCCTRL_Status
{
    LL_OSCCTRL_Status_XOSCRDY   = 1 << 0,
    LL_OSCCTRL_Status_OSC16MRDY = 1 << 4,
    LL_OSCCTRL_Status_DFLLRDY   = 1 << 8,
    LL_OSCCTRL_Status_DFLLOOB   = 1 << 9,
    LL_OSCCTRL_Status_DFLLLCKF  = 1 << 10,
    LL_OSCCTRL_Status_DFLLLCKC  = 1 << 11,
    LL_OSCCTRL_Status_DFLLRCS   = 1 << 12,
    LL_OSCCTRL_Status_DPLLLCKR  = 1 << 16,
    LL_OSCCTRL_Status_DPLLLCKF  = 1 << 17,
    LL_OSCCTRL_Status_DPLLLTO   = 1 << 18,
    LL_OSCCTRL_Status_DPLLLDRTO = 1 << 19
};


enum LL_OSCCTRL_OSC16M_Frequency
{
    LL_OSCCTRL_OSC16M_Frequency_4MHz  = 0x0,
    LL_OSCCTRL_OSC16M_Frequency_8MHz  = 0x1,
    LL_OSCCTRL_OSC16M_Frequency_12MHz = 0x2,
    LL_OSCCTRL_OSC16M_Frequency_16MHz = 0x3
};


enum LL_OSCCTRL_XOSC_Gain
{
    LL_OSCCTRL_XOSC_Gain_For_2MHz  = 0x0,
    LL_OSCCTRL_XOSC_Gain_For_4MHz  = 0x1,
    LL_OSCCTRL_XOSC_Gain_For_8MHz  = 0x2,
    LL_OSCCTRL_XOSC_Gain_For_16MHz = 0x3,
    LL_OSCCTRL_XOSC_Gain_For_30MHz = 0x4
};


enum LL_OSCCTRL_XOSC_StartUpTime
{
    LL_OSCCTRL_XOSC_StartUpTime_31us  = 0x0,
    LL_OSCCTRL_XOSC_StartUpTime_61us  = 0x1,
    LL_OSCCTRL_XOSC_StartUpTime_122us = 0x2,
    LL_OSCCTRL_XOSC_StartUpTime_244us = 0x3,
    LL_OSCCTRL_XOSC_StartUpTime_488us = 0x4,
    LL_OSCCTRL_XOSC_StartUpTime_977   = 0x5,
    LL_OSCCTRL_XOSC_StartUpTime_2ms   = 0x6,
    LL_OSCCTRL_XOSC_StartUpTime_4ms   = 0x7,
    LL_OSCCTRL_XOSC_StartUpTime_8ms   = 0x8,
    LL_OSCCTRL_XOSC_StartUpTime_16ms  = 0x9,
    LL_OSCCTRL_XOSC_StartUpTime_31ms  = 0xA,
    LL_OSCCTRL_XOSC_StartUpTime_62ms  = 0xB,
    LL_OSCCTRL_XOSC_StartUpTime_125ms = 0xC,
    LL_OSCCTRL_XOSC_StartUpTime_250ms = 0xD,
    LL_OSCCTRL_XOSC_StartUpTime_500ms = 0xE,
    LL_OSCCTRL_XOSC_StartUpTime_1s    = 0xF
};


enum LL_OSCCTRL_DFLL48M_Mode
{
    LL_OSCCTRL_DFLL48M_Mode_OpenLoop   = 0,
    LL_OSCCTRL_DFLL48M_Mode_ClosedLoop = 1
};


enum LL_OSCCTRL_DPLL_Filter
{
    LL_OSCCTRL_DPLL_Filter_DEFAULT = 0x0,
    LL_OSCCTRL_DPLL_Filter_LBFILT  = 0x1,
    LL_OSCCTRL_DPLL_Filter_HBFILT  = 0x2,
    LL_OSCCTRL_DPLL_Filter_HDFILT  = 0x3
};


enum LL_OSCCTRL_DPLL_LockTime
{
    LL_OSCCTRL_DPLL_LockTime_DEFAULT = 0x0,
    LL_OSCCTRL_DPLL_LockTime_8MS     = 0x4,
    LL_OSCCTRL_DPLL_LockTime_9MS     = 0x5,
    LL_OSCCTRL_DPLL_LockTime_10MS    = 0x6,
    LL_OSCCTRL_DPLL_LockTime_11MS    = 0x7
};


enum LL_OSCCTRL_DPLL_Prescaler
{
    LL_OSCCTRL_DPLL_Prescaler_DIV1 = 0x0,
    LL_OSCCTRL_DPLL_Prescaler_DIV2 = 0x1,
    LL_OSCCTRL_DPLL_Prescaler_DIV4 = 0x2
};


enum LL_OSCCTRL_DPLL_ReferenceClock
{
    LL_OSCCTRL_DPLL_ReferenceClock_XOSC32K = 0x0,
    LL_OSCCTRL_DPLL_ReferenceClock_XOSC    = 0x1,
    LL_OSCCTRL_DPLL_ReferenceClock_GCLK    = 0x2
};


enum LL_OSCCTRL_DPLL_Status
{
    LL_OSCCTRL_DPLL_Status_LOCK   = 1 << 0,
    LL_OSCCTRL_DPLL_Status_CLKRDY = 1 << 1
};


enum LL_OSCCTRL_InterruptFlag
{
    LL_OSCCTRL_InterruptFlag_XOSCRDY   = 1 << 0,
    LL_OSCCTRL_InterruptFlag_OSC16MRDY = 1 << 4,
    LL_OSCCTRL_InterruptFlag_DFLLRDY   = 1 << 8,
    LL_OSCCTRL_InterruptFlag_DFLLOOB   = 1 << 9,
    LL_OSCCTRL_InterruptFlag_DFLLLCKF  = 1 << 10,
    LL_OSCCTRL_InterruptFlag_DFLLLCKC  = 1 << 11,
    LL_OSCCTRL_InterruptFlag_DFLLRCS   = 1 << 12,
    LL_OSCCTRL_InterruptFlag_DPLLLCKR  = 1 << 16,
    LL_OSCCTRL_InterruptFlag_DPLLLCKF  = 1 << 17,
    LL_OSCCTRL_InterruptFlag_DPLLLTO   = 1 << 18,
    LL_OSCCTRL_InterruptFlag_DPLLLDRTO = 1 << 19
};


enum LL_OSCCTRL_Status LL_OSCCTRL_GetStatus(void);
void LL_OSCCTRL_OSC16M_Enable(uint32_t enable, enum LL_OSCCTRL_OSC16M_Frequency frequency, uint32_t runInStandby, uint32_t onDemand);
void LL_OSCCTRL_XOSC_Enable(uint32_t enable, uint32_t crystalOscillatorEnable, uint32_t runInStandby, uint32_t onDemand, enum LL_OSCCTRL_XOSC_Gain gain, uint32_t automaticAmplitudeGainControl, enum LL_OSCCTRL_XOSC_StartUpTime startUpTime);
void LL_OSCCTRL_DFLL48M_Enable(uint32_t enable, uint32_t runInStandby, uint32_t onDemand, enum LL_OSCCTRL_DFLL48M_Mode mode);
void LL_OSCCTRL_DFLL48M_SetValue(uint32_t coarse, uint32_t fine);

void LL_OSCCTRL_DPLL_Enable(uint32_t enable, uint32_t runInStandby, uint32_t onDemand);
void LL_OSCCTRL_DPLL_Initialize(enum LL_OSCCTRL_DPLL_Filter filter, uint32_t lowPowerEnable, uint32_t wakeUpFastEnable, enum LL_OSCCTRL_DPLL_LockTime lockTime, uint32_t lockBypassEnable);
void LL_OSCCTRL_DPLL_SetClock(enum LL_OSCCTRL_DPLL_ReferenceClock referenceClock, uint32_t clockDivider);
void LL_OSCCTRL_DPLL_SetRatio(uint32_t loopDividerRatio, uint32_t loopDividerRatioFractionalPart);
void LL_OSCCTRL_DPLL_SetPrescaler(enum LL_OSCCTRL_DPLL_Prescaler prescaler);
enum LL_OSCCTRL_DPLL_Status LL_OSCCTRL_DPLL_GetStatus(void);

void LL_OSCCTRL_InterruptEnableClear(enum LL_OSCCTRL_InterruptFlag interruptFlag);
void LL_OSCCTRL_InterruptEnableSet(enum LL_OSCCTRL_InterruptFlag interruptFlag);
enum LL_OSCCTRL_InterruptFlag LL_OSCCTRL_GetInterruptFlag(void);
void LL_OSCCTRL_ClearInterruptFlag(enum LL_OSCCTRL_InterruptFlag interruptFlag);


#ifdef __cplusplus
}
#endif

#endif // LL_OSCCTRL_INCLUDED
