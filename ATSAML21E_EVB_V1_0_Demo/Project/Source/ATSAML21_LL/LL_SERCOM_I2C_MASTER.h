//
// Peripheral Driver Low Layer (SERCOM_I2CM module).
//

#ifndef LL_SERCOM_I2C_MASTER_INCLUDED
#define LL_SERCOM_I2C_MASTER_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


// Number of SERCOM-I2C-MASTER modules
#define LL_SERCOM_I2C_MASTER_NUM     SERCOM_INST_NUM


enum LL_SERCOM_I2C_MASTER_Mode
{
    LL_SERCOM_I2C_MASTER_Mode_Master = 0x5
};


enum LL_SERCOM_I2C_MASTER_Speed
{
    LL_SERCOM_I2C_MASTER_Speed_Standard = 0x0,
    LL_SERCOM_I2C_MASTER_Speed_Fast     = 0x1,
    LL_SERCOM_I2C_MASTER_Speed_High     = 0x2
};


enum LL_SERCOM_I2C_MASTER_ClockStretchMode
{
    LL_SERCOM_I2C_MASTER_ClockStretchMode_Standard = 0x0,
    LL_SERCOM_I2C_MASTER_ClockStretchMode_AfterACK = 0x1
};


enum LL_SERCOM_I2C_MASTER_SDAHoldTime
{
    LL_SERCOM_I2C_MASTER_SDAHoldTime_DIS   = 0x0,
    LL_SERCOM_I2C_MASTER_SDAHoldTime_75NS  = 0x1,
    LL_SERCOM_I2C_MASTER_SDAHoldTime_450NS = 0x2,
    LL_SERCOM_I2C_MASTER_SDAHoldTime_600NS = 0x3
};


enum LL_SERCOM_I2C_MASTER_InactiveTimeOut
{
    LL_SERCOM_I2C_MASTER_InactiveTimeOut_DIS          = 0x0,
    LL_SERCOM_I2C_MASTER_InactiveTimeOut_5_SCL_Cycle  = 0x1,
    LL_SERCOM_I2C_MASTER_InactiveTimeOut_10_SCL_Cycle = 0x2,
    LL_SERCOM_I2C_MASTER_InactiveTimeOut_20_SCL_Cycle = 0x3
};


enum LL_SERCOM_I2C_MASTER_AcknowledgeAction
{
    LL_SERCOM_I2C_MASTER_AcknowledgeAction_SendACK  = 0x0,
    LL_SERCOM_I2C_MASTER_AcknowledgeAction_SendNACK = 0x1
};


enum LL_SERCOM_I2C_MASTER_Command
{
    LL_SERCOM_I2C_MASTER_Command_RepeatedStart = 0x1,
    LL_SERCOM_I2C_MASTER_Command_ReadByte      = 0x2,
    LL_SERCOM_I2C_MASTER_Command_Stop          = 0x3
};


enum LL_SERCOM_I2C_MASTER_InterruptFlag
{
    LL_SERCOM_I2C_MASTER_InterruptFlag_MB    = 1 << 0,
    LL_SERCOM_I2C_MASTER_InterruptFlag_SB    = 1 << 1,
    LL_SERCOM_I2C_MASTER_InterruptFlag_ERROR = 1 << 7
};


enum LL_SERCOM_I2C_MASTER_Status
{
    LL_SERCOM_I2C_MASTER_Status_BUSERR    = 1 << 0,
    LL_SERCOM_I2C_MASTER_Status_ARBLOST   = 1 << 1,
    LL_SERCOM_I2C_MASTER_Status_RXNACK    = 1 << 2,
    LL_SERCOM_I2C_MASTER_Status_BUSSTATE0 = 1 << 4,
    LL_SERCOM_I2C_MASTER_Status_BUSSTATE1 = 1 << 5,
    LL_SERCOM_I2C_MASTER_Status_LOWTOUT   = 1 << 6,
    LL_SERCOM_I2C_MASTER_Status_CLKHOLD   = 1 << 7,
    LL_SERCOM_I2C_MASTER_Status_MEXTTOUT  = 1 << 8,
    LL_SERCOM_I2C_MASTER_Status_SEXTTOUT  = 1 << 9,
    LL_SERCOM_I2C_MASTER_Status_LENERR    = 1 << 10
};


void LL_SERCOM_I2C_MASTER_Reset(uint32_t sercomIndex);
void LL_SERCOM_I2C_MASTER_Enable(uint32_t sercomIndex, uint32_t enable);
void LL_SERCOM_I2C_MASTER_SetRunInStandby(uint32_t sercomIndex, uint32_t runInStandby);
void LL_SERCOM_I2C_MASTER_SetDebugControl(uint32_t sercomIndex, uint32_t runInDebugHaltedState);

void LL_SERCOM_I2C_MASTER_EnableFourWireMode(uint32_t sercomIndex, uint32_t enable);
void LL_SERCOM_I2C_MASTER_SetMode(uint32_t sercomIndex, enum LL_SERCOM_I2C_MASTER_Mode mode);
void LL_SERCOM_I2C_MASTER_SetSpeed(uint32_t sercomIndex, enum LL_SERCOM_I2C_MASTER_Speed speed);
void LL_SERCOM_I2C_MASTER_SetClockStretchMode(uint32_t sercomIndex, enum LL_SERCOM_I2C_MASTER_ClockStretchMode mode);
void LL_SERCOM_I2C_MASTER_SetTimings(uint32_t sercomIndex, enum LL_SERCOM_I2C_MASTER_SDAHoldTime sdaHoldTime, uint32_t masterSCLLowExtendTimeOutEnable, uint32_t slaveSCLLowExtendTimeOutEnable, enum LL_SERCOM_I2C_MASTER_InactiveTimeOut inactiveTimeOut, uint32_t sclLowTimeOutEnable);
void LL_SERCOM_I2C_MASTER_SetBaudRate(uint32_t sercomIndex, uint8_t baudRate, uint8_t baudRateLow, uint8_t highSpeedBaudRate, uint8_t highSpeedBaudRateLow);
void LL_SERCOM_I2C_MASTER_SetAcknowledgeAction(uint32_t sercomIndex, enum LL_SERCOM_I2C_MASTER_AcknowledgeAction action);
void LL_SERCOM_I2C_MASTER_SetAddress(uint32_t sercomIndex, uint32_t address, uint32_t tenBitAddressingEnable);
void LL_SERCOM_I2C_MASTER_SetTransactionLength(uint32_t sercomIndex, uint32_t transactionLength, uint32_t transferLengthEnable);
void LL_SERCOM_I2C_MASTER_EnableSmartMode(uint32_t sercomIndex, uint32_t enable);
void LL_SERCOM_I2C_MASTER_EnableQuickCommand(uint32_t sercomIndex, uint32_t enable);
void LL_SERCOM_I2C_MASTER_TriggerCommand(uint32_t sercomIndex, enum LL_SERCOM_I2C_MASTER_Command command);
void LL_SERCOM_I2C_MASTER_SetBusStateIdle(uint32_t sercomIndex);

enum LL_SERCOM_I2C_MASTER_Status LL_SERCOM_I2C_MASTER_GetStatus(uint32_t sercomIndex);
void LL_SERCOM_I2C_MASTER_ClearStatus(uint32_t sercomIndex, enum LL_SERCOM_I2C_MASTER_Status status);

uint8_t LL_SERCOM_I2C_MASTER_ReadData(uint32_t sercomIndex);
void LL_SERCOM_I2C_MASTER_WriteData(uint32_t sercomIndex, uint8_t dataTx);

void LL_SERCOM_I2C_MASTER_InterruptEnableClear(uint32_t sercomIndex, enum LL_SERCOM_I2C_MASTER_InterruptFlag interruptFlag);
void LL_SERCOM_I2C_MASTER_InterruptEnableSet(uint32_t sercomIndex, enum LL_SERCOM_I2C_MASTER_InterruptFlag interruptFlag);
enum LL_SERCOM_I2C_MASTER_InterruptFlag LL_SERCOM_I2C_MASTER_GetInterruptFlag(uint32_t sercomIndex);
void LL_SERCOM_I2C_MASTER_ClearInterruptFlag(uint32_t sercomIndex, enum LL_SERCOM_I2C_MASTER_InterruptFlag interruptFlag);


#ifdef __cplusplus
}
#endif

#endif // LL_SERCOM_I2C_MASTER_INCLUDED
