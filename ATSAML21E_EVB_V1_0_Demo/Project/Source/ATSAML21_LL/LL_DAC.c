#include <sam.h>
#include "LL_DAC.h"


void LL_DAC_Reset(void)
{
    DAC->CTRLA.bit.SWRST = 1;

    while (DAC->SYNCBUSY.bit.SWRST == 1)
    {
    }    
}


void LL_DAC_Enable(uint32_t enable)
{
    DAC->CTRLA.bit.ENABLE = (enable != 0);

    while (DAC->SYNCBUSY.bit.ENABLE == 1)
    {
    }
}


void LL_DAC_SetDebugControl(uint32_t runInDebugHaltedState)
{
    DAC->DBGCTRL.reg = (runInDebugHaltedState != 0) << DAC_DBGCTRL_DBGRUN_Pos;
}


enum LL_DAC_Status LL_DAC_GetStatus(void)
{
    return DAC->STATUS.reg;
}


void LL_DAC_SetReferenceVoltage(enum LL_DAC_ReferenceVoltage referenceVoltage)
{
    DAC_CTRLB_Type ctrlb = DAC->CTRLB;

    ctrlb.bit.REFSEL = referenceVoltage & 0x3;

    DAC->CTRLB = ctrlb;
}


void LL_DAC_EnableEventOutput(uint32_t dacIndex, uint32_t dataBufferEmptyEventOutputEnable)
{
    DAC_EVCTRL_Type evctrl = DAC->EVCTRL;

    switch (dacIndex)
    {
        case 0:
            evctrl.bit.EMPTYEO0 = (dataBufferEmptyEventOutputEnable != 0);
            break;

        case 1:
            evctrl.bit.EMPTYEO1 = (dataBufferEmptyEventOutputEnable != 0);
            break;

        default:
            return;
    }

    DAC->EVCTRL = evctrl;
}


void LL_DAC_EnableEventInput(uint32_t dacIndex, uint32_t startConversionEventInputEnable, uint32_t invert)
{
    DAC_EVCTRL_Type evctrl = DAC->EVCTRL;

    switch (dacIndex)
    {
        case 0:
            evctrl.bit.STARTEI0 = (startConversionEventInputEnable != 0);
            evctrl.bit.INVEI0 = (invert != 0);
            break;

        case 1:
            evctrl.bit.STARTEI1 = (startConversionEventInputEnable != 0);
            evctrl.bit.INVEI1 = (invert != 0);
            break;

        default:
            return;
    }

    DAC->EVCTRL = evctrl;
}


void LL_DAC_InterruptEnableClear(enum LL_DAC_InterruptFlag interruptFlag)
{
    DAC->INTENCLR.reg = interruptFlag;
}


void LL_DAC_InterruptEnableSet(enum LL_DAC_InterruptFlag interruptFlag)
{
    DAC->INTENSET.reg = interruptFlag;
}


enum LL_DAC_InterruptFlag LL_DAC_GetInterruptFlag(void)
{
    return DAC->INTFLAG.reg;
}


void LL_DAC_ClearInterruptFlag(enum LL_DAC_InterruptFlag interruptFlag)
{
    DAC->INTFLAG.reg = interruptFlag;
}


void LL_DAC_ChannelEnable(uint32_t dacIndex, uint32_t enable, uint32_t runInStandby)
{
    DAC_DACCTRL_Type dacctrl = DAC->DACCTRL[dacIndex];

    dacctrl.bit.ENABLE = (enable != 0);
    dacctrl.bit.RUNSTDBY = (runInStandby != 0);

    DAC->DACCTRL[dacIndex] = dacctrl;
}


void LL_DAC_ChannelSetConfiguration(uint32_t dacIndex,
                                    uint32_t leftAdjustedData,
                                    enum LL_DAC_Current currentControl,
                                    uint32_t ditheringMode,
                                    enum LL_DAC_RefreshPeriod refreshPeriod)
{
    DAC_DACCTRL_Type dacctrl = DAC->DACCTRL[dacIndex];

    dacctrl.bit.LEFTADJ = (leftAdjustedData != 0);
    dacctrl.bit.CCTRL = currentControl & 0x3;
    dacctrl.bit.DITHER = (ditheringMode != 0);
    dacctrl.bit.REFRESH = refreshPeriod & 0xF;

    DAC->DACCTRL[dacIndex] = dacctrl;
}


void LL_DAC_ChannelWriteData(uint32_t dacIndex, uint16_t data)
{
    DAC->DATA[dacIndex].reg = data;

    switch (dacIndex)
    {
        case 0:
            while (DAC->SYNCBUSY.bit.DATA0 == 1)
            {
            }
            break;

        case 1:
            while (DAC->SYNCBUSY.bit.DATA1 == 1)
            {
            }
            break;

        default:
            return;
    }
}


void LL_DAC_ChannelWriteDataBuffer(uint32_t dacIndex, uint16_t data)
{
    DAC->DATABUF[dacIndex].reg = data;

    switch (dacIndex)
    {
        case 0:
            while (DAC->SYNCBUSY.bit.DATABUF0 == 1)
            {
            }
            break;

        case 1:
            while (DAC->SYNCBUSY.bit.DATABUF1 == 1)
            {
            }
            break;

        default:
            return;
    }
}
