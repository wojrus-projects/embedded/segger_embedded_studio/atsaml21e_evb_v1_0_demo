#include <sam.h>
#include "LL_TRNG.h"


void LL_TRNG_Initialize(uint32_t enable, uint32_t runInSleep)
{
    TRNG->CTRLA.reg = ((enable != 0) << TRNG_CTRLA_ENABLE_Pos) | ((runInSleep != 0) << TRNG_CTRLA_RUNSTDBY_Pos);
}


void LL_TRNG_EnableDataReadyEventOutput(uint32_t enable)
{
    TRNG->EVCTRL.reg = (enable != 0) << TRNG_EVCTRL_DATARDYEO_Pos;
}


uint32_t LL_TRNG_GetData(void)
{
    return TRNG->DATA.reg;
}


void LL_TRNG_InterruptEnableClear(void)
{
    TRNG->INTENCLR.reg = TRNG_INTENCLR_DATARDY;
}


void LL_TRNG_InterruptEnableSet(void)
{
    TRNG->INTENSET.reg = TRNG_INTENSET_DATARDY;
}


uint32_t LL_TRNG_GetAndClearInterruptFlag(void)
{
    return (TRNG->INTFLAG.bit.DATARDY == 1);
}
