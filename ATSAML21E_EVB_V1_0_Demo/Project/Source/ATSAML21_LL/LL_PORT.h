//
// Peripheral Driver Low Layer (PORT module).
//

#ifndef LL_PORT_INCLUDED
#define LL_PORT_INCLUDED

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif


enum LL_PORT_PinDirection
{
    LL_PORT_PinDirection_Input,
    LL_PORT_PinDirection_Output
};


enum LL_PORT_PullMode
{
    LL_PORT_PullMode_Disable,
    LL_PORT_PullMode_Up,
    LL_PORT_PullMode_Down
};


enum LL_PORT_OutputDriverStrength
{
    LL_PORT_OutputDriverStrength_Low,
    LL_PORT_OutputDriverStrength_High
};


enum LL_PORT_PeripheralFunction
{
    LL_PORT_PeripheralFunction_A = 0x0,
    LL_PORT_PeripheralFunction_B = 0x1,
    LL_PORT_PeripheralFunction_C = 0x2,
    LL_PORT_PeripheralFunction_D = 0x3,
    LL_PORT_PeripheralFunction_E = 0x4,
    LL_PORT_PeripheralFunction_F = 0x5,
    LL_PORT_PeripheralFunction_G = 0x6,
    LL_PORT_PeripheralFunction_H = 0x7,
    LL_PORT_PeripheralFunction_I = 0x8
};


enum LL_PORT_EventAction
{
    LL_PORT_EventAction_OUT = 0x0,
    LL_PORT_EventAction_SET = 0x1,
    LL_PORT_EventAction_CLR = 0x2,
    LL_PORT_EventAction_TGL = 0x3
};


void LL_PORT_SetDirection(uint32_t portNumber, uint32_t pinNumber, enum LL_PORT_PinDirection direction);
void LL_PORT_SetPullMode(uint32_t portNumber, uint32_t pinNumber, enum LL_PORT_PullMode mode);
void LL_PORT_SetOutputDriverStrength(uint32_t portNumber, uint32_t pinNumber, enum LL_PORT_OutputDriverStrength driverStrength);
void LL_PORT_EnableInput(uint32_t portNumber, uint32_t pinNumber, uint32_t enable);
void LL_PORT_EnableInputSynchronizer(uint32_t portNumber, uint32_t pinNumber, uint32_t enable);

void LL_PORT_EnablePeripheralMultiplexer(uint32_t portNumber, uint32_t pinNumber, uint32_t enable);
void LL_PORT_SetPeripheralMultiplexing(uint32_t portNumber, uint32_t pinNumber, enum LL_PORT_PeripheralFunction peripheralFunction);
void LL_PORT_SetEventAction(uint32_t portNumber, uint32_t actionIndex, uint32_t enable, enum LL_PORT_EventAction actionType, uint32_t pinNumber);

void LL_PORT_SetOutput(uint32_t portNumber, uint32_t pinNumber, uint32_t state);
void LL_PORT_ToggleOutput(uint32_t portNumber, uint32_t pinNumber);
uint32_t LL_PORT_ReadInput(uint32_t portNumber, uint32_t pinNumber);


#ifdef __cplusplus
}
#endif

#endif // LL_PORT_INCLUDED
