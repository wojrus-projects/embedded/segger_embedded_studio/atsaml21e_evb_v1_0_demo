//
// Peripheral Driver Low Layer (NVMCTRL module).
//

#ifndef LL_NVMCTRL_INCLUDED
#define LL_NVMCTRL_INCLUDED

#include <stdint.h>
#include <assert.h>

#ifdef __cplusplus
extern "C" {
#endif


#define LL_NVMCTRL_SERIAL_NUMBER_LENGTH_BYTES     16


struct LL_NVMCTRL_Parameters
{
    uint32_t NVM_PagesNumber;
    uint16_t RWW_EEPROM_PagesNumber;
    uint16_t PageSizeBytes;
};


struct LL_NVMCTRL_SoftwareCalibrationData
{
    uint32_t BIASREFBUF         : 3;
    uint32_t BIASCOMP           : 3;
    uint32_t OSC32KCAL          : 7;
    uint32_t USB_TRANSN         : 5;
    uint32_t USB_TRANSP         : 5;
    uint32_t USB_TRIM           : 3;
    uint32_t DFLL48M_COARSE_CAL : 6;
};


static_assert(sizeof(struct LL_NVMCTRL_SoftwareCalibrationData) == 4, "Invalid size");


struct LL_NVMCTRL_UserRow
{
    uint64_t BOOTPROT         : 3;      // NVMCTRL
    uint64_t Reserved_1       : 1;      // -
    uint64_t EEPROM           : 3;      // NVMCTRL
    uint64_t Reserved_2       : 1;      // -
    uint64_t BOD33_Level      : 6;      // SUPC.BOD33
    uint64_t BOD33_Disable    : 1;      // SUPC.BOD33
    uint64_t BOD33_Action     : 2;      // SUPC.BOD33
    uint64_t Reserved_3       : 9;      // -
    uint64_t WDT_Enable       : 1;      // WDT.CTRLA
    uint64_t WDT_AlwaysOn     : 1;      // WDT.CTRLA
    uint64_t WDT_Period       : 4;      // WDT.CONFIG
    uint64_t WDT_Window       : 4;      // WDT.CONFIG
    uint64_t WDT_EWOFFSET     : 4;      // WDT.EWCTRL
    uint64_t WDT_WEN          : 1;      // WDT.CTRLA
    uint64_t BOD33_Hysteresis : 1;      // SUPC.BOD33
    uint64_t BOD12_Hysteresis : 1;      // SUPC.BOD12
    uint64_t Reserved_4       : 5;      // -
    uint64_t LOCK             : 16;     // NVMCTRL
};


static_assert(sizeof(struct LL_NVMCTRL_UserRow) == 8, "Invalid size");


#define LL_NVMCTRL_USER_ROW_FACTORY_VALUE   \
    .BOOTPROT         = 0x7,                \
    .Reserved_1       = 0x1,                \
    .EEPROM           = 0x7,                \
    .Reserved_2       = 0x1,                \
    .BOD33_Level      = 0x06,               \
    .BOD33_Disable    = 0x0,                \
    .BOD33_Action     = 0x1,                \
    .Reserved_3       = 0x08F,              \
    .WDT_Enable       = 0x0,                \
    .WDT_AlwaysOn     = 0x0,                \
    .WDT_Period       = 0xB,                \
    .WDT_Window       = 0xB,                \
    .WDT_EWOFFSET     = 0xB,                \
    .WDT_WEN          = 0x0,                \
    .BOD33_Hysteresis = 0x0,                \
    .BOD12_Hysteresis = 0x0,                \
    .Reserved_4       = 0x1F,               \
    .LOCK             = 0xFFFF


enum LL_NVMCTRL_CacheReadMode
{
    LL_NVMCTRL_CacheReadMode_NO_MISS_PENALTY = 0x0,
    LL_NVMCTRL_CacheReadMode_LOW_POWER       = 0x1,
    LL_NVMCTRL_CacheReadMode_DETERMINISTIC   = 0x2
};


enum LL_NVMCTRL_PowerReductionMode
{
    LL_NVMCTRL_PowerReductionMode_WAKEUPACCESS  = 0x0,
    LL_NVMCTRL_PowerReductionMode_WAKEUPINSTANT = 0x1
};


enum LL_NVMCTRL_Command
{
    LL_NVMCTRL_Command_ER      = 0x02,
    LL_NVMCTRL_Command_WP      = 0x04,
    LL_NVMCTRL_Command_EAR     = 0x05,
    LL_NVMCTRL_Command_WAP     = 0x06,
    LL_NVMCTRL_Command_RWWEEER = 0x1A,
    LL_NVMCTRL_Command_RWWEEWP = 0x1C,
    LL_NVMCTRL_Command_LR      = 0x40,
    LL_NVMCTRL_Command_UR      = 0x41,
    LL_NVMCTRL_Command_SPRM    = 0x42,
    LL_NVMCTRL_Command_CPRM    = 0x43,
    LL_NVMCTRL_Command_PBC     = 0x44,
    LL_NVMCTRL_Command_SSB     = 0x45,
    LL_NVMCTRL_Command_INVALL  = 0x46
};


enum LL_NVMCTRL_Status
{
    LL_NVMCTRL_Status_PRM   = 1 << 0,
    LL_NVMCTRL_Status_LOAD  = 1 << 1,
    LL_NVMCTRL_Status_PROGE = 1 << 2,
    LL_NVMCTRL_Status_LOCKE = 1 << 3,
    LL_NVMCTRL_Status_NVME  = 1 << 4,
    LL_NVMCTRL_Status_SB    = 1 << 8
};


enum LL_NVMCTRL_InterruptFlag
{
    LL_NVMCTRL_InterruptFlag_READY = 1 << 0,
    LL_NVMCTRL_InterruptFlag_ERROR = 1 << 1
};


struct LL_NVMCTRL_Parameters LL_NVMCTRL_GetParameter(void);
void LL_NVMCTRL_SetCache(uint32_t cacheDisable, enum LL_NVMCTRL_CacheReadMode cacheReadMode);
void LL_NVMCTRL_InvalidateCache(void);
void LL_NVMCTRL_SetReadWaitStates(uint32_t waitStates);
void LL_NVMCTRL_SetPowerReductionMode(enum LL_NVMCTRL_PowerReductionMode mode);

void LL_NVMCTRL_ExecuteCommand(enum LL_NVMCTRL_Command command);
void LL_NVMCTRL_SetAddress(uint32_t address);
uint32_t LL_NVMCTRL_GetLockSection(void);
enum LL_NVMCTRL_Status LL_NVMCTRL_GetStatus(void);
void LL_NVMCTRL_ClearStatus(enum LL_NVMCTRL_Status statusBits);

void LL_NVMCTRL_InterruptEnableClear(enum LL_NVMCTRL_InterruptFlag interruptFlag);
void LL_NVMCTRL_InterruptEnableSet(enum LL_NVMCTRL_InterruptFlag interruptFlag);
enum LL_NVMCTRL_InterruptFlag LL_NVMCTRL_GetInterruptFlag(void);
void LL_NVMCTRL_ClearInterruptFlag(enum LL_NVMCTRL_InterruptFlag interruptFlag);

void LL_NVMCTRL_ReadSerialNumber(uint8_t * const pOutSerialNumber);
struct LL_NVMCTRL_SoftwareCalibrationData LL_NVMCTRL_ReadSoftwareCalibrationData(void);
void LL_NVMCTRL_ReadUserRow(struct LL_NVMCTRL_UserRow * const pOutUserRow);
void LL_NVMCTRL_InitializeFactoryUserRow(struct LL_NVMCTRL_UserRow * pUserRow);


#ifdef __cplusplus
}
#endif

#endif // LL_NVMCTRL_INCLUDED
