#include <sam.h>
#include "LL_RTC.h"


void LL_RTC_Reset(void)
{
    RTC->MODE0.CTRLA.bit.SWRST = 1;

    while (RTC->MODE0.SYNCBUSY.bit.SWRST == 1)
    {
    }
}
