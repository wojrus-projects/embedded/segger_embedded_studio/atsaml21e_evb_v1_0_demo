#ifndef DAC_INCLUDED_H
#define DAC_INCLUDED_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

//
// DAC output voltage range and reference voltage
//

#define DAC_VOUT_MIN    0.00f
#define DAC_VOUT_MAX    3.28f
#define DAC_VREF        3.28f


//
// DAC indexes
//

enum DAC_Index
{
    DAC_0 = 0,
    DAC_1 = 1
};


void DAC_Initialize(bool enableDac0, bool enableDac1);
void DAC_SetOutput(enum DAC_Index dacIndex, float voltage);


#ifdef __cplusplus
}
#endif

#endif // DAC_INCLUDED_H
