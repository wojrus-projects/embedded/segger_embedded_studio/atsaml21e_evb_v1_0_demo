#include <stdint.h>


static volatile uint32_t TimerCounter_ms = 0;
static volatile uint32_t TimerCounter_s = 0;


void Timer_Task(void)
{
    static volatile uint16_t secondsDivider = 0;

    if (++secondsDivider == 1000)
    {
        secondsDivider = 0;
        TimerCounter_s++;
    }

    TimerCounter_ms++;
}


uint32_t Timer_GetMilliseconds(void)
{
    return TimerCounter_ms;
}


uint32_t Timer_GetSeconds(void)
{
    return TimerCounter_s;
}


void Timer_DelayMilliseconds(uint32_t timeDelay_ms)
{
    uint32_t timeStart_ms = TimerCounter_ms;
    uint32_t timeDelta_ms;

    do
    {
        timeDelta_ms = TimerCounter_ms - timeStart_ms;
    }
    while (timeDelta_ms < timeDelay_ms);
}
