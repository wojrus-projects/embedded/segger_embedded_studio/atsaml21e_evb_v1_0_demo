#include <stdint.h>

#include "ATSAML21_LL/LL.h"
#include "DAC.h"
#include "Board.h"


void DAC_Initialize(bool enableDac0, bool enableDac1)
{
    LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_DAC, 0, 1);

    LL_DAC_Reset();
    LL_DAC_Enable(0);
    LL_DAC_SetDebugControl(1);
    LL_DAC_SetReferenceVoltage(LL_DAC_ReferenceVoltage_VDDANA);
    LL_DAC_EnableEventOutput(0, 0);
    LL_DAC_EnableEventOutput(1, 0);
    LL_DAC_EnableEventInput(0, 0, 0);
    LL_DAC_EnableEventInput(1, 0, 0);

    LL_DAC_InterruptEnableClear(LL_DAC_InterruptFlag_UNDERRUN0 | LL_DAC_InterruptFlag_UNDERRUN1
                              | LL_DAC_InterruptFlag_EMPTY0 | LL_DAC_InterruptFlag_EMPTY1);

    NVIC_DisableIRQ(DAC_IRQn);

    //
    // DAC-0
    //

    if (enableDac0 != false)
    {
        LL_PORT_EnablePeripheralMultiplexer(PORT_DAC_0, PIN_DAC_0, 1);
        LL_PORT_SetPeripheralMultiplexing(PORT_DAC_0, PIN_DAC_0, LL_PORT_PeripheralFunction_B);

        LL_DAC_ChannelSetConfiguration(0, 0, LL_DAC_Current_CC12M, 0, LL_DAC_RefreshPeriod_30us);
        LL_DAC_ChannelEnable(0, 1, 1);
    }
    else
    {
        LL_PORT_EnablePeripheralMultiplexer(PORT_DAC_0, PIN_DAC_0, 0);
        LL_DAC_ChannelEnable(0, 0, 0);
    }

    //
    // DAC-1
    //

    if (enableDac1 != false)
    {
        LL_PORT_EnablePeripheralMultiplexer(PORT_DAC_1, PIN_DAC_1, 1);
        LL_PORT_SetPeripheralMultiplexing(PORT_DAC_1, PIN_DAC_1, LL_PORT_PeripheralFunction_B);

        LL_DAC_ChannelSetConfiguration(1, 0, LL_DAC_Current_CC12M, 0, LL_DAC_RefreshPeriod_30us);
        LL_DAC_ChannelEnable(1, 1, 1);
    }
    else
    {
        LL_PORT_EnablePeripheralMultiplexer(PORT_DAC_1, PIN_DAC_1, 0);
        LL_DAC_ChannelEnable(1, 0, 0);
    }

    if ((enableDac0 != false) || (enableDac1 != false))
    {
        bool dac0isReady;
        bool dac1isReady;

        LL_DAC_Enable(1);

        do
        {
            const enum LL_DAC_Status status = LL_DAC_GetStatus();

            if (enableDac0 != false)
            {
                dac0isReady = ((status & LL_DAC_Status_READY0) != 0);
            }
            else
            {
                dac0isReady = true;
            }

            if (enableDac1 != false)
            {
                dac1isReady = ((status & LL_DAC_Status_READY1) != 0);
            }
            else
            {
                dac1isReady = true;
            }
        }
        while ((dac0isReady == false) || (dac1isReady == false));
    }
    else
    {
        LL_GCLK_EnablePeripheralChannel(LL_GCLK_Channel_DAC, 0, 0);
    }
}


void DAC_SetOutput(enum DAC_Index dacIndex, float voltage)
{
    if (voltage < DAC_VOUT_MIN)
    {
        voltage = DAC_VOUT_MIN;
    }
    else if (voltage > DAC_VOUT_MAX)
    {
        voltage = DAC_VOUT_MAX;
    }

    uint32_t rawData = (uint32_t)((voltage * 4096.0f) / DAC_VREF);

    if (rawData > 0xFFF)
    {
        rawData = 0xFFF;
    }

    LL_DAC_ChannelWriteData(dacIndex, (uint16_t)rawData);
}
