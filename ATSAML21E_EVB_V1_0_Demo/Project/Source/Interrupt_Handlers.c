#include <stdio.h>

#include "ATSAML21_LL/LL.h"
#include "ATSAML21_LL_Test/LL_Test_Main.h"
#include "Timer.h"
#include "Keypad.h"
#include "tusb.h"


static void Default_Handler(void)
{
    for (;;)
    {
    }
}


void NMI_Handler(void)
{
    printf("* IRQ NMI *\n");
    Default_Handler();
}


void SVC_Handler(void)
{
    printf("* IRQ SVC *\n");
    Default_Handler();
}


void PendSV_Handler(void)
{
    printf("* IRQ PendSV *\n");
    Default_Handler();
}


void SysTick_Handler(void)
{
    Timer_Task();
    Keypad_DebouncerTask();
}


void SYSTEM_Handler(void)
{
    printf("* IRQ SYSTEM *\n");
    Default_Handler();
}


void MCLK_Handler(void)
{
    printf("* IRQ MCLK *\n");
    Default_Handler();
}


void OSCCTRL_Handler(void)
{
    printf("* IRQ OSCCTRL *\n");
    Default_Handler();
}


void OSC32KCTRL_Handler(void)
{
    printf("* IRQ OSC32KCTRL *\n");
    Default_Handler();
}


void PAC_Handler(void)
{
    printf("* IRQ PAC *\n");
    Default_Handler();
}


void PM_Handler(void)
{
    printf("* IRQ PM *\n");
    Default_Handler();
}


void SUPC_Handler(void)
{
    printf("* IRQ SUPC *\n");
    Default_Handler();
}


void TAL_Handler(void)
{
    printf("* IRQ TAL *\n");
    Default_Handler();
}


void WDT_Handler(void)
{
    printf("* IRQ WDT *\n");
    LL_WDT_ClearInterruptFlag();
}


void RTC_Handler(void)
{
    printf("* IRQ RTC *\n");
    Default_Handler();
}


void EIC_Handler(void)
{
    printf("* IRQ EIC *\n");
    Default_Handler();
}


void NVCTRL_Handler(void)
{
    printf("* IRQ NVCTRL *\n");
    Default_Handler();
}


void DMAC_Handler(void)
{
#if LL_TEST == 1
    #if LL_TEST_SERCOM_USART_DMA_ENABLE == 1
    LL_Test_SERCOM_USART_InterruptHandlerDMAC_1();
    #endif

    #if LL_TEST_SERCOM_USART_DMA_CIRCULAR_RX_ENABLE == 1
    LL_Test_SERCOM_USART_InterruptHandlerDMAC_2();
    #endif

    #if LL_TEST_SERCOM_USART_DMA_LINEAR_RX_ENABLE == 1
    LL_Test_SERCOM_USART_InterruptHandlerDMAC_3();
    #endif

    #if LL_TEST_SERCOM_USART_DMA_WITH_RX_TIMEOUT_ENABLE == 1
    LL_Test_SERCOM_USART_InterruptHandlerDMAC_3();
    #endif

    #if LL_TEST_SERCOM_SPI_MASTER_DMA_ENABLE == 1
    LL_Test_SERCOM_SPI_Master_InterruptHandlerDMAC();
    #endif
#else
    printf("* IRQ DMAC *\n");
    Default_Handler();
#endif
}


void USB_Handler(void)
{
    tud_int_handler(0);
}


void EVSYS_Handler(void)
{
    printf("* IRQ EVSYS *\n");
    Default_Handler();
}


void SERCOM0_Handler(void)
{
#if LL_TEST == 1
    LL_Test_SERCOM_SPI_Master_InterruptHandlerSPI();
#else
    printf("* IRQ SERCOM0 *\n");
    Default_Handler();
#endif
}


void SERCOM1_Handler(void)
{
#if LL_TEST == 1
    LL_Test_SERCOM_USART_InterruptHandlerUSART();
#else
    printf("* IRQ SERCOM1 *\n");
    Default_Handler();
#endif
}


void SERCOM2_Handler(void)
{
    printf("* IRQ SERCOM2 *\n");
    Default_Handler();
}


void SERCOM3_Handler(void)
{
    printf("* IRQ SERCOM3 *\n");
    Default_Handler();
}


void SERCOM4_Handler(void)
{
    printf("* IRQ SERCOM4 *\n");
    Default_Handler();
}


void SERCOM5_Handler(void)
{
    printf("* IRQ SERCOM5 *\n");
    Default_Handler();
}


void TCC0_Handler(void)
{
    printf("* IRQ TCC0 *\n");
    Default_Handler();
}


void TCC1_Handler(void)
{
    printf("* IRQ TCC1 *\n");
    Default_Handler();
}


void TCC2_Handler(void)
{
    printf("* IRQ TCC2 *\n");
    Default_Handler();
}


void TC0_Handler(void)
{
#if LL_TEST == 1
    #if LL_TEST_TC0_CAPTURE_PWP_ENABLE == 1
    LL_Test_TC0_Capture_PWP_InterruptHandler();
    #endif

    #if LL_TEST_TC0_CAPTURE_PW_ENABLE == 1
    LL_Test_TC0_Capture_PW_InterruptHandler();
    #endif
#else
    printf("* IRQ TC0 *\n");
    Default_Handler();
#endif
}


void TC1_Handler(void)
{
    printf("* IRQ TC1 *\n");
    Default_Handler();
}


void TC4_Handler(void)
{
#if LL_TEST == 1
    #if LL_TEST_TC4_COUNTER_EXTERNAL_ENABLE == 1
    LL_Test_TC4_Counter_External_InterruptHandler();
    #endif
#else
    printf("* IRQ TC4 *\n");
    Default_Handler();
#endif
}


void ADC_Handler(void)
{
    printf("* IRQ ADC *\n");
    Default_Handler();
}


void AC_Handler(void)
{
    printf("* IRQ AC *\n");
    Default_Handler();
}


void DAC_Handler(void)
{
    printf("* IRQ DAC *\n");
    Default_Handler();
}


void PTC_Handler(void)
{
    printf("* IRQ PTC *\n");
    Default_Handler();
}


void AES_Handler(void)
{
    printf("* IRQ AES *\n");
    Default_Handler();
}


void TRNG_Handler(void)
{
    printf("* IRQ TRNG *\n");
    Default_Handler();
}
