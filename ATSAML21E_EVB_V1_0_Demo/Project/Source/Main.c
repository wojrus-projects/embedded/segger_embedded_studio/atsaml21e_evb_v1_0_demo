//
// Demo firmware for board 'ATSAML21E EVB v1.0':
// https://gitlab.com/wojrus-projects/altium-designer/atsaml21e_evb_v1_0
//
// MCU: ATSAML21E15B (32 KB flash, 4+2 KB SRAM, USB)
//
// Author: W.R. <rwxrwx@interia.pl>
//
// License: MIT
//

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "ATSAML21_LL/LL.h"
#include "Board.h"
#include "Keypad.h"
#include "Timer.h"
#include "DAC.h"
#include "tusb.h"


#define FIRMWARE_VERSION    "ATSAML21E-EVB Demo v1.3 beta (" __DATE__ ", " __TIME__ ")"


static void USB_Application_Task(void);
static bool USB_CDC_Send(const char * pStr);


static volatile bool USBSerialPortIsActive = false;
static volatile float DAC0_Voltage = 0.0f;
static volatile float DAC1_Voltage = 0.0f;


#define DAC_VOLTAGE_STEP    0.200f


int main(void)
{
    Board_Initialize();

    LED_Set(LED_GREEN, 1);
    LED_Set(LED_RED, 1);
    Timer_DelayMilliseconds(500);
    LED_Set(LED_GREEN, 0);
    LED_Set(LED_RED, 0);

    //
    // Main loop
    //

    printf("Start main loop\n");

    for (;;)
    {
        //
        // LED 'heart beat'
        //

        static uint32_t timePrevious_ms = 0;

        const uint32_t timeDelta_ms = Timer_GetMilliseconds() - timePrevious_ms;

        if (timeDelta_ms >= 1000)
        {
            timePrevious_ms = Timer_GetMilliseconds();
            LED_Toggle(LED_GREEN);
            LL_WDT_Clear();
        }

        //
        // Tasks
        //

        tud_task();
        USB_Application_Task();
    }

    return 0;
}

//-------------------------------------------------------------------------------------------------
// USB device callbacks
//-------------------------------------------------------------------------------------------------

// Invoked when device is mounted.
void tud_mount_cb(void)
{
    printf("USB mount\n");
}


// Invoked when device is unmounted.
void tud_umount_cb(void)
{
    printf("USB unmount\n");
}


// Invoked when USB bus is suspended.
void tud_suspend_cb(bool remote_wakeup_en)
{
    printf("USB suspend: remote_wakeup_en=%u\n", remote_wakeup_en);
}


// Invoked when USB bus is resumed.
void tud_resume_cb(void)
{
    printf("USB resume\n");
}

//-------------------------------------------------------------------------------------------------
// USB CDC callbacks
//-------------------------------------------------------------------------------------------------

// Invoked when received new data
void tud_cdc_rx_cb(uint8_t itf)
{
    printf("tud_cdc_rx_cb: itf=%u\n", itf);

    while (tud_cdc_available())
    {
        const int32_t c = tud_cdc_read_char();

        printf("[%c]", c);

        if ((c == 'x') || (c == 'X'))
        {
            LED_Toggle(LED_RED);
        }
    }

    printf("\r\n");
}


// Invoked when received `wanted_char`
void tud_cdc_rx_wanted_cb(uint8_t itf, char wanted_char)
{
    printf("tud_cdc_rx_wanted_cb: itf=%u, wanted_char='%c'\n", itf, wanted_char);
}


// Invoked when space becomes available in TX buffer
void tud_cdc_tx_complete_cb(uint8_t itf)
{
    printf("tud_cdc_tx_complete_cb: itf=%u\n", itf);
}


// Invoked when line state DTR & RTS are changed via SET_CONTROL_LINE_STATE
void tud_cdc_line_state_cb(uint8_t itf, bool dtr, bool rts)
{
    printf("tud_cdc_line_state_cb: itf=%u, dtr=%u, rts=%u\n", itf, dtr, rts);

    if ((dtr == 1) && (rts == 1))
    {
        printf("USB CDC connected\n");

        USB_CDC_Send(FIRMWARE_VERSION "\r\n");
        USBSerialPortIsActive = true;
    }
    else if ((dtr == 0) && (rts == 1))
    {
        printf("USB CDC disconnected\n");

        USBSerialPortIsActive = false;
    }
}


// Invoked when line coding is change via SET_LINE_CODING
void tud_cdc_line_coding_cb(uint8_t itf, cdc_line_coding_t const* p_line_coding)
{
    printf("tud_cdc_line_coding_cb: itf=%u, bit_rate=%u, stop_bits=%u, parity=%u, data_bits=%u\n",
        itf,
        p_line_coding->bit_rate,
        p_line_coding->stop_bits,
        p_line_coding->parity,
        p_line_coding->data_bits);
}


// Invoked when received send break
void tud_cdc_send_break_cb(uint8_t itf, uint16_t duration_ms)
{
    printf("tud_cdc_send_break_cb: itf=%u, duration_ms=%u\n", itf, duration_ms);
}

//-------------------------------------------------------------------------------------------------
// USB HID callbacks
//-------------------------------------------------------------------------------------------------

// Invoked when received GET_REPORT control request.
// Application must fill buffer report's content and return its length.
// Return zero will cause the stack to STALL request.
uint16_t tud_hid_get_report_cb(uint8_t itf, uint8_t report_id, hid_report_type_t report_type, uint8_t* buffer, uint16_t reqlen)
{
    (void) buffer;

    printf("HID get report: itf=%u, ID=%u, type=%u, length=%u\n",
        itf, report_type, report_id, reqlen);

    return 0;
}


// Invoked when received SET_REPORT control request or
// received data on OUT endpoint ( Report ID = 0, Type = 0 ).
void tud_hid_set_report_cb(uint8_t itf, uint8_t report_id, hid_report_type_t report_type, uint8_t const* buffer, uint16_t bufsize)
{
    (void) buffer;

    printf("HID set report: itf=%u, ID=%u, type=%u, length=%u\n",
        itf, report_id, report_type, bufsize);
}


static volatile bool fl_HID_PressButton = false;


// Invoked when sent REPORT successfully to host
// Application can use this to send the next report
// Note: For composite reports, report[0] is report ID
void tud_hid_report_complete_cb(uint8_t instance, uint8_t const* report, uint8_t len)
{
    (void) report;

    printf("tud_hid_report_complete_cb: instance=%u, length=%u\n", instance, len);

    if (fl_HID_PressButton == true)
    {
        fl_HID_PressButton = false;

        tud_hid_keyboard_report(0, 0, NULL);
        printf("HID send release button\n");
    }
}


static void USB_HID_PressButton(enum Button buttonIndex)
{
    uint8_t keyCode[6];

    if (tud_hid_ready())
    {
        if (fl_HID_PressButton == false)
        {
            fl_HID_PressButton = true;

            keyCode[0] = HID_KEY_B;
            keyCode[1] = HID_KEY_T;
            keyCode[2] = HID_KEY_N;
            keyCode[3] = HID_KEY_MINUS;
            keyCode[4] = (buttonIndex == Button_1) ? HID_KEY_1 : HID_KEY_2;
            keyCode[5] = HID_KEY_NONE;

            tud_hid_keyboard_report(0, 0, keyCode);
            printf("HID send press button %u\n", buttonIndex + 1);
        }
    }
}


static bool USB_CDC_Send(const char * pStr)
{
    if (tud_cdc_connected() && (tud_cdc_write_available() >= strlen(pStr)))
    {
        tud_cdc_write_str(pStr);
        tud_cdc_write_flush();

        return true;
    }
    else
    {
        return false;
    }
}


static void USB_Application_Task(void)
{
    static uint32_t secondsCounterPrevious = 0;
    static uint16_t button1Counter = 0;
    static uint16_t button2Counter = 0;

    bool buttonState;
    char str[40];

    if (USBSerialPortIsActive == true)
    {
        const uint32_t secondsCounter = Timer_GetSeconds();

        if (secondsCounter != secondsCounterPrevious)
        {
            secondsCounterPrevious = secondsCounter;

            snprintf(str, sizeof(str), "Tick %u\r\n", secondsCounter);
            USB_CDC_Send(str);
        }
    }

    if (Keypad_GetButtonState(Button_1, &buttonState))
    {
        if (buttonState == 0)
        {
            button1Counter++;

            DAC0_Voltage += DAC_VOLTAGE_STEP;
            if (DAC0_Voltage > DAC_VOUT_MAX)
            {
                DAC0_Voltage = DAC_VOUT_MIN;
            }

            DAC_SetOutput(DAC_0, DAC0_Voltage);

            LED_Set(LED_RED, 1);
            printf("Button 1 [%u] DAC0=%f V\n", button1Counter, DAC0_Voltage);

            if (USBSerialPortIsActive == true)
            {
                snprintf(str, sizeof(str), "Button 1 [%u] DAC0=%f V\r\n", button1Counter, DAC0_Voltage);
                USB_CDC_Send(str);
            }

            USB_HID_PressButton(Button_1);
        }
    }

    if (Keypad_GetButtonState(Button_2, &buttonState))
    {
        if (buttonState == 0)
        {
            button2Counter++;

            DAC1_Voltage += DAC_VOLTAGE_STEP;
            if (DAC1_Voltage > DAC_VOUT_MAX)
            {
                DAC1_Voltage = DAC_VOUT_MIN;
            }

            DAC_SetOutput(DAC_1, DAC1_Voltage);

            LED_Set(LED_RED, 0);
            printf("Button 2 [%u] DAC1=%f V\n", button2Counter, DAC1_Voltage);

            if (USBSerialPortIsActive == true)
            {
                snprintf(str, sizeof(str), "Button 2 [%u] DAC1=%f V\r\n", button2Counter, DAC1_Voltage);
                USB_CDC_Send(str);
            }

            USB_HID_PressButton(Button_2);
        }
    }
}
