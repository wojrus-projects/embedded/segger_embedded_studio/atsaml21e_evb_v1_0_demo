# Opis

Projekt zawiera firmware demonstracyjny do PCB **ATSAML21E_EVB_V1_0** (https://gitlab.com/wojrus-projects/altium-designer/atsaml21e_evb_v1_0).

Firmware jest skompilowany na MCU **ATSAML21E15B** (32 KB flash, 6 KB SRAM, USB) (https://www.microchip.com/en-us/product/ATSAML21E15B).

Toolchain: Segger Embedded Studio v6.32a (Windows) (https://www.segger.com/products/development-tools/embedded-studio)

# Funkcje

Po podłączeniu USB do komputera obie LEDy włączą się na 500 ms i następnie LED-1 (zielona) będzie zmieniać stan co 1 s (*heart beat*).

Naciśnięcie Button-1:
- Włącza LED-2
- Zwiększa o 200 mV napięcie na wyjściu DAC-0 (PA02 / CLICK_AN)

Naciśnięcie Button-2:
- Wyłącza LED-2
- Zwiększa o 200 mV napięcie na wyjściu DAC-1 (PA05 / CLICK_SCK)

Urządzenie będzie widoczne w systemie operacyjnym jako urządzenie USB kompozytowe (VID=0xCAFE, PID=0x0001) zawierające:
- Klasę HID emulującą klawiaturę.
- Klasę CDC emulującą port szeregowy.

Firmware najlepiej działa z Windows 10 (Windows 7 może wymagać pliku konfiguracyjnego INF do CDC).

Przyciski Button-1/2 generują jednoczesne naciśnięcia pięciu klawiszy "btn-1" lub "btn-2" na emulowanej klawiaturze USB.

Po uruchomieniu terminala tekstowego np. TeraTerm i otworzeniu emulowanego portu szeregowego wyświetli się wersja firmware i licznik sekund.

Przycisk 'x' w terminalu zmienia stan LED-2 na przeciwny.

Przyciski Button-1/2 wyświetlają w terminalu liczniki ich naciśnięć oraz ustawione napięcia na wyjściach DAC-0/DAC-1.

# Licencja

MIT
